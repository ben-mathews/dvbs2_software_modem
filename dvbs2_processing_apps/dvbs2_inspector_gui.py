#!/usr/bin/env python
"""
IQ File Processor
"""

import os
import sys
import copy
from enum import Enum
import traceback

from enum import Enum
import numpy as np
from scipy.signal import welch

from PyQt5 import QtWidgets, QtCore, QtGui, uic
from PyQt5.QtWidgets import QInputDialog, QAction
from PyQt5.QtGui import QColor, QPalette
from PyQt5.QtWidgets import QMessageBox

from pyqtgraph.Qt import QtGui, QtCore, QtWidgets
from PyQt5.QtGui import QColor, QPalette
from PyQt5.QtWidgets import QInputDialog, QAction
from PyQt5.QtCore import Qt

import pyqtgraph as pg
from pyqtgraph import GraphicsLayoutWidget


from dsp_utilities.file_handlers.iq_file_store import IQFileStore, IQFileType
from dvbs2_processing.utilities import process_command_line_args
from dvbs2_processing.constant_data import DVBS2_Frame_Length, DVBS2_Modulation_Type, DVBS2_Pilots, DVBS2_Code_Rates
from dvbs2_processing_apps.carrier_acquire import carrier_acquire
from dvbs2_processing.stream_processor import DVBS2StreamProcessor

# Need this for reasons I don't understand
from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration, DVBS2_Frame_Length, DVBS2_Pilots, \
    DVBS2_ModCods, DVBS2_MATYPE1_Rolloff

from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import pyqtgraph

import numpy as np
from scipy import signal
QT_CREATOR_FILE = os.path.dirname(os.path.abspath(__file__)) + "/" + "dvbs2_inspector_gui.ui"
UI_MAIN_WINDOW, QT_BASE_CLASS = uic.loadUiType(QT_CREATOR_FILE)

# sudo apt-get install python3-pyqt5 python3-pyqt5.qtsql qttools5-dev-tools qttools5-dev qt5-default

# TODO: Filter design dialog
# TODO: Add support for npy and npz files
# TODO: FSD not correctly detecting BF type when specifying tmp filename


class FrequencyUnits(Enum):
    """
    FrequencyUnits enumeration
    """

    Hz = 0
    KHz = 1
    MHz = 2
    GHz = 3


class PowerSpectralDensityPlot(GraphicsLayoutWidget):

    def __init__(self, iq, fs_hz, center_frequency, settings):
        super().__init__()

        self.iq = iq
        self.fs_hz = fs_hz
        self.center_frequency_hz = center_frequency
        self.settings = settings
        self.plot_item = None
        self.frequency_span_selector = None
        self.f_welch = None
        self.s_welch = None
        self.generate_plot()


    def generate_plot(self):
        if len(self.iq) < 2:
            self.f_welch = [-0.5, 0.5]
            self.s_welch =[0, 0]
        else:
            self.f_welch, self.s_welch = welch(self.iq, self.fs_hz, 'hann',
                                               self.settings["psd"]["nfft"], return_onesided=False, detrend=False)
            self.f_welch = self.f_welch / 10 ** (self.settings["frequency_units"].value * 3)
            self.f_welch = self.center_frequency_hz / \
                           10 ** (self.settings["frequency_units"].value * 3) + np.fft.fftshift(self.f_welch)
            self.s_welch = np.fft.fftshift(10 * np.log10(self.s_welch))

        self.plot_item = self.addPlot(row=0, col=0)
        self.plot_item.showGrid(x=True, y=True)
        self.plot_item.setLabel('left', 'Power (dB)')
        self.plot_item.setLabel('bottom', 'Frequency', units=self.settings["frequency_units"].name)
        self.plot_item.getAxis('left').enableAutoSIPrefix(False)
        self.plot_item.getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_item.setLimits(xMin=self.f_welch[0], xMax=self.f_welch[-1],
                                 yMin=np.min(self.s_welch) - np.abs(np.max(self.s_welch)),
                                 yMax=10*np.floor((np.max(self.s_welch)+20)/10))
        self.plot_item.plot(self.f_welch, self.s_welch, pen=pg.mkColor("y"))

        self.pen_select_box_outline = pg.mkColor("r")
        self.pen_select_box_fill = pg.mkColor("b").setAlpha(50)
        self.frequency_span_selector = pg.LinearRegionItem(
            values=(self.f_welch[int(0.25 * len(self.f_welch))], self.f_welch[int(0.75 * len(self.f_welch))]),
            bounds=(min(self.f_welch), max(self.f_welch)),
            orientation='vertical', brush=self.pen_select_box_fill, pen=self.pen_select_box_outline,
            hoverBrush=self.pen_select_box_fill, hoverPen=self.pen_select_box_outline, movable=True, swapMode='blovk')
        self.frequency_span_selector.setObjectName('psd_plot_frequency_span_selector')

        for line in self.frequency_span_selector.lines:
            line.pen.setWidth(2)


    def remove_selectors(self):
        self.remove_frequency_span_selector()


    def add_frequency_span_selector(self):
        self.plot_item.addItem(self.frequency_span_selector)


    def remove_frequency_span_selector(self):
        self.plot_item.removeItem(self.frequency_span_selector)


class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data):
        super(TableModel, self).__init__()
        self._data = data
        self.headerdata = ['#', 'ModCod', 'Es/N0 (dB)', 'Freq (Hz)', 'Frame Length', 'Pilots', 'Passed FEC']

    def data(self, index, role):
        if role == Qt.DisplayRole:
            return self._data[index.row()][index.column()]

    def rowCount(self, index):
        return len(self._data)

    def columnCount(self, index):
        return len(self._data[0])

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QtCore.QVariant(self.headerdata[col])
        return QtCore.QVariant()


class IQFileProcessorMainWindow(QtWidgets.QMainWindow, UI_MAIN_WINDOW):
    """
    The IQFileProcessorMainWindow objects contains Time Series, PSD, and Waterfall windows and associated functionality
    for manipulating IQ data
    """


    def __init__(self):
        """
        Constructor for IQFileProcessorMainWindow objects
        """
        #
        # Basic set-up
        #
        QtWidgets.QMainWindow.__init__(self)
        UI_MAIN_WINDOW.__init__(self)
        self.setupUi(self)
        self.setMouseTracking(True)

        self.__initialize_settings__()

        self.psd_layout_widget = \
            PowerSpectralDensityPlot(iq=[], fs_hz=1, center_frequency=0, settings=self.settings)

        self.psd_layout_widget.frequency_span_selector.sigRegionChanged.connect(
            self.__span_region_change_finished_callback__)
        self.psd_layout_widget.frequency_span_selector.sigRegionChangeFinished.connect(
            self.__span_region_change_finished_callback__)

        self.frame_psd_plot.layout().addWidget(self.psd_layout_widget)
        self.btnSelect.clicked.connect(self.select_file)
        self.btn_select_bandwidth.clicked.connect(self.select_bandwidth)
        self.btn_process_file.clicked.connect(self.process_file)
        self.tbl_plframes.clicked.connect(self.__plframe_results_table_callback__)


    def __initialize_settings__(self):
        self.settings = {}
        self.settings["psd"] = {}
        self.settings["psd"]["default_parameters"] = {}
        self.settings["psd"]["default_parameters"]["show_psd_plot"] = True
        self.settings["psd"]["default_parameters"]["do_multi_threaded"] = False
        self.settings["psd"]["default_parameters"]["window"] = "hann"
        self.settings["psd"]["default_parameters"]["nperseg"] = 4096
        self.settings["psd"]["default_parameters"]["noverlap"] = 512
        self.settings["psd"]["default_parameters"]["nfft"] = 4096
        self.settings["recent_files"] = {}
        self.settings["recent_files"]["max_num"] = 4
        self.load_settings()
        self.update_recent_file_actions()

    def load_settings(self):
        """
        Loads settings from Qsettings file into the IQFileProcessorMainWindow settings container
        :return: void
        """
        settings = QtCore.QSettings("Cosmic_AES", "DVBS2_GUI")
        if settings.value("frequency_units", "MHz", str) in FrequencyUnits.__dict__["_member_names_"]:
            self.settings["frequency_units"] = FrequencyUnits(
                FrequencyUnits.__dict__["_member_names_"].index(settings.value("frequency_units", "MHz", str))
            )
        else:
            self.settings["frequency_units"] = FrequencyUnits.Hz
        self.settings["frequency_units"] = FrequencyUnits.Hz
        self.settings["psd"]["show_psd_plot"] = settings.value("psd/show_psd_plot", True, bool)
        self.settings["psd"]["do_multi_threaded"] = settings.value("psd/do_multi_threaded", True, bool)
        self.settings["psd"]["window"] = settings.value("psd/window", "hann", str)
        self.settings["psd"]["nperseg"] = settings.value("psd/nperseg", 4096, int)
        self.settings["psd"]["noverlap"] = settings.value("psd/noverlap", 512, int)
        self.settings["psd"]["nfft"] = settings.value("psd/nfft", 4096, int)
        self.settings["recent_files"]["recent_file_list"] = settings.value("recent_file_list", [], list)


    def save_settings(self):
        """
        Saves settings from the IQFileProcessorMainWindow settings container into the Qsettings file
        :return: void
        """
        settings = QtCore.QSettings("Cosmic_AES", "DVBS2_GUI")
        settings.setValue("frequency_units", self.settings["frequency_units"].value)
        settings.setValue("psd/show_psd_plot", self.settings["psd"]["show_psd_plot"])
        settings.setValue("psd/nfft", self.settings["psd"]["nfft"])
        settings.setValue("psd/do_multi_threaded", self.settings["psd"]["do_multi_threaded"])
        settings.setValue("psd/window", self.settings["psd"]["window"])
        settings.setValue("psd/nperseg", self.settings["psd"]["nperseg"])
        settings.setValue("psd/noverlap", self.settings["psd"]["noverlap"])
        settings.setValue("psd/nfft", self.settings["psd"]["nfft"])
        settings.setValue('recent_file_list', self.settings["recent_files"]["recent_file_list"])


    def update_recent_file_actions(self):
        """
        Updates the "Recent Files" submenu
        :return: void
        """
        for action in self.menuImport_Recent.actions():
            self.menuImport_Recent.removeAction(action)
        if self.settings["recent_files"]["recent_file_list"] is not None:
            num_recent_files = min(
                len(self.settings["recent_files"]["recent_file_list"]), self.settings["recent_files"]["max_num"]
            )
            self.recent_file_actions = []
            for filename in self.settings["recent_files"]["recent_file_list"][0:num_recent_files]:
                self.recent_file_actions.append(QAction(self, visible=True, triggered=self.__open_recent_file__))
                self.recent_file_actions[-1].setText(filename)
                self.recent_file_actions[-1].setData(filename)
                self.menuImport_Recent.addAction(self.recent_file_actions[-1])


    def __open_recent_file__(self):
        """
        Opens a file from the "Recent Files" submenu
        :return: void
        """
        action = self.sender()
        if action:
            if os.path.isfile(action.data()):
                self.open_file(action.data())
            else:
                self.menuImport_Recent.removeAction(action)


    def select_file(self):
        if 'recent_files' in self.settings and 'recent_file_list' in self.settings["recent_files"] and \
                len(self.settings["recent_files"]["recent_file_list"]) > 0:
            start_path = str(self.settings["recent_files"]["recent_file_list"][0])
        else:
            start_path = ''

        options = QtWidgets.QFileDialog.Options()
        options = options | QtWidgets.QFileDialog.DontUseNativeDialog
        filename, filter = QtWidgets.QFileDialog.getOpenFileName(None, "Select File to Import", start_path,
                                                                 "BLUE Files (*.tmp);;All Files (*)",
                                                                 options=options)

        self.open_file(filename)


    def open_file(self, filename):
        # Update recent files
        while filename in self.settings["recent_files"]["recent_file_list"]:
            self.settings["recent_files"]["recent_file_list"].remove(filename)
        self.settings["recent_files"]["recent_file_list"].insert(0, filename)
        while len(self.settings["recent_files"]["recent_file_list"]) > self.settings["recent_files"]["max_num"]:
            self.settings["recent_files"]["recent_file_list"].remove(
                self.settings["recent_files"]["recent_file_list"][-1]
            )
        self.update_recent_file_actions()
        self.save_settings()

        # Destroy the old plot widget
        self.txt_file_name.setText(filename)
        #psd_layout_widget = self.layout.getWidget(5, 0)
        self.psd_layout_widget.deleteLater()

        # Import IQ
        self.iq_file_store = IQFileStore(filename=filename, file_type=IQFileType.BLUEFILE)
        self.iq_file_store.read_file(t_start_sec=self.iq_file_store.iq_file.t_start_sec,
                                     num_samples=int(np.min([self.iq_file_store.iq_file.num_samples, 1e6])))

        self.psd_layout_widget = \
            PowerSpectralDensityPlot(iq=self.iq_file_store.iq_file.iq,
                                     fs_hz=self.iq_file_store.iq_file.fs_hz,
                                     center_frequency=self.iq_file_store.iq_file.center_frequency_hz,
                                     settings=self.settings)
        self.frame_psd_plot.layout().addWidget(self.psd_layout_widget)

        self.txt_sample_rate_hz.setText(str(self.iq_file_store.iq_file.fs_hz))
        self.txt_num_samples.setText(str(self.iq_file_store.iq_file.num_samples))
        self.txt_center_frequency_hz.setText(str(self.iq_file_store.iq_file.center_frequency_hz))


    def select_bandwidth(self):
        self.psd_layout_widget.add_frequency_span_selector()
        self.psd_layout_widget.frequency_span_selector.sigRegionChangeFinished.connect(
            self.__span_region_change_finished_callback__)


    def process_file(self):
        args_override = []
        args_override.append('--input_file')
        args_override.append(self.txt_file_name.text())
        filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths, mod_types, dvbs2_code_rates, fec_processing_mode, \
        zmq_fec_server, pilots, num_fec_workers = process_command_line_args(args_override)
        try:
            from py_dvbs2_fec import DVBS2_FEC_Processor_Bank
        except ImportError:
            fec_processing_mode = 'none'
        fec_processing_mode = 'none'

        if self.psd_layout_widget.items().count(self.psd_layout_widget.frequency_span_selector) == 1:
            bandwidth_hz = self.psd_layout_widget.frequency_span_selector.boundingRect().right()-\
                           self.psd_layout_widget.frequency_span_selector.boundingRect().left()
            fc_estimate_hz = self.psd_layout_widget.frequency_span_selector.boundingRect().left() + bandwidth_hz/2
            fsym_estimate_hz = bandwidth_hz / 1.1
        else:
            bandwidth_hz = None
            fc_estimate_hz = 0.0

        num_sync_frames = int(self.txt_num_sync_frames.text())
        num_proc_frames = int(self.txt_num_proc_frames.text())

        frame_lengths = []
        if self.cb_short_frames.isChecked() == True:
            frame_lengths.append(DVBS2_Frame_Length.Short_Frames)
        if self.cb_normal_frames.isChecked() == True:
            frame_lengths.append(DVBS2_Frame_Length.Normal_Frames)
        if len(frame_lengths) == 0:
            raise Exception('Must select at least one frame length')

        mod_types = []
        if self.cb_mod_qpsk.isChecked() == True:
            mod_types.append(DVBS2_Modulation_Type.QPSK)
        if self.cb_mod_8psk.isChecked() == True:
            mod_types.append(DVBS2_Modulation_Type.PSK8)
        if self.cb_mod_16apsk.isChecked() == True:
            mod_types.append(DVBS2_Modulation_Type.APSK16)
        if self.cb_mod_32apsk.isChecked() == True:
            mod_types.append(DVBS2_Modulation_Type.APSK32)
        if self.cb_mod_dummy.isChecked() == True:
            mod_types.append(DVBS2_Modulation_Type.DUMMY)

        pilots = []
        if self.cb_pilots.isChecked() == True:
            pilots.append(DVBS2_Pilots.Pilots_On)
        if self.cb_no_pilots.isChecked() == True:
            pilots.append(DVBS2_Pilots.Pilots_Off)

        dvbs2_code_rates = []
        if self.cb_rate_1_4.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_1_4)
        if self.cb_rate_1_3.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_1_3)
        if self.cb_rate_2_5.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_2_5)
        if self.cb_rate_1_2.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_1_2)
        if self.cb_rate_3_5.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_3_5)
        if self.cb_rate_2_3.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_2_3)
        if self.cb_rate_3_4.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_3_4)
        if self.cb_rate_4_5.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_4_5)
        if self.cb_rate_5_6.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_5_6)
        if self.cb_rate_8_9.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_8_9)
        if self.cb_rate_9_10.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_9_10)
        if self.cb_rate_dummy.isChecked() == True:
            dvbs2_code_rates.append(DVBS2_Code_Rates.RATE_DUMMY)

        fec_processing_mode = 'none'
        zmq_fec_server = None
        if self.cb_fec_mode.currentText() == 'No FEC Processing':
            fec_processing_mode = 'none'
        elif self.cb_fec_mode.currentText() == 'Local Processor':
            fec_processing_mode = 'local_processor'
        elif self.cb_fec_mode.currentText() == 'ZMQ Processor':
            fec_processing_mode = 'zmq_processor'
            zmq_fec_server = self.txt_zmq_fec_server.text()
        else:
            raise Exception('Unhandled FEC Mode: {}'.format(self.cb_fec_mode.currentText()))

        prefilter_iq_data = False
        if self.cb_prefilter_iq.isChecked() == True:
            prefilter_iq_data = True

        print('Launched carrier_acquire...')
        self.initial_sync_results = None
        try:
            self.initial_sync_results = carrier_acquire(filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths,
                                                        mod_types, dvbs2_code_rates, fec_processing_mode,
                                                        zmq_fec_server, pilots,
                                                        prefilter_iq_data=prefilter_iq_data,
                                                        num_consecutive_plframes=num_sync_frames)
        except Exception as ex:
            formatted_lines = traceback.format_exc()
            formatted_lines = formatted_lines.replace('Exception:', '\n\n\nException:')
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error launching carrier_acquire()")
            msg.setInformativeText(str(formatted_lines))
            msg.setWindowTitle("Error")
            retval = msg.exec_()
            return

        dvbs2_stream_processor = DVBS2StreamProcessor(
            dvbs2_frame_length=frame_lengths,
            dvbs2_pilots=pilots,
            dvbs2_modulation_type=mod_types,
            dvbs2_code_rates=dvbs2_code_rates,
            fec_processing_mode=fec_processing_mode,
            zmq_fec_server=zmq_fec_server,
            threading_config=1)
        dvbs2_stream_processor.initialize_file_store(filename=filename, fs_hz=None,
                                                     file_type=IQFileType.BLUEFILE,
                                                     iq_data_format=None)
        block_data, self.dvbs2_frames = dvbs2_stream_processor.process_plframes(
            initial_sync_results=self.initial_sync_results,
            block_size_num_samples=
            int(50 * self.initial_sync_results['first_dvbs2_plframe'].dvbs2_modcod.get_num_symbols() * self.initial_sync_results['fs_hz'] / self.initial_sync_results['fsym_hz']),
            prefilter_iq_data=prefilter_iq_data,
            num_blocks=np.int(np.ceil(num_proc_frames/50)))

        """
        except Exception as ex:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Exception running carrier_acquire")
            msg.setInformativeText(str(ex))
            msg.setWindowTitle("Exception")
            msg.exec_()
            return
        """

        carrier_results_txt = []
        carrier_results_txt.append(
            '<u><b>Initial Acq results:</b></u>')
        carrier_results_txt.append(
            '<b>Sample Rate (Hz): </b>{}'.format(self.initial_sync_results['fs_hz']))
        carrier_results_txt.append(
            '<b>Symbol Rate (Hz):</b> {}'.format(self.initial_sync_results['fsym_hz']))
        carrier_results_txt.append(
            '<b>Center Frequency Offset (Hz):</b> {}'.format(self.initial_sync_results['frequency_offset_hz']))
        carrier_results_txt.append(
            '<b>Center Frequency based on BlueFile Center Frequency (Hz):</b> {}'.format(
                self.initial_sync_results['frequency_offset_hz']))
        carrier_results_txt.append(
            '<b>Frame Length:</b> {}'.format(self.initial_sync_results['dvbs2_frame_length'].name.replace('_', ' ')))
        carrier_results_txt.append(
            '<b>Pilot Configuration:</b> {}'.format(self.initial_sync_results['dvbs2_pilots'].name.replace('_', ' ')))
        if self.initial_sync_results['dvbs2_rolloff'] is None:
            carrier_results_txt.append('<b>RRC Roll-Off:</b> {}'.format('UNKNOWN'))
        else:
            carrier_results_txt.append(
                '<b>RRC Roll-Off:</b> {}'.format(
                    self.initial_sync_results['dvbs2_rolloff'].name.replace('_', '.').replace('RO.','')))
        carrier_results_txt.append(
            '<b>First PLFRAME ModCod:</b> {}'.format(self.initial_sync_results['first_dvbs2_plframe'].dvbs2_modcod))
        self.txt_carrier_results.setText('<br>'.join(carrier_results_txt))

        rows = []
        for dvbs2_frame in self.dvbs2_frames:
            print(dvbs2_frame)
            rows.append([dvbs2_frame.index,
                         dvbs2_frame.dvbs2_modcod.modcod_pretty(),
                         '{:4.2f}'.format(dvbs2_frame.esn0),
                         '{:6.3f}'.format(dvbs2_frame.frequency_offset_hz) + '     ',
                         dvbs2_frame.dvbs2_modcod.frames_pretty() + '     ',
                         dvbs2_frame.dvbs2_modcod.pilots_pretty().replace('Pilots ', ''),
                         str(dvbs2_frame.passed_fec)])

        model = TableModel(rows)
        #model.setHorizontalHeaderLabels(['#', 'ModCod', 'Es/N0'])
        self.tbl_plframes.setModel(model)

        self.tbl_plframes.horizontalHeader().setSectionResizeMode(0, QtGui.QHeaderView.ResizeToContents)
        self.tbl_plframes.horizontalHeader().setSectionResizeMode(1, QtGui.QHeaderView.Stretch)
        self.tbl_plframes.horizontalHeader().setSectionResizeMode(2, QtGui.QHeaderView.ResizeToContents)
        self.tbl_plframes.horizontalHeader().setSectionResizeMode(3, QtGui.QHeaderView.ResizeToContents)
        self.tbl_plframes.horizontalHeader().setSectionResizeMode(4, QtGui.QHeaderView.ResizeToContents)
        self.tbl_plframes.horizontalHeader().setSectionResizeMode(5, QtGui.QHeaderView.ResizeToContents)
        self.tbl_plframes.horizontalHeader().setSectionResizeMode(6, QtGui.QHeaderView.ResizeToContents)


        self.plot_constellations(plframe_index=0)

        self.plot_carrier_frequency(dvbs2_frames=self.dvbs2_frames)

    def plot_constellations(self, plframe_index):
        dvbs2_plframe = self.dvbs2_frames[plframe_index]
        self.scatterplot_plheader(dvbs2_plframe)
        self.scatterplot_plframe(dvbs2_plframe)


    def scatterplot_plheader(self, dvbs2_plframe):
        if self.frame_plheader_scatterplot.layout().itemAt(0) is not None:
            self.frame_plheader_scatterplot.layout().removeWidget(self.pl_header_view)
            self.pl_header_view.deleteLater()

        self.pl_header_scatter = pg.ScatterPlotItem(pen=pg.mkPen(width=3, color='y'), symbol='o', size=3)
        data = np.asarray([np.real(dvbs2_plframe.plframe_symbols_corrected[0:90]),
                           np.imag(dvbs2_plframe.plframe_symbols_corrected[0:90])])
        pos = [{'pos': data[:, i]} for i in range(np.shape(data)[1])]

        self.pl_header_scatter.setData(pos)
        self.pl_header_view = pg.PlotWidget()
        self.pl_header_view.setAspectLocked(True)
        self.pl_header_view.showGrid(True, True, 1)
        self.pl_header_view.addItem(self.pl_header_scatter)
        self.pl_header_view.setLimits(xMin=-1 * np.max(np.abs(data)) - 0.5, xMax=np.max(np.abs(data)) + 0.5,
                                      yMin=-1 * np.max(np.abs(data)) - 0.5, yMax=np.max(np.abs(data)) + 0.5)
        # self.pl_header_view.getPlotItem().setContentsMargins(0, 10, 30, 00)
        self.pl_header_scatter.getViewWidget().setTitle('PLHEADER')
        self.pl_header_view.getPlotItem().showAxis('top')
        self.pl_header_view.getPlotItem().showAxis('right')
        self.frame_plheader_scatterplot.layout().addWidget(self.pl_header_view)
        self.frame_plheader_scatterplot.layout().setContentsMargins(0, 0, 0, 0)

        max_val = 1.5*np.max(np.abs([np.real(dvbs2_plframe.plframe_symbols_corrected[0:90]),
                                     np.imag(dvbs2_plframe.plframe_symbols_corrected[0:90])]))
        self.pl_header_view.setLimits(xMin=-1*max_val, xMax=max_val, yMin=-1*max_val, yMax=max_val)
        self.pl_header_view.autoRange()


    def scatterplot_plframe(self, dvbs2_plframe):
        """
        Scatterplots the data and pilot symbols for the given PLFRAME
        :param dvbs2_plframe:
        :return:
        """
        if self.frame_plframe_scatterplot.layout().itemAt(0) is not None:
            self.frame_plframe_scatterplot.layout().removeWidget(self.pl_frame_view)
            self.pl_frame_view.deleteLater()

        # Data symbols
        self.data_symbols_scatterplot = pg.ScatterPlotItem(pen=pg.mkPen(width=3, color='y'), symbol='o', size=3)
        data_symbols = np.asarray(
            [np.real(dvbs2_plframe.plframe_symbols_corrected[90:dvbs2_plframe.dvbs2_modcod.get_num_symbols()]),
             np.imag(dvbs2_plframe.plframe_symbols_corrected[90:dvbs2_plframe.dvbs2_modcod.get_num_symbols()])])
        data_symbols_dict = [{'pos': data_symbols[:, i]} for i in range(np.shape(data_symbols)[1])]
        self.data_symbols_scatterplot.setData(data_symbols_dict, name='Pilot Symbols')

        # Pilot symbols
        self.pilot_symbols_scatterplot = pg.ScatterPlotItem(pen=pg.mkPen(width=3, color='r'), symbol='o', size=3)
        pilot_symbols = np.asarray(
            [np.real(dvbs2_plframe.plframe_symbols_corrected[dvbs2_plframe.dvbs2_modcod.get_pilot_symbol_indicies()]),
             np.imag(dvbs2_plframe.plframe_symbols_corrected[dvbs2_plframe.dvbs2_modcod.get_pilot_symbol_indicies()])])
        pilot_symbols_dict = [{'pos': pilot_symbols[:, i]} for i in range(np.shape(pilot_symbols)[1])]
        self.pilot_symbols_scatterplot.setData(pilot_symbols_dict, name='Pilot Symbols')

        # Ideal data symbols
        self.ideal_data_symbols_scatterplot = pg.ScatterPlotItem(pen=pg.mkPen(width=6, color='m'), symbol='+', size=3)
        mapping, constellation = dvbs2_plframe.dvbs2_modcod.get_constellation()
        ideal_data_symbols = np.asarray(
            [np.real(constellation),
             np.imag(constellation)])
        ideal_data_symbols_dict = [{'pos': ideal_data_symbols[:, i]} for i in range(np.shape(ideal_data_symbols)[1])]
        self.ideal_data_symbols_scatterplot.setData(ideal_data_symbols_dict, name='Ideal Data Symbols')

        # Ideal pilot symbols
        self.ideal_pilots_symbols_scatterplot = pg.ScatterPlotItem(pen=pg.mkPen(width=6, color='g'), symbol='+', size=3)
        ideal_pilot_symbols = np.asarray(
            [np.asarray([1/np.sqrt(2)]),
             np.asarray([1/np.sqrt(2)])])
        ideal_pilot_symbols_dict = [{'pos': ideal_pilot_symbols[:, i]} for i in range(np.shape(ideal_pilot_symbols)[1])]
        self.ideal_pilots_symbols_scatterplot.setData(ideal_pilot_symbols_dict, name='Ideal Pilot Symbols')

        # Build the scatterplot
        self.pl_frame_view = pg.PlotWidget()
        self.pl_frame_view.setAspectLocked(True)
        self.pl_frame_view.showGrid(True, True, 1)
        self.pl_frame_view.addItem(self.data_symbols_scatterplot)
        self.pl_frame_view.addItem(self.pilot_symbols_scatterplot)
        self.pl_frame_view.addItem(self.ideal_data_symbols_scatterplot)
        self.pl_frame_view.addItem(self.ideal_pilots_symbols_scatterplot)
        # self.pl_frame_view.getPlotItem().setContentsMargins(0, 10, 30, 00)
        self.pl_frame_view.getViewWidget().setTitle('PLFRAME')
        self.pl_frame_view.getPlotItem().showAxis('top')
        self.pl_frame_view.getPlotItem().showAxis('right')
        self.frame_plframe_scatterplot.layout().addWidget(self.pl_frame_view)
        self.frame_plframe_scatterplot.layout().setContentsMargins(0, 0, 0, 0)
        # self.pl_frame_view.setLimits(xMin=-2.5, xMax=2.5, yMin=-2.5, yMax=2.5)
        max_val = 1.5*np.max(np.abs([np.real(dvbs2_plframe.plframe_symbols_corrected),
                                     np.imag(dvbs2_plframe.plframe_symbols_corrected)]))
        self.pl_frame_view.setLimits(xMin=-1*max_val, xMax=max_val, yMin=-1*max_val, yMax=max_val)
        self.pl_frame_view.autoRange()

        # Build the legend
        legendItem = pg.LegendItem(verSpacing=-10)
        legendItem.addItem(self.data_symbols_scatterplot, 'Data Symbols', )
        legendItem.addItem(self.pilot_symbols_scatterplot, 'Pilot Symbols')
        legendItem.addItem(self.ideal_data_symbols_scatterplot, 'Ideal Data Symbols')
        legendItem.addItem(self.ideal_pilots_symbols_scatterplot, 'Ideal Pilot Symbols')
        legendItem.setPos(np.min(np.real(dvbs2_plframe.plframe_symbols_corrected)) - 0.2,
                          np.max(np.imag(dvbs2_plframe.plframe_symbols_corrected)) + 0.2)
        self.pl_frame_view.addItem(legendItem)


    def plot_carrier_frequency(self, dvbs2_frames):
        if self.frame_carrier_frequency.layout().itemAt(0) is not None:
            self.frame_carrier_frequency.layout().removeWidget(self.carrier_frequency_view)
            self.carrier_frequency_view.deleteLater()
        t_plframe_sec = [dvbs2_frame.t_plframe_symbols[0] for dvbs2_frame in dvbs2_frames]
        frequency_offsets_hz = np.asarray([-1*np.sum(dvbs2_frame.frequency_adjustments_hz) for dvbs2_frame in dvbs2_frames])
        esn0_db = [dvbs2_frame.esn0 for dvbs2_frame in dvbs2_frames]

        self.carrier_frequency_plot = pg.PlotCurveItem()
        self.carrier_frequency_plot.setData(x=t_plframe_sec, y=frequency_offsets_hz)
        self.carrier_frequency_view = pg.PlotWidget()
        self.carrier_frequency_view.showGrid(True, True, 1)
        self.carrier_frequency_view.addItem(self.carrier_frequency_plot)
        self.carrier_frequency_view.getPlotItem().showAxis('top')
        self.carrier_frequency_view.getPlotItem().showAxis('right')
        self.frame_carrier_frequency.layout().addWidget(self.carrier_frequency_view)
        self.frame_carrier_frequency.layout().setContentsMargins(0, 0, 0, 0)


    def __span_region_change_finished_callback__(self):
        bandwidth_hz = self.psd_layout_widget.frequency_span_selector.boundingRect().right()-\
                       self.psd_layout_widget.frequency_span_selector.boundingRect().left()
        fc_estimate_hz = self.psd_layout_widget.frequency_span_selector.boundingRect().left() + bandwidth_hz/2
        #self.center_frequency_hz.setText(str(fc_estimate_hz))
        #self.symbol_rate_hz.setText(str(bandwidth_hz))


    def __plframe_results_table_callback__(self, item):
        self.plot_constellations(plframe_index=item.row())


def main():
    """
    Main method
    """
    app = QtWidgets.QApplication(sys.argv)
    app.setOrganizationName("IQFileProcessor")
    app.setOrganizationDomain("https://gitlab.com/ben-mathews/iq_file_processor")
    app.setApplicationName("IQFileProcessor")

    # Ripped off from https://gist.github.com/gph03n1x/7281135
    app.setStyle("Fusion")
    palette = QtGui.QPalette()
    black_level = 35
    palette.setColor(QtGui.QPalette.Window, QtGui.QColor(black_level, black_level, black_level))
    palette.setColor(QtGui.QPalette.WindowText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Base, QtGui.QColor(15, 15, 15))
    palette.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(black_level, black_level, black_level))
    palette.setColor(QtGui.QPalette.ToolTipBase, QtGui.QColor(black_level, black_level, black_level))
    palette.setColor(QtGui.QPalette.ToolTipText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Text, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Button, QtGui.QColor(black_level, black_level, black_level))
    palette.setColor(QtGui.QPalette.ButtonText, QtCore.Qt.white)
    palette.setColor(QPalette.Disabled, QtGui.QPalette.ButtonText, QtCore.Qt.darkGray)
    palette.setColor(QPalette.Disabled, QtGui.QPalette.Text, QtCore.Qt.darkGray)
    palette.setColor(QtGui.QPalette.BrightText, QtCore.Qt.red)
    palette.setColor(QtGui.QPalette.Highlight, QtGui.QColor(11, 22, 229).lighter())
    palette.setColor(QtGui.QPalette.HighlightedText, QtCore.Qt.black)
    app.setPalette(palette)

    window = IQFileProcessorMainWindow()
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
