#matplotlib.use("Agg")

from dsp_utilities.file_handlers import IQFileType, IQDataFormat

from dvbs2_processing.stream_processor import DVBS2StreamProcessor
from dvbs2_processing.stream_processor import DVBS2StreamProcessorDebug
from dvbs2_processing.constant_data import DVBS2_Frame_Length, DVBS2_Pilots, DVBS2_Modulation_Type

# from advanced_processing import carrier_process

if __name__ == '__main__':
    filename, fs_hz, fsym_estimate_hz, file_type, iq_data_format = '../iq_sample_data/16_APSK/16-APSK-Long-Almost_critically_sampled.tmp', None, 5e6, IQFileType.BLUEFILE, IQDataFormat.INT16
    filename, fs_hz, fsym_estimate_hz, file_type, iq_data_format = '../iq_sample_data/ACM/TestCase01/testcase01.tmp', None, 1.08e6, IQFileType.BLUEFILE, IQDataFormat.INT16
    #filename, fs_hz, fsym_estimate_hz, file_type, iq_data_format = '../iq_sample_data/ACM/TestCase00/testcase00.tmp', None, 7e6, IQFileType.BLUEFILE, IQDataFormat.INT16
    filename, fs_hz, fsym_estimate_hz, file_type, iq_data_format = '../iq_sample_data/gnuradio/qpsk_3_5_short_pilots.cfile', 10e6, 7e6, IQFileType.RAW, IQDataFormat.FLOAT32
    filename, fs_hz, fsym_estimate_hz, file_type, iq_data_format = '../iq_sample_data/gnuradio/qpsk_3_5_short_pilots_freq_offset.cfile', 10e6, 7e6, IQFileType.RAW, IQDataFormat.FLOAT32
    #filename, fs_hz, fsym_estimate_hz, file_type, iq_data_format = '/home/ben/dev/gnuradio/qpsk_3_5_short_pilots_freq_offset_long.cfile', 9e6, 7e6, IQFileType.RAW, IQDataFormat.FLOAT32
    #filename, fs_hz, fsym_estimate_hz, file_type, iq_data_format = '/mnt/ramdisk/qpsk_3_5_short_pilots_freq_offset_long.cfile', 9e6, 7e6, IQFileType.RAW, IQDataFormat.FLOAT32

    #dvbs2_stream_processor = DVBS2StreamProcessor(dvbs2_frame_length=[DVBS2_Frame_Length.Short_Frames], dvbs2_pilots=[DVBS2_Pilots.Pilots_On], dvbs2_modulation_type=[DVBS2_Modulation_Type.QPSK, DVBS2_Modulation_Type.PSK8], no_fec_processor=True)
    dvbs2_stream_processor = DVBS2StreamProcessor(dvbs2_frame_length=[DVBS2_Frame_Length.Short_Frames], dvbs2_pilots=[DVBS2_Pilots.Pilots_On], dvbs2_modulation_type=[DVBS2_Modulation_Type.QPSK, DVBS2_Modulation_Type.PSK8], no_fec_processor=True, threading_config=1)
    dvbs2_stream_processor.initialize_file_store(filename=filename, fs_hz=10e6, file_type=file_type, iq_data_format=iq_data_format)
    initial_sync_results = dvbs2_stream_processor.perform_initial_sync(fsym_estimate_hz=fsym_estimate_hz)
    if False:
        import time
        start = time.time()
        carrier_process_data = \
            dvbs2_stream_processor.process_plframes_pipelined(initial_sync_results=initial_sync_results,
                                                      block_size_num_samples=50 * initial_sync_results['dvbs2_modcod'].get_num_symbols() * initial_sync_results['fs_hz'] / initial_sync_results['fsym_hz'],
                                                      num_blocks=1000)

        end = time.time()
        elapsed = end - start
        print('\n\n -------------------------------------------------- Time Elapsed: {} --------------------------------------------------\n\n'.format(elapsed))

    if False:
        import time
        start = time.time()
        carrier_process_data = \
            dvbs2_stream_processor.process_plframes_parallelized(initial_sync_results=initial_sync_results,
                                                                 block_size_num_samples=50 * initial_sync_results['dvbs2_modcod'].get_num_symbols() * initial_sync_results['fs_hz'] / initial_sync_results['fsym_hz'],
                                                                 num_blocks=1000)

        end = time.time()
        elapsed = end - start
        print('\n\n -------------------------------------------------- Time Elapsed: {} --------------------------------------------------\n\n'.format(elapsed))

    if True:
        import time
        start = time.time()
        carrier_process_data, dvbs2_frames = \
            dvbs2_stream_processor.process_plframes(initial_sync_results=initial_sync_results,
                                                  block_size_num_samples=50 * initial_sync_results['dvbs2_modcod'].get_num_symbols() * initial_sync_results['fs_hz'] / initial_sync_results['fsym_hz'],
                                                  num_blocks=100)
        end = time.time()
        elapsed = end - start
        print(elapsed)

    #carrier_process(dvbs2_pl_frame_processor=dvbs2_pl_frame_processor, carrier_process_data=carrier_process_data)
