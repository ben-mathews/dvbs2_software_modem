import os
import sys
import argparse

# Test configurations:
# --input_file /home/ben/dev/gitlab/dvbs2_demod/test/gnuradio/iq_data/dvbs2_iq_normal.tmp --perform_fec_processing True --fsym_estimate_hz 5000000 --mod_types QPSK --code_rates 3/4 --frame_lengths Normal_Frames
# --input_file /home/ben/dev/gitlab/iq_sample_data/ACM/TestCase01/testcase01.tmp --perform_fec_processing True --fsym_estimate_hz 1080000

from dsp_utilities.file_handlers import IQFileType, IQDataFormat
from dvbs2_processing.stream_processor import DVBS2StreamProcessor
from dvbs2_processing.constant_data import DVBS2_Frame_Length, DVBS2_Pilots, DVBS2_Modulation_Type, DVBS2_Code_Rates
from dvbs2_processing.utilities import process_command_line_args


def carrier_acquire(filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths, mod_types, dvbs2_code_rates,
                    fec_processing_mode, zmq_fec_server, pilots, prefilter_iq_data=False, num_consecutive_plframes=10):

    dvbs2_stream_processor = DVBS2StreamProcessor(
        dvbs2_frame_length=frame_lengths,
        dvbs2_pilots=pilots,
        dvbs2_modulation_type=mod_types,
        dvbs2_code_rates=dvbs2_code_rates,
        fec_processing_mode=fec_processing_mode,
        threading_config=0,
        zmq_fec_server=zmq_fec_server)

    dvbs2_stream_processor.initialize_file_store(filename=filename, fs_hz=None, file_type=IQFileType.BLUEFILE,
                                                 iq_data_format=None)
    initial_sync_results = dvbs2_stream_processor.perform_initial_sync(fc_estimate_hz=fc_estimate_hz,
                                                                       fsym_estimate_hz=fsym_estimate_hz,
                                                                       prefilter_iq_data=prefilter_iq_data,
                                                                       num_consecutive_plframes=num_consecutive_plframes)

    print('-----------------------------------------------------------------------------------------------------------')
    print('Initial Acq results:')
    print('Sample Rate (Hz): {}'.format(initial_sync_results['fs_hz']))
    print('Symbol Rate (Hz): {}'.format(initial_sync_results['fsym_hz']))
    print('Center Frequency Offset (Hz): {}'.format(initial_sync_results['frequency_offset_hz']))
    print('Center Frequency based on BlueFile Center Frequency (Hz): {}'.format(dvbs2_stream_processor.iq_file_store.iq_file.center_frequency_hz + initial_sync_results['frequency_offset_hz']))
    print('Frame Length: {}'.format(initial_sync_results['dvbs2_frame_length'].name.replace('_', ' ')))
    print('Pilot Configuration: {}'.format(initial_sync_results['dvbs2_pilots'].name.replace('_', ' ')))
    if initial_sync_results['dvbs2_rolloff'] is not None:
        print('RRC Roll-Off: {}'.format(initial_sync_results['dvbs2_rolloff'].name.replace('_', '.')))
    else:
        print('RRC Roll-Off: {}'.format('UNKNOWN'))
    print('First PLFRAME ModCod: {}'.format(initial_sync_results['first_dvbs2_plframe'].dvbs2_modcod))

    return initial_sync_results


if __name__ == '__main__':
    filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths, mod_types, dvbs2_code_rates, fec_processing_mode, \
    zmq_fec_server, pilots, num_fec_workers = process_command_line_args()

    carrier_acquire(filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths, mod_types, dvbs2_code_rates,
                    fec_processing_mode, pilots)
