from setuptools import setup

setup(name='dvbs2_processing',
      version='0.0.1',
      description='Python 3 functions for processing DVB-S2 signals and data',
      author="Ben Mathews",
      author_email="beniam@yahoo.com",
      url="https://gitlab.com/ben-mathews/dvbs2_demod",
      install_requires=[
          "bchlib @ git+https://github.com/Ben-Mathews/python-bchlib.git",
          "bitstring>=3.1.6",
          "digital-demod @ git+https://gitlab.com/ben-mathews/digital_demod.git",
          "dsp-utilities @ git+https://gitlab.com/ben-mathews/dsp_utilities.git",
          "joblib>=0.14.1",
          "matplotlib>=3.0.3",
          "mpipe>=1.0.8",
          "numpy>=1.16.3",
          "PyQt5>=5.14.0",
          "pyqtgraph>=0.11.0.dev0+g7506ee3",
          "pytest>=5.4.3",
          "scipy>=1.2.1",
          "sklearn>=0.0",
          "zmq>=0.0.0"
      ],
      dependency_links=[
          "https://github.com/pyqtgraph/pyqtgraph/archive/7506ee3d3f8156f6c77184cceffd774dea2d216f.zip#egg=pyqtgraph-0.11.0.dev0+g7506ee3"
      ],
      packages=['dvbs2_processing']
)
