import numpy as np
import matplotlib.pyplot as plt
from dsp_utilities.file_handlers import IQFileType

from dvbs2_processing.stream_processor import DVBS2StreamProcessor
from dvbs2_processing.utilities import process_command_line_args

# Test configurations:
# --input_file /home/ben/dev/gitlab/dvbs2_demod/test/gnuradio/iq_data/dvbs2_iq_normal.tmp --perform_fec_processing True --fsym_estimate_hz 5000000 --mod_types QPSK --code_rates 3/4 --frame_lengths Normal_Frames
# --input_file /home/ben/dev/gitlab/iq_sample_data/ACM/TestCase01/testcase01.tmp --perform_fec_processing True --fsym_estimate_hz 1080000
# --input_file /home/ben/dev/gitlab/dvbs2_demod/test/gnuradio/iq_data/dvbs2_iq_short_qpsk_short_frames.tmp --perform_fec_processing False --fsym_estimate_hz 5000000 --mod_types QPSK --code_rates 3/4 --frame_lengths Short_Frames
# --input_file /home/ben/dev/gitlab/dvbs2_demod/test/gnuradio/iq_data/dvbs2_iq_normal_QPSK_short_frames.tmp --perform_fec_processing False --fsym_estimate_hz 5000000 --mod_types QPSK --code_rates 3/4 --frame_lengths Short_Frames
def process_dvbs2_iq_file():
    filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths, mod_types, dvbs2_code_rates, no_fec_processor, \
    zmq_fec_server, pilots, num_fec_workers = process_command_line_args()

    dvbs2_stream_processor = DVBS2StreamProcessor(
        dvbs2_frame_length=frame_lengths,
        dvbs2_pilots=pilots,
        dvbs2_modulation_type=mod_types,
        dvbs2_code_rates=dvbs2_code_rates,
        fec_processing_mode='none',
        # fec_processing_mode='zmq_processor',
        # fec_processing_mode='local_processor',
        threading_config=0,
        zmq_fec_server=None)

    dvbs2_stream_processor.initialize_file_store(filename=filename, fs_hz=None, file_type=IQFileType.BLUEFILE,
                                                 iq_data_format=None)
    initial_sync_results = dvbs2_stream_processor.perform_initial_sync(fsym_estimate_hz=fsym_estimate_hz,
                                                                       num_consecutive_plframes=100)

    print('-----------------------------------------------------------------------------------------------------------')
    print('Initial Acq results:')
    print('Sample Rate (Hz): {}'.format(initial_sync_results['fs_hz']))
    print('Symbol Rate (Hz): {}'.format(initial_sync_results['fsym_hz']))
    print('Center Frequency Offset (Hz): {}'.format(initial_sync_results['frequency_offset_hz']))
    print('Center Frequency based on BlueFile Center Frequency (Hz): {}'.format(dvbs2_stream_processor.iq_file_store.iq_file.center_frequency_hz + initial_sync_results['frequency_offset_hz']))
    print('Frame Length: {}'.format(initial_sync_results['dvbs2_frame_length'].name.replace('_', ' ')))
    print('Pilot Configuration: {}'.format(initial_sync_results['dvbs2_pilots'].name.replace('_', ' ')))
    print('First PLFRAME ModCod: {}'.format(initial_sync_results['first_dvbs2_plframe'].dvbs2_modcod))

    block_data, dvbs2_frames = dvbs2_stream_processor.process_plframes(
        initial_sync_results=initial_sync_results,
        block_size_num_samples=50 * initial_sync_results['first_dvbs2_plframe'].dvbs2_modcod.get_num_symbols() * initial_sync_results['fs_hz'] / initial_sync_results['fsym_hz'],
        num_blocks=10000)

    debug_plots = False
    if debug_plots:
        vco_filename = '/home/ben/dev/gitlab/dvbs2_demod/test/gnuradio/iq_data/dvbs2_iq_freq_offset.cfile'
        vco_data = np.fromfile(vco_filename, dtype=np.float32) / 2 / np.pi
        t_vco_sec = np.linspace(0, len(vco_data)-1, len(vco_data)) / dvbs2_stream_processor.fs_hz

        t_plframe_sec = [dvbs2_frame.t_plframe_symbols[0] for dvbs2_frame in dvbs2_frames]
        frequency_offsets_hz = np.asarray([dvbs2_frame.frequency_offset_hz + -1*np.sum(dvbs2_frame.frequency_adjustments_hz) for dvbs2_frame in dvbs2_frames])
        esn0_db = [dvbs2_frame.esn0 for dvbs2_frame in dvbs2_frames]

        plt.figure()
        plt.subplot(2,1,1)
        plt.plot(t_plframe_sec, frequency_offsets_hz, '.')
        plt.plot(t_vco_sec, vco_data)
        plt.title('Carrier Frequency')
        plt.subplot(2, 1, 2)
        plt.plot(t_plframe_sec, esn0_db, '.')
        plt.title('Es/N0')

if __name__ == '__main__':
    process_dvbs2_iq_file()

