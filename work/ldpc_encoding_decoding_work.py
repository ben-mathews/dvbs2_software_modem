import numpy as np
from dvbs2_processing.constant_data.dvbs2_ldpc_parity_bit_accumulator_addresses import dvbs2_ldpc_parity_bit_accumulator_addresses
from dvb_s2_constant_data import DVBS2_ModCod_Configuration, DVBS2_ModCods, DVBS2_Frame_Length, DVBS2_Pilots




modcod = DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_8PSK_3_4, frame_length=DVBS2_Frame_Length.Normal_Frames, pilots=DVBS2_Pilots.Pilots_On)

code_rate = modcod.get_code_rate()
(kbch, kldpc, t_error, nldpc, q) = modcod.get_coding_parameters()
ldpc_parity_bit_accumulator_addresses = dvbs2_ldpc_parity_bit_accumulator_addresses[nldpc][code_rate]
databits = np.random.randint(0, 2, kbch, dtype=np.int8)

if False:
    from scipy.io import loadmat
    ttt = loadmat('/home/ben/debug.mat')
    databits = np.squeeze(ttt['databits'])
    m_matlab = np.squeeze(ttt['m'])
    q_matlab = np.squeeze(ttt['q'])
    r_matlab = np.squeeze(ttt['r'])

    from pyGF2 import gf2_mul, gf2_div
    g = modcod.get_bch_generator_polynomial()
    x = np.zeros(kldpc-kbch+1, dtype=np.int8)
    x[-1] = 1
    m = gf2_mul(np.flip(np.asarray(databits, dtype=np.uint8)), np.asarray(x, dtype=np.uint8))
    (q, r) = gf2_div(np.asarray(m, dtype=np.uint8), np.asarray(np.flip(g), dtype=np.uint8))


    a = [1, 1, 0, 1, 1, 0, 1, 0, 1]
    b = [1, 0, 1]
    (q, r) = np.polydiv(a, b)
    from pyGF2 import gf2_mul, gf2_div
    x = np.zeros(kldpc-kbch+1, dtype=np.int8)
    x[-1] = 1
    m2 = gf2_mul(np.flip(np.asarray(databits, dtype=np.uint8)), np.asarray(x, dtype=np.uint8))
    (q, r) = gf2_div(np.asarray(np.flip(a), dtype=np.uint8), np.asarray(np.flip(b), dtype=np.uint8))


#from scipy.io import loadmat
#ttt = loadmat('/home/ben/debug.mat')
#databits = np.squeeze(ttt['databits'])
#bchEncOut_matlab = np.squeeze(ttt['bchEncOut'])

bitsout = modcod.generate_bch_bits(databits=databits)

bitsout_ldpc = modcod.generate_ldpc_bits(databits=bitsout)

p = np.zeros(nldpc-kldpc, dtype=np.int8)
P = modcod.compute_ldpc_parity_matrix()
H = np.transpose(P)
I = np.identity(int(nldpc-kldpc))
H2 = np.concatenate((H, I))
z = np.mod(bitsout_ldpc@H2, 2)
np.sum(np.mod(z,2) != 0)



for idx, row in enumerate(ldpc_parity_bit_accumulator_addresses):
    databit_index = 0
    for m in range(0, 360):
        i = m + idx * 360
        for x in row:
            address = np.mod(x+np.mod(m, 360)*q, nldpc-kldpc)
            p[address] = p[address] ^ databits[i]
for i in range(1, nldpc-kldpc-1):
    p[i] = p[i] ^ p[i-1]




# BCH
a12=((1+2**1+2**3+2**5+2**14) * (1+2**6+2**8+2**11+2**14) * (1+2**1+2**2+2**6+2**9+2**10+2**14) * (1+2**4+2**7+2**8+2**10+2**12+2**14) * (1+2**2+2**4+2**6+2**8+2**9+2**11+2**13+2**14) * (1+2**3+2**7+2**8+2**9+2**13+2**14) * (1+2**2+2**5+2**6+2**7+2**10+2**11+2**13+2**14) * (1+2**5+2**8+2**9+2**10+2**11+2**14) * (1+2**1+2**2+2**3+2**9+2**10+2**14) * (1+2**3+2**6+2**9+2**11+2**12+2**14) * (1+2**4+2**11+2**12+2**14) * (1+2**1+2**2+2**3+2**5+2**6+2**7+2**8+2**10+2**13+2**14));

bin(a12).replace("0b", "")
11111010010010110100010011100000111101000000110110111100111110000000011000101000000010001011101001101000001111111001001010000010010100111001111000001001000011101111110100101

bin(a12).replace("0b", "")
11111010010010110100010011100000111101000000110110111100111110000000011000101000000010001011101001101000001111111001001010000010010100111001111000001001000011101111110100101
