import bchlib
import hashlib
import os
import random

import numpy as np
from py_dvbs2_fec import DVBS2_FEC_Processor, DVBS2_FEC_Processor_Bank

from dvb_s2_constant_data import DVBS2_ModCod_Configuration, DVBS2_ModCods, DVBS2_Pilots, DVBS2_Frame_Length


def bytearray_to_array_of_bits(bytearray_data):
    array_of_bits = []
    for num in bytearray_data:
        for bit in [int(b) for b in '{0:08b}'.format(num)]:
            array_of_bits.append(bit)
    return np.asarray(array_of_bits)



################################ Begin This Works ################################################

if False:

    # Generate modcod and polynomial bits
    modcod = DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_8_9, frame_length=DVBS2_Frame_Length.Short_Frames, pilots=DVBS2_Pilots.Pilots_On)
    (k_bch, k_ldpc, t_error, n_ldpc, q) = modcod.get_coding_parameters()
    bch_generator_polynomial = modcod.get_bch_generator_polynomial()
    np.packbits(bch_generator_polynomial.astype(np.int32))


    # Generate bits to encode
    bits = np.random.randint(0, 2, k_bch, dtype=np.int)
    bits = np.zeros(k_bch, dtype=int)
    #bits[14231] = int(1)
    bits[0] = int(1)
    bytes = os.urandom(int(k_bch/8))
    bits = bytearray_to_array_of_bits(bytes)

    BCH_POLYNOMIAL = 0b00100000000101011  # Short frames
    BCH_BITS = 12
    bch = bchlib.BCH(BCH_POLYNOMIAL, BCH_BITS, False)
    ecc = bch.encode(bytes)

    bits_bchlib = bytearray_to_array_of_bits(ecc)
    bits_modcod = modcod.generate_bch_bits(bits.astype(np.int8))

    print(bits_bchlib[-168::])
    print()
    print(bits_modcod[-168::])
    print()

    bits_padded = bits
    while len(bits_padded) < 2**14:
        bits_padded = np.append(np.uint8(0), bits_padded)
    data = bytearray(np.packbits(bits_padded))
    ecc = bch.encode(data)
    bits_bchlib2 = bytearray_to_array_of_bits(ecc)

    print(bits_bchlib2[-168::])
    print()


################################ End This Works ################################################



















################################ Begin This Works ################################################

if False:
    # Generate modcod and polynomial bits
    modcod = DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_8PSK_3_4, frame_length=DVBS2_Frame_Length.Normal_Frames, pilots=DVBS2_Pilots.Pilots_On)
    (k_bch, k_ldpc, t_error, n_ldpc, q) = modcod.get_coding_parameters()
    n_bch_bits = int(np.ceil(np.log2(n_ldpc))*t_error)
    bch_generator_polynomial = modcod.get_bch_generator_polynomial()
    np.packbits(bch_generator_polynomial.astype(np.int32))


    # Generate bits to encode
    bits = np.zeros(k_bch, dtype=int)
    bits[0] = int(1)
    bits_padded = bits
    while len(bits_padded) < 2**14:
        bits_padded = np.append(np.uint8(0), bits_padded)
    bytes = bytearray(np.packbits(bits_padded))

    BCH_POLYNOMIAL = 0b00100000000101011  # Short frames
    BCH_POLYNOMIAL = 0b10000000000101101  # Normal frames
    BCH_BITS = 12
    bch = bchlib.BCH(BCH_POLYNOMIAL, BCH_BITS, False)
    ecc = bch.encode(bytes)

    bits_bchlib = bytearray_to_array_of_bits(ecc)
    bits_modcod = modcod.generate_bch_bits(bits.astype(np.int8))

    print(bits_bchlib[-n_bch_bits::])
    print()
    print(bits_modcod[-n_bch_bits::])
    print()


################################ End This Works ################################################

























################################ Begin This Works ################################################

# Generate modcod and polynomial bits
modcod = DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_8PSK_3_4, frame_length=DVBS2_Frame_Length.Normal_Frames, pilots=DVBS2_Pilots.Pilots_On)
(k_bch, k_ldpc, t_error, n_ldpc, q) = modcod.get_coding_parameters()
n_bch_bits = int(np.ceil(np.log2(n_ldpc))*t_error)
bch_generator_polynomial = modcod.get_bch_generator_polynomial()
bch_primitive_polynomial = modcod.get_bch_primitive_polynomial()
bch_primitive_polynomial_int = int("".join(str(x) for x in bch_primitive_polynomial), 2)

problem_tests = 0
for test_index in range(0, 1000):
    print('test_index = {}'.format(test_index))
    # Generate bits to encode
    bits = np.random.randint(0, 2, k_bch, dtype=int)
    for i in range(0, int(len(bits)/2)):
        bits[2 * i] = 1
        bits[2 * i + 1] = 0
    #bits = np.zeros(k_bch, dtype=int)
    #bits[0] = int(1)
    bits_padded = bits
    #while len(bits_padded) < 2**14:
    #    bits_padded = np.append(np.uint8(0), bits_padded)
    bytes = bytearray(np.packbits(bits))

    BCH_POLYNOMIAL = bch_primitive_polynomial_int
    BCH_BITS = t_error
    bch = bchlib.BCH(BCH_POLYNOMIAL, BCH_BITS, False)
    ecc = bch.encode(bytes)

    bits_bchlib = bytearray_to_array_of_bits(ecc)
    bits_modcod = modcod.generate_bch_bits(bits.astype(np.int8))

    num_errors = 0
    for index in range(0, n_bch_bits):
        if bits_bchlib[-1-index] != bits_modcod[-1-index]:
            print(index)
            num_errors += 1
    if num_errors > 0:
        problem_tests += 1
        print('Problem')

    packet = bytes + ecc

    def bitflip(packet):
        byte_num = random.randint(0, len(packet) - 1)
        bit_num = random.randint(0, 7)
        packet[byte_num] ^= (1 << bit_num)

    # make BCH_BITS errors
    if False:
        for _ in range(BCH_BITS-6):
            bitflip(packet)
    if True:
        packet[100] = 255

    bytes_corrupted, ecc_corrupted = packet[:-bch.ecc_bytes], packet[-bch.ecc_bytes:]
    bitflips = bch.decode_inplace(bytes_corrupted, ecc_corrupted)
    print('bitflips: %d' % (bitflips))



print(problem_tests)

################################ End This Works ################################################



























run_other = True
if run_other:
    BCH_POLYNOMIAL = 0b00100000000101011  # Short frames
    #BCH_POLYNOMIAL = 0b10000000000101101  # Normal frames

    BCH_BITS = 12
    bch = bchlib.BCH(BCH_POLYNOMIAL, BCH_BITS, False)
    bch_reversed = bchlib.BCH(BCH_POLYNOMIAL, BCH_BITS, True)
















# Generate modcod and polynomial bits
modcod = DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_8_9, frame_length=DVBS2_Frame_Length.Short_Frames, pilots=DVBS2_Pilots.Pilots_On)
(k_bch, k_ldpc, t_error, n_ldpc, q) = modcod.get_coding_parameters()
bch_generator_polynomial = modcod.get_bch_generator_polynomial()
np.packbits(bch_generator_polynomial.astype(np.int32))


# Generate bits to encode
bits = np.random.randint(0, 2, k_bch, dtype=np.int)
bits = np.zeros(k_bch, dtype=int)
#bits[14231] = int(1)
bits[0] = int(1)
bytes = os.urandom(int(k_bch/8))


run_other = True
if run_other:
    BCH_POLYNOMIAL = 0b00100000000101011  # Short frames
    #BCH_POLYNOMIAL = 0b10000000000101101  # Normal frames

    BCH_BITS = 12
    bch = bchlib.BCH(BCH_POLYNOMIAL, BCH_BITS, False)
    bch_reversed = bchlib.BCH(BCH_POLYNOMIAL, BCH_BITS, True)

bits_padded = bits
while len(bits_padded) < 2**14:
    bits_padded = np.append(bits_padded, np.uint8(0))
data = bytearray(np.packbits(bits_padded))

if False:
    for index in range(0, int(len(data)/4)):
        i0 = index * 4
        i1 = index * 4 + 1
        i2 = index * 4 + 2
        i3 = index * 4 + 3
        temp = data[i0]
        data[i0] = data[i3]
        data[i3] = temp
        temp = data[i1]
        data[i1] = data[i2]
        data[i2] = temp


def bytearray_to_array_of_bits(bytearray_data):
    array_of_bits = []
    for num in bytearray_data:
        for bit in [int(b) for b in '{0:08b}'.format(num)]:
            array_of_bits.append(bit)
    return np.asarray(array_of_bits)

if run_other:
    ecc = bch.encode(data)
    ecc_reversed = bch_reversed.encode(data)

    ecc_bits = bytearray_to_array_of_bits(ecc)
    ecc_bits_reversed = bytearray_to_array_of_bits(ecc_reversed)

#x = np.fromstring(ecc, dtype=np.uint8)
#ecc_bits = np.unpackbits(x)

dvbs2_fec_processor = DVBS2_FEC_Processor(n_ldpc=n_ldpc, k_ldpc=k_ldpc, k_bch=k_bch, t_bch=t_error)
bch_bits = dvbs2_fec_processor.encode_bch(bits)
bits2 = dvbs2_fec_processor.decode_bch(bch_bits, False)

bits_modcod = modcod.generate_bch_bits(np.asarray(bits, dtype=np.int8))

print(bch_bits[-168::])
print()
print(bits_modcod[-168::])
print()
if run_other:
    print(ecc_bits)
    print()
    print(ecc_bits_reversed)
    print()


int_values1 = [np.uint8(x) for x in ecc]
int_values2 = [np.uint8(x) for x in ecc_reversed]
int_values3 = np.packbits(bch_bits[-168::]).tolist()
int_values4 = np.packbits(bch_bits[-168::], bitorder='little').tolist()

from_clib = [2687593973, 1278531889, 1720832456, 883509235, 3957886082, 3531603968, 131569] # Short frames

for num in from_clib:
    print('{0:b}'.format(num))


for x in ecc:
    print('{0:b}'.format(x))



# create a bch object
BCH_POLYNOMIAL = 8219
BCH_BITS = 16
bch = bchlib.BCH(BCH_POLYNOMIAL, BCH_BITS)

# random data
data = bytearray(os.urandom(512))

# encode and make a "packet"
ecc = bch.encode(data)
packet = data + ecc

# print hash of packet
sha1_initial = hashlib.sha1(packet)
print('sha1: %s' % (sha1_initial.hexdigest(),))

def bitflip(packet):
    byte_num = random.randint(0, len(packet) - 1)
    bit_num = random.randint(0, 7)
    packet[byte_num] ^= (1 << bit_num)

# make BCH_BITS errors
for _ in range(BCH_BITS):
    bitflip(packet)

# print hash of packet
sha1_corrupt = hashlib.sha1(packet)
print('sha1: %s' % (sha1_corrupt.hexdigest(),))

# de-packetize
data, ecc = packet[:-bch.ecc_bytes], packet[-bch.ecc_bytes:]

# correct
bitflips = bch.decode_inplace(data, ecc)
print('bitflips: %d' % (bitflips))

# packetize
packet = data + ecc

# print hash of packet
sha1_corrected = hashlib.sha1(packet)
print('sha1: %s' % (sha1_corrected.hexdigest(),))

if sha1_initial.digest() == sha1_corrected.digest():
    print('Corrected!')
else:
    print('Failed')



dvbs2_fec_processor = DVBS2_FEC_Processor(n_ldpc=16200, k_ldpc=14400, k_bch=14232, t_bch=12)
import numpy as np


bch_bits = dvbs2_fec_processor.encode_bch(bits)
bits2 = dvbs2_fec_processor.decode_bch(bch_bits, False)