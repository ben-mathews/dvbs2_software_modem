# DVB-S2 Software-Based Demodulation

Python-based software demod for DVB-S2 signals.

![DVB-S2 Signal Inspector GUI](doc/images/inspector_gui_screenshot.png)

This library is intended to be used to process captured IQ samples from DVB-S2 signals and extract PLFRAME network traffic, which can be saved to files or rebroadcast as UDP packets.  This implementation follows the [ETSI EN 302 307-1 V1.4.1 standard](https://www.etsi.org/deliver/etsi_en/302300_302399/30230701/01.04.01_20/en_30230701v010401a.pdf).

## Functionality and Performance

This software operates on unsynchronized IQ data containing DVB-S2 waveforms and produces user bits.

Known issues and limitations:
* Performance degrades significantly for Es/N0 values below 5 dB
* PLFRAME detection regularly fails at Es/N0 values less than 2 dB
* This software is not well tested on waveforms without pilot tones
* Support for reserved and dummy frames is very minimal
* No support for DVB-S2X (yet)

![DVB-S2 BER Performance](https://gitlab.com/ben-mathews/dvbs2_software_modem/-/wikis/uploads/47189226d570982edfe7549c185cdb7c/ber_performance.png)

## Installation

For development is is frequently desirable to have in-place installs of the [dsp_utilities]() and [digital_demod]() libraries.  To do this, go to to your development directory and run:
```shell script
git clone https://gitlab.com/ben-mathews/dsp_utilities.git
pushd dsp_utilities
pip3 install -e .
popd

git clone https://gitlab.com/ben-mathews/digital_demod.git
pushd digital_demod
pip3 install -e .
popd
```
Note: This step is optional.  If this step is nor run before installing the main project, then these libraries will be installed to the Python site-packages folder.

Note that to do LDPC FEC processing, the [DVB-S2 FEC Processor](https://gitlab.com/ben-mathews/dvbs2_fec_processor) must be manually installed (see instructions in its README.md).  This requirement is not currently reflected in the setup.py file due to the immaturity of both projects.  You will not be able to do LDPC FEC processing without having this installed.

To clone and install the main project, run:
```shell script
git clone https://gitlab.com/ben-mathews/dvbs2_software_modem.git
pushd dvbs2_software_modem
pip3 install -e .
popd
```

To compile and install the python-bchlib dependency, Windows users will likely need to download and install Microsoft C++ Build Tools.  To do this, download and run the installer from [https://visualstudio.microsoft.com/visual-cpp-build-tools](), and select the "Desktop development with C++" Workload.  You may have to manually adjust the path to allow Python to find the cl.exe executable.  Do this by setting `set PATH=%PATH%;C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\bin\Hostx64\x64\`.

To use the GUI functionality, QT5 must be installed.  On Ubuntu 20.04, do this by executing:

    sudo apt-get install qt5-default

## Basic Architecture

There are several basic classes that provide the functionality used in this module.  These classes and some of their important methods are described below:

1) [DVBS2StreamProcessor](dvbs2_processing/stream_processor/stream_processor.py): Flow control logic and general orchastration of other processing.  Responsible for hosting the DVBS2SymbolProcessor, DVBS2PLFrameProcessor, and DVBS2BBFrameProcessor objects which wraps additional unsynchronized symbol level, PLFRAME level, and BBFRAME level processing to do the heavy lifting.  Also responsible for handling optional mutlithreading support.  Internally performs clock recovery to provide unsynchronized symbols to downstream processing.
    * __DVBS2StreamProcessor.process_iq_block()__: A fundamental building block method that does full DVB-S2 processing of a single block of IQ data.  It assumes coarse initial sync and performs all steps necessary to go from samples to symbols to FECFRAMEs to user bits.  This is called by process_plframes() and process_plframes_parallelized().  The basic data flow and logic is shown in the image below:

        ![Stream Processing Architecture](doc/images/high_level_architecture.png)
    
    There are three basic methods to process samples in PLFRAMES and get down to user bits:
    * __DVBS2StreamProcessor.process_plframes()__: A single-threaded (Windows or Linux) implementation that does all processing in the main thread.  Good for debugging and exploring how this package works.
    * __DVBS2StreamProcessor.process_plframes_parallelized()__: A coarse multi-threaded (Linux only) implementation that uses Python multiprocesing to parallelize multiple DVBS2StreamProcessor.process_iq_block() calls.
    * __DVBS2StreamProcessor.process_plframes_pipelined()__: A finer-grained multi-threaded (Linux only) implementation that uses Python multiprocesing and has multiple pipelined stages that can run in parallel.  The stages consist of 
        
        1) IQ data and carrier recovery
        2) Clock recovery and symbol generation 
        3) Symbol correction 
        4) Demodulation
        5) Debug printing
     
        These are the same steps that are taken in DVBS2StreamProcessor.process_iq_block(), but are broken down and run in parallel in multiple threads, and then pipelined together for improved performance.   
    
2) [DVBS2SymbolProcessor](dvbs2_processing/symbol_processor/symbol_processor.py) Hosts a file store object and provides clock recovery and symbol generation functionality to provide symbols to downstream processing
    * __DVBS2SymbolProcessor.get_iq_data()__: Reads IQ data at specified time and duration from IQ file store
    * __DVBS2SymbolProcessor.generate_symbols()__: Performs RRC filtering to generate symbols from samples

3) [DVBS2PLFrameProcessor](dvbs2_processing/plframe_processor/plframe_processor.py): Functionality to detect and output carrier and amplitude corrected PLFRAME symbol blocks from coarsely synchronized symbols.  
    * __DVBS2PLFrameProcessor.get_next_plframe()__: Operates on to provide the next detected PLFRAME.  If necessary, performs coarse carrier correction.  The graphic below describes the basic approach to performing the PLHEADER search:
    ![PLHEADER Search](doc/images/plheader_search.png) 
    * __DVBS2PLFrameProcessor.descramble_plframe()__: Descrambles the symbols 
    * __DVBS2PLFrameProcessor.get_corrected_plframe()__: Computes coarse or fine carrier and amplitude correction of specified PLFRAME
    * __DVBS2PLFrameProcessor.apply_corrections()__: Applies carrier and amplitude corrections computed in DVBS2PLFrameProcessor.get_corrected_plframe()
    * __DVBS2PLFrameProcessor.perform_symbol_corrections()__: Performs additional corrections (TODO: improve documentation to describe this)
    
    The diagram below describes the basic data PLFRAME symbol correction approach used by methods in DVBS2PLFrameProcessor:
    
    ![PLHEADER Search](doc/images/symbol_corrections.png)

4) [DVBS2BBFrameProcessor](dvbs2_processing/bbframe_processor/bbframe_processor.py): Functionality to process synchronized and corrected soft symbols, convert them to LLR values, and to run LDPC and BCH FEC processing to product user databits.  
    * __DVBS2BBFrameProcessor.demod_plframes()__: Operates on PLFRAME soft symbols and generates LLR values
    * __DVBS2BBFrameProcessor.decode_plframe()__: Operates on PLFRAME LLR values, performs LDPC and BCH FEC processing, and outputs BBFRAME bits


## FEC Processing

LDPC FEC processing is provided by [my DVB-S2 FEC Processor Library](https://gitlab.com/ben-mathews/dvbs2_fec_processor).  This has to be downloaded, compiled, and installed separately.  Sorry!  Warning: This library is rough to set up and install, so I tried providing verbose instructions for doing so.  It used to run on both Windows and Linux, but now only runs on Linux due to some changes I made.  If there are any CMake wizards out there that would like to fix this, I'd welcome the help.

BCH FEC processing is provided by [my fork of the Linux Kernel's BCH code processor and the associated Python wrapper](https://github.com/Ben-Mathews/python-bchlib), which I extended to support GF orders up to 31 (which is required for normal length PLFRAMEs).  If you do a `pip install` of dvbs2_processing this will be automatically downloaded and installed for you.  Someday if I can ever figure out who owns maintenance of this part of the Kernel source code I'll try and do a pull request.

FEC processing can be disabled by passing the appropriate values into the constructor of DVBS2StreamProcessor().

Three FEC servers are available for use: a single-threaded version, a multi-threaded version, and a multi-process version.  This is described in more detail in the [FEC Server README.md](dvbs2_processing/fec_server/README.md)

## Other Dependencies

The GUI components require a relatively recent version of pyqtgraph.  To install the last known working/tested version, run:

```shell script
pip install git+https://github.com/pyqtgraph/pyqtgraph.git@f43b2973125a4510fbd1dda300cce2cee249e422
```

## Usage

Current usage is via [demod_dvbs2_file.py](demod_dvbs2_file.py) script which has IQ file names and parameters hard coded in it.  Future versions will make these command-line arguments.

BBFRAME data can optionally be sent locally via UDP and received in Wireshark for additional network traffic analysis.  Look inside of bbframe_processor.py for details.

## Testing

GNURadio can be used to generate test vectors if real IQ data is not available.  See [the README.md in the testing directory](test/README.md) for more details.


## Acknowledgements

Thank you to [GitLab](https://www.gitlab.com) for hosting this project.

Thank you to JetBrains for sponsoring this Open Source projects with free [PyCharm](https://www.jetbrains.com/pycharm/) and [CLion](https://www.jetbrains.com/clion/) licenses. 



