import numpy as np
from scipy.signal import correlate
from matplotlib import pyplot as plt
from bitstring import BitArray


def differential_encoder(bits_in):
    bits_out = [bits_in[0]]
    for index in range(1, len(bits_in)):
        bits_out.append(bits_out[-1] ^ bits_in[index])
    return bits_out


def differential_decoder(bits_in):
    bits_out = [bits_in[0]]
    for index in range(1, len(bits_in)):
        bits_out.append(bits_in[index-1] ^ bits_in[index])
    return bits_out


def generate_spreading_sequence():
    seed = 0b010100001001000
    result = seed
    mask = 0b100000000000000
    scrambling_sequence = []
    for index in range(0, 4096):
        if mask & result == mask:
            scrambling_sequence.append(1)
        else:
            scrambling_sequence.append(0)
        result = ((result << 1) & 0b111111111111111) + \
                 (( ((result & 0b100000000000000) != 0) ^ ((result & 0b010000000000000) != 0)))
    scrambling_sequence = 2 * (np.asarray(scrambling_sequence) - 0.5)
    return scrambling_sequence


def generate_scrambling_sequence():
    seed = 0b001000001
    result = seed
    mask = 0b000000001
    scrambling_sequence = 22*[0]
    for index in range(0, 222):
        result = ((result << 1) & 0b111111111) + \
                 (( ((result & 0b100000000) != 0) ^ ((result & 0b000010000) != 0)))
        if mask & result == mask:
            scrambling_sequence.append(1)
        else:
            scrambling_sequence.append(0)
    return scrambling_sequence


def generate_unique_words():
    # [int(c) for c in '{:b}'.format(0x147147)]
    # [int(c) for c in '{:b}'.format(0x2b8eb8)]
    uw1 = [0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1]
    uw2 = [1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0]

    return uw1, uw2


def parse_frame_bits(bits):
    frame = {}
    frame['uw'] = BitArray(bits[0:22])
    frame['guid_high'] = BitArray(bits[22:(22+32)])
    frame['content_id_1'] = BitArray(bits[54:(54+5)])
    frame['information_1'] = BitArray(bits[59:(59+24)])
    frame['crc1'] = BitArray(bits[83:(83+8)])
    frame['fec1'] = BitArray(bits[91:(91+41)])
    frame['guid_low'] = BitArray(bits[133:(133+32)])
    frame['content_id_2'] = BitArray(bits[165:(165+5)])
    frame['information_2'] = BitArray(bits[170:(170+24)])
    frame['crc2'] = BitArray(bits[194:(194+8)])
    frame['fec2'] = BitArray(bits[202:(202+42)])
    return frame


if __name__ == '__main__':
    """
    Example code
    """
    descrambling_sequence = generate_scrambling_sequence()

    carrier_id_bits = [] # Get these from somewhere

    carrier_id_bits = differential_decoder(carrier_id_bits)

    uw1, uw2 = generate_unique_words()

    corr1 = correlate(2 * np.asarray(carrier_id_bits) - 1, 2 * np.asarray(uw1) - 1, 'same')

    corr2 = correlate(2 * np.asarray(carrier_id_bits) - 1, 2 * np.asarray(uw2) - 1, 'same')

    do_plot = False
    if do_plot:
        plt.figure()
        plt.plot(corr1)
        plt.plot(corr2)

    frame_starts = np.where((corr1 == len(uw1)) | (corr2 == len(uw2)))[0]

    frames = []
    for frame_start in frame_starts:
        frame_start_ = frame_start - int(len(uw1) / 2)
        if len(carrier_id_bits) >= frame_start_ + 244:
            descrambled_frame = descrambling_sequence ^ np.asarray(carrier_id_bits[frame_start_:(frame_start_ + 244)])
            frames.append(parse_frame_bits(descrambled_frame))

    for frame in frames:
        print('content_id_1: {}'.format(frame['content_id_1']))
        print('content_id_2: {}'.format(frame['content_id_2']))