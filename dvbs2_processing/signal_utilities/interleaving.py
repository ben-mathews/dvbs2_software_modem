import numpy as np
from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration


def deinterleave_plframe(dvbs2_modcod: DVBS2_ModCod_Configuration, plframe_bits: np.array):
    # TODO: Handle 8PSK 3/5 special case
    if dvbs2_modcod.modcod.name.find('QPSK') >= 0:
        return plframe_bits
    ncols = int(np.log2(dvbs2_modcod.get_mod_order()))

    interleave_order = np.arange(0, len(plframe_bits))
    interleave_order = np.reshape(interleave_order, (int(len(plframe_bits) / ncols), ncols), 'F')
    if dvbs2_modcod.modcod.name.find('DVBS2_8PSK_3_5') >= 0: # Per section 5.3.3 and Figure 8 of ETSI EN 302 307-1
        interleave_order = interleave_order[:, [2, 1, 0]]
    interleave_order = np.reshape(interleave_order, (1, len(plframe_bits)), 'C')
    deinterleave_order = np.argsort(interleave_order)
    plframe_bits = plframe_bits[deinterleave_order][0]

    return plframe_bits


def interleave_plframe(dvbs2_modcod: DVBS2_ModCod_Configuration, plframe_bits: np.array):
    # TODO: Handle 8PSK 3/5 special case
    if dvbs2_modcod.modcod.name.find('QPSK') >= 0:
        return plframe_bits
    # raise Exception('Untested code')
    ncols = int(np.log2(dvbs2_modcod.get_mod_order()))

    interleave_order = np.arange(0, len(plframe_bits))
    interleave_order = np.reshape(interleave_order, (int(len(plframe_bits) / ncols), ncols), 'C')
    if dvbs2_modcod.modcod.name.find('DVBS2_8PSK_3_5') >= 0: # Per section 5.3.3 and Figure 8 of ETSI EN 302 307-1
        interleave_order = interleave_order[:, [2, 1, 0]]
    interleave_order = np.reshape(interleave_order, (1, len(plframe_bits)), 'F')

    interleave_order = np.argsort(interleave_order)
    plframe_bits = plframe_bits[interleave_order][0]

    return plframe_bits