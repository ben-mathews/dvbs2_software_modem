import numpy as np
from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration, DVBS2_ModCods, DVBS2_Pilots, DVBS2_Frame_Length


def demodulate_plframe(dvbs2_modcod: DVBS2_ModCod_Configuration, symbols: np.array, noise_var: float):
    """
    Demodulates the PLframe, converting from symbols to soft bits.  Removes PLHeader and any Pilot Symbols.
    :param dvbs2_modcod:
    :param symbols:
    :param noise_var:
    :return:
    """
    assert len(symbols) == dvbs2_modcod.get_num_symbols(), 'Incorrect number of PLFRAME symbols'
    m = dvbs2_modcod.get_mod_order()
    mapping, constellation = dvbs2_modcod.get_constellation()
    mapping = np.asarray(mapping)
    mapping_bits = np.transpose(((mapping[:, None] & (1 << np.arange(int(np.log2(m))))) > 0).astype(int))

    deltas = np.empty([m, len(symbols)])

    for symbol_index in range(0, m):
        deltas[symbol_index, :] = (np.real(symbols) - np.real(constellation[symbol_index])) ** 2 + \
                                  (np.imag(symbols) - np.imag(constellation[symbol_index])) ** 2
    deltas = np.exp(deltas * (-1 / noise_var))

    llr = np.empty(shape=(int(np.log2(m)), len(symbols)))
    for bit_index in range(0, int(np.log2(m))):
        llr[int(np.log2(m)) - bit_index - 1, :] = np.log(np.sum(deltas[mapping_bits[bit_index, :] == 0, :], 0)) - \
                                                  np.log(np.sum(deltas[mapping_bits[bit_index, :] == 1, :], 0))

    # Remove PLHeader and Pilot symbols
    len_pls_symbols = 90
    pilot_symbol_indices = dvbs2_modcod.get_pilot_symbol_indicies()
    header_and_pilot_indicies = np.empty(shape=[len_pls_symbols + len(pilot_symbol_indices)], dtype=int)
    header_and_pilot_indicies[0:len_pls_symbols] = np.arange(0, len_pls_symbols, dtype=np.int)
    header_and_pilot_indicies[len_pls_symbols:(len_pls_symbols + len(pilot_symbol_indices))] = pilot_symbol_indices
    llr = np.delete(llr, header_and_pilot_indicies, 1)

    llr = np.reshape(llr, int(np.log2(m)) * (len(symbols) - len(header_and_pilot_indicies)), 'F')

    return llr


def modulate_plframe(dvbs2_modcod: DVBS2_ModCod_Configuration, bits: np.array):
    """
    Demodulates the PLframe, converting from symbols to soft bits.  Removes PLHeader and any Pilot Symbols.
    :param dvbs2_modcod: DVBS2_ModCod_Configuration object representing the modcod to process
    :param bits:
    :return:
    """
    if dvbs2_modcod.modcod is not DVBS2_ModCods.DVBS2_DUMMY_PLFRAME:
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        assert len(bits) == n_ldpc, "Expecting len(bits) == n_ldpc  ({} != {})  (dvbs2_modcod = {})".format(len(bits), n_ldpc, dvbs2_modcod)
    else:
        if dvbs2_modcod.frame_length is DVBS2_Frame_Length.Normal_Frames:
            n_ldpc = 64800
        elif dvbs2_modcod.frame_length is DVBS2_Frame_Length.Short_Frames:
            n_ldpc = 16200
        else:
            raise Exception('Unhandled frame length: ' + dvbs2_modcod.frame_length)
        bits = np.empty(n_ldpc)
        bits.fill(0)

    m = dvbs2_modcod.get_mod_order()
    log_m = int(np.log2(m))
    # num_data_symbols = int(len(bits)/log_m)
    convert_array = []
    for index in range(log_m - 1, -1, -1):
        convert_array.append(2 ** index)
    convert_array = np.asarray(convert_array)

    mapping, constellation = dvbs2_modcod.get_constellation()
    mapping = np.asarray(mapping)
    constellation = constellation[np.argsort(mapping)]
    # mapping_bits = np.transpose(((mapping[:, None] & (1 << np.arange(int(np.log2(m))))) > 0).astype(int))

    num_pilot_blocks = 0
    pls_symbols = dvbs2_modcod.generate_pls_symbols().tolist()
    symbols = np.empty(dvbs2_modcod.get_num_symbols(), dtype=np.complex_)
    symbols[0:90] = pls_symbols
    # pilot_symbol_indicies = dvbs2_modcod.get_pilot_symbol_indicies()
    my_pilot_symbol_indicies = []
    symbol_index = 90
    bits = np.asarray(bits, dtype=np.int8)
    data_symbol_ints = np.matmul(np.asarray(convert_array), np.reshape(bits, [log_m, int(len(bits) / log_m)], 'F'))
    data_symbol_index = 0
    while symbol_index < dvbs2_modcod.get_num_symbols():
        if dvbs2_modcod.pilots is DVBS2_Pilots.Pilots_On and \
                (symbol_index == 90 + (num_pilot_blocks + 1) * 16 * 90 + num_pilot_blocks * 36):
            num_pilot_blocks += 1
            for pilot_index in range(0, 36):
                my_pilot_symbol_indicies.append(symbol_index)
                symbols[symbol_index] = 1 / np.sqrt(2) + 1j / np.sqrt(2)
                symbol_index += 1
        else:
            symbols[symbol_index] = constellation[data_symbol_ints[data_symbol_index]]
            data_symbol_index += 1
            symbol_index += 1
    symbols = np.asarray(symbols)
    return symbols
