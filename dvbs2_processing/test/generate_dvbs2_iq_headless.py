#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# This script is programmatically modified version of generate_dvbs2_iq_headless.py,
# which is generated from generate_dvbs2_iq_headless.grc in GNURadio Companion.
# It is modified by using the updated_headles_py_file.py file.  The purpose of
# this script is to provide a programmatic way of generating simulated DVB-S2 IQ
# data outside of GNURadio companion.
#
# This script was last updated with a flowgraph generated from GNURadio 3.8.1.0
# using Python 3.8.2
#

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Generate DVB-S2 IQ Headless
# GNU Radio version: 3.8.1.0

from gnuradio import analog
from gnuradio import blocks
import pmt
from gnuradio import dtv
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import dvbs2_processing.test.epy_module_0 as epy_module_0
import dvbs2_processing.test.epy_module_1 as epy_module_1
import numpy as np
import os

class generate_dvbs2_iq_headless(gr.top_block):

    def __init__(self, code_rate="3/4", constellation="QPSK", dest_filename='../iq_data/dvbs2_iq.cfile', esn0_db=20.0, fecframe_size='normal', frequency_drift_max_hz=0.0, frequency_drift_model='none', frequency_drift_rate_hz=0.0, frequency_offset_hz=0, num_samples=10000000, phase_offset_rad=0, pilots='on', resample_decimation=10, resample_interpolation=9, rolloff=0.35, rrc_filter_numtaps=100, samples_offset_beginning=123, samples_per_symbol=2, source_filename='adv16apsk910.ts', symbol_rate=5e6):
        gr.top_block.__init__(self, "Generate DVB-S2 IQ Headless")

        ##################################################
        # Parameters
        ##################################################
        self.code_rate = code_rate
        self.constellation = constellation
        self.dest_filename = dest_filename
        self.esn0_db = esn0_db
        self.fecframe_size = fecframe_size
        self.frequency_drift_max_hz = frequency_drift_max_hz
        self.frequency_drift_model = frequency_drift_model
        self.frequency_drift_rate_hz = frequency_drift_rate_hz
        self.frequency_offset_hz = frequency_offset_hz
        self.num_samples = num_samples
        self.phase_offset_rad = phase_offset_rad
        self.pilots = pilots
        self.resample_decimation = resample_decimation
        self.resample_interpolation = resample_interpolation
        self.rolloff = rolloff
        self.rrc_filter_numtaps = rrc_filter_numtaps
        self.samples_offset_beginning = samples_offset_beginning
        self.samples_per_symbol = samples_per_symbol
        self.source_filename = source_filename
        self.symbol_rate = symbol_rate

        self = epy_module_1.update_gr_top_block(self)

        ##################################################
        # Variables
        ##################################################
        self.sample_rate_hz = sample_rate_hz = samples_per_symbol * symbol_rate
        self.output_sample_rate_hz = output_sample_rate_hz = sample_rate_hz * resample_interpolation / resample_decimation
        self.noise_var = noise_var = 1.0 / (10**(esn0_db/10.0))
        #self.noise_var = noise_var = (2 * (1.0 / (10 ** (esn0_db / 10.0)) * 2 * self.resample_interpolation  / self.resample_decimation)) ** 2

        ##################################################
        # Blocks
        ##################################################
        self.rational_resampler_xxx_0 = filter.rational_resampler_ccc(
                interpolation=resample_interpolation,
                decimation=resample_decimation,
                taps=None,
                fractional_bw=None)
        self.fft_filter_xxx_0 = filter.fft_filter_ccc(1, firdes.root_raised_cosine(1.41, sample_rate_hz, symbol_rate, rolloff, rrc_filter_numtaps), 1)
        self.fft_filter_xxx_0.declare_sample_delay(0)
        self.dtv_dvbs2_physical_cc_0 = dtv.dvbs2_physical_cc(
            self.fecframe_size,
            self.code_rate,
            self.constellation,
            self.pilots,
            0)
        self.dtv_dvbs2_modulator_bc_0 = dtv.dvbs2_modulator_bc(
            self.fecframe_size,
            self.code_rate,
            self.constellation,
            dtv.INTERPOLATION_OFF)
        self.dtv_dvbs2_interleaver_bb_0 = dtv.dvbs2_interleaver_bb(
            self.fecframe_size,
            self.code_rate,
            self.constellation)
        self.dtv_dvb_ldpc_bb_0 = dtv.dvb_ldpc_bb(
            dtv.STANDARD_DVBS2,
            self.fecframe_size,
            self.code_rate,
            self.constellation)
        self.dtv_dvb_bch_bb_0 = dtv.dvb_bch_bb(
            dtv.STANDARD_DVBS2,
            self.fecframe_size,
            self.code_rate
            )
        self.dtv_dvb_bbscrambler_bb_0 = dtv.dvb_bbscrambler_bb(
            dtv.STANDARD_DVBS2,
            self.fecframe_size,
            self.code_rate
            )
        self.dtv_dvb_bbheader_bb_0 = dtv.dvb_bbheader_bb(
        dtv.STANDARD_DVBS2,
        self.fecframe_size,
        self.code_rate,
        self.rolloff,
        dtv.INPUTMODE_NORMAL,
        dtv.INBAND_OFF,
        168,
        4000000)
        self.blocks_vco_c_0 = blocks.vco_c(output_sample_rate_hz, 1, 1)
        self.blocks_skiphead_0 = blocks.skiphead(gr.sizeof_gr_complex*1, samples_offset_beginning)
        self.blocks_pack_k_bits_bb_0 = blocks.pack_k_bits_bb(8)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_cc(np.exp(1j*phase_offset_rad))
        self.blocks_head_0 = blocks.head(gr.sizeof_gr_complex*1, num_samples)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, source_filename, True, 0, )
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_file_sink_0_0 = blocks.file_sink(gr.sizeof_char*1, 'bbframes.dat', False)
        self.blocks_file_sink_0_0.set_unbuffered(False)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, dest_filename, False)
        self.blocks_file_sink_0.set_unbuffered(False)
        self.blocks_add_xx_1 = blocks.add_vff(1)
        self.blocks_add_xx_0 = blocks.add_vcc(1)
        self.analog_sig_source_x_0 = analog.sig_source_f(output_sample_rate_hz, self.frequency_drift_model, self.frequency_drift_rate_hz, self.frequency_drift_max_hz * 4 * np.pi, self.frequency_drift_model_offset , np.pi/2)
        self.analog_noise_source_x_0 = analog.noise_source_c(analog.GR_GAUSSIAN, np.sqrt(noise_var), 0)
        self.analog_const_source_x_0_1 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, frequency_offset_hz * 2 * np.pi)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source_x_0_1, 0), (self.blocks_add_xx_1, 0))
        self.connect((self.analog_noise_source_x_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_add_xx_1, 1))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_skiphead_0, 0))
        self.connect((self.blocks_add_xx_1, 0), (self.blocks_vco_c_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.dtv_dvb_bbheader_bb_0, 0))
        self.connect((self.blocks_head_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.blocks_pack_k_bits_bb_0, 0), (self.blocks_file_sink_0_0, 0))
        self.connect((self.blocks_skiphead_0, 0), (self.blocks_head_0, 0))
        self.connect((self.blocks_vco_c_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.dtv_dvb_bbheader_bb_0, 0), (self.blocks_pack_k_bits_bb_0, 0))
        self.connect((self.dtv_dvb_bbheader_bb_0, 0), (self.dtv_dvb_bbscrambler_bb_0, 0))
        self.connect((self.dtv_dvb_bbscrambler_bb_0, 0), (self.dtv_dvb_bch_bb_0, 0))
        self.connect((self.dtv_dvb_bch_bb_0, 0), (self.dtv_dvb_ldpc_bb_0, 0))
        self.connect((self.dtv_dvb_ldpc_bb_0, 0), (self.dtv_dvbs2_interleaver_bb_0, 0))
        self.connect((self.dtv_dvbs2_interleaver_bb_0, 0), (self.dtv_dvbs2_modulator_bc_0, 0))
        self.connect((self.dtv_dvbs2_modulator_bc_0, 0), (self.dtv_dvbs2_physical_cc_0, 0))
        self.connect((self.dtv_dvbs2_physical_cc_0, 0), (self.fft_filter_xxx_0, 0))
        self.connect((self.fft_filter_xxx_0, 0), (self.rational_resampler_xxx_0, 0))
        self.connect((self.rational_resampler_xxx_0, 0), (self.blocks_multiply_const_vxx_0, 0))


    def get_code_rate(self):
        return self.code_rate

    def set_code_rate(self, code_rate):
        self.code_rate = code_rate

    def get_constellation(self):
        return self.constellation

    def set_constellation(self, constellation):
        self.constellation = constellation

    def get_dest_filename(self):
        return self.dest_filename

    def set_dest_filename(self, dest_filename):
        self.dest_filename = dest_filename
        self.blocks_file_sink_0.open(self.dest_filename)

    def get_esn0_db(self):
        return self.esn0_db

    def set_esn0_db(self, esn0_db):
        self.esn0_db = esn0_db
        self.set_noise_var(1.0 / (10**(self.esn0_db/10.0)))

    def get_fecframe_size(self):
        return self.fecframe_size

    def set_fecframe_size(self, fecframe_size):
        self.fecframe_size = fecframe_size

    def get_frequency_drift_max_hz(self):
        return self.frequency_drift_max_hz

    def set_frequency_drift_max_hz(self, frequency_drift_max_hz):
        self.frequency_drift_max_hz = frequency_drift_max_hz
        self.analog_sig_source_x_0.set_amplitude(self.frequency_drift_max_hz * 4 * np.pi)

    def get_frequency_drift_model(self):
        return self.frequency_drift_model

    def set_frequency_drift_model(self, frequency_drift_model):
        self.frequency_drift_model = frequency_drift_model

    def get_frequency_drift_rate_hz(self):
        return self.frequency_drift_rate_hz

    def set_frequency_drift_rate_hz(self, frequency_drift_rate_hz):
        self.frequency_drift_rate_hz = frequency_drift_rate_hz
        self.analog_sig_source_x_0.set_frequency(self.frequency_drift_rate_hz)

    def get_frequency_offset_hz(self):
        return self.frequency_offset_hz

    def set_frequency_offset_hz(self, frequency_offset_hz):
        self.frequency_offset_hz = frequency_offset_hz
        self.analog_const_source_x_0_1.set_offset(self.frequency_offset_hz * 2 * np.pi)

    def get_num_samples(self):
        return self.num_samples

    def set_num_samples(self, num_samples):
        self.num_samples = num_samples
        self.blocks_head_0.set_length(self.num_samples)

    def get_phase_offset_rad(self):
        return self.phase_offset_rad

    def set_phase_offset_rad(self, phase_offset_rad):
        self.phase_offset_rad = phase_offset_rad
        self.blocks_multiply_const_vxx_0.set_k(np.exp(1j*self.phase_offset_rad))

    def get_pilots(self):
        return self.pilots

    def set_pilots(self, pilots):
        self.pilots = pilots

    def get_resample_decimation(self):
        return self.resample_decimation

    def set_resample_decimation(self, resample_decimation):
        self.resample_decimation = resample_decimation
        self.set_output_sample_rate_hz(self.sample_rate_hz * self.resample_interpolation / self.resample_decimation)

    def get_resample_interpolation(self):
        return self.resample_interpolation

    def set_resample_interpolation(self, resample_interpolation):
        self.resample_interpolation = resample_interpolation
        self.set_output_sample_rate_hz(self.sample_rate_hz * self.resample_interpolation / self.resample_decimation)

    def get_rolloff(self):
        return self.rolloff

    def set_rolloff(self, rolloff):
        self.rolloff = rolloff
        self.fft_filter_xxx_0.set_taps(firdes.root_raised_cosine(1.41, self.sample_rate_hz, self.symbol_rate, self.rolloff, self.rrc_filter_numtaps))

    def get_rrc_filter_numtaps(self):
        return self.rrc_filter_numtaps

    def set_rrc_filter_numtaps(self, rrc_filter_numtaps):
        self.rrc_filter_numtaps = rrc_filter_numtaps
        self.fft_filter_xxx_0.set_taps(firdes.root_raised_cosine(1.41, self.sample_rate_hz, self.symbol_rate, self.rolloff, self.rrc_filter_numtaps))

    def get_samples_offset_beginning(self):
        return self.samples_offset_beginning

    def set_samples_offset_beginning(self, samples_offset_beginning):
        self.samples_offset_beginning = samples_offset_beginning

    def get_samples_per_symbol(self):
        return self.samples_per_symbol

    def set_samples_per_symbol(self, samples_per_symbol):
        self.samples_per_symbol = samples_per_symbol
        self.set_sample_rate_hz(self.samples_per_symbol * self.symbol_rate)

    def get_source_filename(self):
        return self.source_filename

    def set_source_filename(self, source_filename):
        self.source_filename = source_filename
        self.blocks_file_source_0.open(self.source_filename, True)

    def get_symbol_rate(self):
        return self.symbol_rate

    def set_symbol_rate(self, symbol_rate):
        self.symbol_rate = symbol_rate
        self.set_sample_rate_hz(self.samples_per_symbol * self.symbol_rate)
        self.fft_filter_xxx_0.set_taps(firdes.root_raised_cosine(1.41, self.sample_rate_hz, self.symbol_rate, self.rolloff, self.rrc_filter_numtaps))

    def get_sample_rate_hz(self):
        return self.sample_rate_hz

    def set_sample_rate_hz(self, sample_rate_hz):
        self.sample_rate_hz = sample_rate_hz
        self.set_output_sample_rate_hz(self.sample_rate_hz * self.resample_interpolation / self.resample_decimation)
        self.fft_filter_xxx_0.set_taps(firdes.root_raised_cosine(1.41, self.sample_rate_hz, self.symbol_rate, self.rolloff, self.rrc_filter_numtaps))

    def get_output_sample_rate_hz(self):
        return self.output_sample_rate_hz

    def set_output_sample_rate_hz(self, output_sample_rate_hz):
        self.output_sample_rate_hz = output_sample_rate_hz
        self.analog_sig_source_x_0.set_sampling_freq(self.output_sample_rate_hz)

    def get_noise_var(self):
        return self.noise_var

    def set_noise_var(self, noise_var):
        self.noise_var = noise_var
        self.analog_noise_source_x_0.set_amplitude(np.sqrt(self.noise_var))


def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "--code-rate", dest="code_rate", type=str, default="3/4",
        help="Set Code Rate [default=%(default)r]")
    parser.add_argument(
        "--constellation", dest="constellation", type=str, default="QPSK",
        help="Set Constellation [default=%(default)r]")
    parser.add_argument(
        "--dest-filename", dest="dest_filename", type=str, default='../iq_data/dvbs2_iq.cfile',
        help="Set Dest Filename [default=%(default)r] (Set extension to .cfile for raw binary IQ double pairs, and .tmp for bluefile)")
    parser.add_argument(
        "--esn0-db", dest="esn0_db", type=eng_float, default="20.0",
        help="Set Es/N0 (Symbol to Noise Ratio) in dB [default=%(default)r]")
    parser.add_argument(
        "--fecframe-size", dest="fecframe_size", type=str, default='normal',
        help="Set FECFRAME size ('Normal' or 'Short') [default=%(default)r]")
    parser.add_argument(
        "--frequency-drift-max-hz", dest="frequency_drift_max_hz", type=eng_float, default="0.0",
        help="Set Frequency Deviation (Hz) [default=%(default)r]")
    parser.add_argument(
        "--frequency-drift-model", dest="frequency_drift_model", type=str, default='none',
        help="Set Frequency Drift Model [default=%(default)r] (Valid options are 'cosine', 'triangle', and 'none')")
    parser.add_argument(
        "--frequency-drift-rate-hz", dest="frequency_drift_rate_hz", type=eng_float, default="0.0",
        help="Set Frequency Deviation Rate (Hz) [default=%(default)r]")
    parser.add_argument(
        "--frequency-offset-hz", dest="frequency_offset_hz", type=eng_float, default="0.0",
        help="Set Frequency Offset (Hz) [default=%(default)r]")
    parser.add_argument(
        "--num-samples", dest="num_samples", type=intx, default=10000000,
        help="Set Number of IQ samples to generate [default=%(default)r]")
    parser.add_argument(
        "--phase-offset-rad", dest="phase_offset_rad", type=eng_float, default="0.0",
        help="Set Phase Offset (rad) [default=%(default)r]")
    parser.add_argument(
        "--pilots", dest="pilots", type=str, default="on",
        help="Set Pilots [default=%(default)r]")
    parser.add_argument(
        "--resample-decimation", dest="resample_decimation", type=intx, default=10,
        help="Set Resampler Decimation [default=%(default)r]")
    parser.add_argument(
        "--resample-interpolation", dest="resample_interpolation", type=intx, default=9,
        help="Set Resampler Interpolation [default=%(default)r]")
    parser.add_argument(
        "--rolloff", dest="rolloff", type=eng_float, default="350.0m",
        help="Set RRC Roll-Off Percentage (0.20, 0.25, 0.35) [default=%(default)r]")
    parser.add_argument(
        "--rrc-filter-numtaps", dest="rrc_filter_numtaps", type=intx, default=100,
        help="Set RRC Roll-Off Percentage (0.20, 0.30) [default=%(default)r]")
    parser.add_argument(
        "--samples-offset-beginning", dest="samples_offset_beginning", type=intx, default=123,
        help="Set Number of samples to offset from start (helps ensure first symbol has non-zero symbol phase) [default=%(default)r]")
    parser.add_argument(
        "--samples-per-symbol", dest="samples_per_symbol", type=intx, default=2,
        help="Set Samples per Symbol [default=%(default)r]")
    parser.add_argument(
        "--source-filename", dest="source_filename", type=str, default='adv16apsk910.ts',
        help="Set Source Filename (from http://www.w6rz.net/adv16apsk910.ts) [default=%(default)r]")
    parser.add_argument(
        "--symbol-rate", dest="symbol_rate", type=eng_float, default="5.0M",
        help="Set Symbol Rate in Symbols/Second [default=%(default)r]")
    return parser


def main(top_block_cls=generate_dvbs2_iq_headless, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(code_rate=options.code_rate, constellation=options.constellation, dest_filename=options.dest_filename, esn0_db=options.esn0_db, fecframe_size=options.fecframe_size, frequency_drift_max_hz=options.frequency_drift_max_hz, frequency_drift_model=options.frequency_drift_model, frequency_drift_rate_hz=options.frequency_drift_rate_hz, frequency_offset_hz=options.frequency_offset_hz, num_samples=options.num_samples, phase_offset_rad=options.phase_offset_rad, pilots=options.pilots, resample_decimation=options.resample_decimation, resample_interpolation=options.resample_interpolation, rolloff=options.rolloff, rrc_filter_numtaps=options.rrc_filter_numtaps, samples_offset_beginning=options.samples_offset_beginning, samples_per_symbol=options.samples_per_symbol, source_filename=options.source_filename, symbol_rate=options.symbol_rate)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    tb.wait()

    fs_hz = options.symbol_rate * options.samples_per_symbol * options.resample_interpolation/options.resample_decimation
    dest_filename, dest_filename_extension = os.path.splitext(options.dest_filename)
    if dest_filename_extension == '.tmp':
        epy_module_1.convert_to_bluefile(filename=options.dest_filename, fs_hz=fs_hz, t_start_sec=0.0)

    tb.stop()
    tb.wait()
    tb = None


if __name__ == '__main__':
    main()
