import sys
import json
import math
import numpy as np
from gnuradio import dtv
from gnuradio import analog
from dsp_utilities.file_handlers.convert_rawfile_to_bluefile import convert_rawfile_to_bluefile
from dsp_utilities.file_handlers import bluefile


def update_gr_top_block(gr_obj):
    ##################################################
    # Ben Custom Code
    ##################################################
    if gr_obj.fecframe_size.lower() == 'normal':
        gr_obj.fecframe_size = dtv.FECFRAME_NORMAL
    elif gr_obj.fecframe_size.lower() == 'short':
        gr_obj.fecframe_size = dtv.FECFRAME_SHORT
    else:
        sys.exit('Unrecogized fecframe_size: {}  -  Should be normal or short'.format(gr_obj.fecframe_size))

    if gr_obj.constellation.lower() == 'qpsk':
        gr_obj.constellation = dtv.MOD_QPSK
    elif gr_obj.constellation.lower() == '8psk':
        gr_obj.constellation = dtv.MOD_8PSK
    elif gr_obj.constellation.lower() == '16apsk':
        gr_obj.constellation = dtv.MOD_16APSK
    elif gr_obj.constellation.lower() == '32apsk':
        gr_obj.constellation = dtv.MOD_32APSK
    else:
        sys.exit(
            'Unrecogized constellation: {}  -  Should be QPSK, 8PSK, 16APSK, or 32APSK'.format(gr_obj.constellation))

    if gr_obj.code_rate == '1/4':
        gr_obj.code_rate = dtv.C1_4
    elif gr_obj.code_rate == '1/3':
        gr_obj.code_rate = dtv.C1_3
    elif gr_obj.code_rate == '2/5':
        gr_obj.code_rate = dtv.C2_5
    elif gr_obj.code_rate == '1/2':
        gr_obj.code_rate = dtv.C1_2
    elif gr_obj.code_rate == '3/5':
        gr_obj.code_rate = dtv.C3_5
    elif gr_obj.code_rate == '2/3':
        gr_obj.code_rate = dtv.C2_3
    elif gr_obj.code_rate == '3/4':
        gr_obj.code_rate = dtv.C3_4
    elif gr_obj.code_rate == '4/5':
        gr_obj.code_rate = dtv.C4_5
    elif gr_obj.code_rate == '5/6':
        gr_obj.code_rate = dtv.C5_6
    elif gr_obj.code_rate == '8/9':
        gr_obj.code_rate = dtv.C8_9
    elif gr_obj.code_rate == '9/10':
        gr_obj.code_rate = dtv.C9_10
    else:
        sys.exit(
            'Unrecogized code rate: {}  -  Should be 1/4, 1/3, 2/5, 1/2, 3/5, 2/3, 3/4, 4/5, 5/6, 8/9, or 9/10'.format(
                gr_obj.code_rate))

    if math.isclose(gr_obj.rolloff, 0.2, rel_tol=0.0001):
        gr_obj.rolloff = dtv.RO_0_20
    elif math.isclose(gr_obj.rolloff, 0.25, rel_tol=0.0001):
        gr_obj.rolloff = dtv.RO_0_25
    elif math.isclose(gr_obj.rolloff, 0.35, rel_tol=0.0001):
        gr_obj.rolloff = dtv.RO_0_35
    else:
        sys.exit('Unrecogized rolloff: {}  -  Should be 0.20, 0.25, or 0.35'.format(gr_obj.rolloff))

    if gr_obj.pilots.lower() == 'on':
        gr_obj.pilots = dtv.PILOTS_ON
    elif gr_obj.pilots.lower() == 'off':
        gr_obj.pilots = dtv.PILOTS_OFF
    else:
        sys.exit('Unrecogized pilots: {}  -  Should be On or Off'.format(gr_obj.pilots))

    if gr_obj.frequency_drift_model.lower() == 'cosine':
        gr_obj.frequency_drift_model = analog.GR_COS_WAVE
        gr_obj.frequency_drift_model_offset = 0
    elif gr_obj.frequency_drift_model.lower() == 'triangle':
        gr_obj.frequency_drift_model = analog.GR_TRI_WAVE
        gr_obj.frequency_drift_model_offset = gr_obj.frequency_drift_max_hz * -2 * np.pi
    elif gr_obj.frequency_drift_model.lower() == 'none':
        gr_obj.frequency_drift_model = analog.GR_CONST_WAVE
        gr_obj.frequency_drift_model_offset = 0.0
        gr_obj.frequency_drift_max_hz = 0.0
        gr_obj.frequency_drift_rate_hz = 0.0
    else:
        sys.exit('Unrecogized frequency_drift_model: {}  -  \'cosine\' and \'triangle\; '
                 'are the only formats currently supported'.format(gr_obj.frequency_drift_model))

    return gr_obj


def convert_to_bluefile(filename, fs_hz, t_start_sec):
    new_filename = filename.replace('.cfile', '.tmp')
    convert_rawfile_to_bluefile(filename=filename, new_filename=new_filename,
                                fs_hz=fs_hz, t_start_sec=t_start_sec, center_frequency_hz=0.0)
    hdr, iq_array = bluefile.read(new_filename, start=None, end=None, ext_header_type=str)
    ext_header = json.loads(hdr['ext_header'])
    ext_header.update({'argv': ' '.join(sys.argv)})
    hdr['ext_header'] = json.dumps(ext_header)
    bluefile.write(filename=new_filename, hdr=hdr, ext_header_type=type(hdr['ext_header']), data=iq_array)