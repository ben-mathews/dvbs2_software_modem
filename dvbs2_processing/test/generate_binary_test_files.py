import numpy as np
import struct
import random

ramp = np.linspace(0, 255, 256, dtype=np.uint8)

ramp_start_val = 0
num_bytes_to_write = int(4*16384*len(ramp))
num_bytes_written = 0

filename = '/home/ben/dev/gitlab/dvbs2_software_modem/dvbs2_processing/test/int_ramp.bin'
f = open(filename,'wb')
while num_bytes_written < num_bytes_to_write:
    ramp = np.mod(np.linspace(ramp_start_val, ramp_start_val+186, 187, dtype=np.uint8), 256, dtype=np.uint16)
    ramp = np.insert(ramp, 0, 0x47)
    ramp = ramp.astype(np.uint8)
    f.write(bytes(ramp))
    num_bytes_written += len(ramp)
    ramp_start_val = np.mod(ramp_start_val+187, 256)
f.close()