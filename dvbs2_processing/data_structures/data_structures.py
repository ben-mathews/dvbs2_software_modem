from bitstring import BitArray

from copy import deepcopy
import numpy as np

from dsp_utilities.plotting.scatterplot import scatterplot

from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration, DVBS2_Frame_Length, DVBS2_Pilots, DVBS2_Modulation_Type
from dvbs2_processing.constant_data import DVBS2_MATYPE1_TS_GS, DVBS2_MATYPE1_SIS_MIS, DVBS2_MATYPE1_CCM_ACM, \
    DVBS2_MATYPE1_ISSYI, DVBS2_MATYPE1_Rolloff, DVBS2_MATYPE1_NPD, DVBS2_ModCods

from dvbs2_processing.signal_utilities.interleaving import interleave_plframe, deinterleave_plframe

from dvbs2_processing.utilities import send_via_udp, compute_crc8

from dvbs2_processing.signal_utilities.modulation import demodulate_plframe

import matplotlib.pyplot as plt

#matplotlib.pyplot.switch_backend('Qt5Agg')
plt.style.use('dark_background')


# noinspection PyUnreachableCode
class DVBS2_Frame:
    """
    Functionality to detect and output PLFRAMEs from coarsely synchronized symbols

    Assumptions:
     * Symbols are coarsely synced in clock and carrier frequency (exactly what that means: TBD)

    """

    def __init__(self, plframe_index=None, plframe_symbols=None, t_plframe_symbols=None, dvbs2_modcod=None,
                 symbol_start_index=None, plheader_frequency_offset_hz=None, frequency_adjustments_hz=[],
                 plheader_phase_offset_rad=None, plheader_amplitude=None,
                 plheader_snr=None, next_symbol_index=None):
        self.index = plframe_index
        self.plframe_symbols = plframe_symbols
        self.plframe_symbols_descrambled = False
        self.plframe_symbols_corrected = None
        self.t_plframe_symbols = t_plframe_symbols
        self.dvbs2_modcod = dvbs2_modcod
        self.start_index = symbol_start_index
        self.plheader_frequency_offset_hz = plheader_frequency_offset_hz
        self.plheader_phase_offset_rad = plheader_phase_offset_rad
        self.plheader_amplitude = plheader_amplitude
        self.plheader_snr = plheader_snr
        self.descrambled = False
        self.next_symbol_index = next_symbol_index
        self.frequency_offset_hz = None
        self.phase_offset_rad = None # To correct for this phase shift by -1 * self.phase_offset_rad
        self.amplitude_offset = None # To correct for this do self.plframe_symbols / self.amplitude_offset
        self.bbheader = None
        self.bits = None
        self.bits_descrambled = None
        self.passed_fec = None
        self.esn0 = None
        self.frequency_adjustments_hz = deepcopy(frequency_adjustments_hz)


    def __repr__(self):
        index = self.index if self.esn0 is not None else np.nan
        t0 = self.esn0 if self.esn0 is not None else np.nan
        esn0 = self.t_plframe_symbols[0] if self.t_plframe_symbols is not None is not None else np.nan
        plheader_frequency_offset_hz = self.plheader_frequency_offset_hz if self.plheader_frequency_offset_hz is not None else np.nan
        return 'PLFRAME:  Index: {:<6}    ModCod: {:<50}    Symbol Start Index: {:<9}    Time: {:<11.6f}    Header Freq Offset: {:<14.6f}    Es/N0: {:<6}'.format(index, self.dvbs2_modcod.__repr__(), self.start_index, t0, plheader_frequency_offset_hz, esn0)


class DVBS2_BBHEADER:
    def __init__(self, bbheader_bits_descrambled):
        if BitArray(bbheader_bits_descrambled[0:2]).uint == 0:
            self.gs = DVBS2_MATYPE1_TS_GS.GENERIC_PACKETIZED
        elif BitArray(bbheader_bits_descrambled[0:2]).uint == 1:
            self.gs = DVBS2_MATYPE1_TS_GS.GENERIC_CONTINUOUS
        elif BitArray(bbheader_bits_descrambled[0:2]).uint == 2:
            self.gs = DVBS2_MATYPE1_TS_GS.RESERVED
        elif BitArray(bbheader_bits_descrambled[0:2]).uint == 3:
            self.gs = DVBS2_MATYPE1_TS_GS.TRANSPORT
        else:
            raise Exception('Unhandled TS/GS value in BBHEADER: {}'.format(BitArray(bbheader_bits_descrambled[0:2]).uint ))

        if int(bbheader_bits_descrambled[2]) == 0:
            self.mis = DVBS2_MATYPE1_SIS_MIS.MULTIPLE
        elif int(bbheader_bits_descrambled[2]) == 1:
            self.mis = DVBS2_MATYPE1_SIS_MIS.SINGLE
        else:
            raise Exception('Unhandled SIS/MIS value in BBHEADER: {}'.format(BitArray(bbheader_bits_descrambled[2]).uint))

        if int(bbheader_bits_descrambled[3]) == 0:
            self.acm = DVBS2_MATYPE1_CCM_ACM.ACM
        elif int(bbheader_bits_descrambled[3]) == 1:
            self.acm = DVBS2_MATYPE1_CCM_ACM.CCM
        else:
            raise Exception('Unhandled CCM/ACM value in BBHEADER: {}'.format(int(bbheader_bits_descrambled[3])))

        if int(bbheader_bits_descrambled[4]) == 0:
            self.issyi = DVBS2_MATYPE1_ISSYI.NOT_ACTIVE
        elif int(bbheader_bits_descrambled[4]) == 1:
            self.issyi = DVBS2_MATYPE1_ISSYI.ACTIVE
        else:
            raise Exception('Unhandled ISSYI value in BBHEADER: {}'.format(int(bbheader_bits_descrambled[4])))

        if int(bbheader_bits_descrambled[5]) == 0:
            self.npd = DVBS2_MATYPE1_NPD.NOT_ACTIVE
        elif int(bbheader_bits_descrambled[5]) == 1:
            self.issyi = DVBS2_MATYPE1_NPD.ACTIVE
        else:
            raise Exception('Unhandled NPD value in BBHEADER: {}'.format(int(bbheader_bits_descrambled[5])))

        if BitArray(bbheader_bits_descrambled[6:8]).uint == 0:
            self.ro = DVBS2_MATYPE1_Rolloff.RO_0_35
        elif BitArray(bbheader_bits_descrambled[6:8]).uint == 1:
            self.ro = DVBS2_MATYPE1_Rolloff.RO_0_25
        elif BitArray(bbheader_bits_descrambled[6:8]).uint == 2:
            self.ro = DVBS2_MATYPE1_Rolloff.RO_0_20
        elif BitArray(bbheader_bits_descrambled[6:8]).uint == 3:
            self.ro = DVBS2_MATYPE1_Rolloff.Reserved
        else:
            raise Exception('Unhandled RO value in BBHEADER: {}'.format(BitArray(bbheader_bits_descrambled[6:8]).uint))

        self.matype2 = BitArray(bbheader_bits_descrambled[8:16]).uint
        self.upl = BitArray(bbheader_bits_descrambled[16:32]).uint
        self.dfl = BitArray(bbheader_bits_descrambled[32:48]).uint
        self.sync = BitArray(bbheader_bits_descrambled[48:56]).uint
        self.syncd = BitArray(bbheader_bits_descrambled[56:72]).uint
        self.crc = BitArray(bbheader_bits_descrambled[72:80]).uint

        crc_check = compute_crc8(bbheader_bits_descrambled[0:72])
        self.passed_crc = False
        if crc_check == self.crc:
            self.passed_crc = True
