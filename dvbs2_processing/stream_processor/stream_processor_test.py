import os
import time
import uuid
from multiprocessing import Pool, Manager
import pytest
import numpy as np
import matplotlib.pyplot as plt

from dvbs2_processing.stream_processor import DVBS2StreamProcessor
from dvbs2_processing.utilities import process_command_line_args

from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration, DVBS2_Frame_Length, DVBS2_Pilots, \
    DVBS2_ModCods, DVBS2_MATYPE1_Rolloff

from dvbs2_processing.test import argument_parser
from dvbs2_processing.test import main as generate_dvbs2_iq_headless_main
from dvbs2_processing.data_structures.data_structures import DVBS2_Frame

from dsp_utilities.file_handlers.iq_file_store import IQFileType

from dvbs2_processing_apps.carrier_acquire import carrier_acquire

#
# A list of quick test cases for regression testing.  All of these cases shoudl pass with no packet errors.
#
quick_test_cases = [
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_QPSK_3_5, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 4.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_QPSK_2_3, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 4.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_QPSK_3_4, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 2.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_QPSK_4_5, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 2.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_QPSK_5_6, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_QPSK_8_9, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_QPSK_9_10, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_8PSK_2_3, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_8PSK_3_4, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_8PSK_5_6, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_8PSK_8_9, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_8PSK_9_10, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_2_3, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_3_4, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_4_5, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_5_6, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_8_9, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_9_10, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_32APSK_3_4, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_32APSK_4_5, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_32APSK_5_6, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_32APSK_8_9, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
    (DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_32APSK_9_10, frame_length=DVBS2_Frame_Length.Normal_Frames,
                                pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20), 1.0, 25),
]


def process_test_case(dvbs2_modcod: DVBS2_ModCod_Configuration, margin: float, num_frames: int,
                      fec_processing_mode: str, zmq_fec_server: str) -> tuple:
    """
    The function to run a test case.  This is called by various other functions in this file.
    @param dvbs2_modcod: DVBS2_ModCod_Configuration object to test
    @param margin: Margin over QEF Es/N0
    @param num_frames: Number of frames to run
    @param fec_processing_mode: FEC processing mode
    @param zmq_fec_server: ZMQ server URL
    @return:
    """
    dvbs2_frames = []
    esn0_db = np.nan
    # Don't process RESERVED or DUMMY frames
    if dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_DUMMY_PLFRAME or \
            dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_RESERVED0 or \
            dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_RESERVED1 or \
            dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_RESERVED2:
        return dvbs2_frames, esn0_db
    # These MODCODs are not valid for SHORT frames
    if (dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_QPSK_9_10 or
        dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_8PSK_9_10 or
        dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_16APSK_9_10 or
        dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_32APSK_9_10) \
            and dvbs2_modcod.frame_length == DVBS2_Frame_Length.Short_Frames:
        return dvbs2_frames, esn0_db
    # These MODCODs are too low Es/N0 for the demod right now (stuff needs to be fixed!)
    if dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_QPSK_1_4 or dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_QPSK_1_3 or \
            dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_QPSK_2_5:
        return dvbs2_frames, esn0_db

    print('----------------------------------------------------------------------------------------------')
    print('Testing {} at {} dB margin'.format(dvbs2_modcod.modcod, margin))
    print('----------------------------------------------------------------------------------------------')

    # Simulation and data generation parameters
    modtype, code_rate = dvbs2_modcod.modulation_type_and_coding_rate()
    esn0_db = max(dvbs2_modcod.get_threshold_esn0() + margin, 2.0)
    # esn0_db = dvbs2_modcod.get_threshold_esn0() + 1.5  # Sometimes useful for debugging
    symbol_rate = 1e6
    resample_decimation = 10
    resample_interpolation = 9
    samples_per_symbol = 2
    sample_rate_hz = symbol_rate * samples_per_symbol * resample_interpolation / resample_decimation
    num_symbols_to_generate = num_frames * dvbs2_modcod.get_num_symbols()
    num_samples_to_generate = num_symbols_to_generate * samples_per_symbol * \
                              resample_interpolation / resample_decimation

    # Generate and build options for data generation
    uuid_ = uuid.uuid4()
    options = argument_parser().parse_args(args=[])
    options.dest_filename = '/tmp/stream_processor_test_' + \
                            dvbs2_modcod.modcod_pretty().replace(' ', '_').replace('/', '_') + \
                            '_{:2.2f}_db'.format(esn0_db) + '_{}'.format(str(uuid_)) + '.tmp'
    options.source_filename = '/home/ben/dev/gitlab/dvbs2_demod/test/gnuradio/flowgraphs/adv16apsk910.ts'
    options.constellation = modtype
    options.code_rate = code_rate
    options.esn0_db = esn0_db
    options.fecframe_size = dvbs2_modcod.frames_pretty().split(' ')[0].lower()
    options.pilots = dvbs2_modcod.pilots_pretty().split(' ')[1].lower()
    options.rolloff = dvbs2_modcod.rolloff.value
    options.symbol_rate = symbol_rate
    options.num_samples = int(num_samples_to_generate)
    options.resample_decimation = resample_decimation
    options.resample_interpolation = resample_interpolation
    options.samples_per_symbol = samples_per_symbol
    options.frequency_offset_hz = 1234.56789
    options.frequency_offset_hz = 0.0

    # Generate the data; Due to GNURadio limitations we generally have to call data generation in a
    # separate Python environment or it will start to fail after 100 runs.
    run_data_gen_in_shell = True
    if not run_data_gen_in_shell:
        generate_dvbs2_iq_headless_main(options=options)
    else:
        args = []
        for kvp in options.__dict__:
            args.append('--' + kvp.replace('_', '-'))
            if type(options.__dict__[kvp]) is not str:
                args.append(str(options.__dict__[kvp]))
            else:
                args.append(options.__dict__[kvp])
        os.system('python3 ../test/generate_dvbs2_iq_headless.py ' + ' '.join(args))

    # Configuration parameters for inital sync
    args_override = ['--input_file', options.dest_filename,
                     '--fec_processing_mode', fec_processing_mode,
                     '--zmq_fec_server', zmq_fec_server]
    filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths, mod_types, \
    dvbs2_code_rates, fec_processing_mode, zmq_fec_server, pilots, num_fec_workers = \
        process_command_line_args(args_override)
    fc_estimate_hz = 0.0
    fsym_estimate_hz = options.symbol_rate
    num_sync_frames = 10
    use_hints = True

    if use_hints:
        frame_lengths = [dvbs2_modcod.frame_length]
        mod_types = [dvbs2_modcod.modulation_type_enum()]
        pilots = [dvbs2_modcod.pilots]
        dvbs2_code_rates = [dvbs2_modcod.get_code_rate_enum()]
    fake_initial_sync = True
    if fake_initial_sync:
        # Initial sync can take a while, so in some cases we bypass and jump straight to processing frames
        initial_sync_results = {
            'fs_hz': sample_rate_hz,
            'fsym_hz': symbol_rate,
            'first_dvbs2_plframe': DVBS2_Frame(dvbs2_modcod=
                                               DVBS2_ModCod_Configuration(modcod=dvbs2_modcod.modcod,
                                                                          frame_length=dvbs2_modcod.frame_length,
                                                                          pilots=dvbs2_modcod.pilots,
                                                                          rolloff=dvbs2_modcod.rolloff)),
            't_start_sec': 0.0,
            'frequency_offset_hz': options.frequency_offset_hz,
            'dvbs2_rolloff': dvbs2_modcod.rolloff
        }
    else:
        initial_sync_results = carrier_acquire(filename=filename, fc_estimate_hz=fc_estimate_hz,
                                               fsym_estimate_hz=fsym_estimate_hz, frame_lengths=frame_lengths,
                                               mod_types=mod_types, dvbs2_code_rates=dvbs2_code_rates,
                                               fec_processing_mode='none', zmq_fec_server=zmq_fec_server,
                                               pilots=pilots, num_consecutive_plframes=num_sync_frames)
    # Instantiate the processor and process the PLFRAMES
    dvbs2_stream_processor = DVBS2StreamProcessor(
        dvbs2_frame_length=frame_lengths,
        dvbs2_pilots=pilots,
        dvbs2_modulation_type=mod_types,
        dvbs2_code_rates=dvbs2_code_rates,
        # fec_processing_mode='none',
        # fec_processing_mode='zmq_processor',
        # fec_processing_mode='local_processor',
        fec_processing_mode=fec_processing_mode,
        threading_config=0,
        zmq_fec_server=zmq_fec_server)
    dvbs2_stream_processor.initialize_file_store(filename=filename, fs_hz=None,
                                                 file_type=IQFileType.BLUEFILE,
                                                 iq_data_format=None)
    block_data, dvbs2_frames = dvbs2_stream_processor.process_plframes(
        initial_sync_results=initial_sync_results,
        block_size_num_samples=
        int(50 * initial_sync_results['first_dvbs2_plframe'].dvbs2_modcod.get_num_symbols() *
            initial_sync_results['fs_hz'] / initial_sync_results['fsym_hz']),
        num_blocks=10000)

    os.remove(options.dest_filename)

    return dvbs2_frames, esn0_db


@pytest.mark.parametrize("dvbs2_modcod, margin, num_frames", quick_test_cases)
def test_quick_tests(dvbs2_modcod: DVBS2_ModCod_Configuration, margin: float, num_frames: int):
    """
    Runs the quick tests
    @param dvbs2_modcod: DVBS2_ModCod_Configuration object to test
    @param margin: Margin over QEF Es/N0
    @param num_frames: Number of frames to run
    """
    fec_processing_mode = 'zmq_processor'
    # fec_processing_mode = 'local_processor'
    zmq_fec_server = 'tcp://localhost:5555'
    dvbs2_frames, esn0_db = process_test_case(dvbs2_modcod, margin, num_frames, fec_processing_mode, zmq_fec_server)
    per = np.sum([not dvbs2_frame.passed_fec for dvbs2_frame in dvbs2_frames]) / len(dvbs2_frames)
    assert per == 0.0


def manual_test_basic(num_frames=100):
    """
    A manually-run test that does a single-thred run over all test cases
    """
    fec_processing_mode = 'zmq_processor'
    zmq_fec_server = 'tcp://localhost:5555'
    test_data = []
    margins = np.linspace(-2, 3, 51)  # 1/10th dB
    # margins = np.linspace(-1, 3, 17)  # 1/4th dB
    # margins = np.linspace(-1, 3, 5)  # 1 dB
    # for frame_length in [DVBS2_Frame_Length.Short_Frames, DVBS2_Frame_Length.Normal_Frames]:
    for frame_length in [DVBS2_Frame_Length.Normal_Frames]:
        for modcod in [DVBS2_ModCods.DVBS2_32APSK_3_4]:
            # for modcod in DVBS2_ModCods:
            for margin in margins:
                rolloff = DVBS2_MATYPE1_Rolloff.RO_0_20
                dvbs2_modcod = DVBS2_ModCod_Configuration(modcod=modcod,
                                                          frame_length=frame_length,
                                                          pilots=DVBS2_Pilots.Pilots_On,
                                                          rolloff=rolloff)

                dvbs2_frames, esn0_db = process_test_case(dvbs2_modcod, margin, num_frames, fec_processing_mode,
                                                          zmq_fec_server)

                if len(dvbs2_frames) == 0 and np.isnan(esn0_db):
                    continue
                # Collect some statistics
                assert len(dvbs2_frames) > num_frames - 10, \
                    'Expecting at least {} frames, received {}'.format(num_frames - 10, len(dvbs2_frames))
                mean_esn0_db = np.mean([dvbs2_frame.esn0 for dvbs2_frame in dvbs2_frames])
                per = np.sum([not dvbs2_frame.passed_fec for dvbs2_frame in dvbs2_frames]) / len(dvbs2_frames)
                test_data.append({'dvbs2_modcod': dvbs2_modcod,
                                  'threshold_esn0': dvbs2_modcod.get_threshold_esn0(),
                                  'processed_num_frames': len(dvbs2_frames),
                                  'simulated_esn0_db': esn0_db,
                                  'mean_esn0_db': mean_esn0_db,
                                  'per': per})

                print('Done with {}  |  Sim Es/N0 = {}  |  Actual Es/N0 = {}  |  PER = {}'.format(
                    dvbs2_modcod.modcod_pretty(), esn0_db, mean_esn0_db, per))
                print('--------------------------------------------------------------------')

    plot_results(test_data)

    for dvbs2_modcod in set([test_data_['dvbs2_modcod'] for test_data_ in test_data]):
        print(dvbs2_modcod)


#
# Wrapper function used in the multi-threaded and multi-process approaches in the next function
#
def process_test_case_wrapper(dvbs2_modcod, margin, num_frames, fec_processing_mode, zmq_fec_server, test_data,
                              test_data_index):
    """
    @param dvbs2_modcod: DVBS2_ModCod_Configuration object to test
    @param margin: Margin over QEF Es/N0
    @param num_frames: Number of frames to run
    @param fec_processing_mode: FEC processing mode
    @param zmq_fec_server: ZMQ server URL
    @param test_data: List of test results to put results into
    @param test_data_index:  Index of test results to put results into
    @return:
    """
    dvbs2_frames, esn0_db = process_test_case(dvbs2_modcod, margin, num_frames, fec_processing_mode, zmq_fec_server)
    if len(dvbs2_frames) == 0 and np.isnan(esn0_db):
        return
    # Collect some statistics
    assert len(dvbs2_frames) > num_frames - 10, \
        'Expecting at least {} frames, received {}'.format(num_frames - 10, len(dvbs2_frames))
    mean_esn0_db = np.mean([dvbs2_frame.esn0 for dvbs2_frame in dvbs2_frames])
    per = np.sum([not dvbs2_frame.passed_fec for dvbs2_frame in dvbs2_frames]) / len(dvbs2_frames)
    per = np.sum([not dvbs2_frame.bbheader.passed_crc for dvbs2_frame in dvbs2_frames]) / len(dvbs2_frames)
    passed_fec = [dvbs2_frame.passed_fec for dvbs2_frame in dvbs2_frames]
    passed_crc = [dvbs2_frame.bbheader.passed_crc for dvbs2_frame in dvbs2_frames]
    esn0 = [dvbs2_frame.esn0 for dvbs2_frame in dvbs2_frames]
    plheader_snr = [dvbs2_frame.plheader_snr for dvbs2_frame in dvbs2_frames]
    test_data[test_data_index] = \
        {'dvbs2_modcod': dvbs2_modcod,
         'threshold_esn0': dvbs2_modcod.get_threshold_esn0(),
         'processed_num_frames': len(dvbs2_frames),
         'simulated_esn0_db': esn0_db,
         'mean_esn0_db': mean_esn0_db,
         'per': per,
         'passed_fec': passed_fec,
         'esn0': esn0,
         'plheader_snr': plheader_snr}


#
# A parallel implementation of manual_test_basic() that runs different test cases in different threads or on
# different cores
#
def manual_test_basic_parallel(num_frames = 100):
    fec_processing_mode = 'zmq_processor'
    zmq_fec_server = 'tcp://localhost:5555'
    margins = np.linspace(-1, 2, 31) # 1/10th dB
    margins = np.linspace(-1, 2, 13)  # 1/4th dB
    # margins = np.linspace(-1, 1, 21)  # 1/10th dB
    # margins = np.linspace(-1, 1, 5)  # 1/10th dB
    # margins = np.linspace(-1, 3, 17) # 1/4th dB
    # margins = np.linspace(-1, 2, 7)  # 1/4th dB
    # margins = np.linspace(-1, 3, 5) # 1 dB
    test_cases = []

    # for frame_length in [DVBS2_Frame_Length.Short_Frames, DVBS2_Frame_Length.Normal_Frames]:
    for frame_length in [DVBS2_Frame_Length.Normal_Frames]:
        for modcod in DVBS2_ModCods:
            # for modcod in [DVBS2_ModCods.DVBS2_QPSK_1_2]:
            if modcod == DVBS2_ModCods.DVBS2_DUMMY_PLFRAME or \
                    modcod == DVBS2_ModCods.DVBS2_RESERVED0 or \
                    modcod == DVBS2_ModCods.DVBS2_RESERVED1 or \
                    modcod == DVBS2_ModCods.DVBS2_RESERVED2:
                continue
            # These MODCODs are not valid for SHORT frames
            if (modcod == DVBS2_ModCods.DVBS2_QPSK_9_10 or
                modcod == DVBS2_ModCods.DVBS2_8PSK_9_10 or
                modcod == DVBS2_ModCods.DVBS2_16APSK_9_10 or
                modcod == DVBS2_ModCods.DVBS2_32APSK_9_10) \
                    and frame_length == DVBS2_Frame_Length.Short_Frames:
                continue
            # These MODCODs are too low Es/N0 for the demod right now (stuff needs to be fixed!)
            if modcod == DVBS2_ModCods.DVBS2_QPSK_1_4 or modcod == DVBS2_ModCods.DVBS2_QPSK_1_3 or \
                    modcod == DVBS2_ModCods.DVBS2_QPSK_2_5:
                continue

            for margin in margins:
                test_cases.append(
                    {'dvbs2_modcod': DVBS2_ModCod_Configuration(modcod=modcod,
                                                                frame_length=frame_length,
                                                                pilots=DVBS2_Pilots.Pilots_On,
                                                                rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20),
                     'margin': margin, 'num_frames': num_frames})
    test_data = [None]*len(test_cases)

    parallel_mode = 'processes'  # processes, threads, or none; processes seems to run fastest
    if parallel_mode == 'processes':
        # If using PyCharm, be very careful putting breakpoints in the following block.  It doesn't deal well with
        # the multiple processes created when using multiprocessing.Manager
        manager = Manager()
        test_data_managed = manager.list(test_data)
        test_case_args = []
        for test_data_index, test_case in enumerate(test_cases):
            test_case_args.append((test_case['dvbs2_modcod'], test_case['margin'], int(test_case['num_frames']),
                                   fec_processing_mode, zmq_fec_server, test_data_managed, test_data_index))
        with Pool(processes=8) as pool:
            pool.starmap(process_test_case_wrapper, test_case_args)
        test_data = list(test_data_managed)
        del manager, test_data_managed, test_case_args
        time.sleep(1.0)  # Give time for the Manager process to die to help PyCharm debugger not barf
    elif parallel_mode == 'threads':
        from joblib import Parallel, delayed
        Parallel(n_jobs=8, require='sharedmem') \
            (delayed(process_test_case_wrapper)
             (dvbs2_modcod=test_case['dvbs2_modcod'],
              margin=test_case['margin'],
              num_frames=test_case['num_frames'],
              fec_processing_mode=fec_processing_mode,
              zmq_fec_server=zmq_fec_server,
              test_data=test_data,
              test_data_index=test_data_index)
             for test_data_index, test_case in enumerate(test_cases))
    else:
        for test_data_index, test_case in enumerate(test_cases):
            process_test_case_wrapper(dvbs2_modcod=test_case['dvbs2_modcod'], margin=test_case['margin'],
                                      num_frames=test_case['num_frames'], fec_processing_mode=fec_processing_mode,
                                      zmq_fec_server=zmq_fec_server, test_data=test_data,
                                      test_data_index=test_data_index)

    plot_results(test_data)

    for dvbs2_modcod in set([test_data_['dvbs2_modcod'] for test_data_ in test_data]):
        print(dvbs2_modcod)


def plot_results(test_data):
    """
    A helper function to plot results
    @param test_data: List of test results to plot
    """
    plt.figure()
    for modcod in DVBS2_ModCods:
        test_data_this_modcod = \
            [test_data_ for test_data_ in test_data if test_data_['dvbs2_modcod'].modcod == modcod]
        test_data_this_modcod = \
            np.asarray(test_data_this_modcod)[np.argsort([td['simulated_esn0_db'] for td in test_data_this_modcod])]
        if len(test_data_this_modcod) == 0:
            continue
        dvbs2_modcod = [test_data_['dvbs2_modcod'] for test_data_ in test_data_this_modcod]
        threshold_esn0_db = dvbs2_modcod[0].get_threshold_esn0()
        # simulated_esn0_db = [test_data_['mean_esn0_db'] for test_data_ in test_data_this_modcod]
        simulated_esn0_db = [test_data_['simulated_esn0_db'] for test_data_ in test_data_this_modcod]
        per = [test_data_['per'] for test_data_ in test_data_this_modcod]
        if len(np.where(np.asarray(per) == 0.0)[0]) > 0:
            qef_esno = simulated_esn0_db[np.where(np.asarray(per) == 0.0)[0][0]]
        else:
            qef_esno = -1
        plt.semilogy(simulated_esn0_db, per,
                     label='{} ({:2.2f} dB / {:2.2f} dB)'.format(dvbs2_modcod[0].modcod_pretty(), qef_esno,
                                                                 threshold_esn0_db))
    plt.xlabel('Es/N0 (dB)')
    plt.ylabel('PLFRAME Error Rate')
    plt.legend()
    plt.xlim(0.75, 21)
    plt.title('DVBS2 PER Performance for Normal Frames')

    plt.figure()
    plt.plot([test_data_['simulated_esn0_db'] for test_data_ in test_data],
             [test_data_['mean_esn0_db'] - test_data_['simulated_esn0_db'] for test_data_ in test_data], '.')


if __name__ == '__main__':
    #manual_test_basic()
    manual_test_basic_parallel(num_frames=100)
