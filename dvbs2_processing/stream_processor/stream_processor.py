import os
import logging
import logging.config
import json
from copy import deepcopy
import time
from time import sleep
from time import perf_counter
from typing import List

import numpy as np
from scipy import signal

from multiprocessing import Value
import multiprocessing
from mpipe import OrderedStage, Pipeline

import matplotlib.pyplot as plt
import matplotlib

from digital_demod.carrier_recovery.carrier_frequency_recovery import carrier_frequency_shift
from digital_demod.measurements.freq_domain_measurements import basic_freq_domain_analysis

from dsp_utilities.plotting.scatterplot import scatterplot
from dsp_utilities.plotting.iq_plot import iq_plot
from dvbs2_processing.utilities.dvbs2_utilities import plot_constellation
from dsp_utilities.file_handlers.iq_file_store import IQFileStore, IQFileType

from dvbs2_processing.symbol_processor.clock_recovery import clock_recovery_and_resample, \
    clock_recovery_and_generate_symbols
from dvbs2_processing.plframe_processor import DVBS2PLFrameProcessor
from dvbs2_processing.symbol_processor import DVBS2SymbolProcessor
from dvbs2_processing.bbframe_processor import DVBS2BBFrameProcessor
from dvbs2_processing.utilities import send_via_udp, compute_plframe_statistics, get_package_root
from dvbs2_processing.signal_utilities.interleaving import deinterleave_plframe
from dvbs2_processing.signal_utilities.modulation import demodulate_plframe
from dvbs2_processing.data_structures.data_structures import DVBS2_BBHEADER
from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration, DVBS2_Frame_Length, DVBS2_Pilots, \
    DVBS2_Modulation_Type, DVBS2_ModCods, DVBS2_MATYPE1_Rolloff, DVBS2_Code_Rates

from dvbs2_processing.constant_data import DVBS2_ModCods, DVBS2_Frame_Length, DVBS2_Pilots, \
    DVBS2_Modulation_Type, DVBS2_Code_Rates, DVBS2_MATYPE1_TS_GS, DVBS2_MATYPE1_SIS_MIS, \
    DVBS2_MATYPE1_CCM_ACM, DVBS2_MATYPE1_ISSYI, DVBS2_MATYPE1_Rolloff, DVBS2_MATYPE1_NPD, \
    DVBS2_ModCod_Configuration

# matplotlib.pyplot.switch_backend('Qt5Agg')
plt.style.use('dark_background')

num_blocks_iq_read = Value('l', -1)
num_blocks_iq_processed = Value('l', -1)


class DVBS2StreamProcessor:
    """
    Functionality to detect and output PLFRAMEs from coarsely synchronized symbols

    Assumptions:
     * Symbols are coarsely synced in clock and carrier frequency (exactly what that means: TBD)

    """
    def __init__(self,
                 dvbs2_frame_length: List[DVBS2_Frame_Length] = None,
                 dvbs2_pilots: List[DVBS2_Pilots] = None,
                 dvbs2_modulation_type: List[DVBS2_Modulation_Type] = None,
                 dvbs2_code_rates: List[DVBS2_Code_Rates] = None,
                 fec_processing_mode: str = 'none',
                 threading_config: int = None,
                 zmq_fec_server: str = None):
        """
        Constructor for DVBS2StreamProcessor object
        :param dvbs2_frame_length: list of DVBS2_Frame_Length values that the processor can support
        :type dvbs2_frame_length: list[DVBS2_Frame_Length]
        :param dvbs2_pilots: list of DVBS2_Pilots values that the processor can support
        :type dvbs2_pilots: list[DVBS2_Pilots]
        :param dvbs2_modulation_type: list of DVBS2_Modulation_Type values that the processor can support
        :type dvbs2_modulation_type: list[DVBS2_Modulation_Type]
        :param fec_processing_mode: Indicates FEC mode
        :type fec_processing_mode: str
        :param threading_config: Configuration value for threading (0 = no multithreading, 1,2,3,16 = multithreading)
        :type threading_config: int
        :param zmq_fec_server: URL for ZMQ FEC server (if using this mode)
        :type zmq_fec_server: str
        """

        self.set_logger()

        self.dvbs2_symbol_processor = DVBS2SymbolProcessor()
        self.save_all_data = False
        self.iq_file_store = None
        self.fs_hz = None

        self.dvbs2_plframe_processor = DVBS2PLFrameProcessor(dvbs2_frame_length=dvbs2_frame_length,
                                                             dvbs2_pilots=dvbs2_pilots,
                                                             dvbs2_modulation_type=dvbs2_modulation_type,
                                                             dvbs2_code_rates=dvbs2_code_rates)

        self.dvbs2_bbframe_processor = DVBS2BBFrameProcessor(dvbs2_frame_length=dvbs2_frame_length,
                                                             dvbs2_pilots=dvbs2_pilots,
                                                             dvbs2_modulation_type=dvbs2_modulation_type,
                                                             dvbs2_code_rates=dvbs2_code_rates,
                                                             fec_processing_mode=fec_processing_mode,
                                                             threading_config=threading_config,
                                                             zmq_fec_server=zmq_fec_server)

        self.threading_config = threading_config
        if self.threading_config == 0:
            self.num_fec_banks = 1
            self.num_processing_threads = 1
        elif self.threading_config == 1:
            self.num_fec_banks = 2
            self.num_processing_threads = 2
        elif self.threading_config == 2:
            self.num_fec_banks = 4
            self.num_processing_threads = 2
        elif self.threading_config == 3:
            self.num_fec_banks = 8
            self.num_processing_threads = 8
        elif self.threading_config == 16:
            self.num_fec_banks = 16
            self.num_processing_threads = 16
        else:
            raise Exception('Unhandled threading_config')


    def set_logger(self):
        """
        Sets up logger
        @return:
        """
        self.logger = logging.getLogger("stream_processor")
        if len(self.logger.root.handlers) == 0:  # Configure the logger if we have not already
            log_config_filename = os.path.join(get_package_root(), "logging.json")
            with open(log_config_filename, "rt") as fp:
                config = json.load(fp)
            logging.config.dictConfig(config)


    def initialize_file_store(self, filename, file_type=IQFileType.BLUEFILE, iq_data_format=None, fs_hz=None,
                              t_start_sec=None):
        """
        Initializes the IQ file store and updates the sampling rate
        :param filename: Filename to read from
        :type filename: str
        :param file_type: File type (IQFileType enum)
        :type file_type: IQFileType
        :param iq_data_format: IQ data format (IQDataFormat enum)
        :type iq_data_format: IQDataFormat
        :param fs_hz: sample rate in hz (samples/sec)
        :type fs_hz: float
        :param t_start_sec: Start time of IQ data
        :type t_start_sec: float
        :return: void
        """
        self.iq_file_store = IQFileStore(filename=filename, file_type=file_type, iq_data_format=iq_data_format,
                                         fs_hz=fs_hz, t_start_sec=t_start_sec)
        self.fs_hz = self.iq_file_store.fs_hz


    def get_iq_data(self, t_start_sec, num_samples_to_read):
        """
        Reads specified IQ data from self.iq_file_store
        :param t_start_sec: Desired start time
        :param num_samples_to_read: Number of samples to read
        :return:
        """
        last_block = False
        if t_start_sec > self.iq_file_store.t_end_sec:  # Trying to start reading after end of file
            last_block = True
            iq = []
            return iq, last_block
        if t_start_sec + num_samples_to_read / self.fs_hz > self.iq_file_store.t_end_sec:
            last_block = True
            num_samples_remaining_in_file = np.int(
                np.floor((self.iq_file_store.t_end_sec - t_start_sec) * self.fs_hz) - 10)
            t_start_actual_sec, t_end_actual_sec = \
                self.iq_file_store.read_file(t_start_sec=t_start_sec, num_samples=num_samples_remaining_in_file)
        else:
            t_start_actual_sec, t_end_actual_sec = \
                self.iq_file_store.read_file(t_start_sec=t_start_sec, num_samples=num_samples_to_read)
        iq = deepcopy(self.iq_file_store.iq_file.iq)
        self.iq_file_store.iq_file.generate_t_vector(t_start_sec=t_start_actual_sec)
        t_iq_sec = self.iq_file_store.iq_file.t_iq_sec
        return iq, t_iq_sec, last_block


    def perform_initial_sync(self, fc_estimate_hz: float = 0.0, fsym_estimate_hz: float = None,
                             t_start_sec: float = 0.0, prefilter_iq_data: bool = False,
                             num_consecutive_plframes: int = 10) -> dict:
        """
        Performs an initial synchronization on unprocessed IQ data.  Finds and returns first PLFRAME that passes FEC.
        :param fc_estimate_hz: Center frequency estimate in Hz; If provided this will be applied to IQ data before
                               processing
        :type fc_estimate_hz: float
        :param fsym_estimate_hz: Symbol rate estimate in symbols/sec; If not provided, the symbol rate will be
                                 estimated based on the PSD of the IQ data, which can yield unpredictable results
        :type fsym_estimate_hz: float
        :param t_start_sec: Start time in seconds
        :type t_start_sec: float
        :param num_consecutive_plframes: Number of consecutive PLFrames that have to pass FEC before exiting
        :type num_consecutive_plframes: int

        :return:
        dict: initial_sync_results - A dict containing initial sync results

        Notes:
            * This method operates on uncorrected iq data held in self.iq_file_store.iq_file.iq to start looking for
              the first PLFRAME after t_start_sec.  If fc_estimate_hz, IQ data will be corrected before being
              processed.
        """
        debug_plots = False

        #
        # If we're not given a symbol rate estimate, try to guess one from the spectrum - This frequently doesn't
        # work well, so it's better to provide the estimate.
        #
        num_samples = np.min([100000, self.iq_file_store.iq_file.num_samples])
        self.iq_file_store.read_file(t_start_sec=self.iq_file_store.iq_file.t_start_sec, num_samples=num_samples)
        if fsym_estimate_hz is None:
            center_frequency_hz, bandwidth_hz, cn_db = \
                basic_freq_domain_analysis(iq=self.iq_file_store.iq_file.iq, fs_hz=self.fs_hz, usable_ibw_fraction=0.95,
                                           fraction_power_carrier=0.95, fraction_power_noise=0.05,
                                           debug_plots=False)
            fsym_estimate_hz = bandwidth_hz / 1.3
        fsym_hz = None

        # Determine the minimum number of samples required to detect num_consecutive_plframes PLFRAMES.  nWe use the
        # list in self.dvbs2_plframe_processor.dvbs2_pl_header_data of valid ModCods to determine the max number of
        # symbols in a single PLFRAME.  This list is based on thedvbs2_frame_length, dvbs2_pilots, and
        # dvbs2_code_rates parameters that are passed into the constructor of this object
        min_num_symbols = num_consecutive_plframes * self.dvbs2_plframe_processor.min_symbols_for_plframe_detection()
        min_num_samples = num_consecutive_plframes * \
                          np.int(self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() *
                                 (self.iq_file_store.fs_hz / fsym_estimate_hz))

        dvbs2_frames = []
        while len(dvbs2_frames) < num_consecutive_plframes:
            #
            # Read in minimum number of symbols - Get an IQ block that has enough samples to contain the specified
            # number of consecutive PLFRAMES.
            #
            t_start_sec = np.max([self.iq_file_store.iq_file.t_start_sec, t_start_sec - np.ceil(self.fs_hz/fsym_estimate_hz) / self.fs_hz])
            num_samples = np.min([min_num_samples+4, self.iq_file_store.iq_file.num_samples])
            self.iq_file_store.read_file(t_start_sec=t_start_sec, num_samples=num_samples)
            self.dvbs2_symbol_processor.iq = self.iq_file_store.iq_file.iq
            # TODO: Fix this.  Later only used in carrier_recovery_mfd, where the vector is changed
            iq = deepcopy(self.iq_file_store.iq_file.iq)
            self.iq_file_store.iq_file.generate_t_vector()

            #
            # Adjust the center frequency if we're provided with a non-zero estimate
            #
            if fc_estimate_hz != 0.0:
                initial_carrier_adjustment_hz = -1.0 * (fc_estimate_hz - self.iq_file_store.iq_file.center_frequency_hz)
                iq = carrier_frequency_shift(
                    iq=iq, frequency_hz=initial_carrier_adjustment_hz, t_sec=self.iq_file_store.iq_file.t_iq_sec)
            else:
                initial_carrier_adjustment_hz = 0.0
            if prefilter_iq_data and fsym_estimate_hz is not None and self.fs_hz / fsym_estimate_hz > 2:
                b = signal.firwin(numtaps=1291, cutoff=(1.5*fsym_estimate_hz)/self.fs_hz, window='hamming')
                iq = signal.filtfilt(b, [1], iq)

            #
            # Estimate the symbol rate.  Oversample by 2 to make sure we don't get aliasing.
            #
            fsym_estimate_hz, _, _, _ = \
                clock_recovery_and_resample(iq=iq, fs_hz=self.fs_hz,
                                            fsym_initial_estimate_hz=fsym_estimate_hz, t_start_sec=t_start_sec,
                                            oversample_by_2=True)

            #
            # ------------------------------------- Clock detect and resample -------------------------------------
            #
            fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec = \
                clock_recovery_and_resample(iq=iq, fs_hz=self.fs_hz,
                                            fsym_initial_estimate_hz=fsym_estimate_hz, t_start_sec=t_start_sec,
                                            oversample_by_2=False)

            #
            # ---------------------------------------- Extract symbols --------------------------------------------
            #
            # Design the RRC filter using assumptions about filter's roll-off
            symbols, t_symbols_sec, iq_resampled_rrc_filtered, oversample_factor = \
                self.dvbs2_symbol_processor.generate_symbols(iq_resampled, t_iq_resampled_sec,
                                                             fs_resampled_hz, fsym_hz,
                                                             self.dvbs2_symbol_processor.rrc_beta)

            #
            # ------------------------------------------ PL Header Search ------------------------------------------
            #
            next_symbol_index, dvbs2_frame = \
                self.dvbs2_plframe_processor.plheader_initial_acq(symbols=symbols, fsym_hz=fsym_hz,
                                                                  current_symbol_index=0,
                                                                  search_for_pl_headers=True,
                                                                  max_num_plsc_symbol_errors=12,
                                                                  max_num_sof_symbol_errors=8)

            if dvbs2_frame is None:
                if False:
                    next_symbol_index, dvbs2_frame = \
                        self.dvbs2_plframe_processor.plheader_initial_acq(symbols=symbols, fsym_hz=fsym_hz,
                                                                          current_symbol_index=0,
                                                                          search_for_pl_headers=True,
                                                                          max_num_plsc_symbol_errors=12,
                                                                          max_num_sof_symbol_errors=8)
                t_start_sec += 10000 / fsym_hz
                continue
            initial_frequency_offset_hz = np.sum(-1*np.asarray(dvbs2_frame.frequency_adjustments_hz))

            # If we get to this point and don't have a valid next_symbol_index then we were unable to find a PLHEADER
            if next_symbol_index == -1:
                raise Exception('Unable to detect PLFRAME with fsym_hz = {}'.format(fsym_hz))

            symbols_temp = carrier_frequency_shift(iq=symbols, frequency_hz=-1*initial_frequency_offset_hz,
                                              t_sec=t_symbols_sec)
            cfs = self.dvbs2_plframe_processor.coarse_plframe_frequency_recovery(
                symbols_temp, t_symbols_sec, fsym_hz, dvbs2_frame)

            initial_frequency_offset_hz += np.median(cfs)

            #
            # At this point we have detected the first PLHEADER.  If we have to process multiple blocks of IQ then
            # sanity check the symbol rate we just detected (fsym_hz_) with shat was previously detected (fsym_hz)
            # and make sure they don't differ by too much
            #


            #
            # Starting with the first PLHEADER we detected, operate over all IQ data and find subsequent PLHEADERs.
            # This is straight forward because the PLHEADER contains the length of the PLFRAME, and the next PLHEADER
            # begins with the first symbol after the last symbol of the previous PLFRAME.  Note that we are not
            # processing PLFRAMEs at this point, just searching for their PLHEADERS in our IQ stream.  We'll process
            # the full PLFRAMEs later.  If a PLFRAME header is not found at the end of a previous PLFRAME, an exception
            # will be thrown.  Future development should add more robustness to this processing.
            #
            dvbs2_frames_all = \
                self.dvbs2_plframe_processor.find_plframes(
                    symbols=symbols, fsym_hz=fsym_hz, first_plframe_index=dvbs2_frame.start_index,
                    initial_frequency_offset_hz=initial_frequency_offset_hz, suppress_symbol_slip_error=True)

            # TODO: Need some logic here if we fail to detect PLHEADERS in previous step

            #
            # We've found num_consecutive_plframes, and in the course of doing so determine a very coarse frequency
            # offset for each one.  Average these to get an improved estimate and then adjust the IQ data accordingly.
            #
            # TODO: This section of the processing is ripe for improvement.
            coarse_frequency_adjustment_hz = 0.0
            frequency_offsets_hz = []
            metric_snrs = []
            coarse_correction_mode = 'plframe_symbols'
            if coarse_correction_mode == 'plframe_symbols':
                from digital_demod.carrier_recovery.carrier_frequency_recovery import carrier_recovery_mfd
                for dvbs2_frame in dvbs2_frames_all:
                    mod_order = dvbs2_frame.dvbs2_modcod.get_mod_order()
                    plframe_symbols = symbols[dvbs2_frame.start_index:(
                                dvbs2_frame.start_index + dvbs2_frame.dvbs2_modcod.get_num_symbols())]
                    if mod_order == 4:
                        frequency_offset_hz, metric_snr = carrier_recovery_mfd(plframe_symbols, mod_order,
                                                                               fs_hz=fsym_hz, max_offset_hz=fsym_hz / 8)
                    elif mod_order == 8:
                        frequency_offset_hz, metric_snr = carrier_recovery_mfd(plframe_symbols, mod_order,
                                                                               fs_hz=fsym_hz, max_offset_hz=fsym_hz / 8)
                    else:
                        frequency_offset_hz = np.nan
                        metric_snr = np.nan
                    if metric_snr > 8.5:
                        frequency_offsets_hz.append(frequency_offset_hz)
                        metric_snrs.append(metric_snr)
                if len(frequency_offsets_hz) > 0:
                    coarse_frequency_adjustment_hz = -1*np.nanmean(frequency_offsets_hz)
                    symbols = carrier_frequency_shift(iq=symbols, frequency_hz=coarse_frequency_adjustment_hz,
                                                      t_sec=t_symbols_sec)

            if coarse_correction_mode == 'plframe_headers' or len(frequency_offsets_hz) == 0:
                for dvbs2_frame in dvbs2_frames_all:
                    cfs_ = self.dvbs2_plframe_processor.coarse_plframe_frequency_recovery(
                        symbols=symbols, t_symbols_sec=t_symbols_sec, fsym_hz=fsym_hz, dvbs2_frame=dvbs2_frame)
                    for cf in cfs_:
                        frequency_offsets_hz.append(cf)
                coarse_frequency_adjustment_hz = -1*np.median(frequency_offsets_hz)
                symbols = carrier_frequency_shift(iq=symbols, frequency_hz=-1*coarse_frequency_adjustment_hz,
                                                  t_sec=t_symbols_sec)

            for dvbs2_frame in dvbs2_frames_all:
                dvbs2_frame.frequency_adjustments_hz.append(initial_carrier_adjustment_hz)
                dvbs2_frame.frequency_adjustments_hz.append(coarse_frequency_adjustment_hz)

            #
            # In dvbs2_frames_all we have a list of dvbs2_frames, but have not done any corrections or demodulation
            # on them.  Now iterate over this last and perform corrections, demodulation, and user data extraction.
            #
            for dvbs2_frame in dvbs2_frames_all:
                # ---- Get the symbols that correspond to this frame
                num_symbols_this_plframe = dvbs2_frame.dvbs2_modcod.get_num_symbols() + 90
                dvbs2_frame.t_plframe_symbols = \
                    t_symbols_sec[0] + dvbs2_frame.start_index / fsym_hz + \
                    np.linspace(0, num_symbols_this_plframe - 1, num_symbols_this_plframe) / fsym_hz
                dvbs2_frame.plframe_symbols = \
                    symbols[dvbs2_frame.start_index:(dvbs2_frame.start_index + num_symbols_this_plframe)]
                self.dvbs2_plframe_processor.descramble_plframe(dvbs2_frame)

                # ---- Compute and apply fine corrections to this frame
                symbols_, results_dict_plframe_fine = \
                    self.dvbs2_plframe_processor.get_corrected_plframe(
                        symbols=dvbs2_frame.plframe_symbols, t_symbols_start_sec=dvbs2_frame.t_plframe_symbols[0],
                        fsym_hz=fsym_hz, dvbs2_modcod=dvbs2_frame.dvbs2_modcod, mode='fine')
                plframe_symbols = \
                    self.dvbs2_plframe_processor.apply_corrections(
                        results_dict_plframe_fine['cs_amplitude'], results_dict_plframe_fine['cs_phase'],
                        dvbs2_frame.plframe_symbols, dvbs2_frame.t_plframe_symbols)
                dvbs2_frame.plframe_symbols_corrected = plframe_symbols

                # ---- Update some statistics that we'll use later
                dvbs2_frame.frequency_offset_hz = results_dict_plframe_fine['frequency_offset_fine_hz']
                dvbs2_frame.esn0 = compute_plframe_statistics(symbols=plframe_symbols,
                                                              dvbs2_modcod=dvbs2_frame.dvbs2_modcod)
                print(dvbs2_frame)

                # ---- Demodulate and deinterleave the frame
                if dvbs2_frame.dvbs2_modcod.modcod != DVBS2_ModCods.DVBS2_DUMMY_PLFRAME:
                    noise_var = np.var(plframe_symbols[dvbs2_frame.dvbs2_modcod.get_pilot_symbol_indicies()] -
                                       (1 / np.sqrt(2) + 1j / np.sqrt(2)))
                    llr = demodulate_plframe(dvbs2_modcod=dvbs2_frame.dvbs2_modcod,
                                             symbols=plframe_symbols[0:dvbs2_frame.dvbs2_modcod.get_num_symbols()],
                                             noise_var=float(noise_var))
                    llr = deinterleave_plframe(dvbs2_modcod=dvbs2_frame.dvbs2_modcod, plframe_bits=llr)

                    # ---- Optionally perform FEC processing on the frame
                    bits_llr = np.array(llr < 0, dtype=np.int)
                    (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_frame.dvbs2_modcod.get_coding_parameters()
                    if self.dvbs2_bbframe_processor.dvbs2_fec_processor_banks is not None:
                        dvbs2_frame.bits, dvbs2_frame.passed_fec = \
                            self.dvbs2_bbframe_processor.decode_plframe(llr=llr, dvbs2_modcod=dvbs2_frame.dvbs2_modcod)
                        self.logger.info('Corrected {} bit errors (BER = {}'.format(
                            np.sum(dvbs2_frame.bits != bits_llr), np.sum(dvbs2_frame.bits != bits_llr) / k_ldpc))
                    else:
                        dvbs2_frame.bits = bits_llr
                        dvbs2_frame.passed_fec = None

                    # ---- Extract out the BBHEADER so we can get to the roll-off factor and ACM settings
                    dvbs2_pl_header_data = \
                        self.dvbs2_plframe_processor.find_matching_pl_header_data(dvbs2_frame.dvbs2_modcod)
                    scrambling_sequence = dvbs2_pl_header_data['bbframe_scrambling_sequence']
                    bits_descrambled = np.asarray(dvbs2_frame.bits[0:k_bch]) ^ np.asarray(scrambling_sequence)
                    dvbs2_frame.bbheader = DVBS2_BBHEADER(bits_descrambled)
                else:
                    dvbs2_frame.bits = None
                    dvbs2_frame.passed_fec = None
                    dvbs2_frame.bbheader = None

                # ---- Append to the list of processed frames that we're saving
                dvbs2_frames.append(dvbs2_frame)

                # ---- Send via UDP if necessary
                send_udp_packets = False
                if send_udp_packets:
                    send_via_udp(dvbs2_frame.bits[0:k_bch], dvbs2_frame.dvbs2_modcod)

                if len(dvbs2_frames) >= num_consecutive_plframes:
                    break

            if len(dvbs2_frames) >= num_consecutive_plframes:
                break

            # ---- Update the start time for the next block to process - Note that this is largely untested
            t_start_sec += (dvbs2_frame.start_index + dvbs2_frame.dvbs2_modcod.get_num_symbols() - 100) / fsym_hz

        #
        # We're done processing and have successfully found and processed num_consecutive_plframes PLFRAMES
        #

        #
        # Make sure all our frames passed FEC and filter out the ones that don't
        #
        if False:
            if len(set([dvbs2_frame.passed_fec for dvbs2_frame in dvbs2_frames])) != 1:
                print('Warning: One more PLFRAMES did not pass FEC')
                # Filter out PLFRAMES that didn't pass FEC
                dvbs2_frames = \
                    list(np.array(dvbs2_frames)[
                             np.where(np.array([dvbs2_frame.passed_fec for dvbs2_frame in dvbs2_frames]) is True)[0]
                         ])

        #
        # Get carrier operating parameters
        #
        unique_modcods = list(set([dvbs2_frame.dvbs2_modcod.modcod for dvbs2_frame in dvbs2_frames]))
        if len(unique_modcods) == 1 and unique_modcods[0] == DVBS2_ModCods.DVBS2_DUMMY_PLFRAME:
            dvbs2_rolloff = DVBS2_MATYPE1_Rolloff.RO_0_20
            # TODO: Do this right!
        else:
            if len(set([dvbs2_frame.bbheader.ro for dvbs2_frame in dvbs2_frames])) != 1:
                # raise Exception('Expecting exactly one unique roll-off factor')
                print('Warning: Expecting exactly one unique roll-off factor')
                dvbs2_rolloff = max(set([dvbs2_frame.bbheader.ro for dvbs2_frame in dvbs2_frames]),
                                    key=[dvbs2_frame.bbheader.ro for dvbs2_frame in dvbs2_frames].count)
            else:
                dvbs2_rolloff = dvbs2_frames[0].bbheader.ro
        dvbs2_modcods = set([dvbs2_frame.dvbs2_modcod for dvbs2_frame in dvbs2_frames])
        dvbs2_pilots = set([dvbs2_modcod.pilots for dvbs2_modcod in dvbs2_modcods])
        dvbs2_frame_lengths = set([dvbs2_modcod.frame_length for dvbs2_modcod in dvbs2_modcods])
        if len(dvbs2_pilots) != 1:
            # raise Exception('Expecting exactly one unique pilot configuration')
            print('Warning: Expecting exactly one unique pilot configuration')
            dvbs2_pilot = max(set([dvbs2_modcod.pilots for dvbs2_modcod in dvbs2_modcods]),
                              key=[dvbs2_modcod.pilots for dvbs2_modcod in dvbs2_modcods].count)
        else:
            dvbs2_pilot = dvbs2_frames[0].dvbs2_modcod.pilots
        if len(dvbs2_frame_lengths) != 1:
            dvbs2_frame_length = max(set([dvbs2_modcod.frame_length for dvbs2_modcod in dvbs2_modcods]),
                                     key=[dvbs2_modcod.frame_length for dvbs2_modcod in dvbs2_modcods].count)
            raise Exception('Expecting exactly one unique frame length configuration')
        else:
            dvbs2_frame_length = dvbs2_frames[0].dvbs2_modcod.frame_length

        #
        # Update the carrier frequency offset
        #
        frequency_offset_hz = np.mean([np.sum(-1*np.asarray(dvbs2_frame.frequency_adjustments_hz)) +
                                       dvbs2_frame.frequency_offset_hz for dvbs2_frame in dvbs2_frames])

        if debug_plots:
            plot_constellation(dvbs2_frames[3].dvbs2_modcod, dvbs2_frames[3].plframe_symbols_corrected/1.2)
            plot_constellation(dvbs2_frames[-1].dvbs2_modcod, dvbs2_frames[-1].plframe_symbols_corrected)

        #
        # Build and return initial sync results dict
        #
        initial_sync_results = {
            'fs_hz': self.fs_hz,
            'fsym_hz': fsym_hz,
            't_start_sec': dvbs2_frames[0].t_plframe_symbols[0],
            'frequency_offset_hz': frequency_offset_hz,
            'dvbs2_pilots': dvbs2_pilot,
            'dvbs2_frame_length': dvbs2_frame_length,
            'dvbs2_rolloff': dvbs2_rolloff,
            'first_dvbs2_plframe': dvbs2_frames[0],
            'dvbs2_plframes': dvbs2_frames
        }

        return initial_sync_results


    def process_plframes(self, initial_sync_results, block_size_num_samples, num_blocks=1, prefilter_iq_data=False):
        """
        Wrapper around process_iq_block() to call over specific number of blocks and block sizes.
        :param initial_sync_results: dict containing values described in notes below
        :type initial_sync_results: dict
        :param block_size_num_samples: Number of samples per block
        :type block_size_num_samples: int
        :param num_blocks: Number of blocks
        :type num_blocks: int
        :return:

        Notes:
            initial_sync_results = {
                'dvbs2_modcod': results_dict['dvbs2_modcod'],
                't_start_sec': t_start_plframe_sec,
                'current_symbol_index': results_dict['current_symbol_index'],
                'fsym_hz': fsym_hz,
                'clock_phase_offset_rad': clock_phase_offset_rad,
                'fractional_sample_shift': fractional_sample_shift,
                'fs_resampled_hz': fs_resampled_hz,
                'frequency_offset_hz': frequency_offset_hz
            }
        """

        assert type(block_size_num_samples) is int, 'Number of samples must be an int'

        # Set up
        block_data = []
        time_stats = []
        overlap_plframe_blocks = True
        t_start_sec = initial_sync_results['t_start_sec']
        fs_hz = self.iq_file_store.fs_hz
        fsym_hz = initial_sync_results['fsym_hz']
        t_start_sec = np.max([0.0, t_start_sec - np.ceil(fs_hz / fsym_hz) / fs_hz])

        # Iterate over all blocks of IQ data and perform DVB-S2 processing
        block_mode = 'fixed'  # fixed = same number of samples each time; synced = adjusted to line up to plframes
        num_plframes = 0
        for block_num in range(0, num_blocks):
            self.logger.info('block_num = {:05d}    t_start_sec = {}'.format(block_num, t_start_sec))
            t_start_sec, last_block, time_stat = \
                self.process_iq_block(t_start_sec, block_num, block_size_num_samples, block_mode,
                                      overlap_plframe_blocks, initial_sync_results, block_data,
                                      prefilter_iq_data=prefilter_iq_data,
                                      plframe_start_index=num_plframes)
            time_stats.append(time_stat)
            initial_sync_results['frequency_offset_hz'] = block_data[-1]['mean_frequency_offset_hz']
            num_plframes += len(block_data[-1]['dvbs2_frames'])
            if last_block:
                break

        # Timing performance analysis and reporting
        total_time_sec = np.sum([[kvp['elapsed_time'] for kvp in kvp_list] for kvp_list in time_stats])
        stages = [kvp['stage'] for kvp in time_stats[0]]
        for stage in stages:
            elapsed_times_sec = [el[0] for el in
                                 [[kvp['elapsed_time'] for kvp in kvp_list if kvp['stage'] == stage]
                                  for kvp_list in time_stats]]
            percentage = 100 * np.sum(elapsed_times_sec) / total_time_sec
            self.logger.info('{:35s} {:12.03f} ms    {:3.1f}%'.format(stage, 1000*np.sum(elapsed_times_sec), percentage))

        dvbs2_frames = []
        t_start_plframe_symbols = []
        for block in block_data:
            for dvbs2_frame in block['dvbs2_frames']:
                if len(t_start_plframe_symbols) == 0 or \
                        np.min(np.abs(dvbs2_frame.t_plframe_symbols[0] - t_start_plframe_symbols)) >= 100/fsym_hz:
                    dvbs2_frames.append(dvbs2_frame)
                    t_start_plframe_symbols.append(dvbs2_frame.t_plframe_symbols[0])
                # TODO: If we have overlapping PLFRAMES, be smarter about which to keep (e.g. keep the one that passes FEC)

        return block_data, dvbs2_frames


    def process_plframes_parallelized(self, initial_sync_results, block_size_num_samples, num_blocks=None):
        """
        Parallelize multiple DVBS2StreamProcessor.process_iq_block() calls to extract user bits from raw samples
        :param initial_sync_results: Initial sync results
        :type initial_sync_results: dict
        :param block_size_num_samples: Number of samples per block
        :type block_size_num_samples: int
        :param num_blocks: Number of blocks to process; if None this will be calculated based on file length
        :type num_blocks: int
        :return: None (for now)

        initial_sync_results = {
            'dvbs2_modcod': results_dict['dvbs2_modcod'],
            't_start_sec': t_start_plframe_sec,
            'current_symbol_index': results_dict['current_symbol_index'],
            'fsym_hz': fsym_hz,
            'clock_phase_offset_rad': clock_phase_offset_rad,
            'fractional_sample_shift': fractional_sample_shift,
            'fs_resampled_hz': fs_resampled_hz,
            'frequency_offset_hz': frequency_offset_hz
        }
        """
        #
        # Read in minimum number of symbols
        #
        # Without knowledge of symbol rate, assume it is 0.5 x Fs

        # Define worker functions needed by mpipe
        raise Exception("This method needs to be updated to use DVBS2_Frame object")
        debug_print = False

        def process_iq_block_worker(input_dict):
            if debug_print:
                print('Entering get_iq_data_worker()')
            global num_blocks_iq_read
            num_blocks_iq_read.value += 1

            end_of_file_event_ = input_dict['end_of_file_event']
            del input_dict['end_of_file_event']
            input_dict_copy = deepcopy(input_dict)
            t_start_sec = input_dict_copy['t_start_sec']
            block_num = input_dict_copy['block_number']
            block_size_num_samples_ = input_dict_copy['block_size_num_samples']
            block_mode = input_dict_copy['block_mode']
            overlap_plframe_blocks_ = input_dict_copy['overlap_plframe_blocks']
            initial_sync_results_ = input_dict_copy['initial_sync_results']
            block_data = []

            t_start_next_sec, last_block, time_list \
                = self.process_iq_block(t_start_sec, block_num, block_size_num_samples_, block_mode,
                                        overlap_plframe_blocks_, initial_sync_results_, block_data)

            if len(block_data) > 0:
                del block_data[0]['symbols']
                del block_data[0]['t_symbols_sec']
                del block_data[0]['cs_amplitude']
                del block_data[0]['cs_phase']
                for results_dict in block_data[0]['results_dicts']:
                    del results_dict['bits']
                    del results_dict['bits_descrambled']
                    del results_dict['plframe_symbols']
                    del results_dict['plframe_symbols_descrambled']

            if last_block:
                end_of_file_event_.set()

            output_dict = input_dict_copy
            output_dict['t_start_next_sec'] = t_start_next_sec
            output_dict['block_data'] = block_data
            return output_dict

        def print_results_worker(input_dict):
            if debug_print:
                print('                        Entering print_results_worker()')
            global num_blocks_iq_read
            num_blocks_iq_processed.value += 1
            print_block = True
            if print_block:
                if len(input_dict['block_data']) == 0:
                    print('End of file reached: Block {:05d}    T_Start: {}'
                          .format(input_dict['block_number'], input_dict['t_start_sec']))
                else:
                    print('Received block of PLFRAME data: Block {:05d}    T_Start: {}'
                          .format(input_dict['block_number'], input_dict['t_start_sec']))
            print_frames = False
            if print_frames:
                self.print_plframe_results(-1, input_dict['results_dicts_this_block'])

            if debug_print:
                print('                        Leaving print_results_worker()')

        global num_blocks_iq_read, num_blocks_iq_processed
        num_blocks_iq_read.value = 0
        num_blocks_iq_processed.value = 0
        end_of_file_event = multiprocessing.Manager().Event()

        overlap_plframe_blocks = True
        fs_hz_ = self.iq_file_store.fs_hz
        t_start_sec_ = initial_sync_results['t_start_sec']
        fsym_hz_ = initial_sync_results['fsym_hz']
        t_start_sec_ = np.max([0.0, t_start_sec_ - np.ceil(fs_hz_ / fsym_hz_) / fs_hz_])

        if num_blocks is None:
            num_blocks = \
                int(5+np.ceil(self.iq_file_store.num_samples /
                          (block_size_num_samples -
                           fs_hz_ * self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() / fsym_hz_)))

        results_dicts = []

        stages = []
        stages.append(OrderedStage(process_iq_block_worker, 16))
        stages.append(OrderedStage(print_results_worker, 1))
        stages[-2].link(stages[-1])

        pipe = Pipeline(stages[0])

        initial_sync_results['t_start_sec'] = t_start_sec_
        initial_sync_results['block_size_num_samples'] = block_size_num_samples
        initial_sync_results['fsym_hz'] = fsym_hz_
        initial_sync_results['fsym_initial_estimate_hz'] = fsym_hz_
        initial_sync_results['results_dicts'] = results_dicts
        initial_sync_results['rrc_beta'] = self.dvbs2_symbol_processor.rrc_beta

        initial_sync_results['block_size_num_samples'] = block_size_num_samples
        initial_sync_results['block_mode'] = 'fixed'
        initial_sync_results['overlap_plframe_blocks'] = overlap_plframe_blocks
        initial_sync_results['initial_sync_results'] = initial_sync_results

        start = time.time()
        print('Starting main processing loop in_plframes_pipelined()')

        do_parallel = True
        if do_parallel:
            for block_number_ in range(num_blocks):
                initial_sync_results['t_start_sec'] = t_start_sec_
                initial_sync_results['block_number'] = block_number_
                initial_sync_results['end_of_file_event'] = end_of_file_event
                print('Putting block_number_ {}'.format(block_number_))
                pipe.put(initial_sync_results)
                t_start_sec_ += block_size_num_samples / fs_hz_ - \
                                self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() / fsym_hz_
                if end_of_file_event.is_set():
                    break
                # pipe.get(timeout=None)
            pipe.put(None)
            print('--------------------------------------------------------------------------------------------------')
            print('--------------------------------------------------------------------------------------------------')
            print('--------------------------------- Waiting for Pipes to Clear -------------------------------------')
            print('--------------------------------------------------------------------------------------------------')
            print('--------------------------------------------------------------------------------------------------')
            while num_blocks_iq_read.value > num_blocks_iq_processed.value:
                sleep(1.0)
        else:
            for block_number_ in range(100):
                initial_sync_results['t_start_sec'] = t_start_sec_
                initial_sync_results['block_number'] = block_number_
                worker_dict = process_iq_block_worker(initial_sync_results)
                print_results_worker(worker_dict)
                t_start_sec_ += block_size_num_samples / fs_hz_ - \
                                self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() / fsym_hz_

        end = time.time()
        elapsed = end - start
        print('Elapsed time in process_plframes_pipelined(): {}'.format(elapsed))

        carrier_process_data = None

        return carrier_process_data


    def process_plframes_pipelined(self, initial_sync_results, block_size_num_samples, num_blocks=1):
        """
        Processes IQ data using Python multiprocesing and multiple pipelined stages that can run in parallel
        :param initial_sync_results: Initial sync results
        :type initial_sync_results: dict
        :param block_size_num_samples: Number of samples per block
        :type block_size_num_samples: int
        :param num_blocks: Number of blocks to process; if None this will be calculated based on file length
        :type num_blocks: int
        :return: None (for now)

         initial_sync_results = {
            'dvbs2_modcod': results_dict['dvbs2_modcod'],
            't_start_sec': t_start_plframe_sec,
            'current_symbol_index': results_dict['current_symbol_index'],
            'fsym_hz': fsym_hz,
            'clock_phase_offset_rad': clock_phase_offset_rad,
            'fractional_sample_shift': fractional_sample_shift,
            'fs_resampled_hz': fs_resampled_hz,
            'frequency_offset_hz': frequency_offset_hz
        }
        """
        #
        # Read in minimum number of symbols
        #
        # Without knowledge of symbol rate, assume it is 0.5 x Fs

        # Define worker functions needed by mpipe
        raise Exception("This method needs to be updated to use DVBS2_Frame object")
        debug_print = False

        def get_iq_data_worker(input_dict):
            if debug_print:
                print('Entering get_iq_data_worker()')
            input_dict_copy = deepcopy(input_dict)
            t_start_sec = input_dict_copy['t_start_sec']
            block_size_num_samples_ = input_dict_copy['block_size_num_samples']
            results_dicts_ = input_dict_copy['results_dicts']
            raise Exception('Need to update IQ data handling since this was moved out of symbol processor')
            iq, last_block = \
                self.dvbs2_symbol_processor.get_iq_data(t_start_sec, block_size_num_samples_, results_dicts_)
            if last_block:
                return None
            global num_blocks_iq_read
            num_blocks_iq_read.value += 1
            output_dict = input_dict_copy
            output_dict['iq'] = iq
            output_dict['last_block'] = last_block
            if debug_print:
                print('Leaving get_iq_data_worker()')
            return output_dict

        def coarse_carrier_frequency_correction_worker(input_dict):
            if debug_print:
                print('    Entering coarse_carrier_frequency_correction_worker()')
            input_dict_copy = deepcopy(input_dict)
            iq = input_dict_copy['iq']
            if len(iq) == 0:
                frequency_offset_hz = None
                fs_hz = None
            else:
                frequency_offset_hz = input_dict_copy['frequency_offset_hz']
                fs_hz = input_dict_copy['fs_hz']
                iq = carrier_frequency_shift(iq=iq, frequency_hz=-1*initial_sync_results['frequency_offset_hz'],
                                             fs_hz=self.fs_hz)
            output_dict = input_dict_copy
            output_dict['iq'] = iq
            if debug_print:
                print('    Leaving coarse_carrier_frequency_correction_worker()')
            return output_dict

        def get_iq_data_and_apply_frequency_correction(input_dict):
            if debug_print:
                print('Entering get_iq_data_worker()')
            input_dict_copy = deepcopy(input_dict)
            t_start_sec = input_dict_copy['t_start_sec']
            block_size_num_samples_ = input_dict_copy['block_size_num_samples']
            results_dicts_ = input_dict_copy['results_dicts']
            iq, last_block = \
                self.dvbs2_symbol_processor.get_iq_data(t_start_sec, block_size_num_samples_, results_dicts_)
            iq = carrier_frequency_shift(iq=iq, frequency_hz=-1 * initial_sync_results['frequency_offset_hz'],
                                         fs_hz=self.fs_hz)
            if last_block:
                return None
            global num_blocks_iq_read
            num_blocks_iq_read.value += 1
            output_dict = input_dict_copy
            output_dict['iq'] = iq
            output_dict['last_block'] = last_block
            if debug_print:
                print('Leaving get_iq_data_worker()')
            return output_dict

        def clock_recovery_and_resample_worker(input_dict):
            if debug_print:
                print('        Entering clock_recovery_and_resample_worker()')
            # start_time = perf_counter()
            input_dict_copy = deepcopy(input_dict)
            iq = input_dict_copy['iq']
            if len(iq) == 0:
                fsym_hz = None
                fs_resampled_hz = []
                iq_resampled = []
                t_iq_resampled_sec = []
            else:
                fs_hz = input_dict_copy['fs_hz']
                fsym_initial_estimate_hz = input_dict_copy['fsym_initial_estimate_hz']
                t_start_sec = input_dict_copy['t_start_sec']
                fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec = \
                    clock_recovery_and_resample(iq, fs_hz, fsym_initial_estimate_hz, t_start_sec)
            output_dict = input_dict_copy
            output_dict['fsym_hz'] = fsym_hz
            output_dict['fs_resampled_hz'] = fs_resampled_hz
            output_dict['iq_resampled'] = iq_resampled
            output_dict['t_iq_resampled_sec'] = t_iq_resampled_sec
            if not self.save_all_data:
                del output_dict['iq']
            # elapsed_time = perf_counter() - start_time
            # print('Time in clock_recovery_and_resample_worker(): {}'.format(elapsed_time))

            if debug_print:
                print('        Leaving clock_recovery_and_resample_worker()')
            return output_dict

        def generate_symbols_worker(input_dict):
            if debug_print:
                print('            Entering generate_symbols_worker()')
            input_dict_copy = deepcopy(input_dict)
            iq_resampled = input_dict_copy['iq_resampled']
            if len(iq_resampled) == 0:
                symbols = []
                t_symbols_sec = []
                iq_resampled_rrc_filtered = []
                oversample_factor = None
            else:
                t_iq_resampled_sec = input_dict_copy['t_iq_resampled_sec']
                fs_resampled_hz = input_dict_copy['fs_resampled_hz']
                fsym_hz = input_dict_copy['fsym_hz']
                rrc_beta = input_dict_copy['rrc_beta']
                symbols, t_symbols_sec, iq_resampled_rrc_filtered, oversample_factor = \
                    self.dvbs2_symbol_processor.generate_symbols(iq_resampled, t_iq_resampled_sec,
                                                                 fs_resampled_hz, fsym_hz, rrc_beta)
            output_dict = input_dict_copy
            output_dict['symbols'] = symbols
            output_dict['t_symbols_sec'] = t_symbols_sec
            output_dict['oversample_factor'] = oversample_factor
            if self.save_all_data:
                output_dict['iq_resampled_rrc_filtered'] = iq_resampled_rrc_filtered
            else:
                output_dict['iq_resampled_rrc_filtered'] = None
                output_dict['iq_resampled'] = None
                output_dict['t_iq_resampled_sec'] = None

            # print(symbols)
            if debug_print:
                print('            Leaving generate_symbols_worker()')
            return output_dict

        def clock_recovery_and_generate_symbols_worker(input_dict):
            if debug_print:
                print('        Entering clock_recovery_and_generate_symbols_worker()')
            # start_time = perf_counter()
            input_dict_copy = deepcopy(input_dict)
            iq = input_dict_copy['iq']
            if len(iq) == 0:
                fsym_hz = None
                fs_resampled_hz = []
                iq_resampled = []
                t_iq_resampled_sec = []
                symbols = []
                t_symbols_sec = []
                oversample_factor = None
            else:
                fs_hz = input_dict_copy['fs_hz']
                fsym_initial_estimate_hz = input_dict_copy['fsym_initial_estimate_hz']
                t_start_sec = input_dict_copy['t_start_sec']
                rrc_beta = input_dict_copy['rrc_beta']

                fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec, symbols, t_symbols_sec, oversample_factor \
                    = clock_recovery_and_generate_symbols(iq, fs_hz, fsym_initial_estimate_hz, t_start_sec, rrc_beta)

            output_dict = input_dict_copy
            output_dict['fsym_hz'] = fsym_hz
            output_dict['fs_resampled_hz'] = fs_resampled_hz

            if not self.save_all_data:
                del output_dict['iq']
            output_dict = input_dict_copy
            output_dict['symbols'] = symbols
            output_dict['t_symbols_sec'] = t_symbols_sec
            output_dict['oversample_factor'] = oversample_factor
            if self.save_all_data:
                output_dict['iq_resampled'] = iq_resampled
                output_dict['t_iq_resampled_sec'] = t_iq_resampled_sec
            else:
                output_dict['iq_resampled'] = None
                output_dict['t_iq_resampled_sec'] = None
            # print(symbols)
            if debug_print:
                print('            Leaving clock_recovery_and_generate_symbols_worker()')
            return output_dict

        def coarse_carrier_recovery_worker(input_dict):
            if debug_print:
                print('      Entering coarse_carrier_recovery_worker()')
            input_dict_copy = deepcopy(input_dict)
            iq = input_dict_copy['iq']
            if len(iq) == 0:
                iq_resampled = []
                symbols = []
            else:
                fs_hz = input_dict_copy['fs_hz']
                iq_resampled = input_dict_copy['iq_resampled']
                fs_resampled_hz = input_dict_copy['fs_resampled_hz']
                symbols = input_dict_copy['symbols']
                # print(symbols)
                fsym_hz = input_dict_copy['fsym_hz']
                iq, iq_resampled, symbols = \
                    self.dvbs2_symbol_processor.coarse_carrier_recovery(iq, fs_hz, iq_resampled,
                                                                        fs_resampled_hz, symbols, fsym_hz)
            output_dict = input_dict_copy
            output_dict['iq'] = iq
            output_dict['iq_resampled'] = iq_resampled
            output_dict['symbols'] = symbols
            if debug_print:
                print('      Leaving coarse_carrier_recovery_worker()')
            return output_dict

        def perform_symbol_corrections_worker(input_dict):
            if debug_print:
                print('                Entering perform_symbol_corrections_worker()')
            input_dict_copy = deepcopy(input_dict)
            symbols = input_dict_copy['symbols']
            if len(symbols) == 0:
                results_dicts_this_block = []
                cs_amplitude = None
                cs_phase = None
                iq_resampled = []
                symbols_corrected = []
                t_iq_resampled_sec = []
                t_symbols_sec = []
            else:
                fs_hz = input_dict_copy['fs_hz']
                fsym_hz = input_dict_copy['fsym_hz']
                t_symbols_sec = input_dict_copy['t_symbols_sec']
                iq_resampled = input_dict_copy['iq_resampled']
                fs_resampled_hz = input_dict_copy['fs_resampled_hz']
                t_iq_resampled_sec = input_dict_copy['t_iq_resampled_sec']
                block_number = input_dict_copy['block_number']
                results_dicts_this_block, first_plframe_symbol_index, frequency_offset_hz, carrier_phase_rad, \
                cs_amplitude, cs_phase, iq_resampled, t_iq_resampled_sec, symbols_corrected, t_symbols_sec = \
                    self.dvbs2_plframe_processor.perform_symbol_corrections(symbols, fsym_hz, t_symbols_sec,
                                                                            iq_resampled, fs_resampled_hz,
                                                                            t_iq_resampled_sec, block_number)
                symbols = symbols[first_plframe_symbol_index::]
            output_dict = input_dict_copy
            output_dict['results_dicts_this_block'] = results_dicts_this_block
            output_dict['symbols_corrected'] = symbols_corrected
            output_dict['t_symbols_sec'] = t_symbols_sec
            if self.save_all_data:
                output_dict['symbols'] = symbols
                output_dict['iq_resampled'] = iq_resampled
                output_dict['t_iq_resampled_sec'] = t_iq_resampled_sec
                output_dict['cs_amplitude'] = cs_amplitude
                output_dict['cs_phase'] = cs_phase
            else:
                output_dict['symbols'] = None
                output_dict['iq_resampled'] = None
                output_dict['t_iq_resampled_sec'] = None
                output_dict['cs_amplitude'] = None
                output_dict['cs_phase'] = None

            if debug_print:
                print('                Leaving perform_symbol_corrections_worker()')
            return output_dict

        def demod_plframes_worker(input_dict):
            if debug_print:
                print('                    Entering demod_plframes_worker()')
            input_dict_copy = deepcopy(input_dict)
            results_dicts_this_block = input_dict_copy['results_dicts_this_block']
            if len(results_dicts_this_block) == 0:
                results_dicts_this_block = []
            else:
                symbols_corrected = input_dict_copy['symbols_corrected']
                self.dvbs2_bbframe_processor.demod_plframes(results_dicts_this_block, symbols_corrected)
            output_dict = input_dict_copy
            if debug_print:
                print('                    Leaving demod_plframes_worker()')
            return output_dict

        def print_results_worker(input_dict):
            if debug_print:
                print('                        Entering print_results_worker()')
            global num_blocks_iq_read
            num_blocks_iq_processed.value += 1
            print_block = True
            if print_block:
                print('Received block of PLFRAME data: Block {:05d}    T_Start: {}'
                      .format(input_dict['block_number'], input_dict['t_start_sec']))
            print_frames = False
            if print_frames:
                # input_dict_copy = deepcopy(input_dict)
                self.print_plframe_results(-1, input_dict['results_dicts_this_block'])

            if debug_print:
                print('                        Leaving print_results_worker()')

        def passthrough_worker(input_dict):
            if debug_print:
                print('        Entering passthrough_worker()')
            input_dict_copy = deepcopy(input_dict)
            output_dict = input_dict_copy
            if debug_print:
                print('        Leaving passthrough_worker()')
            return output_dict

        global num_blocks_iq_read, num_blocks_iq_processed
        num_blocks_iq_read.value = 0
        num_blocks_iq_processed.value = 0

        overlap_plframe_blocks = True
        fs_hz_ = self.iq_file_store.fs_hz
        t_start_sec_ = initial_sync_results['t_start_sec']
        fsym_hz_ = initial_sync_results['fsym_hz']
        t_start_sec_ = np.max([0.0, t_start_sec_ - np.ceil(fs_hz_ / fsym_hz_) / fs_hz_])

        results_dicts = []

        stages = []
        # stages.append(OrderedStage(get_iq_data_worker, 1))
        # stages.append(OrderedStage(coarse_carrier_frequency_correction_worker, 2))
        stages.append(OrderedStage(get_iq_data_and_apply_frequency_correction, 2))
        # stages[-2].link(stages[-1])
        stages.append(OrderedStage(clock_recovery_and_generate_symbols_worker, 4))
        stages[-2].link(stages[-1])
        stages.append(OrderedStage(perform_symbol_corrections_worker, 8))
        stages[-2].link(stages[-1])
        stages.append(OrderedStage(demod_plframes_worker, 8))
        stages[-2].link(stages[-1])
        stages.append(OrderedStage(print_results_worker, 1, disable_result=True))
        stages[-2].link(stages[-1])

        pipe = Pipeline(stages[0])

        initial_sync_results['t_start_sec'] = t_start_sec_
        initial_sync_results['block_size_num_samples'] = block_size_num_samples
        initial_sync_results['fsym_hz'] = fsym_hz_
        initial_sync_results['fsym_initial_estimate_hz'] = fsym_hz_
        initial_sync_results['results_dicts'] = results_dicts
        initial_sync_results['rrc_beta'] = self.dvbs2_symbol_processor.rrc_beta

        start = time.time()
        print('Starting main processing loop in_plframes_pipelined()')

        do_parallel = True
        if do_parallel:
            for block_number_ in range(num_blocks):
                initial_sync_results['t_start_sec'] = t_start_sec_
                initial_sync_results['block_number'] = block_number_
                pipe.put(initial_sync_results)
                t_start_sec_ += block_size_num_samples / fs_hz_ - \
                                self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() / fsym_hz_
                # pipe.get(timeout=None)
            pipe.put(None)
            print('--------------------------------------------------------------------------------------------------')
            print('--------------------------------------------------------------------------------------------------')
            print('--------------------------------- Waiting for Pipes to Clear -------------------------------------')
            print('--------------------------------------------------------------------------------------------------')
            print('--------------------------------------------------------------------------------------------------')
            while num_blocks_iq_read.value > num_blocks_iq_processed.value:
                sleep(1.0)
        else:
            for block_number_ in range(100):
                initial_sync_results['t_start_sec'] = t_start_sec_
                initial_sync_results['block_number'] = block_number_
                worker_dict = get_iq_data_worker(initial_sync_results)
                worker_dict = coarse_carrier_frequency_correction_worker(worker_dict)
                worker_dict = clock_recovery_and_resample_worker(worker_dict)
                worker_dict = generate_symbols_worker(worker_dict)
                # #worker_dict = coarse_carrier_recovery_worker(worker_dict)
                # worker_dict = perform_symbol_corrections_worker(worker_dict)
                # worker_dict = demod_plframes_worker(worker_dict)
                print_results_worker(worker_dict)
                t_start_sec_ += block_size_num_samples / fs_hz_ - \
                                self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() / fsym_hz_

        end = time.time()
        elapsed = end - start
        print('Elapsed time in process_plframes_pipelined(): {}'.format(elapsed))

        carrier_process_data = None

        return carrier_process_data


    def process_iq_block(self, t_start_sec, block_num, block_size_num_samples, block_mode, overlap_plframe_blocks,
                         initial_sync_results, block_data, prefilter_iq_data=False, plframe_start_index=0):
        """
        Processes a single block of IQ to go from raw samples to user bits
        :param t_start_sec: Start time of block
        :type t_start_sec: float
        :param block_num: Block number (basically an index)
        :type block_num: int
        :param block_size_num_samples: Number of samples per block
        :type block_size_num_samples: int
        :param block_mode: fixed = same number of samples each time; synced = adjusted to line up to plframes
        :type block_mode: str
        :param overlap_plframe_blocks: Overlap with the last PLFRAME if block_mode == synched (ignored otherwise)
        :type overlap_plframe_blocks: bool
        :param initial_sync_results: Initial sync results (so that we don't have to do coarse sync)
        :type initial_sync_results: dict
        :param block_data: List of dicts containing block data elements
        :type block_data: list[dict]
        :return: (t_start_next_sec, last_block, time_list)

        Notes:
        block_mode settings and overlap_plframe_blocks settings:
         * block_mode == fixed:   t_start_next_sec is advanced by block_size_num_samples minus the minimum number of
                                  samples to detect a PLFRAME (DVBS2StreamProcessor.min_symbols_for_plframe_detection())
         * block_mode == synced:
            * overlap_plframe_blocks === True:  t_start_next_sec is advanced to align with the beginning of last PLFRAME
                                                in the block (so that PLFRAME will be processed AGAIN in the next block)
            * overlap_plframe_blocks === False: t_start_next_sec is advanced to align with the beginning of last PLFRAME
                                                in the block (so that PLFRAME will be processed AGAIN in the next block)
        """

        print_debug_info = False
        if print_debug_info:
            print('---------------------------------------------------------------------------------------------')
            print(' ------------------------------ T = {} ---------------------------------------'.format(t_start_sec))
            print('---------------------------------------------------------------------------------------------')
        fs_hz = self.iq_file_store.fs_hz
        fsym_initial_estimate_hz = initial_sync_results['fsym_hz']

        time_stats = []
        #
        # Read IQ and make deep copies to local variables
        #
        start_time = perf_counter()
        iq, t_iq_sec, last_block = self.get_iq_data(t_start_sec, block_size_num_samples)
        t_start_sec = t_iq_sec[0]
        elapsed_time = perf_counter() - start_time
        time_stats.append({'stage': 'get_iq_data', 'elapsed_time': elapsed_time})

        if len(iq) < 5000:
            return -1, True, time_stats

        #
        # Coarse carrier frequency adjustment
        #
        start_time = perf_counter()
        frequency_adjustment_hz = -1 * initial_sync_results['frequency_offset_hz']
        iq = carrier_frequency_shift(iq=iq, frequency_hz=frequency_adjustment_hz,
                                     fs_hz=self.fs_hz)
        elapsed_time = perf_counter() - start_time
        time_stats.append({'stage': 'carrier_frequency_shift', 'elapsed_time': elapsed_time})

        #
        # Coarse IQ filtering
        #
        if prefilter_iq_data:
            start_time = perf_counter()
            if self.fs_hz > 1.3*(1+initial_sync_results['dvbs2_rolloff'].value)*initial_sync_results['fsym_hz']:
                from scipy import signal
                b = signal.firwin(numtaps=1291, cutoff=(1.2 * (1+initial_sync_results['dvbs2_rolloff'].value) *
                                                        initial_sync_results['fsym_hz']) / self.fs_hz,
                                  window='hamming')
                iq = signal.filtfilt(b, [1], iq)
            elapsed_time = perf_counter() - start_time
            time_stats.append({'stage': 'coarse_iq_filtering', 'elapsed_time': elapsed_time})

        #
        # ---------------------------------- Clock detect and extract symbols ----------------------------------
        #
        start_time = perf_counter()
        fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec, symbols, t_symbols_sec, oversample_factor = \
            clock_recovery_and_generate_symbols(iq, fs_hz, fsym_initial_estimate_hz, t_start_sec,
                                                self.dvbs2_plframe_processor.rrc_beta)
        elapsed_time = perf_counter() - start_time
        time_stats.append({'stage': 'clock_recovery_and_generate_symbols', 'elapsed_time': elapsed_time})

        #
        # ----------------------------------- Reduce data we no longer need -----------------------------------
        #
        if self.save_all_data is False:
            iq_resampled = None
            t_iq_resampled_sec = None

        #
        # ------------------------------- Align symbols to start of first PLFRAME -------------------------------
        #
        start_time = perf_counter()
        if False:
            self.dvbs2_plframe_processor.plheader_initial_acq(symbols=symbols, fsym_hz=fsym_hz,
                                                              current_symbol_index=0,
                                                              search_for_pl_headers=True,
                                                              max_num_plsc_symbol_errors=12,
                                                              max_num_sof_symbol_errors=8)
        next_symbol_index, dvbs2_frame = \
            self.dvbs2_plframe_processor.get_next_plframe(symbols=symbols, fsym_hz=fsym_hz,
                                                          current_symbol_index=0, max_num_symbols=-1,
                                                          search_for_pl_headers=True,
                                                          max_num_plsc_symbol_errors=12,
                                                          max_num_sof_symbol_errors=8)
        elapsed_time = perf_counter() - start_time
        time_stats.append({'stage': 'get_next_plframe', 'elapsed_time': elapsed_time})

        if next_symbol_index < 0:
            next_symbol_index, dvbs2_frame = \
                self.dvbs2_plframe_processor.get_next_plframe(symbols=symbols, fsym_hz=fsym_hz,
                                                              current_symbol_index=0, max_num_symbols=-1,
                                                              search_for_pl_headers=True,
                                                              max_num_plsc_symbol_errors=12,
                                                              max_num_sof_symbol_errors=8)
            raise Exception('PLHEADER not found')

        #
        # TODO:
        #  1) Run additional processing to get good carrier sync with the frame we just found
        #  2) Adjust carrier frequency based on these results
        #  3) Recheck symbols before first detected frame to make sure we didn't miss anything
        #


        #
        # Starting with the first PLHEADER we detected, operate over all IQ data and find subsequent PLHEADERs.
        # This is straight forward because the PLHEADER contains the length of the PLFRAME, and the next PLHEADER
        # begins with the first symbol after the last symbol of the previous PLFRAME.  Note that we are not
        # processing PLFRAMEs at this point, just searching for their PLHEADERS in our IQ stream.  We'll process
        # the full PLFRAMEs later.  If a PLFRAME header is not found at the end of a previous PLFRAME, an exception
        # will be thrown.  Future development should add more robustness to this processing.
        #
        start_time = perf_counter()
        dvbs2_frames_all = \
            self.dvbs2_plframe_processor.find_plframes(
                symbols=symbols, fsym_hz=fsym_hz, first_plframe_index=dvbs2_frame.start_index,
                max_num_plframes=None,
                initial_frequency_offset_hz=0.0,
                #initial_frequency_offset_hz=dvbs2_frame.plheader_frequency_offset_hz,
                frequency_adjustments_hz=[frequency_adjustment_hz],
                plframe_start_index=plframe_start_index)
        elapsed_time = perf_counter() - start_time
        time_stats.append({'stage': 'find_plframes', 'elapsed_time': elapsed_time})

        if len(dvbs2_frames_all) == 0:
            dvbs2_frames_all = \
                self.dvbs2_plframe_processor.find_plframes(
                    symbols=symbols, fsym_hz=fsym_hz, first_plframe_index=dvbs2_frame.start_index,
                    max_num_plframes=None,
                    initial_frequency_offset_hz=0.0,
                    # initial_frequency_offset_hz=dvbs2_frame.plheader_frequency_offset_hz,
                    frequency_adjustments_hz=[frequency_adjustment_hz],
                    plframe_start_index=plframe_start_index)


        #
        # -------------------------------- Align symbols to start of first PLFRAME --------------------------------
        #
        start_time = perf_counter()
        results_dicts_this_block, first_plframe_symbol_index, frequency_offset_hz, carrier_phase_rad, cs_amplitude, \
        cs_phase, symbols_corrected, t_symbols_sec = self.dvbs2_plframe_processor.perform_symbol_corrections(
            symbols=symbols, fsym_hz=fsym_hz, t_symbols_sec=t_symbols_sec, dvbs2_frames=dvbs2_frames_all)
        #symbols = symbols[first_plframe_symbol_index::]
        elapsed_time = perf_counter() - start_time
        time_stats.append({'stage': 'perform_symbol_corrections', 'elapsed_time': elapsed_time})

        #
        # -------------------------------- Operate on Corrected Symbols and Get Bits -------------------------------
        #
        start_time = perf_counter()
        self.dvbs2_bbframe_processor.demod_plframes(dvbs2_frames_all)
        elapsed_time = perf_counter() - start_time
        time_stats.append({'stage': 'demod_plframes', 'elapsed_time': elapsed_time})

        update_esn0 = False
        if update_esn0:
            for dvbs2_frame in dvbs2_frames_all:
                print(dvbs2_frame)
                len(dvbs2_frame.bits_descrambled)
                modulated_symbols = self.get_modulated_symbols(dvbs2_frame, do_plframe_scrambling=False)
                # iq_plot([dvbs2_frame.plframe_symbols_corrected[0:-90], modulated_symbols, dvbs2_frame.plframe_symbols_corrected[0:-90]-modulated_symbols])
                dvbs2_frame.esn0 = 10 * np.log10(1 / np.var(dvbs2_frame.plframe_symbols_corrected[0:-90]-modulated_symbols))


        #
        # ----------------------------------------- Print Debug Info -----------------------------------------------
        #
        start_time = perf_counter()
        if print_debug_info:
            self.print_plframe_results(block_num, results_dicts_this_block)
        elapsed_time = perf_counter() - start_time
        time_stats.append({'stage': 'print_plframe_results', 'elapsed_time': elapsed_time})
        if print_debug_info:
            print('Initial frequency offset = {};   Mean frequency offset = {}'.format(
                initial_sync_results['frequency_offset_hz'],
                np.mean([results_dict['frequency_offset_hz'] for results_dict in results_dicts_this_block])))

        #
        # ------------------------------------ Re-sort based on ModCods Observed ------------------------------------
        #
        self.dvbs2_plframe_processor.dvbs2_pl_header_data = sorted(self.dvbs2_plframe_processor.dvbs2_pl_header_data,
                                                                   key=lambda i: i['num_occurrences'], reverse=True)

        this_block_data = {
            'iq_resampled': iq_resampled,
            'fs_resampled_hz': fs_resampled_hz,
            't_iq_resampled_sec': t_iq_resampled_sec,
            'symbols': symbols_corrected,
            'fsym_hz': fsym_hz,
            't_symbols_sec': t_symbols_sec,
            'cs_amplitude': cs_amplitude,
            'cs_phase': cs_phase,
            'mean_frequency_offset_hz': -1 * np.mean(
                [dvbs2_frame.frequency_offset_hz for dvbs2_frame in dvbs2_frames_all]),
            'dvbs2_frames': dvbs2_frames_all
        }

        block_data.append(this_block_data)

        if block_mode == 'fixed':
            t_start_next_sec = t_start_sec + block_size_num_samples / fs_hz - \
                               self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() / fsym_hz
        elif block_mode == 'synced':
            if overlap_plframe_blocks:
                t_start_next_sec = dvbs2_frames_all[-1].t_plframe_symbols[0] + (-5) / fsym_hz
            else:
                t_start_next_sec = dvbs2_frames_all[-1].t_plframe_symbols[0] + (
                        dvbs2_frames_all[-1].dvbs2_modcod.get_num_symbols() - 5) / fsym_hz
        else:
            raise Exception('Unhandled block_mode')

        return t_start_next_sec, last_block, time_stats

    def get_modulated_symbols(self, dvbs2_frame, do_plframe_scrambling=True, do_fec_encoding=True):
        """
        Returns unscrambled PLFRAME symbols, including PLHEADER
        :return:
        """
        from dvbs2_processing.signal_utilities.modulation import modulate_plframe
        from dvbs2_processing.signal_utilities.interleaving import interleave_plframe
        bits_tx = self.dvbs2_bbframe_processor.scramble_bbframe(dvbs2_frame.dvbs2_modcod, dvbs2_frame.bits_descrambled)
        if do_fec_encoding:
            bits_tx_bch_ldpc = \
                self.dvbs2_bbframe_processor.encode_plframe(bits_tx, dvbs2_frame.dvbs2_modcod)
        else:
            (k_bch, k_ldpc, t_error, n_ldpc, q) = dvbs2_frame.dvbs2_modcod.get_coding_parameters()
            bits_tx_bch_ldpc = np.append(np.asarray(bits_tx), np.asarray((n_ldpc - k_bch) * [0]))
        bits_tx_bch_ldpc = interleave_plframe(dvbs2_frame.dvbs2_modcod, bits_tx_bch_ldpc)
        modulated_symbols = modulate_plframe(dvbs2_modcod=dvbs2_frame.dvbs2_modcod, bits=bits_tx_bch_ldpc)
        if do_plframe_scrambling:
            self.dvbs2_plframe_processor.scramble_plframe(dvbs2_modcod=dvbs2_frame.dvbs2_modcod,
                                                          symbols=modulated_symbols)
        return modulated_symbols


    @staticmethod
    def print_plframe_results(block_num, results_dicts_this_block):
        for idx, results_dict in enumerate(results_dicts_this_block):
            print(
                'PLFRAME Data:  '
                'Block: {:5}   Packet {:5}:   index={:7}   Time={:11.7}   Frequency={:8.1f}   esn0={:5.2f}   {}'.format(
                    block_num,
                    idx,
                    results_dict['current_symbol_index'],
                    results_dict['t_start_plframe_sec'],
                    results_dict['frequency_offset_hz'],
                    results_dict['esn0'],
                    results_dict['dvbs2_modcod']))
