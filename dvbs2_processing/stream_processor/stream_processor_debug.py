from copy import deepcopy
from time import sleep
from time import perf_counter
import numpy as np
from bitstring import BitArray

from digital_demod.carrier_recovery.carrier_frequency_recovery import carrier_recovery_mfd, carrier_frequency_shift
from dsp_utilities.plotting.scatterplot import scatterplot

from dsp_utilities.file_handlers.iq_file_store import IQFileType

from dvbs2_processing.symbol_processor.clock_recovery import clock_recovery_and_resample

from dvbs2_processing.plframe_processor import DVBS2PLFrameProcessor

from dvbs2_processing.symbol_processor import DVBS2SymbolProcessor

from dvbs2_processing.bbframe_processor import DVBS2BBFrameProcessor

from dvbs2_processing.utilities import send_via_udp, compute_plframe_statistics

from dvbs2_processing.signal_utilities.interleaving import deinterleave_plframe

from dvbs2_processing.signal_utilities.modulation import demodulate_plframe

class DVBS2StreamProcessorDebug:
    """
    Functionality to detect and output PLFRAMEs from coarsely synchronized symbols

    Assumptions:
     * Symbols are coarsely synced in clock and carrier frequency (exactly what that means: TBD)

    """
    def __init__(self, dvbs2_frame_length=None, dvbs2_pilots=None, dvbs2_modulation_type=None, no_fec_processor=False,
                 threading_config=0):

        self.dvbs2_symbol_processor = DVBS2SymbolProcessor()

        self.dvbs2_plframe_processor = DVBS2PLFrameProcessor(dvbs2_frame_length=dvbs2_frame_length,
                                                             dvbs2_pilots=dvbs2_pilots,
                                                             no_fec_processor=no_fec_processor,
                                                             threading_config=threading_config)

        self.dvbs2_bbframe_processor = DVBS2BBFrameProcessor(dvbs2_frame_length=dvbs2_frame_length,
                                                             dvbs2_pilots=dvbs2_pilots,
                                                             no_fec_processor=no_fec_processor,
                                                             threading_config=threading_config)

        self.threading_config = threading_config
        if self.threading_config == 0:
            self.num_fec_banks = 1
            self.num_processing_threads = 1
        elif self.threading_config == 1:
            self.num_fec_banks = 4
            self.num_processing_threads = 2
        elif self.threading_config == 2:
            self.num_fec_banks = 8
            self.num_processing_threads = 4
        elif self.threading_config == 3:
            self.num_fec_banks = 16
            self.num_processing_threads = 8
        elif self.threading_config == 4:
            self.num_fec_banks = 32
            self.num_processing_threads = 16
        else:
            raise Exception('Unhandled threading_config')


    def initialize_file_store(self, filename, file_type=IQFileType.BLUEFILE, iq_data_format=None, fs_hz=None):
        """
        Initializes the IQ file store and updates the sampling rate
        :param filename:
        :param file_type:
        :param iq_data_format:
        :param fs_hz:
        :return:
        """
        self.dvbs2_symbol_processor.initialize_file_store(filename=filename, file_type=file_type,
                                                          iq_data_format=iq_data_format, fs_hz=fs_hz)


    def perform_initial_sync(self, t_start_sec=0.0, fsym_estimate_hz=None):
        """
        Performs an initial synchronization on unprocessed IQ data.  Finds and returns first PLFRAME that passes FEC.
        :param t_start_sec: Start time in seconds
        :param fsym_estimate_hz: Symbol rate estimate in symbols/sec
        :return: initial_sync_results
        """

        #
        # Read in minimum number of symbols
        #
        # Without knowledge of symbol rate, assume it is 0.5 x Fs
        symbol_rate_estimate_sps = 0.5 * self.dvbs2_symbol_processor.iq_file_store.fs_hz
        fsym_hz = symbol_rate_estimate_sps
        min_num_symbols = self.dvbs2_plframe_processor.min_symbols_for_plframe_detection()
        min_num_samples = 10 * np.int(min_num_symbols * (self.dvbs2_symbol_processor.iq_file_store.fs_hz / symbol_rate_estimate_sps))
        fec_lock = False
        num_samples = min_num_samples
        num_pl_frames = 0
        next_symbol_index = -1

        while not fec_lock:
            num_pl_frames += 1
            t_start_sec = np.max([0.0, t_start_sec - np.ceil(self.dvbs2_symbol_processor.fs_hz/fsym_hz) / self.dvbs2_symbol_processor.fs_hz])
            self.dvbs2_symbol_processor.iq_file_store.read_file(t_start_sec=t_start_sec, num_samples=num_samples+4)
            self.dvbs2_symbol_processor.iq = self.dvbs2_symbol_processor.iq_file_store.iq_file.iq
            iq = deepcopy(self.dvbs2_symbol_processor.iq_file_store.iq_file.iq)
            self.dvbs2_symbol_processor.iq_file_store.iq_file.generate_t_vector()
            # t_iq_sec = deepcopy(self.dvbs2_pl_frame_processor.iq_file_store.iq_file.t_iq_sec)

            if fsym_estimate_hz is None:
                from digital_demod.measurements.freq_domain_measurements import basic_freq_domain_analysis
                center_frequency_hz, bandwidth_hz, cn_db = \
                    basic_freq_domain_analysis(iq=iq, fs_hz=self.dvbs2_symbol_processor.fs_hz, usable_ibw_fraction=0.9,
                                               fraction_power_carrier=0.90, fraction_power_noise=0.10,
                                               debug_plots=False)
                fsym_estimate_this_block = bandwidth_hz / 1.3
            else:
                fsym_estimate_this_block = fsym_estimate_hz


            #
            # ------------------------------------- Clock detect and resample -------------------------------------
            #
            fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec = \
                clock_recovery_and_resample(iq=iq, fs_hz=self.dvbs2_symbol_processor.fs_hz,
                                                 fsym_initial_estimate_hz=fsym_estimate_this_block,
                                                 t_start_sec=t_start_sec)

            #
            # ---------------------------------------- Extract symbols --------------------------------------------
            #
            # Design the RRC filter using assumptions about filter's roll-off
            symbols, _, iq_resampled_rrc_filtered, oversample_factor = \
                self.dvbs2_symbol_processor.generate_symbols(iq_resampled, t_iq_resampled_sec, fs_resampled_hz, fsym_hz, self.dvbs2_symbol_processor.rrc_beta)

            #
            # ------------------------------------------ Carrier recovery ------------------------------------------
            #
            frequency_offset_hz, metric_snr = carrier_recovery_mfd(self.dvbs2_symbol_processor.iq, 4, self.dvbs2_symbol_processor.fs_hz, 50e3, False)
            # iq = carrier_frequency_shift(iq, -1*frequency_offset_hz, t_sec=None, fs_hz=self.dvbs2_pl_frame_processor.fs_hz)
            symbols = carrier_frequency_shift(symbols, -1*frequency_offset_hz, t_sec=None, fs_hz=fsym_hz)
            if False:
                scatterplot(symbols[0:1000])

            #
            # ------------------------------------------ PL Header Search ------------------------------------------
            #
            if next_symbol_index == -1:
                next_symbol_index, results_dict = self.dvbs2_plframe_processor.get_next_plframe(symbols=symbols, fsym_hz=fsym_hz,
                                                                                                current_symbol_index=0,
                                                                                                max_num_symbols=-1,
                                                                                                search_for_pl_headers=True,
                                                                                                max_num_plsc_symbol_errors=12,
                                                                                                max_num_sof_symbol_errors=8)
            else:
                next_symbol_index, results_dict = self.dvbs2_plframe_processor.get_next_plframe(symbols=symbols, fsym_hz=fsym_hz,
                                                                                                current_symbol_index=0,
                                                                                                max_num_symbols=10,
                                                                                                search_for_pl_headers=False,
                                                                                                max_num_plsc_symbol_errors=12,
                                                                                                max_num_sof_symbol_errors=10)

            # sof_phase_correction_rad = results_dict['coarse_phase_correction']
            t_start_plframe_sec = t_start_sec + results_dict['current_symbol_index'] / fsym_hz
            dvbs2_pl_header_data = self.dvbs2_plframe_processor.find_matching_pl_header_data(results_dict['dvbs2_modcod'])

            #
            # ------------------------------------------ PL Header Demod ------------------------------------------
            #
            pl_frame_start_index = results_dict['current_symbol_index']
            assert pl_frame_start_index + results_dict['dvbs2_modcod'].get_num_symbols() <= len(symbols), \
                "Trying to access past end of symbols vector"
            plframe_symbols = \
                symbols[pl_frame_start_index:(pl_frame_start_index+results_dict['dvbs2_modcod'].get_num_symbols()+90)]
            t_plframe_symbols_sec = np.linspace(0, len(plframe_symbols) - 1, len(plframe_symbols)) / fsym_hz

            self.dvbs2_plframe_processor.descramble_plframe(results_dict['dvbs2_modcod'], plframe_symbols)
            symbols_, results_dict_pl_frame = \
                self.dvbs2_plframe_processor.get_corrected_plframe(symbols=plframe_symbols, fsym_hz=fsym_hz,
                                                                   dvbs2_modcod=results_dict['dvbs2_modcod'])
            plframe_symbols = self.dvbs2_plframe_processor.apply_corrections(results_dict_pl_frame['cs_amplitude'],
                                                                             results_dict_pl_frame['cs_phase'],
                                                                             plframe_symbols, t_plframe_symbols_sec)

            esn0 = compute_plframe_statistics(symbols=plframe_symbols,
                                                   dvbs2_modcod=results_dict['dvbs2_modcod'])
            plframe_symbols = plframe_symbols[0:results_dict['dvbs2_modcod'].get_num_symbols()]
            results_dict_pl_frame['esn0'] = esn0

            if False:
                scatterplot(plframe_symbols)

                plt.figure()
                plt.plot(np.real(symbols), '.')
                plt.plot(np.imag(symbols), '.')
                plt.grid()


            print('Packet {:5}:   index={:7}   Time={:7.3}   Frequency={:8.1f}   Es/N0={:5.2f}   {}'.format(
                num_pl_frames,
                results_dict['current_symbol_index'],
                t_start_plframe_sec,
                results_dict['frequency_offset_hz'],
                results_dict_pl_frame['esn0'],
                results_dict['dvbs2_modcod']))
            #
            # ------------------------------------------- FEC Processing -------------------------------------------
            #
            noise_var = np.var(plframe_symbols[results_dict['dvbs2_modcod'].get_pilot_symbol_indicies()] -
                               (1 / np.sqrt(2) + 1j / np.sqrt(2)))
            llr = demodulate_plframe(dvbs2_modcod=results_dict['dvbs2_modcod'], symbols=plframe_symbols,
                                     noise_var=float(noise_var))
            llr = deinterleave_plframe(dvbs2_modcod=results_dict['dvbs2_modcod'], plframe_bits=llr)
            bits_llr = np.array(llr < 0, dtype=np.int)

            if self.dvbs2_bbframe_processor.dvbs2_fec_processor_banks is not None:
                bits, passed_fec = self.dvbs2_bbframe_processor.decode_plframe(llr=llr, dvbs2_modcod=results_dict['dvbs2_modcod'])
            else:
                bits = bits_llr

            (k_bch, k_ldpc, t_bch, n_ldpc, q) = results_dict['dvbs2_modcod'].get_coding_parameters()
            scrambling_sequence = dvbs2_pl_header_data['bbframe_scrambling_sequence']
            bits_descrambled = np.asarray(bits[0:k_bch]) ^ np.asarray(scrambling_sequence)
            bbheader = {'GS': BitArray(bits_descrambled[0:2]).uint,
                        'MIS': int(bits_descrambled[2]),
                        'ACM': int(bits_descrambled[3]),
                        'ISSYI': int(bits_descrambled[4]),
                        'NPD': int(bits_descrambled[5]),
                        'RO': BitArray(bits_descrambled[6:8]).uint,
                        'MATYPE2': BitArray(bits_descrambled[8:16]).uint,
                        'UPL': BitArray(bits_descrambled[16:32]).uint,
                        'DFL': BitArray(bits_descrambled[32:48]).uint,
                        'SYNC': BitArray(bits_descrambled[48:56]).uint,
                        'SYNCD': BitArray(bits_descrambled[56:72]).uint,
                        'CRC': BitArray(bits_descrambled[72:80]).uint}
            print(bbheader)

            # bits_transmitted = interleave_plframe(dvbs2_modcod=results_dict['dvbs2_modcod'], plframe_bits=bits)
            # symbols_modulated = self.dvbs2_pl_frame_processor.modulate_plframe(dvbs2_modcod=results_dict['dvbs2_modcod'], bits=bits_transmitted)

            if False:
                print('Corrected {} bit errors (BER = {}'.format(
                    np.sum(bits != bits_llr), np.sum(bits != bits_llr) / k_ldpc))
                send_via_udp(bits[0:k_bch], results_dict)

            initial_sync_results = {
                'fs_hz': self.dvbs2_symbol_processor.fs_hz,
                'fsym_hz': fsym_hz,
                'dvbs2_modcod': results_dict['dvbs2_modcod'],
                't_start_sec': t_start_plframe_sec,
                'current_symbol_index': results_dict['current_symbol_index'],
                'fs_resampled_hz': fs_resampled_hz,
                'frequency_offset_hz': frequency_offset_hz
            }

            return initial_sync_results


            t_start_sec += (results_dict['current_symbol_index'] + results_dict['dvbs2_modcod'].get_num_symbols() - 100) / fsym_hz
            num_samples = np.int(1.25 * results_dict['dvbs2_modcod'].get_num_symbols()
                                 * (self.dvbs2_symbol_processor.iq_file_store.fs_hz / fsym_hz))

        return initial_sync_results



    def process_plframes(self, initial_sync_results, block_size_num_samples, num_blocks=1):
        """
        Performs an initial synchronization on unprocessed IQ data.  Finds and returns first PLFRAME that passes FEC.

        initial_sync_results = {
            'dvbs2_modcod': results_dict['dvbs2_modcod'],
            't_start_sec': t_start_plframe_sec,
            'current_symbol_index': results_dict['current_symbol_index'],
            'fsym_hz': fsym_hz,
            'clock_phase_offset_rad': clock_phase_offset_rad,
            'fractional_sample_shift': fractional_sample_shift,
            'fs_resampled_hz': fs_resampled_hz,
            'frequency_offset_hz': frequency_offset_hz
        }
        """
        #
        # Read in minimum number of symbols
        #
        # Without knowledge of symbol rate, assume it is 0.5 x Fs

        overlap_plframe_blocks = True
        fs_hz = self.dvbs2_symbol_processor.iq_file_store.fs_hz
        t_start_sec = initial_sync_results['t_start_sec']
        fsym_hz = initial_sync_results['fsym_hz']
        fsym_initial_estimate_hz = initial_sync_results['fsym_hz']
        t_start_sec = np.max([0.0, t_start_sec - np.ceil(fs_hz / fsym_hz) / fs_hz])
        t_end_sec = self.dvbs2_symbol_processor.iq_file_store.t_end_sec

        block_data = []
        results_dicts = []
        time_lists = []
        block_mode = 'fixed'  # fixed = same number of samples each time; synced = adjusted to line up to plframes
        block_num = 0
        last_block = False
        for block_num in range(0, num_blocks):
            print('----------------------------------------------------------------------------------------------------')
            print(' ------------------------------ T = {} -------------------------------------------------------'.format(t_start_sec))
            print('----------------------------------------------------------------------------------------------------')
            time_list = []

            #
            # Read IQ and make deep copies to local variaables
            #
            start_time = perf_counter()
            iq, last_block = self.dvbs2_symbol_processor.get_iq_data(t_start_sec, block_size_num_samples, results_dicts)
            if len(iq) == 0:
                break
            elapsed_time = perf_counter() - start_time
            time_list.append(elapsed_time)

            #
            # ------------------------------------- Clock detect and resample -------------------------------------
            #
            start_time = perf_counter()
            fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec = \
                clock_recovery_and_resample(iq, fs_hz, fsym_initial_estimate_hz, t_start_sec)
            elapsed_time = perf_counter() - start_time
            time_list.append(elapsed_time)

            #
            # ---------------------------------------- Extract symbols --------------------------------------------
            #
            start_time = perf_counter()
            symbols, t_symbols_sec, iq_resampled_rrc_filtered, oversample_factor = \
                self.dvbs2_symbol_processor.generate_symbols(iq_resampled, t_iq_resampled_sec, fs_resampled_hz, fsym_hz, self.dvbs2_plframe_processor.rrc_beta)
            elapsed_time = perf_counter() - start_time
            time_list.append(elapsed_time)

            #
            # ------------------------------------------ Carrier recovery ------------------------------------------
            #
            start_time = perf_counter()
            iq, iq_resampled, symbols = \
                self.dvbs2_symbol_processor.coarse_carrier_recovery(iq, fs_hz, iq_resampled, fs_resampled_hz, symbols, fsym_hz)
            elapsed_time = perf_counter() - start_time
            time_list.append(elapsed_time)

            #
            # -------------------------------- Align symbols to start of first PLFRAME --------------------------------
            #
            write_debug_dicts = True
            if write_debug_dicts:
                import pickle
                perform_symbol_corrections_input = {
                    'fs_hz': fs_hz,
                    'symbols': symbols,
                    'fsym_hz': fsym_hz,
                    't_symbols_sec': t_symbols_sec,
                    'iq_resampled': iq_resampled,
                    'fs_resampled_hz': fs_resampled_hz,
                    't_iq_resampled_sec': t_iq_resampled_sec,
                    'block_num': block_num
                }
                with open('perform_symbol_corrections_input-block-{:05d}.pickle'.format(block_num), 'wb') as fp:
                    pickle.dump(perform_symbol_corrections_input, fp, protocol=pickle.HIGHEST_PROTOCOL)

            start_time = perf_counter()
            results_dicts_this_block, first_plframe_symbol_index, frequency_offset_hz, carrier_phase_rad, \
            cs_amplitude, cs_phase, iq_resampled, t_iq_resampled_sec, symbols_corrected, t_symbols_sec = \
                self.dvbs2_plframe_processor.perform_symbol_corrections(symbols, fsym_hz, t_symbols_sec, iq_resampled,
                                                                        fs_resampled_hz, t_iq_resampled_sec, block_num)
            symbols = symbols[first_plframe_symbol_index::]
            elapsed_time = perf_counter() - start_time
            time_list.append(elapsed_time)

            if write_debug_dicts:
                import pickle
                perform_symbol_corrections_output = {
                    'results_dicts_this_block': results_dicts_this_block,
                    'cs_amplitude': cs_amplitude,
                    'cs_phase': cs_phase,
                    'iq_resampled': iq_resampled,
                    'symbols': symbols,
                    'symbols_corrected': symbols_corrected,
                    't_iq_resampled_sec': t_iq_resampled_sec,
                    't_symbols_sec': t_symbols_sec,
                    'block_num': block_num
                }
                with open('perform_symbol_corrections_output-block-{:05d}.pickle'.format(block_num), 'wb') as fp:
                    pickle.dump(perform_symbol_corrections_output, fp, protocol=pickle.HIGHEST_PROTOCOL)

            #
            # -------------------------------- Operate on Corrected Symbols and Get Bits -------------------------------
            #
            start_time = perf_counter()
            self.dvbs2_bbframe_processor.demod_plframes(results_dicts_this_block, symbols_corrected)
            elapsed_time = perf_counter() - start_time
            time_list.append(elapsed_time)

            #
            # ----------------------------------------- Print Debug Info -----------------------------------------------
            #
            start_time = perf_counter()
            for idx, results_dict in enumerate(results_dicts_this_block):
                print(
                    'PLFRAME Data:  Block: {:5}   Packet {:5}:   index={:7}   Time={:11.7}   Frequency={:8.1f}   esn0={:5.2f}   {}'.format(
                        block_num,
                        idx,
                        results_dict['current_symbol_index'],
                        results_dict['t_start_plframe_sec'],
                        results_dict['frequency_offset_hz'],
                        results_dict['esn0'],
                        results_dict['dvbs2_modcod']))
            elapsed_time = perf_counter() - start_time
            time_list.append(elapsed_time)

            #
            # ------------------------------------ Resort based on ModCods Observed ------------------------------------
            #
            self.dvbs2_plframe_processor.dvbs2_pl_header_data = sorted(self.dvbs2_plframe_processor.dvbs2_pl_header_data, key=lambda i: i['num_occurrences'], reverse=True)

            for results_dict in results_dicts_this_block:
                results_dicts.append(results_dict)

            this_block_data = {
                'iq_resampled': iq_resampled,
                'fs_resampled_hz': fs_resampled_hz,
                't_iq_resampled_sec': t_iq_resampled_sec,
                'symbols': symbols,
                'fsym_hz': fsym_hz,
                't_symbols_sec': t_symbols_sec,
                'cs_amplitude': cs_amplitude,
                'cs_phase': cs_phase,
                'results_dicts': results_dicts
            }
            block_data.append(this_block_data)

            time_lists.append(time_list)

            if last_block:
                break

            if block_mode == 'fixed':
                t_start_sec += block_size_num_samples / fs_hz - self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() / fsym_hz
            elif block_mode == 'synced':
                if overlap_plframe_blocks:
                    t_start_sec = results_dicts[-1]['t_start_plframe_sec'] + (-5) / results_dicts[-1]['fsym_hz']
                else:
                    t_start_sec = results_dicts[-1]['t_start_plframe_sec'] + (results_dicts[-1]['dvbs2_modcod'].get_num_symbols() - 5)/results_dicts[-1]['fsym_hz']
            else:
                raise Exception('Unhandled block_mode')

        if False:
            carrier_process_data = {
                'dvbs2_pl_frame_processor': self,
                'results_dicts': results_dicts,
                'iq_resampled': iq_resampled,
                'fs_resampled_hz': fs_resampled_hz,
                't_iq_resampled_sec': t_iq_resampled_sec,
                'symbols': symbols,
                'fsym_hz': fsym_hz,
                't_symbols_sec': t_symbols_sec,
                'fs_hz': fs_hz,
                'cs_amplitude': cs_amplitude,
                'cs_phase': cs_phase
            }

        stages = ['get_iq_data', 'clock_recovery_and_resample', 'generate_symbols', 'coarse_carrier_recovery', 'perform_symbol_corrections', 'demod_plframes', 'debug_print']
        for stage, time_sec in zip(stages, np.mean(np.asarray(time_lists), 0)):
            percentage = 100 * time_sec / np.sum(np.mean(np.asarray(time_lists), 0))
            print('{:35s} {:05.05f} ms    {:3.1f}%'.format(stage, 100*time_sec, percentage))

        return block_data


    def process_plframes_pipelined(self, initial_sync_results, block_size_num_samples, num_blocks=1):
        """
        Performs an initial synchronization on unprocessed IQ data.  Finds and returns first PLFRAME that passes FEC.

        initial_sync_results = {
            'dvbs2_modcod': results_dict['dvbs2_modcod'],
            't_start_sec': t_start_plframe_sec,
            'current_symbol_index': results_dict['current_symbol_index'],
            'fsym_hz': fsym_hz,
            'clock_phase_offset_rad': clock_phase_offset_rad,
            'fractional_sample_shift': fractional_sample_shift,
            'fs_resampled_hz': fs_resampled_hz,
            'frequency_offset_hz': frequency_offset_hz
        }
        """
        #
        # Read in minimum number of symbols
        #
        # Without knowledge of symbol rate, assume it is 0.5 x Fs

        # Define worker functions needed by mpipe
        import copy
        debug_print = False

        def get_iq_data_worker(input_dict):
            if debug_print:
                print('Entering get_iq_data_worker()')
            input_dict_copy = copy.deepcopy(input_dict)
            fs_hz = input_dict_copy['fs_hz']
            t_start_sec = input_dict_copy['t_start_sec']
            block_size_num_samples_ = input_dict_copy['block_size_num_samples']
            fsym_hz = input_dict_copy['fsym_hz']
            results_dicts_ = input_dict_copy['results_dicts']
            iq, last_block = self.dvbs2_symbol_processor.get_iq_data(t_start_sec, block_size_num_samples_, results_dicts_)
            output_dict = input_dict_copy
            output_dict['iq'] = iq
            output_dict['last_block'] = last_block
            if debug_print:
                print('Leaving get_iq_data_worker()')
            return output_dict

        def clock_recovery_and_resample_worker(input_dict):
            if debug_print:
                print('  Entering clock_recovery_and_resample_worker()')
            input_dict_copy = copy.deepcopy(input_dict)
            iq = input_dict_copy['iq']
            if len(iq) == 0:
                fsym_hz = None
                fs_resampled_hz = []
                iq_resampled = []
                t_iq_resampled_sec = []
            else:
                fs_hz = input_dict_copy['fs_hz']
                fsym_initial_estimate_hz = input_dict_copy['fsym_initial_estimate_hz']
                t_start_sec = input_dict_copy['t_start_sec']
                fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec = \
                    clock_recovery_and_resample(iq, fs_hz, fsym_initial_estimate_hz, t_start_sec)
            output_dict = input_dict_copy
            output_dict['fsym_hz'] = fsym_hz
            output_dict['fs_resampled_hz'] = fs_resampled_hz
            output_dict['iq_resampled'] = iq_resampled
            output_dict['t_iq_resampled_sec'] = t_iq_resampled_sec
            if debug_print:
                print('  Leaving clock_recovery_and_resample_worker()')
            return output_dict

        def generate_symbols_worker(input_dict):
            if debug_print:
                print('    Entering generate_symbols_worker()')
            input_dict_copy = copy.deepcopy(input_dict)
            iq_resampled = input_dict_copy['iq_resampled']
            if len(iq_resampled) == 0:
                symbols = []
                t_symbols_sec = []
                iq_resampled_rrc_filtered = []
                oversample_factor = None
            else:
                t_iq_resampled_sec = input_dict_copy['t_iq_resampled_sec']
                fs_resampled_hz = input_dict_copy['fs_resampled_hz']
                fsym_hz = input_dict_copy['fsym_hz']
                rrc_beta = input_dict_copy['rrc_beta']
                symbols, t_symbols_sec, iq_resampled_rrc_filtered, oversample_factor = \
                    self.dvbs2_symbol_processor.generate_symbols(iq_resampled, t_iq_resampled_sec, fs_resampled_hz, fsym_hz, rrc_beta)
            output_dict = input_dict_copy
            output_dict['symbols'] = symbols
            output_dict['t_symbols_sec'] = t_symbols_sec
            output_dict['iq_resampled_rrc_filtered'] = iq_resampled_rrc_filtered
            output_dict['oversample_factor'] = oversample_factor
            # print(symbols)
            if debug_print:
                print('    Leaving generate_symbols_worker()')
            return output_dict

        def coarse_carrier_recovery_worker(input_dict):
            if debug_print:
                print('      Entering coarse_carrier_recovery_worker()')
            input_dict_copy = copy.deepcopy(input_dict)
            iq = input_dict_copy['iq']
            if len(iq) == 0:
                iq_resampled = []
                symbols = []
            else:
                fs_hz = input_dict_copy['fs_hz']
                iq_resampled = input_dict_copy['iq_resampled']
                fs_resampled_hz = input_dict_copy['fs_resampled_hz']
                symbols = input_dict_copy['symbols']
                # print(symbols)
                fsym_hz = input_dict_copy['fsym_hz']
                iq, iq_resampled, symbols = \
                    self.dvbs2_symbol_processor.coarse_carrier_recovery(iq, fs_hz, iq_resampled, fs_resampled_hz, symbols, fsym_hz)
            output_dict = input_dict_copy
            output_dict['iq'] = iq
            output_dict['iq_resampled'] = iq_resampled
            output_dict['symbols'] = symbols
            if debug_print:
                print('      Leaving coarse_carrier_recovery_worker()')
            return output_dict

        def perform_symbol_corrections_worker(input_dict):
            if debug_print:
                print('        Entering perform_symbol_corrections_worker()')
            input_dict_copy = copy.deepcopy(input_dict)
            symbols = input_dict_copy['symbols']
            if len(symbols) == 0:
                results_dicts_this_block = []
                cs_amplitude = None
                cs_phase = None
                iq_resampled = []
                symbols_corrected = []
                t_iq_resampled_sec = []
                t_symbols_sec = []
            else:
                fs_hz = input_dict_copy['fs_hz']
                fsym_hz = input_dict_copy['fsym_hz']
                t_symbols_sec = input_dict_copy['t_symbols_sec']
                iq_resampled = input_dict_copy['iq_resampled']
                fs_resampled_hz = input_dict_copy['fs_resampled_hz']
                t_iq_resampled_sec = input_dict_copy['t_iq_resampled_sec']
                block_number = input_dict_copy['block_number']
                results_dicts_this_block, first_plframe_symbol_index, frequency_offset_hz, carrier_phase_rad, \
                cs_amplitude, cs_phase, iq_resampled, t_iq_resampled_sec, symbols_corrected, t_symbols_sec = \
                    self.dvbs2_plframe_processor.perform_symbol_corrections(symbols, fsym_hz, t_symbols_sec,
                                                                            iq_resampled, fs_resampled_hz,
                                                                            t_iq_resampled_sec, block_number)
                symbols = symbols[first_plframe_symbol_index::]
            output_dict = input_dict_copy
            output_dict['results_dicts_this_block'] = results_dicts_this_block
            output_dict['cs_amplitude'] = cs_amplitude
            output_dict['cs_phase'] = cs_phase
            output_dict['iq_resampled'] = iq_resampled
            output_dict['symbols'] = symbols
            output_dict['symbols_corrected'] = symbols_corrected
            output_dict['t_iq_resampled_sec'] = t_iq_resampled_sec
            output_dict['t_symbols_sec'] = t_symbols_sec
            if debug_print:
                print('        Leaving perform_symbol_corrections_worker()')
            return output_dict

        def demod_plframes_worker(input_dict):
            if debug_print:
                print('        Entering demod_plframes_worker()')
            input_dict_copy = copy.deepcopy(input_dict)
            results_dicts_this_block = input_dict_copy['results_dicts_this_block']
            if len(results_dicts_this_block) == 0:
                results_dicts_this_block = []
            else:
                symbols_corrected = input_dict_copy['symbols_corrected']
                self.dvbs2_bbframe_processor.demod_plframes(results_dicts_this_block, symbols_corrected)
            output_dict = input_dict_copy
            if debug_print:
                print('        Leaving demod_plframes_worker()')
            return output_dict

        def print_results_worker(input_dict):
            if debug_print:
                print('        Entering pull_results_worker()')
            print_block = True
            if print_block:
                print('Received block of PLFRAME data')
            print_frames = False
            if print_frames:
                input_dict_copy = copy.deepcopy(input_dict)
                for idx, results_dict in enumerate(input_dict_copy['results_dicts_this_block']):
                    print(
                        'PLFRAME Data:  Block: {:5}   Packet {:5}:   index={:7}   Time={:11.7}   Frequency={:8.1f}   esn0={:5.2f}   {}'.format(
                            -1,
                            idx,
                            results_dict['current_symbol_index'],
                            results_dict['t_start_plframe_sec'],
                            results_dict['frequency_offset_hz'],
                            results_dict['esn0'],
                            results_dict['dvbs2_modcod']))

            if debug_print:
                print('        Leaving perform_symbol_corrections_worker()')


        overlap_plframe_blocks = True
        fs_hz_ = self.dvbs2_symbol_processor.iq_file_store.fs_hz
        t_start_sec_ = initial_sync_results['t_start_sec']
        fsym_hz_ = initial_sync_results['fsym_hz']
        t_start_sec_ = np.max([0.0, t_start_sec_ - np.ceil(fs_hz_ / fsym_hz_) / fs_hz_])

        results_dicts = []

        from mpipe import OrderedStage, Pipeline

        stages = []
        stages.append(OrderedStage(get_iq_data_worker, 1))
        stages.append(OrderedStage(clock_recovery_and_resample_worker, self.num_processing_threads))
        stages[-2].link(stages[-1])
        stages.append(OrderedStage(generate_symbols_worker, self.num_processing_threads))
        stages[-2].link(stages[-1])
        stages.append(OrderedStage(coarse_carrier_recovery_worker, self.num_processing_threads))
        stages[-2].link(stages[-1])
        stages.append(OrderedStage(perform_symbol_corrections_worker, 4*self.num_processing_threads))
        stages[-2].link(stages[-1])
        stages.append(OrderedStage(demod_plframes_worker, self.dvbs2_bbframe_processor.num_fec_banks))
        stages[-2].link(stages[-1])
        stages.append(OrderedStage(print_results_worker, 1, disable_result=True))
        stages[-2].link(stages[-1])

        pipe = Pipeline(stages[0])

        initial_sync_results['t_start_sec'] = t_start_sec_
        initial_sync_results['block_size_num_samples'] = block_size_num_samples
        initial_sync_results['fsym_hz'] = fsym_hz_
        initial_sync_results['fsym_initial_estimate_hz'] = fsym_hz_
        initial_sync_results['results_dicts'] = results_dicts
        initial_sync_results['rrc_beta'] = self.dvbs2_symbol_processor.rrc_beta

        import time
        start = time.time()
        print('Starting main processing loop in_plframes_pipelined()')

        do_parallel = True
        if do_parallel:
            for block_number_ in range(num_blocks):
                initial_sync_results['t_start_sec'] = t_start_sec_
                initial_sync_results['block_number'] = block_number_
                pipe.put(initial_sync_results)
                t_start_sec_ += block_size_num_samples / fs_hz_ - self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() / fsym_hz_
                # pipe.get(timeout=None)
            pipe.put(None)
            valid, result = pipe.get(timeout=0.1)
            while valid is True:
                sleep(0.1)
                valid, result = pipe.get(timeout=0.1)
        else:
            for block_number_ in range(100):
                initial_sync_results['t_start_sec'] = t_start_sec_
                initial_sync_results['block_number'] = block_number_
                worker_dict = get_iq_data_worker(initial_sync_results)
                worker_dict = clock_recovery_and_resample_worker(worker_dict)
                worker_dict = generate_symbols_worker(worker_dict)
                worker_dict = coarse_carrier_recovery_worker(worker_dict)
                worker_dict = perform_symbol_corrections_worker(worker_dict)
                worker_dict = demod_plframes_worker(worker_dict)
                t_start_sec_ += block_size_num_samples / fs_hz_ - self.dvbs2_plframe_processor.min_symbols_for_plframe_detection() / fsym_hz_

        end = time.time()
        elapsed = end - start
        print('Elapsed time in process_plframes_pipelined(): {}'.format(elapsed))

        carrier_process_data = None

        return carrier_process_data