import numpy as np

from digital_demod.clock_recovery.clock_recovery import clock_recovery
from digital_demod.signal_utilities.resample_signal import resample_signal
from digital_demod.signal_utilities.subsample_shift import subsample_shift

from dsp_utilities.plotting.iq_plot import iq_plot
from dsp_utilities.plotting.scatterplot import scatterplot
from dsp_utilities.plotting.pwelch import pwelch
from dsp_utilities.plotting.filter_visualization import mfreqz, mfreqz_multiple


def resample_iq(iq: np.ndarray, fs_hz: float, fractional_sample_shift: float, fs_resampled_hz: float):
    """
    Resamples IQ to a specified resampling rate.  Typically called after clock_recovery() to resample to N x Fs
    prior to generating symbols.
    :param iq: np.ndarray of IQ data
    :param fs_hz: Sample rate of iq in hz
    :param fractional_sample_shift: The fractional sample shift to apply to iq to correct for symbol phase
    :param fs_resampled_hz: The sample rate of the resampled IQ data
    :return:
     - iq_resampled: Resampled IQ data
     - t_iq_resampled_sec: Time vector for resampled IQ data
    """
    iq_ = subsample_shift(iq, fractional_sample_shift)
    # Resample to oversample_factor x the symbol rate
    iq_resampled = resample_signal(iq_, fs_hz, fs_resampled_hz)
    t_iq_resampled_sec = -1 * fractional_sample_shift / fs_hz \
                         + np.linspace(0, len(iq_resampled) - 1, len(iq_resampled)) / fs_resampled_hz
    return iq_resampled, t_iq_resampled_sec


def clock_recovery_and_resample(iq: np.ndarray, fs_hz: float, fsym_initial_estimate_hz: float,
                                t_start_sec: float, oversample_by_2:bool = False):
    """
    Performs clock recovery and resampling of data to 2 x the detected symbol rate

    :param iq: np.ndarray of IQ data
    :param fs_hz: Sample rate of iq in hz
    :param fsym_initial_estimate_hz: Initial estimate of symbol rate
    :param t_start_sec: Starting time of IQ data
    :return:
     - fs_resampled_hz: The sample rate of the resampled IQ data
     - fsym_hz: The detected symbol rate in Hz
     - fractional_sample_shift: The fractional sample shift to apply to iq to correct for symbol phase
     - clock_phase_offset_rad: The symbol phase in resampled data
    """
    fsym_hz, fractional_sample_shift, clock_phase_offset_rad = \
        clock_recovery(iq=iq, fs_hz=fs_hz, fsym_estimate_hz=fsym_initial_estimate_hz, oversample_by_2=oversample_by_2)
    oversample_factor = 2
    fs_resampled_hz = oversample_factor * fsym_hz
    iq_resampled, t_iq_resampled_sec = \
        resample_iq(iq, fs_hz, fractional_sample_shift, fs_resampled_hz)
    t_iq_resampled_sec += t_start_sec
    return fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec


def clock_recovery_and_generate_symbols(iq: np.ndarray, fs_hz: float, fsym_initial_estimate_hz: float,
                                t_start_sec: float, rrc_beta: float):
    """
    Performs clock recovery and resampling of data to 2 x the detected symbol rate

    :param iq: np.ndarray of IQ data
    :param fs_hz: Sample rate of iq in hz
    :param fsym_initial_estimate_hz: Initial estimate of symbol rate
    :param t_start_sec: Starting time of IQ data
    :param rrc_beta: RRC filter beta
    :return:
     - fs_resampled_hz: The sample rate of the resampled IQ data
     - fsym_hz: The detected symbol rate in Hz
     - fractional_sample_shift: The fractional sample shift to apply to iq to correct for symbol phase
     - clock_phase_offset_rad: The symbol phase in resampled data
     - symbols
      - t_symbols_sec
      - iq_resampled_rrc_filtered
      - oversample_factor
    """
    fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec = \
        clock_recovery_and_resample(iq=iq, fs_hz=fs_hz, fsym_initial_estimate_hz=fsym_initial_estimate_hz,
                                t_start_sec=t_start_sec)

    from .symbol_processor import DVBS2SymbolProcessor
    symbols, t_symbols_sec, iq_resampled_rrc_filtered, oversample_factor = \
        DVBS2SymbolProcessor.generate_symbols(iq_resampled=iq_resampled, t_iq_resampled_sec=t_iq_resampled_sec,
                                              fs_resampled_hz=fs_resampled_hz, fsym_hz=fsym_hz, rrc_beta=rrc_beta)

    return fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec, symbols, t_symbols_sec, oversample_factor