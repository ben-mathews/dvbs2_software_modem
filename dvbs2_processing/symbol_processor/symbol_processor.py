from copy import deepcopy
import numpy as np

from digital_demod.carrier_recovery.carrier_frequency_recovery import carrier_recovery_mfd, carrier_frequency_shift
from digital_demod.utilities.rrc_filter import rrc_filter

try:
    from py_dvbs2_fec import DVBS2_FEC_Processor_Bank
except ImportError:
    DVBS2_FEC_Processor_Bank = []
    print('Skipping DVBS2_FEC_Processor_Bank import')

import matplotlib.pyplot as plt

# matplotlib.pyplot.switch_backend('Qt5Agg')
plt.style.use('dark_background')


# noinspection PyUnreachableCode
class DVBS2SymbolProcessor:
    """
    Functionality to detect and output PLFRAMEs from coarsely synchronized symbols

    Assumptions:
     * Symbols are coarsely synced in clock and carrier frequency (exactly what that means: TBD)

    """
    def __init__(self):
        self.fs_hz = None
        self.rrc_beta = 0.2
        self.debug_plots = False


    @staticmethod
    def generate_symbols(iq_resampled: np.ndarray, t_iq_resampled_sec: np.ndarray, fs_resampled_hz: float,
                         fsym_hz: float, rrc_beta: float):
        """
        Performs RRC filtering and decimation of oversampled IQ samples to generate symbols
        :param iq_resampled: Resampled IQ
        :param t_iq_resampled_sec: Time vector for iq_resampled
        :param fs_resampled_hz: Sample rate of resampled IQ
        :param fsym_hz: Symbol rate to generate symbols at
        :param rrc_beta: RRC filter beta
        :return:
          - symbols
          - t_symbols_sec
          - iq_resampled_rrc_filtered
          - oversample_factor
        """
        oversample_factor = int(np.round(fs_resampled_hz / fsym_hz))
        rrc_length = 64 * oversample_factor
        h_rrc, time_idx = rrc_filter(rrc_length, rrc_beta, fsym_hz, fs_resampled_hz)
        h_rrc = h_rrc / np.sum(h_rrc)
        # RRC filter and remove transients
        iq_resampled_rrc_filtered = np.convolve(iq_resampled, h_rrc)
        iq_resampled_rrc_filtered = \
            iq_resampled_rrc_filtered[int(np.floor(rrc_length / 2)):-(int(np.floor(rrc_length / 2 - 1)))]
        symbols = deepcopy(iq_resampled_rrc_filtered[0::oversample_factor])
        t_symbols_sec = deepcopy(t_iq_resampled_sec[0::oversample_factor])
        return symbols, t_symbols_sec, iq_resampled_rrc_filtered, oversample_factor

    @staticmethod
    def generate_samples(symbols: np.ndarray, fsym_hz: float, fs_hz: float, rrc_beta: float):
        # from dsp_utilities.plotting.scatterplot import scatterplot
        oversample_factor = int(np.ceil(np.round(fs_hz / fsym_hz)))
        rrc_length = 64 * oversample_factor
        h_rrc, time_idx = rrc_filter(rrc_length, rrc_beta, fsym_hz, fs_hz)
        #h_rrc = h_rrc / np.sum(h_rrc)
        from scipy.signal import upfirdn
        iq = upfirdn(h_rrc, symbols, up=oversample_factor, down=1)
        iq = iq[int(len(h_rrc)/2):-int(len(h_rrc)/2-oversample_factor)]
        #iq = iq[int(len(h_rrc)/2-1):-int(len(h_rrc)/2-oversample_factor+1)]
        return iq


    @staticmethod
    def coarse_carrier_recovery(iq: np.ndarray, fs_hz: float,
                                iq_resampled: np.ndarray, fs_resampled_hz: float,
                                symbols: np.ndarray, fsym_hz: float):
        """
        Wrapper around MFD carrier recovery routine to process IQ, resampled IQ, and symbol data
        :param iq: IQ data vector
        :param fs_hz: Sample rate of IQ data vector
        :param iq_resampled:  Resampled IQ data vector
        :param fs_resampled_hz:  Sample rate of resampled IQ data vector
        :param symbols:  Symbol vector
        :param fsym_hz: Sample rate of Symbol vector
        :return: iq, iq_resampled, and symbol vectors with frequency corrections
        """
        frequency_offset_hz, metric_snr = carrier_recovery_mfd(iq, 4, fs_hz, 25e3, False)
        iq = carrier_frequency_shift(iq, -1*frequency_offset_hz, t_sec=None, fs_hz=fs_hz)
        symbols = carrier_frequency_shift(symbols, -1*frequency_offset_hz, t_sec=None, fs_hz=fsym_hz)
        iq_resampled = carrier_frequency_shift(iq_resampled, -1*frequency_offset_hz, t_sec=None, fs_hz=fs_resampled_hz)
        return iq, iq_resampled, symbols

