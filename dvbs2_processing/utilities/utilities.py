import os
import sys
import socket
import numpy as np
import argparse
from copy import deepcopy
from bitstring import BitArray

from dvbs2_processing.constant_data import DVBS2_Frame_Length, DVBS2_Pilots, DVBS2_Modulation_Type, DVBS2_Code_Rates, DVBS2_ModCod_Configuration, DVBS2_Pilots


def process_command_line_args(args_override:list = None, input_file_required:bool = True):
    parser = argparse.ArgumentParser(
        description='Process file containing IQ data from a DVB-S2 stream and extract user bits')
    if input_file_required:
        parser.add_argument('--input_file', '-i', type=str, required=True,
                            help='Input file to process')
    else:
        parser.add_argument('--input_file', '-i', type=str, required=False,
                            help='Input file to process')
    parser.add_argument('--fc_estimate_hz', required=False, default=0.0, type=float,
                        help='Initial estimate of center frequency offset in Hz')
    parser.add_argument('--fsym_estimate_hz', required=False, default=None, type=float,
                        help='Initial estimate of symbol rate in Hz')
    parser.add_argument('--fec_processing_mode', '-f', required=False, default='none', type=str,
                        help='FEC processing mode (\'local_processor\', \'zmq_processor\', or \'none\')')
    parser.add_argument('--zmq_fec_server', '-z', required=False, default='', type=str,
                        help='ZMQ server URL if FEC processing mode is zmq_processor')
    parser.add_argument('--frame_lengths', required=False, default='Normal_Frames,Short_Frames', type=str,
                        help='List of allowed frame lengths (\'Normal_Frames\', \'Short_Frames\')')
    parser.add_argument('--mod_types', required=False, default='QPSK, 8PSK, 16APSK, 32APSK, DUMMY, RESERVED', type=str,
                        help='List of allowed modulation types (\'QPSK\', \'8PSK\', \'16APSK\', \'32APSK\', \'DUMMY\', \'RESERVED\')')
    parser.add_argument('--code_rates', required=False, default='1/4, 1/3, 2/5, 1/2, 3/5, 2/3, 3/4, 4/5, 5/6, 8/9, 9/10, DUMMY', type=str,
                        help='List of allowed code rates (\'1/4\', \'1/3\', \'2/5\', \'1/2\', \'3/5\', \'2/3\', \'3/4\', \'4/5\', \'5/6\', \'8/9\', \'9/10\', \'DUMMY\'')
    parser.add_argument('--pilots', required=False, default='Off, On', type=str,
                        help='List of allowed modulation types (\'Off\', \'On\')')
    parser.add_argument('--num_fec_workers', required=False, default=8, type=int,
                        help='Number of FEC worker threads/processes')

    argv = deepcopy(sys.argv[1::])
    if args_override is not None:
        for arg in args_override:
            argv.append(arg)

    args = parser.parse_args(argv)
    if input_file_required:
        filename = args.input_file
        if not os.path.exists(filename):
            sys.exit('Filename does not exist: {}'.format(filename))
    else:
        filename = None
    fc_estimate_hz = args.fc_estimate_hz
    fsym_estimate_hz = args.fsym_estimate_hz
    num_fec_workers = args.num_fec_workers
    zmq_fec_server = ''
    if args.fec_processing_mode.lower() == 'local_processor':
        fec_processing_mode = 'local_processor'
    elif args.fec_processing_mode.lower() == 'zmq_processor':
        fec_processing_mode = 'zmq_processor'
        zmq_fec_server = args.zmq_fec_server
        if zmq_fec_server == '':
            raise Exception('Must specify zmq_fec_server if fec_processing_mode is zmq_processor')
    elif args.fec_processing_mode.lower() == 'none':
        fec_processing_mode = 'none'
    else:
        sys.exit('Unhandled perform_fec_processing mode: {}.  Allowable values: \'local_processor\', \'zmq_processor\', or \'none\''.format(
            args.fec_processing_mode))

    frame_lengths = []
    for frame_length in args.frame_lengths.replace(' ', '').replace('\'', '').split(','):
        try:
            frame_lengths.append(DVBS2_Frame_Length[frame_length])
        except KeyError as ex:
            sys.exit(
                'Unhandled frame_length value: {}.  Allowable values: Normal_Frames, Short_Frames'.format(ex.args[0]))
        except Exception as ex:
            sys.exit('Unhandled exception: {}'.format(ex))

    mod_types = []
    for mod_type in args.mod_types.replace(' ', '').replace('\'', '').split(','):
        if mod_type == 'QPSK':
            mod_types.append(DVBS2_Modulation_Type.QPSK)
        elif mod_type == '8PSK':
            mod_types.append(DVBS2_Modulation_Type.PSK8)
        elif mod_type == '16APSK':
            mod_types.append(DVBS2_Modulation_Type.APSK16)
        elif mod_type == '32APSK':
            mod_types.append(DVBS2_Modulation_Type.APSK32)
        elif mod_type == 'DUMMY':
            mod_types.append(DVBS2_Modulation_Type.DUMMY)
        elif mod_type == 'RESERVED':
            mod_types.append(DVBS2_Modulation_Type.RESERVED)
        else:
            sys.exit(
                'Unhandled mod_types value: {}.  Allowable values: \'QPSK\', \'8PSK\', \'16APSK\', \'32APSK\', \'DUMMY\', \'RESERVED\''.format(
                    mod_type))

    dvbs2_code_rates = []
    for code_rate in args.code_rates.replace(' ', '').split(','):
        code_rate_str = 'RATE_{}'.format(code_rate.replace('/', '_'))
        try:
            dvbs2_code_rates.append(DVBS2_Code_Rates[code_rate_str])
        except KeyError as ex:
            sys.exit(
                'Unhandled frame_length value: {}.  Allowable values: Normal_Frames, Short_Frames'.format(ex.args[0]))
        except Exception as ex:
            sys.exit('Unhandled exception: {}'.format(ex))


    pilots = []
    for pilot in args.pilots.replace(' ', '').replace('\'', '').split(','):
        if pilot == 'Off':
            pilots.append(DVBS2_Pilots.Pilots_Off)
        elif pilot == 'On':
            pilots.append(DVBS2_Pilots.Pilots_On)
        else:
            sys.exit('Unhandled pilots value: {}.  Allowable values: \'OFF\', \'ON\''.format(pilot))
    return filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths, mod_types, dvbs2_code_rates, fec_processing_mode, \
           zmq_fec_server, pilots, num_fec_workers


def filter_and_decimate_bluefile(input_filename, output_filename, center_frequency_hz, filter_bandwidth_hz,
                                 resample_rate_sps, num_input_samples=-1, numtaps=271):
    from dsp_utilities.file_handlers.iq_file import IQFile, IQFileType
    input_file = IQFile(filename=input_filename, file_type=IQFileType.BLUEFILE)
    input_file.read_metadata()
    if num_input_samples > 0:
        input_file.num_samples = num_input_samples
    input_file.read_file()
    # iq_plot(iq=input_file.iq[0:100000], fs_hz=input_file.fs_hz)

    output_file = IQFile(filename=output_filename, file_type=IQFileType.BLUEFILE,
                         iq_data_format=input_file.iq_data_format, iq_data_order=input_file.iq_data_order,
                         fs_hz=np.float(resample_rate_sps), t_start_sec=input_file.t_start_sec)
    output_file.iq_data_format = input_file.iq_data_format

    input_file.iq = input_file.tune(-1 * center_frequency_hz)
    input_file.iq = input_file.low_pass_filter(filter_bandwidth_hz, numtaps=numtaps, window='hamming')
    output_file.iq, resample_frequency_frac = input_file.resample(resample_rate_sps)
    output_file.num_samples = len(output_file.iq)
    output_file.write_file()


def send_via_udp(bits_message, dvbs2_modcod):
    """
    Debug method to send de-FEC'd bits out via UDP to be processed in Wireshark
    """
    ma_header_dict = [0xB8, dvbs2_modcod.modcod.value
                      + 2 ** 5 * dvbs2_modcod.pilots.value
                      + 2 ** 6 * dvbs2_modcod.frame_length.value]
    (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
    #scrambling_sequence = DVBS2_ModCod_Configuration.generate_bbframe_scrambling_sequence(k_bch)
    #bits2 = np.asarray(bits_message) ^ np.asarray(scrambling_sequence)
    data_bytes = np.packbits(bits_message).tobytes()
    udp_ip = "127.0.0.1"
    udp_port = 5005
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(bytes(ma_header_dict) + data_bytes, (udp_ip, udp_port))


def compute_plframe_statistics(symbols, dvbs2_modcod: DVBS2_ModCod_Configuration):
    """
    Computes statistics for PLFRAMEs
    :param symbols: PLFRAME symbols to operate on.  Assume first symbol is the first symbol in the SOF header
    :param dvbs2_modcod: MODCOD of the PLFRAME to process
    """

    # TODO: Add support for DUMMY PLFRAMES

    assert len(symbols) == dvbs2_modcod.get_num_symbols() + 90, 'Expecting number of symbols to be exactly PLFRAME length + 90'

    plheader_indicies = np.linspace(0, 89, 90, dtype=np.int)
    next_plheader_sof_indicies = np.linspace(dvbs2_modcod.get_num_symbols(),
                                             dvbs2_modcod.get_num_symbols() + 25, 26, dtype=np.int)
    # next_plheader_indicies = np.linspace(dvbs2_modcod.get_num_symbols(), dvbs2_modcod.get_num_symbols() + 89,
    #                                      90, dtype=np.int)

    if dvbs2_modcod.pilots == DVBS2_Pilots.Pilots_On:
        pilot_symbol_indices = dvbs2_modcod.get_pilot_symbol_indicies()
    else:
        pilot_symbol_indices = []

    esn0 = 10 * np.log10(1 / np.var(np.append(np.append(
        symbols[pilot_symbol_indices]       -  (1 / np.sqrt(2) + 1j / np.sqrt(2)),
        symbols[plheader_indicies]          -  dvbs2_modcod.generate_pls_symbols()),
        symbols[next_plheader_sof_indicies] -  dvbs2_modcod.generate_sof_symbols())))

    if False:
        from dsp_utilities.plotting.scatterplot import scatterplot
        scatterplot([symbols[pilot_symbol_indices]       -  (1 / np.sqrt(2) + 1j / np.sqrt(2)),
                     symbols[plheader_indicies]          -  dvbs2_modcod.generate_pls_symbols(),
                     symbols[next_plheader_sof_indicies] - dvbs2_modcod.generate_sof_symbols()],
                    labels=['Pilots (std = {})'.format(np.var(symbols[pilot_symbol_indices] - (1 / np.sqrt(2) + 1j / np.sqrt(2)))),
                            'PLHEADER (std = {})'.format(np.var(symbols[plheader_indicies] - dvbs2_modcod.generate_pls_symbols())),
                            'Next PLHEADER (std = {})'.format(np.var(symbols[next_plheader_sof_indicies] - dvbs2_modcod.generate_sof_symbols()))])


    return esn0


def process_phase_measurements(phase_raw):
    """
    Simple function to smooth out and reduce phase wraps without actually unwrapping phase.
    :param phase_raw: Raw phase measurements
    :return: Processed phase measurements

    Notes:
      Can recover phase rotations of frequencies up to approximately tau * 16 * 90 / 8
    """
    # Smooth phase data out to facilitate linear fit
    # phase_raw = np.angle(received_symbols[0:len(training_symbols)] / training_symbols)
    # phase_raw = np.angle(received_symbols[0:len(sof_symbols)] / sof_symbols)

    # See "Robust phase unwrapping algorithm", Z. Xu, B. Huang and S. Xu
    def M(x):
        adjustment_rad = 0.0
        if x > np.pi:
            adjustment_rad = -2 * np.pi
        elif x < -1*np.pi:
            adjustment_rad = 2 * np.pi
        x += adjustment_rad
        return x

    # Duplicate the first element to work around a bug with the case of having a phase wrap between the
    # first and second elements in the array
    phase_raw_2 = np.append(phase_raw[0], phase_raw)
    phase_processed_last = None
    phase_processed = None
    for index in range(0,len(phase_raw)): # This is a really stupid work-around
        if phase_processed_last is None or np.any((phase_processed_last - phase_processed) > 0.1) == True:
            if phase_processed is not None:
                phase_processed_last = phase_processed
                phase_raw_2 = phase_processed
                phase_raw_2 = np.append(phase_raw_2[0], phase_raw_2)
            tau = 0.35
            phi_hat = [phase_raw_2[0]]
            for index in range(1, len(phase_raw_2)):
                phi_hat.append(phi_hat[index-1] + tau * M(phase_raw_2[index] - phi_hat[index - 1]))
            phi = []
            for index in range(0, len(phase_raw_2)):
                phi.append(phi_hat[index] + M(phase_raw_2[index] - phi_hat[index]))
            phase_processed = np.asarray(phi[1::])
        else:
            break

    return phase_processed


def compute_crc8(bbheader_bits_descrambled):
    # Implement Figure 2 in ETSI EN 302 307 V1.2.1
    crc_check = 8 * [0]
    for index in range(0, len(bbheader_bits_descrambled)):
        newbit = crc_check[0] ^ bbheader_bits_descrambled[index]
        crc_check[0] = crc_check[1] ^ newbit
        crc_check[1] = crc_check[2] ^ newbit
        crc_check[2] = crc_check[3]
        crc_check[3] = crc_check[4] ^ newbit
        crc_check[4] = crc_check[5]
        crc_check[5] = crc_check[6] ^ newbit
        crc_check[6] = crc_check[7]
        crc_check[7] = newbit
    return BitArray(crc_check).uint


def get_package_root():
    from pathlib import Path
    return str(Path(__file__).parent.parent)