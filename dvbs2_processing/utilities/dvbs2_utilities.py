import numpy as np
import matplotlib.pyplot as plt
from dsp_utilities.plotting.scatterplot import scatterplot

from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration

def plot_constellation(dvbs2_modcod: DVBS2_ModCod_Configuration, symbols: np.array, fig=None):

    if fig is None:
        fig = plt.figure()

    mapping, constellation = dvbs2_modcod.get_constellation()

    fig = scatterplot(symbols, fig=fig)
    scatterplot(constellation, fig=fig)

    plt.xlim(-1.6, 1.6)
    plt.ylim(-1.6, 1.6)
    plt.title('{}: {}, {}'.format(dvbs2_modcod.modcod_pretty(), dvbs2_modcod.frames_pretty(), dvbs2_modcod.pilots_pretty()))


