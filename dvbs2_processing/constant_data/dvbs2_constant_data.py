import os
import pickle
import numpy as np
from enum import Enum

class DVBS2_ModCods(Enum):
    DVBS2_DUMMY_PLFRAME = 0
    DVBS2_QPSK_1_4 = 1
    DVBS2_QPSK_1_3 = 2
    DVBS2_QPSK_2_5 = 3
    DVBS2_QPSK_1_2 = 4
    DVBS2_QPSK_3_5 = 5
    DVBS2_QPSK_2_3 = 6
    DVBS2_QPSK_3_4 = 7
    DVBS2_QPSK_4_5 = 8
    DVBS2_QPSK_5_6 = 9
    DVBS2_QPSK_8_9 = 10
    DVBS2_QPSK_9_10 = 11
    DVBS2_8PSK_3_5 = 12
    DVBS2_8PSK_2_3 = 13
    DVBS2_8PSK_3_4 = 14
    DVBS2_8PSK_5_6 = 15
    DVBS2_8PSK_8_9 = 16
    DVBS2_8PSK_9_10 = 17
    DVBS2_16APSK_2_3 = 18
    DVBS2_16APSK_3_4 = 19
    DVBS2_16APSK_4_5 = 20
    DVBS2_16APSK_5_6 = 21
    DVBS2_16APSK_8_9 = 22
    DVBS2_16APSK_9_10 = 23
    DVBS2_32APSK_3_4 = 24
    DVBS2_32APSK_4_5 = 25
    DVBS2_32APSK_5_6 = 26
    DVBS2_32APSK_8_9 = 27
    DVBS2_32APSK_9_10 = 28
    DVBS2_RESERVED0 = 29
    DVBS2_RESERVED1 = 30
    DVBS2_RESERVED2 = 31


class DVBS2_Frame_Length(Enum):
    Normal_Frames = 0
    Short_Frames = 1


class DVBS2_Pilots(Enum):
    Pilots_Off = 0
    Pilots_On = 1


class DVBS2_Modulation_Type(Enum):
    #BPSK = 0 # Placeholder for DVB-S2X
    QPSK = 1
    PSK8 = 2
    APSK16 = 3
    APSK32 = 4
    DUMMY = 100
    RESERVED = 101


class DVBS2_Code_Rates(Enum):
    RATE_1_4 = 1
    RATE_1_3 = 2
    RATE_2_5 = 3
    RATE_1_2 = 4
    RATE_3_5 = 5
    RATE_2_3 = 6
    RATE_3_4 = 7
    RATE_4_5 = 8
    RATE_5_6 = 9
    RATE_8_9 = 10
    RATE_9_10 = 11
    RATE_DUMMY = 12
    RATE_RESERVED = 12


class DVBS2_MATYPE1_TS_GS(Enum):
    GENERIC_PACKETIZED = 0
    GENERIC_CONTINUOUS = 1
    RESERVED = 2
    TRANSPORT = 3


class DVBS2_MATYPE1_SIS_MIS(Enum):
    MULTIPLE = 0
    SINGLE = 1


class DVBS2_MATYPE1_CCM_ACM(Enum):
    ACM = 0
    CCM = 1


class DVBS2_MATYPE1_ISSYI(Enum):
    NOT_ACTIVE = 0
    ACTIVE = 1


class DVBS2_MATYPE1_NPD(Enum):
    NOT_ACTIVE = 0
    ACTIVE = 1


class DVBS2_MATYPE1_Rolloff(Enum):
    RO_0_35 = 0.35
    RO_0_25 = 0.25
    RO_0_20 = 0.2
    Reserved = -1



class DVBS2_ModCod_Configuration():
    """
    An object that represents a DVB-S2 modcod configuration
    """
    # From ETSI EN 302 307 V1.2.1 (2009-08), Section 5.5.2.4
    pls_code_scrambling_sequence = [0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1,
                                    0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0]

    # From ETSI EN 302 307 V1.2.1 (2009-08), Section 5.5.2.1
    sof_bits = np.asarray([0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0])

    # ldpc_code, kbch, nbch/kldpc, bch t-error, nldpc, q
    table5and7 = [
        ['1/4', 16008, 16200, 12, 64800, 135],
        ['1/3', 21408, 21600, 12, 64800, 120],
        ['2/5', 25728, 25920, 12, 64800, 108],
        ['1/2', 32208, 32400, 12, 64800, 90],
        ['3/5', 38688, 38880, 12, 64800, 72],
        ['2/3', 43040, 43200, 10, 64800, 60],
        ['3/4', 48408, 48600, 12, 64800, 45],
        ['4/5', 51648, 51840, 12, 64800, 36],
        ['5/6', 53840, 54000, 10, 64800, 30],
        ['8/9', 57472, 57600, 8, 64800, 20],
        ['9/10', 58192, 58320, 8, 64800, 18],
        ['1/4', 3072, 3240, 12, 16200, 36],
        ['1/3', 5232, 5400, 12, 16200, 30],
        ['2/5', 6312, 6480, 12, 16200, 27],
        ['1/2', 7032, 7200, 12, 16200, 25],
        ['3/5', 9552, 9720, 12, 16200, 18],
        ['2/3', 10632, 10800, 12, 16200, 15],
        ['3/4', 11712, 11880, 12, 16200, 12],
        ['4/5', 12432, 12600, 12, 16200, 10],
        ['5/6', 13152, 13320, 12, 16200, 8],
        ['8/9', 14232, 14400, 12, 16200, 5],
    ]

    table6a = np.asarray([
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**2 + 2**3 + 2**5 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2 + 2**4 + 2**5 + 2**6 + 2**8 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**2 + 2**3 + 2**4 + 2**5 + 2**7 + 2**8 + 2**9 + 2**10 + 2**11 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**2 + 2**4 + 2**6 + 2**9 + 2**11 + 2**12 + 2**14 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2 + 2**2 + 2**3 + 2**5 + 2**8 + 2**9 + 2**10 + 2**11 + 2**12 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**2 + 2**4 + 2**5 + 2**7 + 2**8 + 2**9 + 2**10 + 2**12 + 2**13 + 2**14 + 2**15 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**2 + 2**5 + 2**6 + 2**8 + 2**9 + 2**10 + 2**11 + 2**13 + 2**15 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2 + 2**2 + 2**5 + 2**6 + 2**8 + 2**9 + 2**12 + 2**13 + 2**14 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**5 + 2**7 + 2**9 + 2**10 + 2**11 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2 + 2**2 + 2**5 + 2**7 + 2**8 + 2**10 + 2**12 + 2**13 + 2**14 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**2 + 2**3 + 2**5 + 2**9 + 2**11 + 2**12 + 2**13 + 2**16))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2 + 2**5 + 2**6 + 2**7 + 2**9 + 2**11 + 2**12 + 2**16))]),
    ], dtype=np.int8)

    table6b = np.asarray([
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**1 + 2**3 + 2**5 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**6 + 2**8 + 2**11 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**1 + 2**2 + 2**6 + 2**9 + 2**10 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**4 + 2**7 + 2**8 + 2**10 + 2**12 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**2 + 2**4 + 2**6 + 2**8 + 2**9 + 2**11 + 2**13 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**3 + 2**7 + 2**8 + 2**9 + 2**13 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**2 + 2**5 + 2**6 + 2**7 + 2**10 + 2**11 + 2**13 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**5 + 2**8 + 2**9 + 2**10 + 2**11 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**1 + 2**2 + 2**3 + 2**9 + 2**10 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**3 + 2**6 + 2**9 + 2**11 + 2**12 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**4 + 2**11 + 2**12 + 2**14))]),
        np.asarray([np.int8(d) for d in list('{0:0b}'.format(1 + 2**1 + 2**2 + 2**3 + 2**5 + 2**6 + 2**7 + 2**8 + 2**10 + 2**13 + 2**14))]),
    ], dtype=np.object)

    table13 = [
        (DVBS2_ModCods.DVBS2_QPSK_1_4, -2.35),
        (DVBS2_ModCods.DVBS2_QPSK_1_3, -1.24),
        (DVBS2_ModCods.DVBS2_QPSK_2_5, -0.30),
        (DVBS2_ModCods.DVBS2_QPSK_1_2, 1.00),
        (DVBS2_ModCods.DVBS2_QPSK_3_5, 2.23),
        (DVBS2_ModCods.DVBS2_QPSK_2_3, 3.10),
        (DVBS2_ModCods.DVBS2_QPSK_3_4, 4.03),
        (DVBS2_ModCods.DVBS2_QPSK_4_5, 4.68),
        (DVBS2_ModCods.DVBS2_QPSK_5_6, 5.18),
        (DVBS2_ModCods.DVBS2_QPSK_8_9, 6.20),
        (DVBS2_ModCods.DVBS2_QPSK_9_10, 6.42),
        (DVBS2_ModCods.DVBS2_8PSK_3_5, 5.50),
        (DVBS2_ModCods.DVBS2_8PSK_2_3, 6.62),
        (DVBS2_ModCods.DVBS2_8PSK_3_4, 7.91),
        (DVBS2_ModCods.DVBS2_8PSK_5_6, 9.35),
        (DVBS2_ModCods.DVBS2_8PSK_8_9, 10.69),
        (DVBS2_ModCods.DVBS2_8PSK_9_10, 10.98),
        (DVBS2_ModCods.DVBS2_16APSK_2_3, 8.97),
        (DVBS2_ModCods.DVBS2_16APSK_3_4, 10.21),
        (DVBS2_ModCods.DVBS2_16APSK_4_5, 11.03),
        (DVBS2_ModCods.DVBS2_16APSK_5_6, 11.61),
        (DVBS2_ModCods.DVBS2_16APSK_8_9, 12.89),
        (DVBS2_ModCods.DVBS2_16APSK_9_10, 13.13),
        (DVBS2_ModCods.DVBS2_32APSK_3_4, 12.73),
        (DVBS2_ModCods.DVBS2_32APSK_4_5, 13.64),
        (DVBS2_ModCods.DVBS2_32APSK_5_6, 14.28),
        (DVBS2_ModCods.DVBS2_32APSK_8_9, 15.69),
        (DVBS2_ModCods.DVBS2_32APSK_9_10, 16.05)
    ]


    def __init__(self, modcod, frame_length, pilots, rolloff):
        assert type(modcod) is DVBS2_ModCods, 'Expecting type(modcod) == DVBS2_ModCods'
        assert type(frame_length) is DVBS2_Frame_Length, 'Expecting type(frame_length) == DVBS2_Frame_Length'
        assert type(pilots) is DVBS2_Pilots, 'Expecting type(pilots) == DVBS2_Pilots'
        assert type(rolloff) is DVBS2_MATYPE1_Rolloff, 'Expecting type(rolloff) == DVBS2_MATYPE1_Rolloff'

        if (modcod.name.find('9/10') >= 0) and (modcod.name.find('QPSK') == -1) and (frame_length is DVBS2_Frame_Length.Short_Frames):
            raise Exception('ModCod {} is not supported for Short frames'.format(modcod.name))

        self.modcod = modcod
        self.frame_length = frame_length
        self.pilots = pilots
        self.rolloff = rolloff

        self.num_symbols = None
        self.pls_symbols = None

        # Generator matrizx from 5.5.2.4
        self.pls_code_g_matrix = [0b01010101010101010101010101010101, 0b00110011001100110011001100110011,
                                  0b00001111000011110000111100001111, 0b00000000111111110000000011111111,
                                  0b00000000000000001111111111111111, 0b11111111111111111111111111111111]

        self.pls_code_h_matrix = [0b10101010000000000000000000000000,
                                  0b01101001000000000000000000000000,
                                  0b11100000100000000000000000000000,
                                  0b11010000010000000000000000000000,
                                  0b10110000001000000000000000000000,
                                  0b01110000000100000000000000000000,
                                  0b10011000000010000000000000000000,
                                  0b01011000000001000000000000000000,
                                  0b00111000000000100000000000000000,
                                  0b11111000000000010000000000000000,
                                  0b11001000000000001000000000000000,
                                  0b11000100000000000100000000000000,
                                  0b10100100000000000010000000000000,
                                  0b01100100000000000001000000000000,
                                  0b10001100000000000000100000000000,
                                  0b01001100000000000000010000000000,
                                  0b00101100000000000000001000000000,
                                  0b11101100000000000000000100000000,
                                  0b10010100000000000000000010000000,
                                  0b01010100000000000000000001000000,
                                  0b00110100000000000000000000100000,
                                  0b11110100000000000000000000010000,
                                  0b00011100000000000000000000001000,
                                  0b11011100000000000000000000000100,
                                  0b10111100000000000000000000000010,
                                  0b01111100000000000000000000000001]
        self.pls_code_h_matrix = [
            [1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,1,1,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [1,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [1,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,1,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [1,0,0,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,1,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,1,1,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [1,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
            [1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
            [0,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0],
            [0,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
            [1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],
            [1,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0],
            [0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0],
            [0,0,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0],
            [1,1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0],
            [0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0],
            [1,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0],
            [1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0],
            [0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]
        ]

        #0b10000010111010011110100110010110
        #0b01000001110101011101010101010101
        #0b00100011101100110011001100110011
        #0b00010000011111110000000011111111
        #0b00001011000011111000111100001111
        #0b00000100000000000111111111111111

        self.pls_code = self.generate_pls_code()

    def __repr__(self):
        return 'ModCod: {0:13}   Frame Length: {1:27}   Pilots: {2:11}'.format(self.modcod_pretty(), self.frames_pretty(), self.pilots_pretty())


    def modcod_pretty(self):
        return self.modcod.name.replace('DVBS2_', '').replace('K_', 'K ').replace('_', '/')


    def modulation_type_and_coding_rate(self):
        if 'DVBS2_QPSK' in self.modcod.name:
            modtype = 'QPSK'
        elif 'DVBS2_8PSK' in self.modcod.name:
            modtype = '8PSK'
        elif 'DVBS2_16APSK' in self.modcod.name:
            modtype = '16APSK'
        elif 'DVBS2_32APSK' in self.modcod.name:
            modtype = '32APSK'
        else:
            return None
        code_rate = self.modcod.name.replace('DVBS2_', '').split('_')[1] + '/' \
                    + self.modcod.name.replace('DVBS2_', '').split('_')[2]
        return modtype, code_rate


    def modulation_type_enum(self):
        if 'DVBS2_QPSK' in self.modcod.name:
            modtype = DVBS2_Modulation_Type.QPSK
        elif 'DVBS2_8PSK' in self.modcod.name:
            modtype = DVBS2_Modulation_Type.PSK8
        elif 'DVBS2_16APSK' in self.modcod.name:
            modtype = DVBS2_Modulation_Type.APSK16
        elif 'DVBS2_32APSK' in self.modcod.name:
            modtype = DVBS2_Modulation_Type.APSK32
        elif 'DUMMY' in self.modcod.name:
            modtype = DVBS2_Modulation_Type.DUMMY
        elif 'RESERVED' in self.modcod.name:
            modtype = DVBS2_Modulation_Type.RESERVED
        else:
            modtype = None
        return modtype


    def frames_pretty(self):
        if self.frame_length == DVBS2_Frame_Length.Short_Frames:
            return 'Short Frames (16,200 bits)'
        if self.frame_length == DVBS2_Frame_Length.Normal_Frames:
            return 'Normal Frames (64,800 bits)'


    def pilots_pretty(self):
        return self.pilots.name.replace('_', ' ')


    def get_mod_order(self):
        """
        Returns number of symbols in PL frame for a given modcod.  90 symbols of PL header is included in this value.
        :return:
        """
        # Table 11 of EN 302 307

        if 'DVBS2_QPSK' in self.modcod.name:
            mod_order = 4
        elif 'DVBS2_8PSK' in self.modcod.name:
            mod_order = 8
        elif 'DVBS2_16APSK' in self.modcod.name:
            mod_order = 16
        elif 'DVBS2_32APSK' in self.modcod.name:
            mod_order = 32
        elif 'DVBS2_DUMMY_PLFRAME' in self.modcod.name:
            mod_order = 4
        else:
            raise('Unhandled ModCod')
        return mod_order


    def get_constellation(self):
        if 'DVBS2_QPSK' in self.modcod.name:
            # Figure 9 in ETSI EN302-300
            mapping = [0, 2, 3, 1]
            phi = np.pi/4 + (np.pi/2) * np.arange(0, 4)
            constellation = np.exp(1j * phi)
        elif 'DVBS2_8PSK' in self.modcod.name:
            # Figure 10 in ETSI EN302-300
            mapping = [0, 4, 6, 2, 3, 7, 5, 1]
            phi = np.pi/4 + (np.pi/4) * np.arange(0, 8)
            constellation = np.exp(1j * phi)
        elif 'DVBS2_16APSK' in self.modcod.name:
            # Figure 11 in ETSI EN302-300
            mapping = [12, 14, 15, 13, 4, 0, 8, 10, 2, 6, 7, 3, 11, 9, 1, 5]
            phi = np.append(np.pi/4 + (np.pi/2) * np.arange(0, 4), np.pi/12 + (np.pi/6) * np.arange(0, 12))
            r2 = 1
            if self.modcod.name == 'DVBS2_16APSK_2_3':
                r1 = 1/3.15
            elif self.modcod.name == 'DVBS2_16APSK_3_4':
                r1 = 1/2.85
            elif self.modcod.name == 'DVBS2_16APSK_4_5':
                r1 = 1/2.75
            elif self.modcod.name == 'DVBS2_16APSK_5_6':
                r1 = 1/2.70
            elif self.modcod.name == 'DVBS2_16APSK_8_9':
                r1 = 1/2.60
            elif self.modcod.name == 'DVBS2_16APSK_9_10':
                r1 = 1/2.57
            else:
                raise('Unrecognized modcod')
            a = [r1, r1, r1, r1, r2, r2, r2, r2, r2, r2, r2, r2, r2, r2, r2, r2]
            constellation = a * np.exp(1j * phi)
        elif 'DVBS2_32APSK' in self.modcod.name:
            # Figure 11 in ETSI EN302-300
            mapping = [17, 21, 23, 19, 16, 0, 1, 5, 4, 20, 22, 6, 7, 3, 2, 18, 24, 8, 25, 9, 13, 29, 12, 28, 30, 14, 31, 15, 11, 27, 10, 26]
            phi = np.append(np.append(np.pi / 4 + (np.pi / 2) * np.arange(0, 4), np.pi / 12 + (np.pi / 6) * np.arange(0, 12)), 0 + (np.pi / 8) * np.arange(0, 16))
            r3 = 1
            if self.modcod.name == 'DVBS2_32APSK_3_4':
                r1 = r3 / 5.27
                r2 = 2.84 * r1
            elif self.modcod.name == 'DVBS2_32APSK_4_5':
                r1 = r3 / 4.87
                r2 = 2.72 * r1
            elif self.modcod.name == 'DVBS2_32APSK_5_6':
                r1 = r3 / 4.64
                r2 = 2.64 * r1
            elif self.modcod.name == 'DVBS2_32APSK_8_9':
                r1 = r3 / 4.33
                r2 = 2.54 * r1
            elif self.modcod.name == 'DVBS2_32APSK_9_10':
                r1 = r3 / 4.30
                r2 = 2.53 * r1
            else:
                raise ('Unrecognized modcod')
            a = [r1, r1, r1, r1, r2, r2, r2, r2, r2, r2, r2, r2, r2, r2, r2, r2, r3, r3, r3, r3, r3, r3, r3, r3, r3, r3, r3, r3, r3, r3, r3, r3]
            constellation = a * np.exp(1j * phi)
        elif 'DVBS2_DUMMY_PLFRAME' in self.modcod.name:
            mapping = [0]
            constellation = [np.exp(1j * np.pi/4)]
        else:
            raise ('Unhandled ModCod')
        constellation = constellation / np.sqrt(np.mean(np.abs(constellation)**2))
        return mapping, constellation


    def get_coding_parameters(self):
        """
        Get LDPC and BCH coding parameters
        :return: Tuple with (kbch, Nbch/kldpc, t-error correction, nldpc)

        Example
            modcod = DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_3_4, frame_length=DVBS2_Frame_Length.Short_Frames, pilots=DVBS2_Pilots.Pilots_On)
            (k_bch, k_ldpc, t_error, n_ldpc, q) = modcod.get_coding_parameters()
        """
        row = [row for row in self.table5and7 if row[0] == self.get_code_rate() and row[4] == self.get_ldpc_codeword_length()]
        if len(row) != 1:
            return (None,      None,      None,      None,      None)
        else:
            return (row[0][1], row[0][2], row[0][3], row[0][4], row[0][5])


    def get_code_rate(self):
        """
        Returns a string representing the code rate
        :return: String representing the code rate
        """
        modcod_split = self.modcod.name.split('_')
        if len(modcod_split) == 4:
            return modcod_split[-2] + '/' + modcod_split[-1]
        elif modcod_split[-1].find('PLFRAME') != -1:
            return ('DUMMY')
        elif modcod_split[-1].find('RESERVED') != -1:
            return ('RESERVED')
        else:
            raise Exception('Unhandled Code Rate: {}'.format(self.modcod.name))


    def get_code_rate_enum(self):
        """
        Returns a string representing the code rate
        :return: String representing the code rate
        """
        modcod_split = self.modcod.name.split('_')
        if len(modcod_split) == 4:
            if modcod_split[-2] + '/' + modcod_split[-1] == '1/4':
                return DVBS2_Code_Rates.RATE_1_4
            elif modcod_split[-2] + '/' + modcod_split[-1] == '1/3':
                return DVBS2_Code_Rates.RATE_1_3
            elif modcod_split[-2] + '/' + modcod_split[-1] == '2/5':
                return DVBS2_Code_Rates.RATE_2_5
            elif modcod_split[-2] + '/' + modcod_split[-1] == '1/2':
                return DVBS2_Code_Rates.RATE_1_2
            elif modcod_split[-2] + '/' + modcod_split[-1] == '3/5':
                return DVBS2_Code_Rates.RATE_3_5
            elif modcod_split[-2] + '/' + modcod_split[-1] == '2/3':
                return DVBS2_Code_Rates.RATE_2_3
            elif modcod_split[-2] + '/' + modcod_split[-1] == '3/4':
                return DVBS2_Code_Rates.RATE_3_4
            elif modcod_split[-2] + '/' + modcod_split[-1] == '4/5':
                return DVBS2_Code_Rates.RATE_4_5
            elif modcod_split[-2] + '/' + modcod_split[-1] == '5/6':
                return DVBS2_Code_Rates.RATE_5_6
            elif modcod_split[-2] + '/' + modcod_split[-1] == '8/9':
                return DVBS2_Code_Rates.RATE_8_9
            elif modcod_split[-2] + '/' + modcod_split[-1] == '9/10':
                return DVBS2_Code_Rates.RATE_9_10
        elif modcod_split[-1].find('PLFRAME') != -1:
            return DVBS2_Code_Rates.RATE_DUMMY
        elif modcod_split[-1].find('RESERVED') != -1:
            return DVBS2_Code_Rates.RATE_RESERVED
        else:
            raise Exception('Unhandled Code Rate: {}'.format(self.modcod.name))


    def get_code_rate_numeric(self):
        """
        Returns a string representing the code rate
        :return: String representing the code rate
        """
        modcod_split = self.modcod.name.split('_')
        return int(modcod_split[-2]) / int(modcod_split[-1])


    def get_ldpc_codeword_length(self):
        if self.frame_length == DVBS2_Frame_Length.Short_Frames:
            return 16200
        elif self.frame_length == DVBS2_Frame_Length.Normal_Frames:
            return 64800
        else:
            raise('Unhandled frame length')


    def get_bch_primitive_polynomial(self):
        """
        Gets the product of the first t generator polynomials as described in section 5.3.1
        :return:
        """
        (kbch, kldpc, t_error, nldpc, q) = self.get_coding_parameters()
        if self.frame_length == DVBS2_Frame_Length.Short_Frames:
            table = self.table6b
        elif self.frame_length == DVBS2_Frame_Length.Normal_Frames:
            table = self.table6a
        else:
            raise('Unhandled frame length')
        return table[0]


    def get_bch_generator_polynomial(self):
        """
        Gets the product of the first t generator polynomials as described in section 5.3.1
        :return:
        """
        (kbch, kldpc, t_error, nldpc, q) = self.get_coding_parameters()
        if self.frame_length == DVBS2_Frame_Length.Short_Frames:
            table = self.table6b
        elif self.frame_length == DVBS2_Frame_Length.Normal_Frames:
            table = self.table6a
        else:
            raise('Unhandled frame length')
        cumprod = np.asarray(table[0], dtype=np.int8)
        for index in range(1, t_error):
            cumprod = np.mod(np.polymul(cumprod, table[index]), 2)
        return cumprod


    def generate_pls_code(self):
        """
        Generates the 64 bit PLS code

        Note: PLS codes have a hamming distance of 32 with respect to PLS codes for other modcods, except
        for Normal vs Short frames, where the distance is 64 (PLS code is inverted)
        :return:
        """
        pls_code = self.pilots.value * 2**6 + self.frame_length.value * 2**5 + int('{:05b}'.format(31 & self.modcod.value)[::-1], 2)
        pls_code_encoded = 0
        for bit in range(0, 6):
            pls_code_encoded = pls_code_encoded ^ (int(((pls_code & 2 ** bit) / (2 ** bit))) * self.pls_code_g_matrix[bit])

        if int((pls_code & 2 ** 6) / 2 ** 6) == 0:
            pls_code_encoded = [int(c) for c in (''.join(['{0},{1},'.format(c, c) for c in '{0:032b}'.format(pls_code_encoded)])).replace(',', '')]
        else:
            pls_code_encoded = [int(c) for c in (''.join(['{0},{1},'.format(c, int(c) ^ 1) for c in '{0:032b}'.format(pls_code_encoded)])).replace(',', '')]

        pls_code_encoded = np.asarray(pls_code_encoded)
        return pls_code_encoded


    def generate_pls_symbols(self, force_update=False):
        """
        Generates PLS pi/2 QPSK symbols
        :return: 
        """
        if self.pls_symbols is None or force_update:
            plheader_bits = np.append(self.sof_bits, self.pls_code_scrambling_sequence ^ self.generate_pls_code())
            plheader_amplitude = 1.0 / np.sqrt(2)

            plheader_symbols = np.empty(len(plheader_bits), dtype=np.complex128)
            plheader_symbols[0:90:2] = plheader_amplitude * (
                        1 - 2 * plheader_bits[0:90:2]) + 1j * plheader_amplitude * (1 - 2 * plheader_bits[0:90:2])
            plheader_symbols[1:90:2] = -1.0 * plheader_amplitude * (
                        1 - 2 * plheader_bits[1:90:2]) + 1j * plheader_amplitude * (1 - 2 * plheader_bits[1:90:2])

            self.plheader_symbols = plheader_symbols
        return self.plheader_symbols


    def get_num_slots(self):
        """
        Returns number of symbols in PL frame for a given modcod.  90 symbols of PL header is included in this value.
        :return:
        """
        # Table 11 of EN 302 307

        if 'DVBS2_QPSK' in self.modcod.name:
            if self.frame_length == DVBS2_Frame_Length.Short_Frames:
                num_slots = 90
            elif self.frame_length == DVBS2_Frame_Length.Normal_Frames:
                num_slots = 360
            else:
                raise ('Unhandled frame size')
        elif 'DVBS2_8PSK' in self.modcod.name:
            if self.frame_length == DVBS2_Frame_Length.Short_Frames:
                num_slots = 60
            elif self.frame_length == DVBS2_Frame_Length.Normal_Frames:
                num_slots = 240
            else:
                raise ('Unhandled frame size')
        elif 'DVBS2_16APSK' in self.modcod.name:
            if self.frame_length == DVBS2_Frame_Length.Short_Frames:
                num_slots = 45
            elif self.frame_length == DVBS2_Frame_Length.Normal_Frames:
                num_slots = 180
            else:
                raise ('Unhandled frame size')
        elif 'DVBS2_32APSK' in self.modcod.name:
            if self.frame_length == DVBS2_Frame_Length.Short_Frames:
                num_slots = 36
            elif self.frame_length == DVBS2_Frame_Length.Normal_Frames:
                num_slots = 144
            else:
                raise ('Unhandled frame size')
        elif 'DVBS2_DUMMY_PLFRAME' in self.modcod.name:
            num_slots = 36
        else:
            raise('Unhandled ModCod')
        return num_slots


    def get_num_symbols(self, force_update=False):
        """
        Returns number of symbols in PL frame for a given modcod.  90 symbols of PL header is included in this value.
        :return:
        """
        if self.num_symbols is None or force_update:
            # Table 11 of EN 302 307
            num_symbols_per_slot = 90
            num_symbols_per_pilot_block = 36
            num_slots = self.get_num_slots()

            if self.pilots == DVBS2_Pilots.Pilots_On:
                num_symbols = int(90 + num_slots * num_symbols_per_slot + (np.floor(num_slots / 16 - 1/16) * num_symbols_per_pilot_block))
            elif self.pilots == DVBS2_Pilots.Pilots_Off:
                num_symbols = int(90 + num_slots * num_symbols_per_slot)
                #raise ('Unhandled pilots configuration')
            self.num_symbols = num_symbols
        return self.num_symbols


    def get_pilot_symbol_indicies(self):
        pilot_symbol_indicies = []
        if self.pilots == DVBS2_Pilots.Pilots_Off:
            return pilot_symbol_indicies

        num_slots_per_pilot_block = 16
        num_symbols_per_slot = 90
        num_symbols_per_pilot_block = 36
        num_slots = self.get_num_slots()
        num_symbols = self.get_num_symbols()

        num_pilot_blocks = int(np.floor(num_slots/16 - 1/16))
        for pilot_block in range(0, num_pilot_blocks):
            start_index = 90+(pilot_block+1)*num_slots_per_pilot_block*num_symbols_per_slot+pilot_block*num_symbols_per_pilot_block
            end_index = start_index + 35
            for pilot_index in np.linspace(start_index, end_index, num_symbols_per_pilot_block):
                pilot_symbol_indicies.append(int(pilot_index))

        return pilot_symbol_indicies


    def generate_ldpc_bits(self, databits):
        """
        Generates LDPC FEC bits and appends them to input bit array
        :param databits:
        :return:
        """
        assert type(databits[0]) is np.int8 or type(databits[0]) is np.uint8, 'Expecting data bits to be of type numpy.int8'

        from dvbs2_processing.constant_data.dvbs2_ldpc_parity_bit_accumulator_addresses import dvbs2_ldpc_parity_bit_accumulator_addresses
        code_rate = self.get_code_rate()
        (kbch, kldpc, t_error, nldpc, q) = self.get_coding_parameters()
        ldpc_parity_bit_accumulator_addresses = dvbs2_ldpc_parity_bit_accumulator_addresses[nldpc][code_rate]
        p = np.zeros(nldpc - kldpc, dtype=np.int8)

        compute_using_parity_matrix = False
        if compute_using_parity_matrix:
            P = self.compute_ldpc_parity_matrix()
            p = np.mod(P @ databits, 2)
        else:
            for idx, parity_bit_accumulator_addresses in enumerate(ldpc_parity_bit_accumulator_addresses):
                for m in range(0, 360):
                    databit_index = m + idx * 360
                    for parity_bit_accumulator_address in parity_bit_accumulator_addresses:
                        parity_bit_index = np.mod(parity_bit_accumulator_address + np.mod(m, 360) * q, nldpc - kldpc)
                        p[parity_bit_index] = p[parity_bit_index] ^ databits[databit_index]

            for i in range(1, nldpc - kldpc):
                p[i] = p[i] ^ p[i - 1]

        return np.append(databits, p)


    def compute_ldpc_parity_matrix(self):
        """
        Computes the LDPC parity matrix

        :return: nldpc-kldpc X kldpc parity matrix
        """
        from dvbs2_processing.constant_data.dvbs2_ldpc_parity_bit_accumulator_addresses import dvbs2_ldpc_parity_bit_accumulator_addresses
        code_rate = self.get_code_rate()
        (kbch, kldpc, t_error, nldpc, q) = self.get_coding_parameters()
        ldpc_parity_bit_accumulator_addresses = dvbs2_ldpc_parity_bit_accumulator_addresses[nldpc][code_rate]

        P = np.zeros((nldpc - kldpc, kldpc), dtype=np.uint8)  # N-K x K
        d_ = [[] for i in range(kldpc)]
        for idx, parity_bit_accumulator_addresses in enumerate(ldpc_parity_bit_accumulator_addresses):
            for m in range(0, 360):
                databit_index = m + idx * 360
                for parity_bit_accumulator_address in parity_bit_accumulator_addresses:
                    parity_bit_index = np.mod(parity_bit_accumulator_address + np.mod(m, 360) * q, nldpc - kldpc)
                    d_[databit_index].append(parity_bit_index)

        for col_index, databit_indicies in enumerate(d_):
            for databit_index in databit_indicies:
                P[databit_index, col_index] = 1

        for row_index in range(1, nldpc - kldpc):
            P[row_index, :] = P[row_index - 1, :] ^ P[row_index, :]
        # p = np.mod(P @ databits, 2)

        return P


    def generate_bch_bits(self, databits):
        """
        Generates LDPC FEC bits and appends them to input bit array
        :param databits:
        :return:
        """
        from pyGF2 import gf2_mul, gf2_div

        assert type(databits[0]) is np.int8 or type(databits[0]) is np.uint8, 'Expecting data bits to be of type numpy.int8'
        (kbch, kldpc, t_error, nldpc, q) = self.get_coding_parameters()
        assert (len(databits)) == kbch, 'Expecting {} databits for modcod = '.format(kbch, self.modcod.name)
        g = self.get_bch_generator_polynomial()
        x = np.zeros(kldpc - kbch + 1, dtype=np.int8)
        x[-1] = 1
        m = gf2_mul(np.flip(np.asarray(databits, dtype=np.uint8)), np.asarray(x, dtype=np.uint8))
        m = np.pad(m, pad_width=(0, kldpc-len(m)), mode='constant', constant_values=(0, 0))
        (q, r) = gf2_div(np.asarray(m, dtype=np.uint8), np.asarray(np.flip(g), dtype=np.uint8))
        r = np.pad(r, pad_width=[0, kldpc-kbch-len(r)], mode='constant', constant_values=0)

        m[0:(len(r))] = r
        m = np.flip(m)

        return m


    def get_threshold_esn0(self):
        for row in self.table13:
            if self.modcod == row[0]:
                return row[1]
        raise Exception('Unhandled modcod: {}'.format(self.modcod))


    @staticmethod
    def generate_sof_symbols():
        """
        Generates PLS pi/2 QPSK symbols
        :return:
        """
        plheader_bits = DVBS2_ModCod_Configuration.sof_bits
        plheader_amplitude = 1.0 / np.sqrt(2)

        sof_symbols = np.empty(len(plheader_bits), dtype=np.complex128)
        sof_symbols[0:26:2] = plheader_amplitude * (1 - 2 * plheader_bits[0:26:2]) + 1j * plheader_amplitude * (1 - 2 * plheader_bits[0:26:2])
        sof_symbols[1:26:2] = -1.0 * plheader_amplitude * (1 - 2 * plheader_bits[1:26:2]) + 1j * plheader_amplitude * (1 - 2 * plheader_bits[1:26:2])

        return sof_symbols

    @staticmethod
    def generate_dvb_s2_pls_data():
        """
        Generates a list of DVB-S2 carrier configurations with associated PLS vectors

        :return:
        """
        cache_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'dvbs2_constant_data.cache')
        dvbs2_pl_header_data = []
        if os.path.exists(cache_filename):
            with open(cache_filename, 'rb') as fp:
                dvbs2_pl_header_data = pickle.load(fp)
                # If the previous line fails then you likely need to add the following import to the top-level calling
                # script:
                #
                # from dvbs2_processing.constant_data import DVBS2_ModCods, DVBS2_Frame_Length, DVBS2_Pilots, \
                #                 DVBS2_Modulation_Type, DVBS2_Code_Rates, DVBS2_MATYPE1_TS_GS, DVBS2_MATYPE1_SIS_MIS, \
                #                 DVBS2_MATYPE1_CCM_ACM, DVBS2_MATYPE1_ISSYI, DVBS2_MATYPE1_Rolloff, DVBS2_MATYPE1_NPD, \
                #                 DVBS2_ModCod_Configuration
                #
                # If I was a better Python developer I would understand why this is the case and fix it.
        else:
            print('Cache file \'{}\' does not exist.  Call DVBS2_ModCod_Configuration.generate_cache_files() to generate cached data to accelerate processing.  enerating data.'.format(cache_filename))

            dvbs2_pl_header_data = []
            pls_code_g_matrix = [0b01010101010101010101010101010101, 0b00110011001100110011001100110011,
                                 0b00001111000011110000111100001111, 0b00000000111111110000000011111111,
                                 0b00000000000000001111111111111111, 0b11111111111111111111111111111111]

            for frame_length in DVBS2_Frame_Length:
                for pilots in DVBS2_Pilots:
                    for modcod in DVBS2_ModCods:
                        dvbs2_modcod = DVBS2_ModCod_Configuration(modcod=modcod, frame_length=frame_length, pilots=pilots, rolloff=DVBS2_MATYPE1_Rolloff.Reserved)
                        pls_code_encoded = dvbs2_modcod.generate_pls_code()
                        pls_symbols = dvbs2_modcod.generate_pls_symbols()
                        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
                        if k_bch is not None:
                            bbframe_scrambling_sequence = dvbs2_modcod.generate_bbframe_scrambling_sequence(k_bch)
                        else:
                            bbframe_scrambling_sequence = None
                        if (dvbs2_modcod.modcod.name.find('RESERVED') == -1):
                            num_symbols = dvbs2_modcod.get_num_symbols()
                        else:
                            num_symbols = None
                        dvbs2_pl_header_data.append({'dvbs2_modcod': dvbs2_modcod, 'num_occurrences': 0, 'pls_code': pls_code_encoded, 'pls_code_encoded': pls_code_encoded, 'pls_symbols': pls_symbols, 'num_symbols': num_symbols, 'bbframe_scrambling_sequence': bbframe_scrambling_sequence})

        return dvbs2_pl_header_data


    @staticmethod
    def generate_pl_descrambling_sequence(n=0):
        cache_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'pl_descrambling_sequence.cache')
        if os.path.exists(cache_filename):
            with open(cache_filename, 'rb') as fp:
                C = pickle.load(fp)
        else:
            print('Cache file \'{}\' does not exist.  Call DVBS2_ModCod_Configuration.generate_cache_files() to generate cached data to accelerate processing.  Generating data.'.format(cache_filename))

            x = np.zeros(2**18-20+1+18).astype(int)
            y = np.zeros(2**18-20+1+18).astype(int)
            z = np.zeros(2**18-2+1).astype(int)
            R = np.zeros(66419+1).astype(int)

            x[0] = int(1)
            x[1:18] = [int(n) for n in np.zeros(17)]
            y[0:18] = [int(n) for n in np.ones(18)]

            for i in range(0, 2**18-20+1):
                x[i+18] = x[i+7] ^ x[i]
                y[i+18] = y[i+10] ^ y[i+7] ^ y[i+5] ^ y[i]

            for i in range(0, 2**18-2+1):
                z[i] = x[np.mod(i+n, 2**18-1)] ^ y[i]

            for i in range(0, 66419+1):
                R[i] = 2 * z[np.mod(i + 131072, 2 ** 18 - 1)] ^ z[i]

            C = np.exp(-1j*R*np.pi/2)

        return C


    @staticmethod
    def generate_bbframe_scrambling_sequence(n=0):
        seed = 0b100101010000000
        result = seed
        mask = 0b000000000000011
        scrambling_sequence = []
        for index in range(0, n):
            if (mask & result) == 1 or (mask & result) == 2:
                scrambling_sequence.append(1)
            else:
                scrambling_sequence.append(0)
            result = (result >> 1) + scrambling_sequence[-1] * 2**14
        return scrambling_sequence


    @staticmethod
    def generate_cache_files():

        cache_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'dvbs2_constant_data.cache')
        dvbs2_pl_header_data = DVBS2_ModCod_Configuration.generate_dvb_s2_pls_data()
        with open(cache_filename, 'wb') as fp:
            pickle.dump(dvbs2_pl_header_data, fp)

        cache_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'pl_descrambling_sequence.cache')
        C = DVBS2_ModCod_Configuration.generate_pl_descrambling_sequence()
        with open(cache_filename, 'wb') as fp:
            pickle.dump(C, fp)


if __name__ == '__main__':
    DVBS2_ModCod_Configuration.generate_cache_files()

    if False:
        modcod = DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_8_9, frame_length=DVBS2_Frame_Length.Normal_Frames, pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20)
        modcod = DVBS2_ModCod_Configuration(modcod=DVBS2_ModCods.DVBS2_16APSK_3_4, frame_length=DVBS2_Frame_Length.Short_Frames, pilots=DVBS2_Pilots.Pilots_On, rolloff=DVBS2_MATYPE1_Rolloff.RO_0_20)
        modcod.get_bch_generator_polynomial()
        print(modcod.get_num_symbols())
        dvbs2_pl_header_data = DVBS2_ModCod_Configuration.generate_dvb_s2_pls_data()
