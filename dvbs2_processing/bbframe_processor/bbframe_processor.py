from copy import deepcopy
from typing import List
import uuid
import numpy as np

from dsp_utilities.plotting.scatterplot import scatterplot

from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration, DVBS2_Frame_Length, DVBS2_Pilots, \
    DVBS2_Modulation_Type, DVBS2_Code_Rates, DVBS2_ModCods

from dvbs2_processing.data_structures.data_structures import DVBS2_BBHEADER

from dvbs2_processing.signal_utilities.interleaving import interleave_plframe, deinterleave_plframe

from dvbs2_processing.utilities import send_via_udp

from dvbs2_processing.signal_utilities.modulation import demodulate_plframe, modulate_plframe

from dvbs2_processing.fec_server.fec_server import FECServer

try:
    import bchlib
except ImportError:
    print('Skipping bchlib import')

try:
    from py_dvbs2_fec import DVBS2_FEC_Processor_Bank
except ImportError:
    DVBS2_FEC_Processor_Bank = None
    print('Skipping DVBS2_FEC_Processor_Bank import')

import matplotlib.pyplot as plt

# matplotlib.pyplot.switch_backend('Qt5Agg')
plt.style.use('dark_background')


# noinspection PyUnreachableCode
class DVBS2BBFrameProcessor:
    """
    Functionality to detect and output PLFRAMEs from coarsely synchronized symbols

    Assumptions:
     * Symbols are coarsely synced in clock and carrier frequency (exactly what that means: TBD)

    """
    def __init__(self,
                 dvbs2_frame_length: List[DVBS2_Frame_Length] = None,
                 dvbs2_pilots: List[DVBS2_Pilots] = None,
                 dvbs2_modulation_type: List[DVBS2_Modulation_Type] = None,
                 dvbs2_code_rates: List [DVBS2_Code_Rates] = None,
                 fec_processing_mode: str = 'none',
                 threading_config: int = None,
                 zmq_fec_server: str = None):
        """
        Constructor for DVBS2StreamProcessor object
        :param dvbs2_frame_length: list of DVBS2_Frame_Length values that the processor can support
        :type dvbs2_frame_length: list[DVBS2_Frame_Length]
        :param dvbs2_pilots: list of DVBS2_Pilots values that the processor can support
        :type dvbs2_pilots: list[DVBS2_Pilots]
        :param dvbs2_modulation_type: list of DVBS2_Modulation_Type values that the processor can support
        :type dvbs2_modulation_type: list[DVBS2_Modulation_Type]
        :param fec_processing_mode: Indicates FEC mode
        :type fec_processing_mode: str
        :param threading_config: Configuration value for threading (0 = no multithreading, 1,2,3,16 = multithreading)
        :type threading_config: int
        :param zmq_fec_server: URL for ZMQ FEC server (if using this mode)
        :type zmq_fec_server: str
        """
        self.mod_type = None
        self.fec_rate = None
        self.frame_size = None
        self.pilots = None

        self.fs_hz = None
        self.iq = None
        self.t_iq_sec = None

        self.fsym_hz = None
        self.symbols = None
        self.t_symbols_sec = None

        self.rrc_beta = 0.2
        self.fs_resampled_hz = None
        self.iq_resampled = None
        self.t_iq_resampled_sec = None

        self.fsym_hz = None
        self.clock_phase_offset_rad = None
        self.fractional_sample_shift = None

        self.frames = []

        self.debug_plots = False
        self.dvbs2_pl_header_data = DVBS2_ModCod_Configuration.generate_dvb_s2_pls_data()
        if dvbs2_frame_length is not None:
            if DVBS2_Frame_Length.Short_Frames not in dvbs2_frame_length:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].frame_length is not DVBS2_Frame_Length.Short_Frames]
            if DVBS2_Frame_Length.Normal_Frames not in dvbs2_frame_length:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].frame_length is not DVBS2_Frame_Length.Normal_Frames]
        if dvbs2_pilots is not None:
            if DVBS2_Pilots.Pilots_On not in dvbs2_pilots:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].pilots is not DVBS2_Pilots.Pilots_On]
            if DVBS2_Pilots.Pilots_Off not in dvbs2_pilots:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].pilots is not DVBS2_Pilots.Pilots_Off]
        if dvbs2_modulation_type is not None:
            if DVBS2_Modulation_Type.QPSK not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('QPSK') == -1]
            if DVBS2_Modulation_Type.PSK8 not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('8PSK') == -1]
            if DVBS2_Modulation_Type.APSK16 not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('16APSK') == -1]
            if DVBS2_Modulation_Type.APSK32 not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('32APSK') == -1]
            if DVBS2_Modulation_Type.DUMMY not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('DUMMY') == -1]
            if DVBS2_Modulation_Type.RESERVED not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('RESERVED') == -1]

        if dvbs2_code_rates is not None:
            if DVBS2_Code_Rates.RATE_1_4 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/4') == -1]
            if DVBS2_Code_Rates.RATE_1_3 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/3') == -1]
            if DVBS2_Code_Rates.RATE_2_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('2/5') == -1]
            if DVBS2_Code_Rates.RATE_1_2 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/2') == -1]
            if DVBS2_Code_Rates.RATE_3_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('3/5') == -1]
            if DVBS2_Code_Rates.RATE_2_3 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('2/3') == -1]
            if DVBS2_Code_Rates.RATE_3_4 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('3/4') == -1]
            if DVBS2_Code_Rates.RATE_4_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('4/5') == -1]
            if DVBS2_Code_Rates.RATE_5_6 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('5/6') == -1]
            if DVBS2_Code_Rates.RATE_8_9 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('8/9') == -1]
            if DVBS2_Code_Rates.RATE_9_10 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('9/10') == -1]
            if DVBS2_Code_Rates.RATE_DUMMY not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('DUMMY') == -1]
            if DVBS2_Code_Rates.RATE_RESERVED not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('RESERVED') == -1]

        self.descrambling_sequence = DVBS2_ModCod_Configuration.generate_pl_descrambling_sequence()

        if (dvbs2_frame_length is None) or ((DVBS2_Frame_Length.Short_Frames in dvbs2_frame_length)
                                            and (DVBS2_Frame_Length.Normal_Frames in dvbs2_frame_length)):
            frame_length = 'Both'
        elif (DVBS2_Frame_Length.Normal_Frames in dvbs2_frame_length) and \
                (DVBS2_Frame_Length.Short_Frames not in dvbs2_frame_length):
            frame_length = 'Normal'
        elif (DVBS2_Frame_Length.Short_Frames in dvbs2_frame_length) and \
                (DVBS2_Frame_Length.Normal_Frames not in dvbs2_frame_length):
            frame_length = 'Short'
        else:
            raise Exception('Unhandled frame length configuration')

        self.fec_processing_mode = fec_processing_mode
        if self.fec_processing_mode.lower() == "none":
            self.threading_config = None
            self.num_fec_banks = None
            self.num_processing_threads = None
            self.dvbs2_fec_processor_banks = None
            self.zmq_fec_server = None

        elif self.fec_processing_mode.lower() == "local_processor":
            self.threading_config = threading_config
            self.zmq_fec_server = None
            if self.threading_config == 0:
                self.num_fec_banks = 1
                self.num_processing_threads = 1
            elif self.threading_config == 1:
                self.num_fec_banks = 4
                self.num_processing_threads = 2
            elif self.threading_config == 2:
                self.num_fec_banks = 8
                self.num_processing_threads = 4
            elif self.threading_config == 3:
                self.num_fec_banks = 16
                self.num_processing_threads = 8
            elif self.threading_config == 4:
                self.num_fec_banks = 32
                self.num_processing_threads = 16
            elif self.threading_config == 16:
                self.num_fec_banks = 16
                self.num_processing_threads = 16
            else:
                raise Exception('Unhandled threading_config')

            def instantiate_fec_processor_bank(processor_bank_index):
                if DVBS2_FEC_Processor_Bank is None:
                    raise Exception('DVBS2_FEC_Processor_Bank is None.  ' +
                                    'from py_dvbs2_fec import DVBS2_FEC_Processor_Bank probably failed')
                else:
                    code_rates = list(set([data['dvbs2_modcod'].get_code_rate() for data in self.dvbs2_pl_header_data]))
                    self.dvbs2_fec_processor_banks[processor_bank_index] = \
                        DVBS2_FEC_Processor_Bank(frame_length=frame_length, code_rates=code_rates)
                return None

            self.dvbs2_fec_processor_banks = [None]*self.num_fec_banks
            do_parallel = True
            if do_parallel:
                from joblib import Parallel, delayed
                Parallel(n_jobs=8, require='sharedmem')(delayed(instantiate_fec_processor_bank)(i)
                                                        for i in range(self.num_fec_banks))
            else:
                for bank_index in range(0, self.num_fec_banks):
                    instantiate_fec_processor_bank(bank_index)

        elif self.fec_processing_mode.lower() == "zmq_processor":
            self.threading_config = None
            self.num_fec_banks = None
            self.num_processing_threads = None
            self.dvbs2_fec_processor_banks = None
            self.zmq_fec_server = zmq_fec_server
            import zmq
            self.context = zmq.Context()
            self.socket = self.context.socket(zmq.REQ)
            self.socket.connect(zmq_fec_server)
            self.dvbs2_fec_processor_banks = None

            # Check that the FEC Server is alive and throw an exception if it isn't
            uuid_ = uuid.uuid1()
            config_request = {'command': 'get_config', 'index': 0, 'uuid': uuid_}
            config_response = None
            try:
                self.socket.RCVTIMEO = 5000
                self.socket.send_pyobj(config_request)
                config_response = self.socket.recv_pyobj()
                self.socket.RCVTIMEO = -1
            except Exception as ex:
                print('Failed to receive FEC server response in time.  Exception: {}'.format(ex))
                self.socket.setsockopt(zmq.LINGER, 0)
                self.socket.close()
                self.context.term()
            if config_response is None:
                raise Exception('FEC Server at {} did not return a response.  Is it active?'.format(self.zmq_fec_server))
            if config_response['uuid'] != uuid_:
                raise Exception('UUID does not match')

            self.zmq_fec_server_config = config_response

        else:
            raise Exception('Unhandled FEC processing mode - fec_processing_mode = {}'.format(fec_processing_mode))


    def find_matching_pl_header_data(self, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Finds the pl_header_data entry that matches the given modcod
        :param dvbs2_modcod: DVBS2_ModCod_Configuration object
        :return:
        """
        dvbs2_pl_header_data = \
            [data for data in self.dvbs2_pl_header_data if
             data['dvbs2_modcod'].modcod is dvbs2_modcod.modcod and
             data['dvbs2_modcod'].pilots is dvbs2_modcod.pilots and
             data['dvbs2_modcod'].frame_length is dvbs2_modcod.frame_length]
        assert len(dvbs2_pl_header_data) == 1, 'Expecting a single match'
        return dvbs2_pl_header_data[0]


    def min_symbols_for_plframe_detection(self):
        """
        Returns the minimum number of symbols required to contain at least 1 PL header given the configurations
        in dvbs2_pl_header_data
        :return: Minimum number of symbols required to contain at least 1 PL header given the configurations in
        dvbs2_pl_header_data
        """
        return int(2*np.max([data['num_symbols']
                             for data in self.dvbs2_pl_header_data if data['num_symbols'] is not None]) + 90)


    def demod_plframes(self, dvbs2_frames: list):
        """
        Demod symbols to LLRs and then descramble and de-FEC them
        :param results_dicts_this_block: Results dicts for frames found in this block of IQ data
        :param symbols_corrected: Corrected symbols vector
        :return:
        """
        #
        # -------------------------------- Operate on Corrected Symbols and Get Bits --------------------------------
        #
        for dvbs2_frame in dvbs2_frames:
            print_plframe_demod = True
            if print_plframe_demod:
                print('PLFRAME Demod: Packet {:5}:   index={:7}   Time={:11.7}   Frequency={:8.1f}   esn0={:5.2f}   {}'.format(
                        dvbs2_frame.index,
                        dvbs2_frame.start_index,
                        dvbs2_frame.t_plframe_symbols[0],
                        dvbs2_frame.frequency_offset_hz,
                        dvbs2_frame.esn0,
                        dvbs2_frame.dvbs2_modcod.modcod_pretty()))

            # t_start_plframe_sec = t_start_sec + results_dict['current_symbol_index'] / fsym_hz
            dvbs2_pl_header_data = self.find_matching_pl_header_data(dvbs2_frame.dvbs2_modcod)
            plframe_symbols = \
                deepcopy(dvbs2_frame.plframe_symbols_corrected[0:dvbs2_frame.dvbs2_modcod.get_num_symbols()])
            plframe_symbols_descrambled = deepcopy(plframe_symbols)
            #self.descramble_plframe(dvbs2_frame.dvbs2_modcod, plframe_symbols_descrambled)

            if False:
                scatterplot(plframe_symbols_descrambled)

            if dvbs2_frame.dvbs2_modcod.modcod != DVBS2_ModCods.DVBS2_DUMMY_PLFRAME:
                noise_var = \
                    float(np.var(plframe_symbols_descrambled[dvbs2_frame.dvbs2_modcod.get_pilot_symbol_indicies()] -
                                 (1 / np.sqrt(2) + 1j / np.sqrt(2))))
                llr = demodulate_plframe(dvbs2_modcod=dvbs2_frame.dvbs2_modcod,
                                         symbols=plframe_symbols_descrambled, noise_var=noise_var)
                llr = deinterleave_plframe(dvbs2_modcod=dvbs2_frame.dvbs2_modcod, plframe_bits=llr)
                bits_llr = np.array(llr < 0, dtype=np.int)

                if self.dvbs2_fec_processor_banks is not None or self.zmq_fec_server is not None:
                    dvbs2_frame.bits, dvbs2_frame.passed_fec = \
                        self.decode_plframe(llr=llr, dvbs2_modcod=dvbs2_frame.dvbs2_modcod, esn0_db=dvbs2_frame.esn0)
                else:
                    dvbs2_frame.bits = bits_llr
                    dvbs2_frame.passed_fec = None

                (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_frame.dvbs2_modcod.get_coding_parameters()
                scrambling_sequence = dvbs2_pl_header_data['bbframe_scrambling_sequence']
                dvbs2_frame.bits_descrambled = np.asarray(dvbs2_frame.bits[0:k_bch]) ^ np.asarray(scrambling_sequence)
                dvbs2_frame.bbheader = DVBS2_BBHEADER(dvbs2_frame.bits_descrambled)
            else:
                dvbs2_frame.bits = None
                dvbs2_frame.passed_fec = None
                dvbs2_frame.bbheader = None

            send_results_via_udp = False
            if send_results_via_udp:
                # print('Corrected {} bit errors (BER = {}'.format(
                #    np.sum(bits != bits_llr), np.sum(bits != bits_llr) / k_ldpc))
                send_via_udp(dvbs2_frame.bits_descrambled[0:k_bch], dvbs2_frame.dvbs2_modcod)


    def generate_plframe_symbols(self, results_dicts):
        """
        Generates PLFRAME symbols
        :param results_dicts: List of results_dict dictionarys
        :return: void
        """
        num_pl_frames = 0
        for results_dict in results_dicts:
            print('Packet {:5}:   index={:7}   Time={:7.3}   Frequency={:8.1f}   esn0={:5.2f}   {}'.format(
                num_pl_frames,
                results_dict['current_symbol_index'],
                results_dict['t_start_plframe_sec'],
                results_dict['frequency_offset_hz'],
                results_dict['esn0'],
                results_dict['dvbs2_modcod']))
            num_pl_frames += 1

            bits_transmitted = interleave_plframe(dvbs2_modcod=results_dict['dvbs2_modcod'],
                                                  plframe_bits=results_dict['bits'])
            modulated_symbols = modulate_plframe(dvbs2_modcod=results_dict['dvbs2_modcod'],
                                                 bits=bits_transmitted)
            modulated_symbols_scrambled = deepcopy(modulated_symbols)
            self.scramble_plframe(results_dict['dvbs2_modcod'], modulated_symbols_scrambled)
            results_dict['modulated_symbols'] = modulated_symbols
            results_dict['modulated_symbols_scrambled'] = modulated_symbols_scrambled


    def decode_plframe(self, llr: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration, esn0_db: float = None):
        """
        Performs LDPC and BCH FEC decoding of llr values from a PL frame demodulation
        :param llr:
        :param dvbs2_modcod:
        :return:
        """
        if self.zmq_fec_server is not None:
            uuid_ = uuid.uuid1()
            fec_request = {'command': 'decode_plframe', 'index': 0, 'uuid': uuid_, 'llr': llr, 'dvbs2_modcod': dvbs2_modcod, 'esn0_db': esn0_db}
            self.socket.send_pyobj(fec_request)
            fec_response = self.socket.recv_pyobj()
            if fec_response['uuid'] != uuid_:
                raise Exception('UUID does not match')
            bits_ldpc = fec_response['bits_ldpc']
            passed_fec = fec_response['passed_fec']
        else:
            (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
            fec_processor_index = self.get_ldpc_processor_bank()
            passed_ldpc, bits_ldpc = self.decode_ldpc(llr=llr, dvbs2_modcod=dvbs2_modcod,
                                                      fec_processor_index=fec_processor_index,
                                                      esn0_db=esn0_db)
            self.dvbs2_fec_processor_banks[fec_processor_index].mutex.release()
            bits_bch, bitflips_bch = self.decode_bch(bits=bits_ldpc, dvbs2_modcod=dvbs2_modcod)
            if bitflips_bch != 0:
                bits_ldpc[0:k_ldpc] = bits_bch
            passed_fec = passed_ldpc and (bitflips_bch <= t_bch)
        return bits_ldpc, passed_fec

    def encode_plframe(self, bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Performs LDPC and BCH FEC decoding of llr values from a PL frame demodulation
        :param llr:
        :param dvbs2_modcod:
        :return:
        """
        if self.zmq_fec_server is not None:
            uuid_ = uuid.uuid1()
            fec_request = {'command': 'encode_plframe', 'index': 0, 'uuid': uuid_, 'bits': bits, 'dvbs2_modcod': dvbs2_modcod}
            self.socket.send_pyobj(fec_request)
            fec_response = self.socket.recv_pyobj()
            if fec_response['uuid'] != uuid_:
                raise Exception('UUID does not match')
            bits_bch_ldpc = fec_response['bits_bch_ldpc']
        elif self.num_fec_banks is not None and self.num_fec_banks > 0:
            bits_tx_bch = FECServer.encode_bch(bits, dvbs2_modcod)
            fec_processor_index = self.get_ldpc_processor_bank()
            bits_bch_ldpc = self.encode_ldpc(bits=bits_tx_bch, dvbs2_modcod=dvbs2_modcod, fec_processor_index=fec_processor_index)
            self.dvbs2_fec_processor_banks[fec_processor_index].mutex.release()
        else:
            bits_tx_bch = FECServer.encode_bch(bits, dvbs2_modcod)
            bits_bch_ldpc = np.append(bits_tx_bch, np.random.randint(0, 2, dvbs2_modcod.get_ldpc_codeword_length()-len(bits_tx_bch)))
        return bits_bch_ldpc


    def get_ldpc_processor_bank(self):
        fec_processor_index = -1
        acquire_result = False
        while not acquire_result:
            for fec_processor_index, in_use in enumerate(
                    [dvbs2_fec_processor_bank.in_use for dvbs2_fec_processor_bank in self.dvbs2_fec_processor_banks]):
                if not in_use:
                    acquire_result = self.dvbs2_fec_processor_banks[fec_processor_index].mutex.acquire(timeout=0)
                    if acquire_result is True:
                        break
        return fec_processor_index


    def decode_ldpc(self, llr: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration, fec_processor_index: int = 0,
                    esn0_db: float = None):
        """
        Performs soft symbol LDPC FEC decoding of llr values from a PL frame demodulation
        :param llr: Array of LLR values
        :param dvbs2_modcod: ModCod to operate on
        :param fec_processor_index: Index into processor bank
        :return: (passed_ldpc, bits)
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        [passed_ldpc, bits] = \
            self.dvbs2_fec_processor_banks[fec_processor_index].decode_ldpc(llr=llr, include_cw=True, n_ldpc=n_ldpc,
                                                                            k_ldpc=k_ldpc, k_bch=k_bch, t_bch=t_bch,
                                                                            esn0_db=esn0_db)
        return passed_ldpc, bits


    def encode_ldpc(self, bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration, fec_processor_index: int = 0):
        """
        Performs soft symbol LDPC FEC decoding of llr values from a PL frame demodulation
        :param llr: Array of LLR values
        :param dvbs2_modcod: ModCod to operate on
        :param fec_processor_index: Index into processor bank
        :return: (passed_ldpc, bits)
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        bits_ldpc = \
            self.dvbs2_fec_processor_banks[fec_processor_index].encode_ldpc(bits=bits, n_ldpc=n_ldpc, k_ldpc=k_ldpc,
                                                                            k_bch=k_bch, t_bch=t_bch)
        return bits_ldpc

    @staticmethod
    def decode_bch(bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Performs hard bit BCH DEC decoding of a PL frame
        :param bits:
        :param dvbs2_modcod:
        :return:
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        bch_primitive_polynomial = dvbs2_modcod.get_bch_primitive_polynomial()
        bch_primitive_polynomial_int = int("".join(str(x) for x in bch_primitive_polynomial), 2)
        bch = bchlib.BCH(bch_primitive_polynomial_int, t_bch, False)
        data_bytes = bytearray(np.packbits(bits[0:k_ldpc]))
        bch_bytes, ecc_bytes = data_bytes[:-bch.ecc_bytes], data_bytes[-bch.ecc_bytes:]
        bitflips = bch.decode_inplace(bch_bytes, ecc_bytes)
        bch_bytes.extend(ecc_bytes)

        bits = []
        for num in bch_bytes:
            for bit in [int(b) for b in '{0:08b}'.format(num)]:
                bits.append(bit)
        bits = np.asarray(bits)

        return bits, bitflips


    def scramble_bbframe(self, dvbs2_modcod: DVBS2_ModCod_Configuration, bits: np.ndarray):
        """
        Scrambles PLFRAME symbols in accordance with Section 5.5.4 of ETSI EN 302 307 V1.2.1
        :param dvbs2_modcod: ModCod of frame to scramble
        :param symbols: Symbols to scramble
        :return: void
        """
        dvbs2_pl_header_data = self.find_matching_pl_header_data(dvbs2_modcod)
        scrambling_sequence = dvbs2_pl_header_data['bbframe_scrambling_sequence']
        bits = np.asarray(bits) ^ np.asarray(scrambling_sequence)
        return bits


    def descramble_bbframe(self, dvbs2_modcod: DVBS2_ModCod_Configuration, bits: np.ndarray):
        """
        Scrambles PLFRAME symbols in accordance with Section 5.5.4 of ETSI EN 302 307 V1.2.1
        :param dvbs2_modcod: ModCod of frame to scramble
        :param symbols: Symbols to scramble
        :return: void
        """
        dvbs2_pl_header_data = self.find_matching_pl_header_data(dvbs2_modcod)
        scrambling_sequence = dvbs2_pl_header_data['bbframe_scrambling_sequence']
        bits = np.asarray(bits) ^ np.asarray(scrambling_sequence)
        return bits


    def scramble_plframe(self, dvbs2_modcod: DVBS2_ModCod_Configuration, symbols: np.ndarray):
        """
        Scrambles PLFRAME symbols in accordance with Section 5.5.4 of ETSI EN 302 307 V1.2.1
        :param dvbs2_modcod: ModCod of frame to scramble
        :param symbols: Symbols to scramble
        :return: void
        """
        symbols[90:(dvbs2_modcod.get_num_symbols())] = \
            symbols[90:(dvbs2_modcod.get_num_symbols())] * \
            np.conj(self.descrambling_sequence[0:(dvbs2_modcod.get_num_symbols() - 90)])


    def descramble_plframe(self, dvbs2_modcod, symbols):
        """
        Descrambles PLFRAME symbols in accordance with Section 5.5.4 of ETSI EN 302 307 V1.2.1
        :param dvbs2_modcod: ModCod of frame to scramble
        :param symbols: Symbols to scramble
        :return: void
        """
        symbols[90:(dvbs2_modcod.get_num_symbols())] = symbols[90:(
            dvbs2_modcod.get_num_symbols())] * self.descrambling_sequence[0:(dvbs2_modcod.get_num_symbols() - 90)]
