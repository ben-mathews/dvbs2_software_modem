import os
import glob
from time import perf_counter
import pickle
import pytest

import numpy as np

from dvbs2_processing import DVBS2PLFrameProcessor

def test_perform_symbol_corrections(test_data_dir='test_data'):
    """
    Tests DVBS2PLFrameProcessor.perform_symbol_corrections().  Assumes there are sets of files titled
    perform_symbol_corrections_output-block-#####.pickle and perform_symbol_corrections_output-block-#####.pickle
    in test_dir.  Gets input parameters from dicts contained in these files and compares function output with
    output results from dicts contained in these files
    :param test_data_dir:
    :return:
    """
    files = glob.glob(os.path.join(test_data_dir, '*perform_symbol_corrections*'))
    input_files = []
    output_files = []
    for input_file in files:
        if input_file.find('_input') > 0:
            print('Input file: {}'.format(input_file))
            output_file = [file for file in files if file == input_file.replace('input', 'output')]
            if len(output_file) != 1:
                raise Exception('Expecting a single matching output file for input file {}'.format(input_file))
            output_file = output_file[0]
            print('Output file: {}'.format(output_file))
            input_files.append(input_file)
            output_files.append(output_file)

    dvbs2_plframe_processor = DVBS2PLFrameProcessor(no_fec_processor=True, threading_config=0)

    time_list = []
    for test_index, (input_file, output_file) in enumerate(zip(input_files, output_files)):
        print(test_index)

        with open(input_file, 'rb') as fp:
            input_dict = pickle.load(fp)
        with open(output_file, 'rb') as fp:
            output_dict = pickle.load(fp)

        symbols = input_dict['symbols']
        if len(symbols) == 0:
            raise Exception('Empty symbols vector')
        else:
            fs_hz = input_dict['fs_hz']
            fsym_hz = input_dict['fsym_hz']
            t_symbols_sec = input_dict['t_symbols_sec']
            iq_resampled = input_dict['iq_resampled']
            fs_resampled_hz = input_dict['fs_resampled_hz']
            t_iq_resampled_sec = input_dict['t_iq_resampled_sec']
            block_num = input_dict['block_num']

        start_time = perf_counter()
        results_dicts_this_block, frequency_offset_hz, carrier_phase_rad, cs_amplitude, cs_phase, iq_resampled, \
        symbols, symbols_corrected, t_iq_resampled_sec, t_symbols_sec = \
            dvbs2_plframe_processor.perform_symbol_corrections(symbols, fsym_hz, t_symbols_sec, iq_resampled,
                                                               fs_resampled_hz, t_iq_resampled_sec, block_num)
        elapsed_time = perf_counter() - start_time
        time_list.append(elapsed_time)

        do_checks = False
        if do_checks:
            assert np.sum(iq_resampled != output_dict['iq_resampled']) == 0
            assert np.sum(symbols != output_dict['symbols']) == 0
            assert np.sum(symbols_corrected != output_dict['symbols_corrected']) == 0
            assert np.sum(t_iq_resampled_sec != output_dict['t_iq_resampled_sec']) == 0
            assert np.sum(t_symbols_sec != output_dict['t_symbols_sec']) == 0

    done = True


if __name__ == '__main__':
    test_perform_symbol_corrections()