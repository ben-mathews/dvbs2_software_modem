import os
import logging
import logging.config
import json
from copy import deepcopy
from typing import List

import numpy as np
from scipy import stats
from scipy.interpolate import CubicSpline
from scipy.interpolate.interpolate import interp1d
from sklearn.linear_model import LinearRegression

from digital_demod.carrier_recovery.carrier_frequency_recovery import carrier_frequency_shift, carrier_recovery_mfd
from digital_demod.carrier_recovery.carrier_phase_recovery import carrier_phase_shift

from dsp_utilities.plotting.scatterplot import scatterplot

from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration, DVBS2_Frame_Length, DVBS2_Pilots, \
    DVBS2_Modulation_Type, DVBS2_ModCods, DVBS2_Code_Rates

from dvbs2_processing.utilities import compute_plframe_statistics, process_phase_measurements, get_package_root
from dvbs2_processing.data_structures.data_structures import DVBS2_Frame

# import matplotlib
# matplotlib.pyplot.switch_backend('Qt5Agg')
import matplotlib.pyplot as plt

plt.style.use('dark_background')


# noinspection PyUnreachableCode
class DVBS2PLFrameProcessor:
    """
    Functionality to detect and output PLFRAMEs from coarsely synchronized symbols

    Assumptions:
     * Symbols are coarsely synced in clock and carrier frequency (exactly what that means: TBD)

    """
    def __init__(self,
                 dvbs2_frame_length: List[DVBS2_Frame_Length] = None,
                 dvbs2_pilots: List[DVBS2_Pilots] = None,
                 dvbs2_modulation_type: List[DVBS2_Modulation_Type] = None,
                 dvbs2_code_rates: List[DVBS2_Code_Rates] = None):
        """
        Constructor for DVBS2PLFrameProcessor object
        :param dvbs2_frame_length: list of DVBS2_Frame_Length values that the processor can support
        :type dvbs2_frame_length: list[DVBS2_Frame_Length]
        :param dvbs2_pilots: list of DVBS2_Pilots values that the processor can support
        :type dvbs2_pilots: list[DVBS2_Pilots]
        :param dvbs2_modulation_type: list of DVBS2_Modulation_Type values that the processor can support
        :type dvbs2_modulation_type: list[DVBS2_Modulation_Type]
        """

        self.set_logger()

        self.mod_type = None
        self.fec_rate = None
        self.frame_size = None
        self.pilots = None

        self.fs_hz = None
        self.iq = None
        self.t_iq_sec = None

        self.fsym_hz = None
        self.symbols = None
        self.t_symbols_sec = None

        self.rrc_beta = 0.2
        self.fs_resampled_hz = None
        self.iq_resampled = None
        self.t_iq_resampled_sec = None

        self.fsym_hz = None
        self.clock_phase_offset_rad = None
        self.fractional_sample_shift = None

        self.frames = []

        self.debug_plots = False
        self.dvbs2_pl_header_data = DVBS2_ModCod_Configuration.generate_dvb_s2_pls_data()
        if dvbs2_frame_length is not None:
            if DVBS2_Frame_Length.Short_Frames not in dvbs2_frame_length:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].frame_length is not DVBS2_Frame_Length.Short_Frames]
            if DVBS2_Frame_Length.Normal_Frames not in dvbs2_frame_length:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].frame_length is not DVBS2_Frame_Length.Normal_Frames]
        if dvbs2_pilots is not None:
            if DVBS2_Pilots.Pilots_On not in dvbs2_pilots:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].pilots is not DVBS2_Pilots.Pilots_On]
            if DVBS2_Pilots.Pilots_Off not in dvbs2_pilots:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].pilots is not DVBS2_Pilots.Pilots_Off]
        if dvbs2_modulation_type is not None:
            if DVBS2_Modulation_Type.QPSK not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('QPSK') == -1]
            if DVBS2_Modulation_Type.PSK8 not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('8PSK') == -1]
            if DVBS2_Modulation_Type.APSK16 not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('16APSK') == -1]
            if DVBS2_Modulation_Type.APSK32 not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('32APSK') == -1]
            if DVBS2_Modulation_Type.DUMMY not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('DUMMY') == -1]
            if DVBS2_Modulation_Type.RESERVED not in dvbs2_modulation_type:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('RESERVED') == -1]

        if dvbs2_code_rates is not None:
            if DVBS2_Code_Rates.RATE_1_4 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/4') == -1]
            if DVBS2_Code_Rates.RATE_1_3 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/3') == -1]
            if DVBS2_Code_Rates.RATE_2_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('2/5') == -1]
            if DVBS2_Code_Rates.RATE_1_2 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/2') == -1]
            if DVBS2_Code_Rates.RATE_3_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('3/5') == -1]
            if DVBS2_Code_Rates.RATE_2_3 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('2/3') == -1]
            if DVBS2_Code_Rates.RATE_3_4 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('3/4') == -1]
            if DVBS2_Code_Rates.RATE_4_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('4/5') == -1]
            if DVBS2_Code_Rates.RATE_5_6 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('5/6') == -1]
            if DVBS2_Code_Rates.RATE_8_9 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('8/9') == -1]
            if DVBS2_Code_Rates.RATE_9_10 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('9/10') == -1]
            if DVBS2_Code_Rates.RATE_DUMMY not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('DUMMY') == -1]

        self.descrambling_sequence = DVBS2_ModCod_Configuration.generate_pl_descrambling_sequence()

        # TODO: Still need to filter out elements by frame lengths
        if (dvbs2_frame_length is None) or ((DVBS2_Frame_Length.Short_Frames in dvbs2_frame_length)
                                            and (DVBS2_Frame_Length.Normal_Frames in dvbs2_frame_length)):
            frame_length = 'Both'
        elif (DVBS2_Frame_Length.Normal_Frames in dvbs2_frame_length) and \
                (DVBS2_Frame_Length.Short_Frames not in dvbs2_frame_length):
            frame_length = 'Normal'
        elif (DVBS2_Frame_Length.Short_Frames in dvbs2_frame_length) and \
                (DVBS2_Frame_Length.Normal_Frames not in dvbs2_frame_length):
            frame_length = 'Short'
        else:
            raise Exception('Unhandled frame length configuration')
        if frame_length != 'Need to implement':
            frame_length = 'Need to implement'


    def set_logger(self):
        """
        Sets up logger
        @return:
        """
        self.logger = logging.getLogger("plframe_processor")
        if len(self.logger.root.handlers) == 0:  # Configure the logger if we have not already
            log_config_filename = os.path.join(get_package_root(), "logging.json")
            with open(log_config_filename, "rt") as fp:
                config = json.load(fp)
            logging.config.dictConfig(config)


    def find_matching_pl_header_data(self, dvbs2_modcod: DVBS2_ModCod_Configuration) -> List:
        """
        Finds the pl_header_data entry that matches the given modcod
        :param dvbs2_modcod: The DVBS2_ModCod_Configuration object to find matching data for
        :type dvbs2_modcod: DVBS2_ModCod_Configuration
        :return: List
        """
        dvbs2_pl_header_data = \
            [data for data in self.dvbs2_pl_header_data if
             data['dvbs2_modcod'].modcod is dvbs2_modcod.modcod and
             data['dvbs2_modcod'].pilots is dvbs2_modcod.pilots and
             data['dvbs2_modcod'].frame_length is dvbs2_modcod.frame_length]
        assert len(dvbs2_pl_header_data) == 1, 'Expecting a single match'
        return dvbs2_pl_header_data[0]


    def min_symbols_for_plframe_detection(self):
        """
        Returns the minimum number of symbols required to contain at least 1 PL header given the configurations
        in dvbs2_pl_header_data
        :return: Minimum number of symbols required to contain at least 1 PL header given the configurations in
        dvbs2_pl_header_data
        """
        return int(2*np.max([data['num_symbols'] for data in self.dvbs2_pl_header_data
                             if data['num_symbols'] is not None]) + 90)


    def print_modcod_statistics(self):
        """
        Prints statistics about the number of occurences of modcods in procesed frames
        """
        for mod_cod, num_occurences in zip([data['dvbs2_modcod'] for data in self.dvbs2_pl_header_data],
                                           [data['num_occurrences'] for data in self.dvbs2_pl_header_data]):
            print('{}: {}'.format(mod_cod, num_occurences))


    def get_next_plframe(self,
                         symbols: np.ndarray,
                         fsym_hz: float,
                         current_symbol_index: int = 0,
                         max_num_symbols: int = -1,
                         frequency_offset_hz: float = None,
                         frequency_adjustments_symbols_hz: List[float] = None,
                         search_for_pl_headers: bool = True,
                         max_num_sof_symbol_errors: int = 2,
                         max_num_plsc_symbol_errors: int = 2):
        """
        Operate on symbols to identify the start of the next PL frame and get the PLheader.  Returns starting index of
        next PL frame if SOF and PLS header have symbol errors within the secified margins.

        :param symbols: Array of symbols to operate on
        :type symbols: np.ndarray
        :param fsym_hz: Symbol rate of symbols array in SPS
        :type fsym_hz: float
        :param current_symbol_index: The current index into symbols array to begin looking for the PL header SOF symbols
        :type current_symbol_index: int
        :param max_num_symbols: Number of symbols to search over (current_symbol_index +/- max_num_symbols).  If -1, a
                                range will be generated that spans the longest possible PL frame (QPSK 1/4 Normal
                                frames)
        :type max_num_symbols: int
        :param frequency_offset_hz: Known carrier frequency offset of symbols array.  Symbols are assumed to not be
                                    corrected for this offset
        :type frequency_offset_hz: float
        :param frequency_adjustments_symbols_hz: List of frequency adjustments already applied to symbols
        :type frequency_adjustments_symbols_hz: List[float]
        :param search_for_pl_headers: Specify whether to search for possible PLHEADER candidates
        :type search_for_pl_headers: bool
        :param max_num_sof_symbol_errors: Max allowable errors in the 26 SOF symbols to delcare a valid PLHEADER
                                          candidate
        :type max_num_sof_symbol_errors: int
        :param max_num_plsc_symbol_errors: Max allowable errors in the 90 PLSC bits to delcare a valid PLHEADER
                                           candidate
        :param max_num_plsc_symbol_errors: int
        :return:
         - next_symbol_index: Index into symbols of the next PLHEADER candidate
         - results_dict: Dictionary of search results

        :note: The PL header detection works by taking advantage of the fact that the PL header is pi/2 BPSK encoded.
        The implication of this is that if the symbols are perfectly frequency synchronized, then the phase shift
        between consecutive symbols is +/- pi/2 (not including noise effects).  Frequency synchronization errors will
        cause this difference to increase by up to a factor of FOffset / Fs.  In this algorithm we assume that
        abs(FOffset) < Fs/4, and adjust the threshold for phase shifts accordingly.  With this, we look for 90 (the
        number of symbols in a PL header) consecutive symbols where the phase difference between symbols is pi/2 plus
        an additional pi/4 to account for frequency offset effects.  We when inspect every sequence of 90 symbols that
        meets this criteria with process_plscode(), which looks for the SOF unique word and a valid PLS code that
        corresponds to a valid ModCod.

        :note: This method can be used in two ways:
        1) If doing an initial acq and not yet synchronized to a PLFRAME, then this is called with
           search_for_pl_headers == True, and max_num_symbols == -1.  This will search over a range of symbols in which
           we are guarenteed to find at least 1 PLHEADER.
        2) If already synchronized and simply want to confirm the next PLFRAME, then call with
           search_for_pl_headers == False and max_num_symbols = 0 to just look at the index we expect the next
        """
        if frequency_adjustments_symbols_hz is None:
            frequency_adjustments_symbols_hz = []
        # Set max_num_symbols appropriately

        # max_num_symbols specifies the search window about current_symbol_index.  If it's -1 then we define the search
        # window to be big enough to contain two PLFRAMES, thus ensuring that we'll find at least one PLHEADER
        if max_num_symbols == -1:
            # Max number of symbols we could be searching over is QPSK, long frames, w/ pilots 90 PLHEADEDR
            # symbols + 360 slots * 90 symbols/slot + 360/16 pilots blocks * 36 symbols / pilot block
            max_num_symbols = int(2 * (90 + 360 * 90 + np.ceil(360/16) * 36))
        if current_symbol_index+90+max_num_symbols > len(symbols):
            max_num_symbols = len(symbols) - current_symbol_index - 90 - 1
        if max_num_symbols < 0:
            current_symbol_index, results_dict = -1, {}
            return current_symbol_index, results_dict

        # We have two options for identifying candidate indicies:
        #  1) We do a search and look for indicies that look like they might be associated with PLHEADERs.  To do this
        #     we take advantage of the fact that the PLHEADERs are pi/2 BPSK modulated, and look for 90 symbol runs
        #     where we only see these kinds of phase shifts between symbols.
        #  2) We simply consider all symbols with in the range of current_symbol_index +/- max_num_symbols.  This can
        #     be computationally expensive so we generally only do this when we already sync'd to PLFRAMES and we know
        #     with high confidence where the next PLFRAME will occur
        if search_for_pl_headers:
            # If we have to search, then we use search_for_pl_headers() to find 90 element long sequences of symbols
            # that appear to be pi/2 BPSK encoded
            plheader_candidate_indicies = \
                self.plheader_search(symbols=symbols, current_symbol_index=current_symbol_index,
                                     max_num_symbols=max_num_symbols, max_plsheader_symbol_errors=5)
            if len(plheader_candidate_indicies) > 250:
                self.logger.warning('Many PLHEADER candidates detected')
            # If we fail the search with max_plsheader_symbol_errors==5, then retry with max_plsheader_symbol_errors==15
            if len(plheader_candidate_indicies) == 0:
                plheader_candidate_indicies = \
                    self.plheader_search(symbols=symbols, current_symbol_index=current_symbol_index,
                                         max_num_symbols=max_num_symbols, max_plsheader_symbol_errors=15)
                # Try one more time!
                if len(plheader_candidate_indicies) == 0:
                    plheader_candidate_indicies = \
                        self.plheader_search(symbols=symbols, current_symbol_index=current_symbol_index,
                                             max_num_symbols=max_num_symbols, max_plsheader_symbol_errors=25)
        else:
            # Search +/- max_num_symbols symbols centered around current_symbol_index
            plheader_candidate_indicies = \
                current_symbol_index + np.linspace(-max_num_symbols, max_num_symbols, 2*max_num_symbols+1, dtype=np.int)
            plheader_candidate_indicies = plheader_candidate_indicies[np.where(plheader_candidate_indicies >= 0)]

        # If we don't have any indicies (which can occur if doing the search but not finding anything), then exit
        if len(plheader_candidate_indicies) == 0:
            dvbs2_frame = None
            current_symbol_index = -1
            return current_symbol_index, dvbs2_frame

        # Search over plheader_candidate_indicies for an index where we find a PLHEADER with less than the number of
        # specified SOF header and PLHEADER symbol errors
        plheader_candidate_index = -1
        plheader_search_results_dict = {}
        dvbs2_modcod = None
        for plheader_candidate_index in plheader_candidate_indicies:
            plheader_search_results_dict = self.plheader_process(plheader_candidate_index=plheader_candidate_index,
                                                                 symbols=symbols, fsym_hz=fsym_hz,
                                                                 frequency_offset_hz=frequency_offset_hz)
            dvbs2_modcod = plheader_search_results_dict['dvbs2_modcod']
            if dvbs2_modcod is not None:
                break

        # We failed to find a PLS header with few enough errors - return empty
        if dvbs2_modcod is None:
            dvbs2_frame = None
            current_symbol_index = -1
            return current_symbol_index, dvbs2_frame

        if dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_RESERVED0 or \
                dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_RESERVED1 or \
                dvbs2_modcod.modcod == DVBS2_ModCods.DVBS2_RESERVED2:
            self.logger.warning('Warning: Detected Reserved ModCod')
            dvbs2_frame = None
            current_symbol_index = -1
            return current_symbol_index, dvbs2_frame

        # At this point we've found an index (plheader_candidate_index) that appears to be the start of a PLHEADER.
        # Use the PLHEADER length to compute where we think the next PLHEADER will start
        next_symbol_index = int(plheader_candidate_index + int(dvbs2_modcod.get_num_symbols()))

        # Generate a dvbs2_frame structure for the PLHEADER we just found
        dvbs2_frame = DVBS2_Frame(dvbs2_modcod=dvbs2_modcod,
                                  symbol_start_index=plheader_candidate_index,
                                  plheader_frequency_offset_hz=plheader_search_results_dict['frequency_offset_hz'],
                                  plheader_phase_offset_rad=plheader_search_results_dict['coarse_phase_offset_rad'],
                                  plheader_amplitude=plheader_search_results_dict['amplitude'],
                                  plheader_snr=plheader_search_results_dict['snr_db'],
                                  frequency_adjustments_hz=frequency_adjustments_symbols_hz,
                                  next_symbol_index=next_symbol_index)

        return next_symbol_index, dvbs2_frame


    def find_plframes(self, symbols: np.ndarray, fsym_hz: float, first_plframe_index: int,
                      max_num_plframes: int = None, initial_frequency_offset_hz: float = None,
                      frequency_adjustments_hz: List[float] = None, plframe_start_index: int = 0,
                      suppress_symbol_slip_error: bool = False):
        """
        Starts at the beginning of a known PLFRAME and searches for subsequent frames.  Assumes that symbols are
        clock and carrier synchronized.
        :param symbols: Array of symbols
        :type symbols: np.ndarray
        :param fsym_hz: Symbol rate in Hz
        :type fsym_hz: float
        :param first_plframe_index: Index into symbols of first plframe
        :type first_plframe_index: int
        :param max_num_plframes: Maximum number of PLFRAMEs to detect (None searches to end of IQ data)
        :type max_num_plframes: int or None
        :param initial_frequency_offset_hz: Initial frequency offset
        :type initial_frequency_offset_hz: float
        :param frequency_adjustments_hz: List of adjustments already applied to symbol data
        :type frequency_adjustments_hz: list
        :plframe_start_index: Index of start of PLFRAME (used to set index field of DVBS2_Frame objects)
        :type plframe_start_index: int
        :return:
        """
        if frequency_adjustments_hz is None:
            frequency_adjustments_hz = []
        dvbs2_frames = []   # TODO: Remove this

        next_symbol_index, dvbs2_frame = \
            self.get_next_plframe(symbols=symbols, fsym_hz=fsym_hz,
                                  current_symbol_index=first_plframe_index,
                                  max_num_symbols=0,
                                  frequency_offset_hz=initial_frequency_offset_hz,
                                  frequency_adjustments_symbols_hz=frequency_adjustments_hz,
                                  search_for_pl_headers=False,
                                  max_num_plsc_symbol_errors=12,
                                  max_num_sof_symbol_errors=8)
        if dvbs2_frame is None:
            return []
        dvbs2_frame.index = plframe_start_index

        first_plframe_symbol_index = dvbs2_frame.start_index
        next_symbol_index = first_plframe_symbol_index
        while next_symbol_index < len(symbols) - 180:
            #
            # ------------------------------------------ PL Header Search ------------------------------------------
            #
            last_symbol_index = next_symbol_index
            next_symbol_index, dvbs2_frame = \
                self.get_next_plframe(symbols=symbols, fsym_hz=fsym_hz,
                                      current_symbol_index=next_symbol_index,
                                      max_num_symbols=0,
                                      frequency_offset_hz=initial_frequency_offset_hz,
                                      frequency_adjustments_symbols_hz=frequency_adjustments_hz,
                                      search_for_pl_headers=False,
                                      max_num_plsc_symbol_errors=12,
                                      max_num_sof_symbol_errors=10)
            if dvbs2_frame is None or \
                    (dvbs2_frame.start_index is not None and dvbs2_frame.start_index != last_symbol_index):
                # Try again with an improved initial frequency offset
                next_symbol_index = last_symbol_index
                # next_symbol_index = dvbs2_frames[-1].start_index
                new_frequency_offset_hz = \
                    np.mean([dvbs2_frame.plheader_frequency_offset_hz for dvbs2_frame in dvbs2_frames])
                next_symbol_index, dvbs2_frame = \
                    self.get_next_plframe(symbols=symbols,
                                          fsym_hz=fsym_hz,
                                          current_symbol_index=next_symbol_index,
                                          max_num_symbols=0,
                                          frequency_offset_hz=new_frequency_offset_hz,
                                          frequency_adjustments_symbols_hz=frequency_adjustments_hz,
                                          search_for_pl_headers=False,
                                          max_num_plsc_symbol_errors=12,
                                          max_num_sof_symbol_errors=10)
                next_symbol_index = last_symbol_index
                next_symbol_index, dvbs2_frame = \
                    self.get_next_plframe(symbols=symbols,
                                          fsym_hz=fsym_hz,
                                          current_symbol_index=next_symbol_index,
                                          max_num_symbols=0,
                                          frequency_offset_hz=new_frequency_offset_hz,
                                          frequency_adjustments_symbols_hz=frequency_adjustments_hz,
                                          search_for_pl_headers=False,
                                          max_num_plsc_symbol_errors=12,
                                          max_num_sof_symbol_errors=10)
                if dvbs2_frame is None or \
                        (dvbs2_frame.start_index is not None and dvbs2_frame.start_index != last_symbol_index):
                    # If we don't find the next frame than assume that the last one was also invalid.  Remove it
                    # from dvbs2_frames and exit
                    if not suppress_symbol_slip_error:
                        self.logger.warning('WARNING: Apparent symbol slip or misdetected frame - unhandled condition')
                    if len(dvbs2_frames) <= 1:
                        return []
                    else:
                        dvbs2_frames.pop()
                        return dvbs2_frames
            dvbs2_frame.index = plframe_start_index + len(dvbs2_frames)

            # At this point plframes_dict['current_symbol_index'] has the index of the symbol at the beginning of the
            # current PLFRAME that we're operating on, and next_symbol_index has the index of the symbol at the
            # beginning of the next PLFRAME.  We operate on data starting at the SOF header of the current PLFRAME
            # (which starts at index plframes_dict['current_symbol_index']) and the end of the SOF header of the next
            # PLFRAME (which ends at index next_symbol_index + 90)

            # Break if PLFRAME exceeds duration of current block of symbols
            if next_symbol_index == -1 or \
                    (dvbs2_frame.start_index + dvbs2_frame.dvbs2_modcod.get_num_symbols() + 180 > len(symbols)):
                break

            dvbs2_frames.append(dvbs2_frame)

            if max_num_plframes is not None and len(dvbs2_frames) >= max_num_plframes:
                break

        return dvbs2_frames


    def plheader_search(self, symbols: np.ndarray, current_symbol_index: int, max_num_symbols: int,
                        max_plsheader_symbol_errors: int = 5, return_indicies_sorted: bool = False,
                        method: str = 'correlation'):
        """
        Uses the pi/2 BPSK signal structure of the PLSHEADER to identify possible PLSHEADERs
        :param symbols: Symbols to operate on
        :param current_symbol_index: Index into self.symbols to start searching
        :param max_num_symbols: Maximum number of symbols to search over, starting at current_symbol_index
        :param max_plsheader_symbol_errors: Maximum number of "errors" in the sense that adjacent symbols in PLSHEADER
                                            candidates are not within pi/2+pi/4 of each other
        :param return_indicies_sorted: Return indicies sorted from highest correlation value to loweset
        :param method: Search method to use (either 'correlation', 'phase_processing', or 'phase_processing_old'
        :return: Indicies into self.symbols where the

        ..note:: We take advantage of the fact that the PLSHEADER is pi/2 BPSK encoded and should have phase shifts of
                 pi/2 between adjacent symbols.  We look for 90 symbol sequences where this is the case and treat them
                 as candidate PLSHEADERs.  We allow up to max_plsheader_symbol_errors adjacent symbols to have
                 phase shifts pi/2 +/- pi/4
        """

        symbols_normalized = \
            symbols[current_symbol_index:
                    (current_symbol_index + 90 + max_num_symbols - 1)] / \
            np.mean(np.abs(symbols[current_symbol_index:(current_symbol_index + 90 + max_num_symbols - 1)]))
        if method == 'correlation':
            from scipy.signal import correlate
            plheader_candidate_indicies = []
            plheader_candidate_amplitudes = []
            for dvbs2_pl_header_data in self.dvbs2_pl_header_data:
                pls_symbols = dvbs2_pl_header_data['pls_symbols']
                corr = correlate(symbols_normalized, pls_symbols, 'same')
                if False:
                    plt.figure()
                    plt.plot(np.abs(corr))
                for index in np.where(np.abs(corr) >= 90 - 10*max_plsheader_symbol_errors)[0]:
                    plheader_candidate_amplitudes.append(np.abs(corr[index]))
                    if False:
                        plt.figure()
                        plt.plot(corr)
                    if index + current_symbol_index - int(len(pls_symbols) / 2) >= 0:
                        plheader_candidate_indicies.append(
                            int(index + current_symbol_index - int(len(pls_symbols) / 2)))
            plheader_candidate_amplitudes = \
                np.asarray(plheader_candidate_amplitudes)[np.argsort(plheader_candidate_indicies)]
            plheader_candidate_indicies = np.sort(list(set(plheader_candidate_indicies)))
        elif method == 'phase_processing':
            # Calculate the phase difference between adjacent symbols
            delta_transhold = np.pi/4
            phase_delta = \
                np.mod(np.angle(symbols[current_symbol_index:(current_symbol_index + 90 + max_num_symbols - 1)]) -
                       np.angle(symbols[(current_symbol_index + 1):(current_symbol_index + 90 + max_num_symbols)]),
                       2*np.pi)
            phase_delta_diff = np.abs(np.diff(phase_delta))
            phase_delta_signal_per_symbol = \
                2 * ((phase_delta_diff < delta_transhold)
                     + ((phase_delta_diff > np.pi - delta_transhold) * (phase_delta_diff < np.pi + delta_transhold))
                     ) - 1
            phase_delta_signal = np.convolve(phase_delta_signal_per_symbol, np.ones((90,)), mode='valid')
            plheader_candidate_indicies = np.where(phase_delta_signal >= 90 - 2 * max_plsheader_symbol_errors)[0]
            plheader_candidate_indicies = plheader_candidate_indicies + current_symbol_index
            if len(plheader_candidate_indicies) == -1:  # If we get nothing at least return the max index
                # TODO: Implement better logic for when we have the max_plsheader_symbol_errors set too low
                max_plsheader_symbol_errors_modified = 90-np.max(phase_delta_signal) - 0
                plheader_candidate_indicies = \
                    np.where(phase_delta_signal >= 90 - 2 * max_plsheader_symbol_errors_modified)[0]
                plheader_candidate_indicies = plheader_candidate_indicies + current_symbol_index
            phase_delta_signals = phase_delta_signal[plheader_candidate_indicies]
        elif method == 'phase_processing_old':
            # Search for possible PL headers by looking for a series of 90 symbols where the phase shifts is +/- pi/2
            phase_delta = np.mod(
                np.abs(np.angle(symbols[current_symbol_index:(current_symbol_index + 90 + max_num_symbols - 1)]) -
                       np.angle(symbols[(current_symbol_index + 1):(current_symbol_index + 90 + max_num_symbols)])),
                np.pi)  # Calculate the phase difference between adjacent symbols
            # Signal for if the phase diff between consecutive symbols is within abs(pi)+pi/4 (+1 if True, -1 if False)
            phase_delta_signal_per_symbol = 2.0 * (np.diff(np.abs(phase_delta - np.pi / 2)) < np.pi / 4) - 1.0
            # Compute the running average over 90 symbols (length of PL header).
            phase_delta_signal = np.convolve(phase_delta_signal_per_symbol, np.ones((90,)), mode='valid')
            # TODO: Handle the case that this array is empty
            plheader_candidate_indicies = np.where(phase_delta_signal >= 90 - 2 * max_plsheader_symbol_errors)[0]
            plheader_candidate_indicies = plheader_candidate_indicies + current_symbol_index
            phase_delta_signals = phase_delta_signal[plheader_candidate_indicies]
        else:
            raise Exception('Unhandled method: {}'.format(method))
        if self.debug_plots:
            plt.plot(np.abs(phase_delta - np.pi / 2), '.')
            plt.figure()
            xvals = np.linspace(current_symbol_index, current_symbol_index + len(phase_delta_signal) - 1,
                                len(phase_delta_signal))
            plt.plot(xvals, phase_delta_signal, 'b', label='Phase Delta Signal for SOF Detection')
            plt.plot(plheader_candidate_indicies,
                     phase_delta_signal[plheader_candidate_indicies - current_symbol_index], 'mo',
                     label='Measuremements Above Threshold')
            plt.plot([xvals[0], xvals[-1]],
                     [90 - 2 * max_plsheader_symbol_errors, 90 - 2 * max_plsheader_symbol_errors],
                     'r', label='Detection Threshold')
            plt.grid()
            plt.legend(loc='lower right')
            plt.xlabel('Symbol Number')
            plt.ylabel('Phase Delta Signal')
            plt.title('PLHEADER Detection')
            plt.figure()
            plt.plot(phase_delta, '.')
            plt.plot(plheader_candidate_indicies, phase_delta[plheader_candidate_indicies - current_symbol_index],
                     '.', label='Measuremements Above Threshold')
        if return_indicies_sorted:
            plheader_candidate_indicies = plheader_candidate_indicies[np.argsort(phase_delta_signals)[::-1]]
        return plheader_candidate_indicies


    def plheader_process(self, symbols: np.ndarray, plheader_candidate_index: int, fsym_hz: float,
                         frequency_offset_hz: float = None, perform_frequency_sync=True, max_num_plsc_bit_errors=8):
        """
        Basic search for PLHeader for a given PLHEADER candidate index into the array of symbols.  Assumes symbols
        are already frequency synchronized.  Does a brute force search over
        all ModCods in self.dvbs2_pl_header_data and finds the ModCod that has the fewest symbol errors with respect to
        plheader_candidate.  The returned value is simply the ModCod that most closely matches the plheader_candidate,
        but there is no guarantee that it is the right one (e.g. if plheader_candidate doesn't actually start with
        the PLHEADER, this function will still return the ModCod that most closely matches whatever random symbols
        are in the first 90 symbols of plheader_candidate).
        :param symbols: Array of symbols
        :type symbols: np.ndarray
        :param plheader_candidate_index: Index into array of symbols to perform search on
        :type plheader_candidate_index: int
        :param fsym_hz: Symbol rate in Hz
        :type fsym_hz: float
        :param frequency_offset_hz: Frequency offset in Hz
        :type frequency_offset_hz: float
        :param perform_frequency_sync: Try to perform frequency recovery on PLHEADER
        :type perform_frequency_sync: bool
        :param max_num_plsc_bit_errors: Maximum number of allowable bit errors in PLS code
        :type max_num_plsc_bit_errors: int
        :return:
            - dvbs2_modcod - DVBS2 ModCod with PLHEADER symbols that most closely match plheader_candidate
            - num_plheader_symbol_errors - The number of PLHEADER symbol errors for the returned dvbs2_modcod relative
                                           to plheader_candidate

        : note: We use a crude frequency search over the PLHEADER
        """
        plheader_candidate = symbols[plheader_candidate_index:(plheader_candidate_index + 90)]

        assert len(plheader_candidate) == 90, "Expecting received symbols to by 90 elements long"
        sof_symbols = DVBS2_ModCod_Configuration.generate_sof_symbols()

        if perform_frequency_sync:
            # Perform coarse correction
            if frequency_offset_hz is None:
                corrected_symbols, results_dict = self.compute_carrier_offset(received_symbols=plheader_candidate,
                                                                              training_symbols=sof_symbols,
                                                                              fsym_hz=fsym_hz)
                frequency_offset_hz = results_dict['frequency_offset_hz']
                phase_offset_rad = results_dict['coarse_phase_correction']
            else:
                corrected_symbols = carrier_frequency_shift(plheader_candidate, -1 * frequency_offset_hz,
                                                            t_sec=np.linspace(0, len(plheader_candidate) - 1,
                                                                              len(plheader_candidate)) / fsym_hz)

                phase_difference = np.angle(corrected_symbols[0:len(sof_symbols)] / sof_symbols)
                phase_difference = process_phase_measurements(phase_difference)
                phase_offset_rad = np.mean(phase_difference)
                corrected_symbols = carrier_phase_shift(corrected_symbols, -1.0*phase_offset_rad, [])

        else:
            corrected_symbols = plheader_candidate

        amplitude = np.mean(np.abs(plheader_candidate))
        corrected_symbols = corrected_symbols / amplitude

        plscode_bits = self.demod_plheader_symbols(corrected_symbols)
        results_dict = {'dvbs2_modcod': None}

        # Determine the 7th bit of the PLSCODE (See 5.5.2.4 of ETSI EN 302 307 for explanation).  Use this to reduce
        # the search space
        pilots_on = 1 * (np.sum(plscode_bits[0::2] == plscode_bits[1::2]) < 16)  # PILOTS (0=Off, 1=On)
        if pilots_on == 0:
            dvbs2_pl_header_data = [dvbs2_pl_header_data for dvbs2_pl_header_data in self.dvbs2_pl_header_data if
                                    dvbs2_pl_header_data['dvbs2_modcod'].pilots is DVBS2_Pilots.Pilots_Off]
            if len(dvbs2_pl_header_data) == 0:
                # Condition: We're configured to look only for Pilots_On but we incorrectly detect Pilots_Off
                dvbs2_pl_header_data = \
                    [dvbs2_pl_header_data for dvbs2_pl_header_data in self.dvbs2_pl_header_data if
                     dvbs2_pl_header_data['dvbs2_modcod'].pilots is DVBS2_Pilots.Pilots_On]

        else:
            dvbs2_pl_header_data = [dvbs2_pl_header_data for dvbs2_pl_header_data in self.dvbs2_pl_header_data if
                                    dvbs2_pl_header_data['dvbs2_modcod'].pilots is DVBS2_Pilots.Pilots_On]
            if len(dvbs2_pl_header_data) == 0:
                # Condition: We're configured to look only for Pilots_Off but we incorrectly detect Pilots_On
                dvbs2_pl_header_data = \
                    [dvbs2_pl_header_data for dvbs2_pl_header_data in self.dvbs2_pl_header_data if
                     dvbs2_pl_header_data['dvbs2_modcod'].pilots is DVBS2_Pilots.Pilots_Off]

        if len(dvbs2_pl_header_data) == 0:
            return results_dict

        # Could implement a decoder.  For now just do ML detection
        correlation_sums = []
        for modcod_index, (dvbs2_modcod, pls_code) in \
                enumerate([[data['dvbs2_modcod'], data['pls_code']] for data in dvbs2_pl_header_data]):
            correlation_sums.append(np.sum(plscode_bits == pls_code))
            # TODO: Don't exit on first that exceeds threshold, try them all
            if correlation_sums[-1] >= 64-max_num_plsc_bit_errors:
                break

        if np.max(correlation_sums) < 64 - max_num_plsc_bit_errors:
            correlation_sums = []
            for modcod_index, (dvbs2_modcod, pls_symbols) in \
                    enumerate([[data['dvbs2_modcod'], data['pls_symbols']] for data in dvbs2_pl_header_data]):
                correlation_sums.append(np.abs(np.sum(corrected_symbols * np.conj(pls_symbols))))
                if correlation_sums[-1] >= 64-max_num_plsc_bit_errors:
                    break
            if False:
                modcod_index = 0
                pls_symbols = dvbs2_pl_header_data[modcod_index]['pls_symbols']
                plt.figure()
                plt.plot(np.abs(correlation_sums))
                plt.figure()
                plt.plot(np.angle(corrected_symbols), '.')
                plt.plot(np.angle(pls_symbols), 'x')
                plt.plot(np.angle(corrected_symbols/pls_symbols), 'o')
                plt.figure()
                plt.plot(np.angle(corrected_symbols * np.conj(pls_symbols)), '.')
                plt.plot(np.abs(corrected_symbols * np.conj(pls_symbols)), '.')

        if np.max(correlation_sums) < 64-max_num_plsc_bit_errors:
            return results_dict

        debug_plots = False
        if debug_plots:
            plt.figure()
            plt.plot(correlation_sums)

        modcod_index = int(np.argmax(correlation_sums))
        dvbs2_modcod = dvbs2_pl_header_data[modcod_index]['dvbs2_modcod']
        results_dict = {
            'dvbs2_modcod': dvbs2_modcod,
            'num_plheader_symbol_errors':
                np.sum(np.abs(plscode_bits != dvbs2_pl_header_data[modcod_index]['pls_code'])),
            'frequency_offset_hz': frequency_offset_hz,
            'snr_db': 1 / np.var(corrected_symbols - dvbs2_modcod.generate_pls_symbols()),
            'amplitude': amplitude,
            'coarse_phase_offset_rad': phase_offset_rad
        }

        return results_dict


    def plheader_initial_acq(self,
                             symbols: np.ndarray,
                             fsym_hz: float,
                             current_symbol_index: int = 0,
                             frequency_offset_hz: float = None,
                             frequency_adjustments_symbols_hz: float = 0.0,
                             search_for_pl_headers: bool = True,
                             max_num_sof_symbol_errors: int = 2,
                             max_num_plsc_symbol_errors: int = 2):
        """
        An annoyingly slow but reliable method of getting the first PLFRAME in a set of symbols that have not been
        frequency synchronized
        :param symbols:
        :param fsym_hz:
        :param current_symbol_index:
        :param frequency_offset_hz:
        :param frequency_adjustments_symbols_hz:
        :param search_for_pl_headers:
        :param max_num_sof_symbol_errors:
        :param max_num_plsc_symbol_errors:
        :return:
        """

        # Define the search window to be big enough to contain two PLFRAMES
        num_consecutive_plframes = 3
        max_num_symbols = int(num_consecutive_plframes * (90 + 360 * 90 + np.ceil(360/16) * 36))

        # If we have to search, then we use search_for_pl_headers() to find 90 element long sequences of symbols
        # that appear to be pi/2 BPSK encoded
        freq_search_step_hz = fsym_hz / 360  # Min step size should be fsym_hz / 90 to ensure no PLHEADER rotation
        # Min step size should be fsym_hz / (16*90) to ensure no aliasing using pilots
        # freq_search_step_hz = fsym_hz / (16*90)
        freq_search_offset_hz = 250e3  # Search +/- freq_search_offset_hz centered about 0 hz
        freq_search_offset_hz = freq_search_step_hz * np.ceil(freq_search_offset_hz/freq_search_step_hz)
        freq_search_steps = np.arange(start=-1*freq_search_offset_hz, stop=freq_search_offset_hz,
                                      step=freq_search_step_hz)
        freq_search_steps = freq_search_steps[np.argsort(np.abs(freq_search_steps))]

        # Thought process:
        # 1) Low SNR carriers (QPSK and 8PSK) can be recovered using MFD techniques
        # 2) Higher SNR carriers (16APSK and 32APSK) can be recovered by using intra-pilot block measurements
        min_detection_snr = 5
        min_number_of_detections = 3
        detections = []
        for idx, initial_frequency_offset_hz in enumerate(freq_search_steps):
            self.logger.info('Testing frequency offset {} Hz ({}/{})'.format(
                initial_frequency_offset_hz, idx, len(freq_search_steps)))
            if True and len(detections) > 0 and \
                    np.min([dvbs2_frame.plheader_snr for dvbs2_frame in detections[-1]['dvbs2_frames']]) > min_detection_snr:
                break
            symbols_offset = \
                carrier_frequency_shift(iq=symbols, frequency_hz=initial_frequency_offset_hz, fs_hz=fsym_hz)
            plheader_candidate_indicies = \
                self.plheader_search(symbols=symbols_offset, current_symbol_index=current_symbol_index,
                                     max_num_symbols=max_num_symbols, max_plsheader_symbol_errors=5)
            for plheader_candidate_index in plheader_candidate_indicies:
                next_symbol_index, dvbs2_frame = \
                    self.get_next_plframe(symbols=symbols_offset, fsym_hz=fsym_hz,
                                          current_symbol_index=plheader_candidate_index,
                                          frequency_offset_hz=0.0,
                                          max_num_symbols=-0,
                                          search_for_pl_headers=False,
                                          max_num_plsc_symbol_errors=12,
                                          max_num_sof_symbol_errors=8)
                if dvbs2_frame is None or \
                        dvbs2_frame.start_index + dvbs2_frame.dvbs2_modcod.get_num_symbols()+90 >= len(symbols_offset):
                    continue

                # At this point, the only frequency estimate we have for dvbs2_frame is frequency_offset_hz
                # from our search
                plframe_symbols = \
                    deepcopy(symbols_offset[dvbs2_frame.start_index:
                                            (dvbs2_frame.start_index + dvbs2_frame.dvbs2_modcod.get_num_symbols()+90)])
                dvbs2_frame.plframe_symbols = plframe_symbols
                self.descramble_plframe(dvbs2_frame)

                # Try to get a better frequency estimate.
                # TODO: Better handle low C/N carriers using Short frames
                frequency_offset_syms_hz = 0.0
                if dvbs2_frame.dvbs2_modcod.get_mod_order() == 4 or dvbs2_frame.dvbs2_modcod.get_mod_order() == 8:
                    frequency_offset_syms_hz, frequency_offset_snr = \
                        carrier_recovery_mfd(iq=dvbs2_frame.plframe_symbols,
                                             mod_order=dvbs2_frame.dvbs2_modcod.get_mod_order(),
                                             fs_hz=fsym_hz, max_offset_hz=freq_search_offset_hz)
                else:
                    frequency_offset_snr = np.inf
                if not (dvbs2_frame.dvbs2_modcod.get_mod_order() == 4 or
                        dvbs2_frame.dvbs2_modcod.get_mod_order() == 8) \
                        or frequency_offset_snr < 8.5:
                    plframe_symbols_corrected, results_dict = \
                        self.get_corrected_plframe(symbols=dvbs2_frame.plframe_symbols, fsym_hz=fsym_hz,
                                                   dvbs2_modcod=dvbs2_frame.dvbs2_modcod, mode='fine')
                    frequency_offset_syms_hz = results_dict['frequency_offset_coarse_hz']

                # Now use this improved frequency estimate and re-run processing
                refined_frequency_offset_hz = initial_frequency_offset_hz - frequency_offset_syms_hz
                symbols_offset = \
                    carrier_frequency_shift(iq=symbols, frequency_hz=refined_frequency_offset_hz, fs_hz=fsym_hz)

                dvbs2_frames = \
                    self.find_plframes(symbols=symbols_offset, fsym_hz=fsym_hz,
                                       first_plframe_index=plheader_candidate_index,
                                       max_num_plframes=num_consecutive_plframes, initial_frequency_offset_hz=0.0,
                                       frequency_adjustments_hz=[refined_frequency_offset_hz])

                if len(dvbs2_frames) == num_consecutive_plframes:
                    detection = {'initial_frequency_offset_hz': frequency_offset_hz,
                                 'refined_frequency_offset_hz': refined_frequency_offset_hz,
                                 'symbol_index': plheader_candidate_index,
                                 'dvbs2_frames': dvbs2_frames}
                    detections.append(detection)
                    break  # Don't try subsequent plheader_candidate_indicies
                else:
                    continue  # Try subsequent plheader_candidate_indicies

            # Consider breaking out of loop of len(dvbs2_frames) == num_consecutive_plframes,
            # but for now try all freq offsets

        sorted_detection_indicies = \
            np.argsort([np.sum([dvbs2_frame.plheader_snr for dvbs2_frame in detection['dvbs2_frames']])
                        for detection in detections])
        for detection_index in sorted_detection_indicies[::-1]:
            plheader_snrs = [dvbs2_frame.plheader_snr for dvbs2_frame in detections[detection_index]['dvbs2_frames']]
            if np.mean(plheader_snrs) >= 7.0 and np.min(plheader_snrs) >= 5.0:
                dvbs2_frame = detections[detection_index]['dvbs2_frames'][0]
                next_symbol_index = dvbs2_frame.start_index + dvbs2_frame.dvbs2_modcod.get_num_symbols()
                return next_symbol_index, dvbs2_frame

        # Now check and make sure our results are consistent
        symbol_indicies = set([detection['symbol_index'] for detection in detections])
        dvbs2_frames_list = []
        snr_threshold = 1.0  # TODO: Fix this arbitrarily-selected value
        for start_index in symbol_indicies:
            detections_ = [detection for detection in detections if detection['symbol_index'] == start_index]
            modcods = []
            start_indicies = []
            snrs = []
            refined_frequency_offsets_hz = []
            for detection in detections_:
                modcods.append([det.dvbs2_modcod for det in detection['dvbs2_frames']])
                start_indicies.append([det.start_index for det in detection['dvbs2_frames']])
                snrs.append([det.plheader_snr for det in detection['dvbs2_frames']])
                refined_frequency_offsets_hz.append(detection['refined_frequency_offset_hz'])
            mean_snrs = 10*np.log10([np.mean(10 ** (np.asarray(snrs_) / 10)) for snrs_ in snrs])
            if np.max(mean_snrs) > snr_threshold:
                dvbs2_frames_list.append(detections_[np.argmax(mean_snrs)]['dvbs2_frames'])
        if len(dvbs2_frames_list) > 0:
            start_indicies = [dvbs2_frames[0].start_index for dvbs2_frames in dvbs2_frames_list]
            dvbs2_frame = dvbs2_frames_list[np.argmax(start_indicies)][0]
            next_symbol_index = dvbs2_frames_list[np.argmax(start_indicies)][0].start_index + \
                                dvbs2_frames_list[np.argmax(start_indicies)][0].dvbs2_modcod.get_num_symbols()
        else:
            next_symbol_index = -1
            dvbs2_frame = None
        return next_symbol_index, dvbs2_frame


    def demod_plheader_symbols(self, corrected_symbols, perform_fec=True):
        """
        Demods PLHEADER symbols and returns descrambled bits

        :param corrected_symbols: Frequency and phase corrected symbols to demodulate
        :type corrected_symbols: np.ndarray
        :param perform_fec: Attempt to use 7th bit to do some sanity checking on returned bits
        :type perform_fec: bool
        """
        plscode_soft_bits_even = -1 * np.asarray(np.real(corrected_symbols[26::2]) + np.imag(corrected_symbols[26::2]))
        plscode_soft_bits_odd = np.asarray(np.real(corrected_symbols[27::2]) - np.imag(corrected_symbols[27::2]))
        plscode_soft_bits = \
            np.squeeze(np.transpose(np.asarray([plscode_soft_bits_even, plscode_soft_bits_odd])).reshape((64, 1)))
        # plscode_soft_bits = plscode_soft_bits / np.mean(np.abs(plscode_soft_bits))
        # Apply descrambing sequence
        for plscode_index in range(0, len(DVBS2_ModCod_Configuration.pls_code_scrambling_sequence)):
            if plscode_soft_bits[plscode_index] > 0.0 and \
                    DVBS2_ModCod_Configuration.pls_code_scrambling_sequence[plscode_index] == 1:
                plscode_soft_bits[plscode_index] = -1.0 * plscode_soft_bits[plscode_index]
            elif plscode_soft_bits[plscode_index] < 0.0 and \
                    DVBS2_ModCod_Configuration.pls_code_scrambling_sequence[plscode_index] == 1:
                plscode_soft_bits[plscode_index] = -1.0 * plscode_soft_bits[plscode_index]
        plscode_bits = np.asarray(plscode_soft_bits > 0, dtype=int)

        # Apply some error correction based on the 7th bit
        if perform_fec:
            # FECFRAME size (0=Normal, 1=Short)
            frame_size = 1 * (np.sum(plscode_bits[0::2] == plscode_bits[1::2]) < 16)
            if frame_size == 0:
                plscode_bits[0::2] = np.asarray(plscode_soft_bits[0::2] + plscode_soft_bits[1::2] > 0, dtype=int)
            else:
                plscode_bits[0::2] = np.asarray(plscode_soft_bits[0::2] - plscode_soft_bits[1::2] > 0, dtype=int)
            plscode_bits[1::2] = plscode_bits[0::2] ^ frame_size

        return plscode_bits


    def perform_symbol_corrections(self, symbols: np.ndarray, fsym_hz: float, t_symbols_sec: np.ndarray,
                                   dvbs2_frames: list):
        """
        Computes and applies corrections to symbols and resampled IQ data based on known symbols in PLHEADERs and
        in Pilot symbols
        :param symbols: Symbols
        :param fsym_hz: Symbol rate in Hz
        :param t_symbols_sec: Time vector for Symbol data
        :param dvbs2_frames: List of DVBS2Frames objects
        :return:
            results_dicts_this_block: results_dict for every PLFRAME found in symbols
            first_plframe_symbol_index: index into symbols of the first PLFRAME
            frequency_offset_hz: Detected frequency offset of symbols
            carrier_phase_rad: Detected carrier phase offset of symbols
            cs_amplitude: Amplitude correction interpolation function
            cs_phase: Phase correction interpolation function
            iq_resampled: (Optional) Resampled IQ data after amplitude and phase corrections
            t_iq_resampled_sec: (Optional) Time vector of resampled IQ data after amplitude and phase corrections
            symbols_corrected: Symbols after alignment to first PLFRAME and amplitude and phase corrections
            t_symbols_sec: Time vector for symbols after alignment to first PLFRAME and amplitude and phase corrections

        Can operate on individual PLFRAMES or across large sets of symbols containing multiple PLFRAMES.

        Approach is the following:
        1) Operate across all PLFRAMES in the dvbs2_frames list and get averaged phase and amplitude estimates from each
           block of PLHEADER or PILOT symbols
        2) Using these estimates, perform a linear phase fit to come up with a fine estimate of carrier frequency offset
        3) Correct for frequency based on the estimate found in Step 2
        4) Iterate over the (corrected) PLFRAMES again to get estimates of residual phase drift and dither
        5) Generate a cublic spline estimate of this residual phase error and apply it back to the original symbols
        """
        results_dicts_this_block = []
        results_dicts_pl_frame = []  # TODO: Remove this

        first_plframe_symbol_index = dvbs2_frames[0].start_index

        #
        # Step 1: Operate across all PLFRAMES in the dvbs2_frames list and get averaged phase and amplitude estimates
        #         from each block of PLHEADER or PILOT symbols
        #
        time_offsets_list_of_lists = []
        phase_offsets_rad_list_of_lists = []
        amplitude_list_of_lists = []
        for dvbs2_frame in dvbs2_frames:
            num_symbols_this_plframe = dvbs2_frame.dvbs2_modcod.get_num_symbols() + 90
            if dvbs2_frame.start_index + num_symbols_this_plframe > len(symbols):
                break
            dvbs2_frame.t_plframe_symbols = \
                t_symbols_sec[0] + dvbs2_frame.start_index / fsym_hz + \
                np.linspace(0, num_symbols_this_plframe - 1, num_symbols_this_plframe) / fsym_hz
            dvbs2_frame.plframe_symbols = \
                symbols[dvbs2_frame.start_index:(dvbs2_frame.start_index + num_symbols_this_plframe)]
            self.descramble_plframe(dvbs2_frame)

            # ---- Compute and apply fine corrections to this frame
            if dvbs2_frame == dvbs2_frames[-1]:
                time_offsets_lists, phase_offsets_rad_lists, amplitude_lists = \
                    self.get_pilot_measurements(symbols=dvbs2_frame.plframe_symbols,
                                                t_symbols_start_sec=dvbs2_frame.t_plframe_symbols[0],
                                                fsym_hz=fsym_hz, dvbs2_modcod=dvbs2_frame.dvbs2_modcod,
                                                use_next_pl_header=True)
            else:
                time_offsets_lists, phase_offsets_rad_lists, amplitude_lists = \
                    self.get_pilot_measurements(symbols=dvbs2_frame.plframe_symbols[0:dvbs2_frame.dvbs2_modcod.get_num_symbols()],
                                                t_symbols_start_sec=dvbs2_frame.t_plframe_symbols[0],
                                                fsym_hz=fsym_hz, dvbs2_modcod=dvbs2_frame.dvbs2_modcod,
                                                use_next_pl_header=False)

            time_offsets_list_of_lists.append(time_offsets_lists)
            phase_offsets_rad_list_of_lists.append(phase_offsets_rad_lists)
            amplitude_list_of_lists.append(amplitude_lists)

        time_offsets = []
        for time_offsets_list_of_lists_ in time_offsets_list_of_lists:
            for time_offsets_list in time_offsets_list_of_lists_:
                for time_offset in time_offsets_list:
                    time_offsets.append(time_offset)
        phase_offsets_rad = []
        for phase_offsets_rad_list_of_lists_ in phase_offsets_rad_list_of_lists:
            for phase_offsets_rad_list in phase_offsets_rad_list_of_lists_:
                for phase_offset_rad in phase_offsets_rad_list:
                    phase_offsets_rad.append(phase_offset_rad)
        phase_offsets_rad = process_phase_measurements(phase_offsets_rad)
        amplitudes = []
        for amplitudes_list_of_lists_ in amplitude_list_of_lists:
            for amplitudes_list in amplitudes_list_of_lists_:
                for amplitude in amplitudes_list:
                    amplitudes.append(amplitude)

        if False:
            plt.figure()
            plt.plot(time_offsets, phase_offsets_rad, '.')

        #
        # Step 2: Using the averaged phase estimates from Step 1, perform a linear phase fit to come up with a fine
        #         estimate of carrier frequency offset
        #
        wls_linear_regression = LinearRegression()
        wls_linear_regression.fit(np.reshape(time_offsets, (len(phase_offsets_rad), 1)),
                                  np.reshape(phase_offsets_rad, (len(phase_offsets_rad), 1)), sample_weight=amplitudes)
        phase_slope = wls_linear_regression.coef_[0][0]
        # carrier_phase_rad = wls_linear_regression.intercept_[0]
        frequency_offset_hz = phase_slope / (2 * np.pi)
        frequency_correction_hz = -1 * frequency_offset_hz

        #
        # Step 3: Correct for frequency based on the estimate found in Step 2
        #
        symbols = carrier_frequency_shift(iq=symbols, frequency_hz=frequency_correction_hz, t_sec=t_symbols_sec,
                                          inplace=True)
        for dvbs2_frame in dvbs2_frames:
            dvbs2_frame.frequency_adjustments_hz.append(frequency_correction_hz)

        #
        # Step 4: Iterate over the (corrected) PLFRAMES again to get estimates of residual phase drift and dither
        #
        for dvbs2_frame in dvbs2_frames:
            # ---- Compute and apply fine corrections to this frame
            symbols_, results_dict_plframe_fine = \
                self.get_corrected_plframe(symbols=dvbs2_frame.plframe_symbols,
                                           t_symbols_start_sec=dvbs2_frame.t_plframe_symbols[0],
                                           fsym_hz=fsym_hz, dvbs2_modcod=dvbs2_frame.dvbs2_modcod, mode='fine')

            # ---- Update some statistics that we'll use later
            dvbs2_frame.phase_offset_rad = results_dict_plframe_fine['phase_offsets_rad'][0]
            dvbs2_frame.amplitude_offset = np.mean(results_dict_plframe_fine['amplitudes'])

            do_fine_plframe_corrections = True
            if do_fine_plframe_corrections: # Use interpolated phase measurements to correct data symbols (high fidelity)
                dvbs2_frame.frequency_offset_hz = np.sum(dvbs2_frame.frequency_adjustments_hz)
                plframe_symbols = \
                    self.apply_corrections(results_dict_plframe_fine['cs_amplitude'],
                                           results_dict_plframe_fine['cs_phase'],
                                           dvbs2_frame.plframe_symbols, dvbs2_frame.t_plframe_symbols)
                dvbs2_frame.plframe_symbols_corrected = plframe_symbols
            else: # Just use carrier phase and frequency estimates for each PLFRAME to correct (lower fidelity)
                dvbs2_frame.frequency_adjustments_hz.append(-1 * results_dict_plframe_fine['frequency_offset_fine_hz'])
                dvbs2_frame.frequency_offset_hz = np.sum(dvbs2_frame.frequency_adjustments_hz)

                dvbs2_frame.plframe_symbols_corrected = \
                    carrier_phase_shift(dvbs2_frame.plframe_symbols, -1*dvbs2_frame.phase_offset_rad,
                                        dvbs2_frame.t_plframe_symbols)
                dvbs2_frame.plframe_symbols_corrected = \
                    carrier_frequency_shift(dvbs2_frame.plframe_symbols_corrected,
                                            -1*results_dict_plframe_fine['frequency_offset_fine_hz'], fs_hz=fsym_hz)
                dvbs2_frame.plframe_symbols_corrected = \
                    dvbs2_frame.plframe_symbols_corrected / dvbs2_frame.amplitude_offset

            dvbs2_frame.esn0_db = compute_plframe_statistics(symbols=dvbs2_frame.plframe_symbols_corrected,
                                                             dvbs2_modcod=dvbs2_frame.dvbs2_modcod)

            if False:
                plt.plot(
                    dvbs2_frame.t_plframe_symbols, results_dict_plframe_fine['cs_phase'](dvbs2_frame.t_plframe_symbols))
                plt.plot(
                    results_dict_plframe_fine['time_offsets_sec'], results_dict_plframe_fine['phase_offsets_rad'], 'o')

            results_dicts_pl_frame.append(results_dict_plframe_fine)

        #
        # -------------- Now That We've Processed All PLFRAMES, Combine Corrections and Apply to Symbols --------------
        #
        # TODO: This seems fragile...
        for pl_index in range(0, len(results_dicts_pl_frame) - 1):
            if (results_dicts_pl_frame[pl_index]['time_offsets_sec'][-1] -
                results_dicts_pl_frame[pl_index + 1]['time_offsets_sec'][0]) < 45.0 / fsym_hz:
                while results_dicts_pl_frame[pl_index]['phase_offsets_rad'][-1] - \
                        results_dicts_pl_frame[pl_index + 1]['phase_offsets_rad'][0] > np.pi:
                    for pl_index2 in range(pl_index + 1, len(results_dicts_pl_frame)):
                        results_dicts_pl_frame[pl_index2]['phase_offsets_rad'] += 2 * np.pi
                while results_dicts_pl_frame[pl_index]['phase_offsets_rad'][-1] - \
                        results_dicts_pl_frame[pl_index + 1]['phase_offsets_rad'][0] < -1.0 * np.pi:
                    for pl_index2 in range(pl_index + 1, len(results_dicts_pl_frame)):
                        results_dicts_pl_frame[pl_index2]['phase_offsets_rad'] -= 2 * np.pi
                # Nominally I'd like to remove the last element from everything in results_dicts_pl_frame[index]
                # but don't want to east the cost of resizing arrays

        # Get single vectors for the time, phase, and amplitude values contained in results_dict_pl_frame
        time_offsets_sec = np.asarray([item for sublist in
                                       [results_dict_pl_frame['time_offsets_sec']
                                        for results_dict_pl_frame in results_dicts_pl_frame] for item in sublist])
        phase_offsets_rad = np.asarray([item for sublist in
                                        [results_dict_pl_frame['phase_offsets_rad']
                                         for results_dict_pl_frame in results_dicts_pl_frame] for item in sublist])
        phase_offsets_rad = process_phase_measurements(phase_offsets_rad)
        amplitudes = np.asarray([item for sublist in
                                 [results_dict_pl_frame['amplitudes']
                                  for results_dict_pl_frame in results_dicts_pl_frame] for item in sublist])

        # Compute the spline-interpolation functions
        interp_mode = 'linear'
        if interp_mode == 'cubic_spline':  # Works good where we have data, but extrapolation is worthless
            cs_phase = CubicSpline(time_offsets_sec, phase_offsets_rad, extrapolate=True)
            cs_amplitude = CubicSpline(time_offsets_sec, amplitudes, extrapolate=True)
        elif interp_mode == 'linear':  # Less good where we have data, but better extrapolation
            cs_phase = interp1d(time_offsets_sec, phase_offsets_rad, kind='linear', bounds_error=False,
                                fill_value='extrapolate')
            pilots_amplitude_slope, pilots_amplitude_intercept, pilots_r_value, pilots_p_value, std_err = \
                stats.linregress(time_offsets_sec, amplitudes)
            amplitudes_linear = pilots_amplitude_intercept + time_offsets_sec * pilots_amplitude_slope
            cs_amplitude = interp1d(time_offsets_sec, amplitudes_linear, kind='linear', fill_value='extrapolate')
            # cs_amplitude = interp1d(time_offsets_sec, amplitudes, kind='linear', bounds_error=False,
            #                         fill_value=(amplitudes[0], amplitudes[-1]))
            # TODO: The above is a fix to extrapolation issues, but results in unnecessary computation

        if False:
            plt.figure()
            plt.subplot(2, 1, 1)
            plt.plot(t_symbols_sec, cs_amplitude(t_symbols_sec), '.')
            plt.xlabel('Time (sec)')
            plt.ylabel('Amplitude Correction')
            plt.title('Amplitude Correction')
            plt.subplot(2, 1, 2)
            plt.plot(t_symbols_sec, cs_phase(t_symbols_sec), '.')
            plt.xlabel('Time (sec)')
            plt.ylabel('Phase Correction (rad)')
            plt.title('Phase Correction')

        wls_linear_regression = LinearRegression()
        wls_linear_regression.fit(np.reshape(time_offsets_sec, (len(time_offsets_sec), 1)),
                                  np.reshape(phase_offsets_rad, (len(phase_offsets_rad), 1)), sample_weight=amplitudes)
        phase_slope = wls_linear_regression.coef_[0][0]
        carrier_phase_rad = wls_linear_regression.intercept_[0]
        frequency_offset_hz = phase_slope / (2 * np.pi)

        # Apply corrections to symbols
        symbols_corrected = self.apply_corrections(cs_amplitude, cs_phase, symbols, t_symbols_sec)

        return results_dicts_this_block, first_plframe_symbol_index, frequency_offset_hz, carrier_phase_rad, \
               cs_amplitude, cs_phase, symbols_corrected, t_symbols_sec


    @staticmethod
    def apply_corrections(cs_amplitude: CubicSpline, cs_phase: CubicSpline, symbols: np.ndarray, t_sec: np.ndarray):
        """
        Applies amplitude and phase corrections to symbols in the for of A * exp(j*phi)
        :param cs_amplitude: scipy.interpolate.CubicSpline representing time series of amplitude corrections (A)
        :param cs_phase: scipy.interpolate.CubicSpline representing time series of phase corrections (phi)
        :param symbols: The symbols to correct
        :param t_sec: Time vector of symbols
        :return: Corrected symbols
        """
        amplitude = cs_amplitude(t_sec)
        phase_offset_hz = cs_phase(t_sec)
        symbols = symbols * np.exp(1j * -phase_offset_hz) / amplitude
        return symbols


    def compute_carrier_offset(self, received_symbols: np.ndarray, training_symbols: np.ndarray, fsym_hz: float):
        """
        Processes received symbols and training symbols to compute carrier frequency and phase offsets
        :param received_symbols: Nx1 array of received symbols
        :param training_symbols: Nx1 array of training symbols
        :param fsym_hz: Symbol rate of received and training symbols
        :return:
        """
        # See "Single-Tone Parameter Estimation from Qiscrete-Time Observations", David C Rife and Robert R. Boorstyn
        Z = np.fft.fftshift(np.fft.fft(received_symbols[0:len(training_symbols)] * np.conj(training_symbols), 1024))
        freqs = np.fft.fftshift(np.fft.fftfreq(n=1024, d=1/fsym_hz))
        max_index = np.argmax(np.abs(Z))
        frequency_offset_hz = freqs[max_index]
        coarse_phase_correction = -1 * np.angle(Z[max_index])
        if False:
            plt.figure()
            plt.plot(freqs, 10*np.log10(np.abs(Z)))
            plt.grid(True)
            plt.xlabel('Frequency')
            plt.ylabel('Amplitude')
            plt.title('Frequency Search (detected offset = {:3.3f} hz)'.format(frequency_offset_hz))

        results_dict = {'frequency_offset_hz': frequency_offset_hz,
                        'coarse_phase_correction': coarse_phase_correction}
        corrected_symbols = carrier_frequency_shift(received_symbols, -1 * frequency_offset_hz, fs_hz=fsym_hz)
        corrected_symbols = carrier_phase_shift(corrected_symbols, coarse_phase_correction, [])
        corrected_symbols = corrected_symbols / np.mean(np.abs(corrected_symbols))

        # Perform additional phase correction
        phase_offset_rad = \
            np.mean(process_phase_measurements(
                np.angle(corrected_symbols[0:len(training_symbols)] / training_symbols)))
        corrected_symbols = np.exp(1j*2*np.pi*phase_offset_rad) * corrected_symbols

        return corrected_symbols, results_dict


    def get_pilot_measurements(self, symbols: np.ndarray, fsym_hz: float, dvbs2_modcod: DVBS2_ModCod_Configuration,
                               t_symbols_start_sec: float = 0.0, use_next_pl_header: bool = True):
        """
        Performs frequency and amplitude correction of specified PL frame

        :param symbols: PLFRAME symbols to correct.  Assumes the length is one full PLFRAME + 90 symbols for
                        the PLHEADER of the next frame.  Also assumes the symbols are descrambled.
        :type symbols: np.ndarray
        :param fsym_hz: Symbol rate in Hz
        :type fsym_hz: float
        :param dvbs2_modcod: DVBS2_ModCod_Configuration object associated with the specified PL frame
        :type dvbs2_modcod: DVBS2_ModCod_Configuration
        :param t_symbols_start_sec: Starting time of first symbol in symbols
        :type t_symbols_start_sec: float
        :param use_next_pl_header: Whether to use next PLFRAME head in computations
        :type use_next_pl_header: float
        :return: Corrected phase, frequency, and amplitude corrected symbols
        """
        # TODO: Need to characterize the amount of frequency offset symbols can have and not introduce
        #  ambiguities in get_corrected_plframe()
        assert len(symbols) == dvbs2_modcod.get_num_symbols() or \
               len(symbols) == dvbs2_modcod.get_num_symbols() + 90, \
            'Expecting number of symbols to be exactly PLFRAME length or PLFRAME length + 90'
        time_offsets_lists = []
        phase_offsets_rad_lists = []
        amplitude_lists = []

        plheader_indicies = np.linspace(0, 89, 90, dtype=np.int)

        # Perform coarse frequency and phase correction using the PL header
        plheader_phase_difference = process_phase_measurements(
            np.angle(symbols[plheader_indicies])-np.angle(dvbs2_modcod.generate_pls_symbols()))
        plheader_amplitude = np.abs(symbols[plheader_indicies])
        phase_offsets_rad = [np.mean(plheader_phase_difference)]
        amplitudes = [np.mean(plheader_amplitude)]

        time_offsets_lists.append(t_symbols_start_sec+plheader_indicies/fsym_hz)
        phase_offsets_rad_lists.append(plheader_phase_difference)
        amplitude_lists.append(plheader_amplitude)

        if dvbs2_modcod.pilots == DVBS2_Pilots.Pilots_On:
            # Use Pilot Symbols to recover frequency
            pilot_symbol_indices = dvbs2_modcod.get_pilot_symbol_indicies()
            for block in range(0, int(len(pilot_symbol_indices)/36)):
                symbols_this_block = symbols[np.asarray(pilot_symbol_indices[(block * 36):(block * 36 + 36)])]
                pilot_phase_offsets_rad = process_phase_measurements(np.angle(symbols_this_block)) - np.pi/4
                pilot_amplitudes = np.abs(symbols_this_block)
                time_offsets_sec = \
                    t_symbols_start_sec +\
                    np.linspace(pilot_symbol_indices[block * 36], pilot_symbol_indices[block * 36 + 35], 36) / fsym_hz

                time_offsets_lists.append(time_offsets_sec)
                phase_offsets_rad_lists.append(pilot_phase_offsets_rad)
                amplitude_lists.append(pilot_amplitudes)
        # TODO: Implement DUMMY PLFRAME handling

        next_plheader_indicies = \
            np.linspace(dvbs2_modcod.get_num_symbols(), dvbs2_modcod.get_num_symbols() + 89, 90, dtype=np.int)
        if use_next_pl_header:
            next_plheader_sof_indicies = next_plheader_indicies[0:26]
            sof_phase_difference = process_phase_measurements(np.angle(symbols[next_plheader_sof_indicies]) -
                                                              np.angle(dvbs2_modcod.generate_sof_symbols()))
            phase_offset_rad = np.mean(sof_phase_difference)
            phase_offsets_rad.append(phase_offset_rad)
            amplitude = np.mean(np.abs(symbols[next_plheader_sof_indicies]))
            amplitudes.append(amplitude)
            time_offsets_sec = t_symbols_start_sec + \
                               np.linspace(next_plheader_sof_indicies[0], next_plheader_sof_indicies[-1], 26) / fsym_hz

            time_offsets_lists.append(time_offsets_sec)
            phase_offsets_rad_lists.append(sof_phase_difference)
            amplitude_lists.append(np.abs(symbols[next_plheader_sof_indicies]))

        if False:
            plt.figure()
            for index in range(0, len(time_offsets_lists)):
                plt.plot(time_offsets_lists[index], phase_offsets_rad_lists[index], '.')
                plt.plot(np.mean(time_offsets_lists[index]), np.mean(phase_offsets_rad_lists[index]), 'o')


        return time_offsets_lists, phase_offsets_rad_lists, amplitude_lists


    def get_corrected_plframe(self, symbols: np.ndarray, fsym_hz: float, dvbs2_modcod: DVBS2_ModCod_Configuration,
                              mode: str = 'fine', t_symbols_start_sec: float = 0.0):
        """
        Performs frequency and amplitude correction of specified PL frame

        :param symbols: PLFRAME symbols to correct.  Assumes the length is one full PLFRAME + 90 symbols for
                        the PLHEADER of the next frame.  Also assumes the symbols are descrambled.
        :type symbols: np.ndarray
        :param fsym_hz: Symbol rate in Hz
        :type fsym_hz: float
        :param dvbs2_modcod: DVBS2_ModCod_Configuration object associated with the specified PL frame
        :type dvbs2_modcod: DVBS2_ModCod_Configuration
        :param mode: Correction mode ('coarse' or 'fine')
        :type mode: str
        :param t_symbols_start_sec: Starting time of first symbol in symbols
        :type t_symbols_start_sec: float
        :return:
            symbols: Corrected symbols for specified PLFRAME
            results_dict: Dictionary of results:
                time_offsets_sec: Time vector for raw phase and amplitude estimates
                frequency_offset_coarse_hz: Coarse frequency estimate
                phase_offsets_rad: Raw phase estimates from pilot symbols used to create CubicSpline interpolation
                cs_phase: CubicSpline interpolation of phase
                amplitudes: Raw amplitude estimates from pilot symbols used to create CubicSpline interpolation
                cs_amplitude: CubicSpline interpolation of amplitude
                frequency_offset_fine_hz: Fine estimate of frequency offset
                next_plframe_modcod: Modcod of next PLFRAME

        Notes
         * Using mean phase rotation of pilot symbols, the maximum frequency offset we can observe is about
              fsym_hz / (16 * 90)
           This is because we have blocks of pilot symbols every 16 slots and there are 90 symbols per slot.
           We sanity check these estimates by looking within pilot blocks and the average phase slope, and try
           to come up with coarse estimate of carrier frequency from that.
        """
        # TODO: Need to characterize the amount of frequency offset symbols can have and not introduce
        #  ambiguities in get_corrected_plframe()
        assert len(symbols) == dvbs2_modcod.get_num_symbols() + 90, \
            'Expecting number of symbols to be exactly PLFRAME length + 90'
        assert mode == 'fine', 'Expecting mode to be fine'

        t_sec = t_symbols_start_sec + np.linspace(0, len(symbols) - 1, len(symbols)) / fsym_hz

        block_frequency_estimates_hz = []
        phase_offsets_rad = []
        amplitudes = []
        time_offsets_block_sec = []
        num_pilots = []

        #
        # Get measurements from PLHEADER and PILOT blocks
        #
        time_offsets_lists, phase_offsets_rad_lists, amplitude_lists = \
            self.get_pilot_measurements(symbols=symbols,
                                        t_symbols_start_sec=t_symbols_start_sec,
                                        fsym_hz=fsym_hz, dvbs2_modcod=dvbs2_modcod)
        for index in range(0, len(time_offsets_lists)):
            phase_offsets_rad.append(np.mean(phase_offsets_rad_lists[index]))
            amplitudes.append(np.mean(amplitude_lists[index]))
            time_offsets_block_sec.append(np.mean([time_offsets_lists[index][0], time_offsets_lists[index][-1]]))
            num_pilots.append(len(time_offsets_lists[index]))

            pilots_phase_slope, pilots_phase_intercept, pilots_r_value, pilots_p_value, std_err = \
                stats.linregress(time_offsets_lists[index],
                                 process_phase_measurements(phase_offsets_rad_lists[index]))
            frequency_offset_block_hz = pilots_phase_slope / (2 * np.pi)
            block_frequency_estimates_hz.append(frequency_offset_block_hz)
        phase_offsets_rad = process_phase_measurements(phase_offsets_rad)

        coarse_frequency_offset_hz = np.mean(block_frequency_estimates_hz)
        frequency_correction_type = 'cubic'
        cs_phase, cs_amplitude = None, None
        wls_linear_regression = LinearRegression()
        wls_linear_regression.fit(np.reshape(time_offsets_block_sec, (len(time_offsets_block_sec), 1)),
                np.reshape(phase_offsets_rad, (len(phase_offsets_rad), 1)),
                sample_weight=num_pilots)
        frequency_offset_fine_hz = wls_linear_regression.coef_[0][0] / (2 * np.pi)
        pilots_phase_intercept = wls_linear_regression.intercept_

        if np.abs(coarse_frequency_offset_hz) > fsym_hz/(16*90):
            self.logger.warning('Warning: detected larger frequency offset than can be detected with pilot tone averaging')
        if frequency_correction_type == 'first order':
            coarse_phase_correction = -1 * pilots_phase_intercept
            symbols = carrier_phase_shift(symbols, coarse_phase_correction, [])
            symbols = carrier_frequency_shift(symbols, -1 * frequency_offset_fine_hz, t_sec=t_sec)
        elif frequency_correction_type == 'cubic':
            cs_phase = CubicSpline(time_offsets_block_sec, phase_offsets_rad, extrapolate=True)
        else:
            raise Exception('Unhandled frequency correction')

        amplitude_correction_type = 'cubic'
        if amplitude_correction_type == 'first order':
            pilots_amplitude_slope, pilots_amplitude_intercept, pilots_r_value, pilots_p_value, std_err =\
                stats.linregress(time_offsets_block_sec, amplitudes)
            amplitude = pilots_amplitude_intercept + np.asarray(time_offsets_block_sec) * pilots_amplitude_slope
            symbols = symbols * amplitude / amplitude
        elif amplitude_correction_type == 'cubic':
            cs_amplitude = CubicSpline(time_offsets_block_sec, amplitudes, extrapolate=True)
        else:
            raise Exception('Unhandled amplitude correction')

        if self.debug_plots:
            mapping, constellation = dvbs2_modcod.get_constellation()
            plt.figure()
            plt.plot(np.asarray(time_offsets_block_sec)*1e3, phase_offsets_rad, '.', label='Phase Difference')
            if mode == 'fine':
                if frequency_correction_type == 'first order':
                    plt.plot(np.asarray(time_offsets_block_sec)*1e3,
                             pilots_phase_intercept + np.asarray(time_offsets_block_sec) * pilots_phase_slope,
                             label='Linear Fit')
                if frequency_correction_type == 'cubic':
                    plt.plot(np.asarray(time_offsets_block_sec)*1e3, cs_phase(time_offsets_block_sec),
                             label='Cubic Fit')
            plt.xlabel('Time (msec)')
            plt.legend()
            plt.grid()
            plt.title('Pilots and SOF Header Carrier Phase Offset')

            fig = scatterplot(symbols[0:(dvbs2_modcod.get_num_symbols())])
            fig = scatterplot(symbols[dvbs2_modcod.get_pilot_symbol_indicies()], fig=fig)
            fig = scatterplot(constellation, fig=fig)
            fig.gca().lines[-1].set_marker('x')
            fig.gca().lines[-1].set_markersize(10)
            fig.gca().lines[-1].set_markeredgewidth(3)
            fig.gca().lines[-1].set_markeredgecolor('y')
            fig.gca().lines[-1].set_markerfacecolor('y')

        # TODO: Correcting amplitude using only the training symbols doesn't appear to completely correct things (e.g.
        # TODO: np.mean(np.abs(symbols)) / np.mean(np.abs(dvbs2_modcod.get_constellation()[1])) != 0).
        # Investigate this.

        # Try to detect next PL header
        next_plheader_indicies = \
            np.linspace(dvbs2_modcod.get_num_symbols(), dvbs2_modcod.get_num_symbols() + 89, 90, dtype=np.int)
        results_dict_plheader_search = \
            self.plheader_process(symbols=symbols, plheader_candidate_index=next_plheader_indicies[0],
                                  fsym_hz=fsym_hz, frequency_offset_hz=0.0)
        next_plframe_dvbs2_modcod = results_dict_plheader_search['dvbs2_modcod']

        results_dict = {'mode': mode,
                        'frequency_offset_coarse_hz': coarse_frequency_offset_hz,
                        'time_offsets_sec': time_offsets_block_sec,
                        'phase_offsets_rad': phase_offsets_rad,
                        'amplitudes': amplitudes,
                        'cs_phase': cs_phase,
                        'cs_amplitude': cs_amplitude,
                        'frequency_offset_fine_hz': frequency_offset_fine_hz,
                        'next_plframe_modcod': next_plframe_dvbs2_modcod}

        return symbols[0:dvbs2_modcod.get_num_symbols()], results_dict


    def coarse_plframe_frequency_recovery(self, symbols: np.ndarray, t_symbols_sec: np.ndarray, fsym_hz: float,
                                          dvbs2_frame: DVBS2_Frame):
        """
        Performs coarse frequency recovery of PLFRAME
        @param symbols: PLFRAME symbols + PLHEADER of next PLFRAME
        @type symbols: np.ndarray
        @param t_symbols_sec: Time vector for symbols
        @type t_symbols_sec: np.ndarray
        @param fsym_hz: Symbol rate in Hz
        @type fsym_hz: float
        @param dvbs2_frame: DVBS2_Frame object associated with symbols
        @type dvbs2_frame: DVBS2_Frame
        @return:
        """
        cf = []
        num_symbols_this_plframe = dvbs2_frame.dvbs2_modcod.get_num_symbols() + 90
        if dvbs2_frame.start_index + num_symbols_this_plframe > len(symbols):
            return cf

        dvbs2_frame.t_plframe_symbols = \
            t_symbols_sec[0] + dvbs2_frame.start_index / fsym_hz + \
            np.linspace(0, num_symbols_this_plframe - 1, num_symbols_this_plframe) / fsym_hz
        dvbs2_frame.plframe_symbols = \
            symbols[dvbs2_frame.start_index:(dvbs2_frame.start_index + num_symbols_this_plframe)]
        dvbs2_frame.plframe_symbols_descrambled = False
        self.descramble_plframe(dvbs2_frame)

        # ---- Compute and apply fine corrections to this frame
        time_offsets_lists, phase_offsets_rad_lists, amplitude_lists = \
            self.get_pilot_measurements(symbols=dvbs2_frame.plframe_symbols,
                                        t_symbols_start_sec=dvbs2_frame.t_plframe_symbols[0],
                                        fsym_hz=fsym_hz, dvbs2_modcod=dvbs2_frame.dvbs2_modcod)

        if False:
            plt.figure()
            plt.plot(np.angle(dvbs2_frame.plframe_symbols[dvbs2_frame.dvbs2_modcod.get_pilot_symbol_indicies()]), '.')

        for index in range(0, len(time_offsets_lists)):
            if False:
                plt.plot(process_phase_measurements(phase_offsets_rad_lists[index]), '.')
            wls_linear_regression = LinearRegression()
            wls_linear_regression.fit(np.reshape(time_offsets_lists[index], (len(time_offsets_lists[index]), 1)),
                    np.reshape(process_phase_measurements(phase_offsets_rad_lists[index]),
                               (len(phase_offsets_rad_lists[index]), 1)),
                    sample_weight=len(phase_offsets_rad_lists[index]))
            frequency_offset_hz = wls_linear_regression.coef_[0][0] / (2 * np.pi)
            cf.append(frequency_offset_hz)

        if False:
            plframe_symbols = \
                symbols[dvbs2_frame.start_index:
                        dvbs2_frame.start_index + dvbs2_frame.dvbs2_modcod.get_num_symbols() + 90]
            dvbs2_frame.plframe_symbols = plframe_symbols
            self.descramble_plframe(dvbs2_frame)

            t_plframe_symbols_sec = \
                t_symbols_sec[dvbs2_frame.start_index:
                              dvbs2_frame.start_index + dvbs2_frame.dvbs2_modcod.get_num_symbols() + 90]

            plframe_symbols = carrier_frequency_shift(iq=plframe_symbols, t_sec=t_plframe_symbols_sec,
                                                      frequency_hz=-1 * dvbs2_frame.plheader_frequency_offset_hz)

            if dvbs2_frame.dvbs2_modcod.pilots == DVBS2_Pilots.Pilots_On:
                # Use Pilot Symbols to recover frequency
                pilot_symbol_indices = dvbs2_frame.dvbs2_modcod.get_pilot_symbol_indicies()
                for block in range(0, int(len(pilot_symbol_indices)/36)):
                    symbols_this_block = \
                        plframe_symbols[np.asarray(pilot_symbol_indices[(block * 36):(block * 36 + 36)])]
                    cf.append(
                        dvbs2_frame.plheader_frequency_offset_hz +
                        np.angle(np.sum(symbols_this_block[1::] * np.conj(symbols_this_block[0:-1]))) * fsym_hz
                        / (2*np.pi))
            else:
                raise Exception('Only works with pilots')

        return cf


    def scramble_plframe(self, dvbs2_modcod: DVBS2_ModCod_Configuration, symbols: np.ndarray):
        """
        Scrambles PLFRAME symbols in accordance with Section 5.5.4 of ETSI EN 302 307 V1.2.1
        :param dvbs2_modcod: ModCod of frame to scramble
        :type dvbs2_modcod: DVBS2_ModCod_Configuration
        :param symbols: Symbols to scramble
        :type symbols: np.ndarray
        :return: void
        """
        symbols[90:(dvbs2_modcod.get_num_symbols())] = \
            symbols[90:(dvbs2_modcod.get_num_symbols())] * \
            np.conj(self.descrambling_sequence[0:(dvbs2_modcod.get_num_symbols() - 90)])


    def descramble_plframe(self, dvbs2_frame: DVBS2_Frame):
        """
        Descrambles PLFRAME symbols in accordance with Section 5.5.4 of ETSI EN 302 307 V1.2.1
        :param dvbs2_frame: The frame to descramble
        :type dvbs2_frame: DVBS2_Frame
        :return: void
        """
        if dvbs2_frame.plframe_symbols_descrambled is True:
            return
        dvbs2_frame.plframe_symbols[90:(dvbs2_frame.dvbs2_modcod.get_num_symbols())] = \
            dvbs2_frame.plframe_symbols[90:(dvbs2_frame.dvbs2_modcod.get_num_symbols())] * \
            self.descrambling_sequence[0:(dvbs2_frame.dvbs2_modcod.get_num_symbols() - 90)]
        dvbs2_frame.plframe_symbols_descrambled = True


