#!/usr/bin/env python3

from copy import deepcopy
import numpy as np
import zmq

from dsp_utilities.plotting.scatterplot import scatterplot

from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration, DVBS2_Frame_Length, DVBS2_Pilots, \
    DVBS2_Modulation_Type, DVBS2_Code_Rates

from dvbs2_processing.utilities import process_command_line_args

from dvbs2_processing.signal_utilities.interleaving import interleave_plframe, deinterleave_plframe

from dvbs2_processing.utilities import send_via_udp

from dvbs2_processing.signal_utilities.modulation import demodulate_plframe, modulate_plframe

try:
    import bchlib
except ImportError:
    print('Error importing bchlib')

try:
    from py_dvbs2_fec import DVBS2_FEC_Processor_Bank
except ImportError:
    DVBS2_FEC_Processor_Bank = None
    print('Error importing DVBS2_FEC_Processor_Bank')

import matplotlib.pyplot as plt

# matplotlib.pyplot.switch_backend('Qt5Agg')
plt.style.use('dark_background')


class FECRequest:
    def __init__(self, id: int, llr: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        self.id = id
        self.llr = llr
        self.dvbs2_modcod = dvbs2_modcod


class FECResponse:
    def __init__(self, id: int, bits_ldpc: list, passed_fec: bool):
        self.id = id
        self.bits_ldpc = bits_ldpc
        self.passed_fec = passed_fec

# noinspection PyUnreachableCode
class FECServer:
    """
    Functionality to detect and output PLFRAMEs from coarsely synchronized symbols

    Assumptions:
     * Symbols are coarsely synced in clock and carrier frequency (exactly what that means: TBD)

    """
    def __init__(self, dvbs2_frame_length=None, dvbs2_pilots=None, dvbs2_modulation_types=None, dvbs2_code_rates=None,
                 num_ldpc_iterations=50, no_fec_processor=False, threading_config=0):
        self.num_ldpc_iterations = num_ldpc_iterations

        self.mod_type = None
        self.fec_rate = None
        self.frame_size = None
        self.pilots = None

        self.fs_hz = None
        self.iq = None
        self.t_iq_sec = None

        self.fsym_hz = None
        self.symbols = None
        self.t_symbols_sec = None

        self.rrc_beta = 0.2
        self.fs_resampled_hz = None
        self.iq_resampled = None
        self.t_iq_resampled_sec = None

        self.fsym_hz = None
        self.clock_phase_offset_rad = None
        self.fractional_sample_shift = None

        self.frames = []

        self.debug_plots = False
        self.dvbs2_frame_length = dvbs2_frame_length
        self.dvbs2_code_rates = dvbs2_code_rates
        self.dvbs2_modulation_types = dvbs2_modulation_types
        self.dvbs2_pl_header_data = DVBS2_ModCod_Configuration.generate_dvb_s2_pls_data()
        if dvbs2_frame_length is not None:
            if DVBS2_Frame_Length.Short_Frames not in dvbs2_frame_length:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].frame_length is not DVBS2_Frame_Length.Short_Frames]
            if DVBS2_Frame_Length.Normal_Frames not in dvbs2_frame_length:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].frame_length is not DVBS2_Frame_Length.Normal_Frames]
        if dvbs2_pilots is not None:
            if DVBS2_Pilots.Pilots_On not in dvbs2_pilots:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].pilots is not DVBS2_Pilots.Pilots_On]
            if DVBS2_Pilots.Pilots_Off not in dvbs2_pilots:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].pilots is not DVBS2_Pilots.Pilots_Off]
        if dvbs2_modulation_types is not None:
            if DVBS2_Modulation_Type.QPSK not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('QPSK') == -1]
            if DVBS2_Modulation_Type.PSK8 not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('8PSK') == -1]
            if DVBS2_Modulation_Type.APSK16 not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('16APSK') == -1]
            if DVBS2_Modulation_Type.APSK32 not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('32APSK') == -1]
            if DVBS2_Modulation_Type.DUMMY not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('DUMMY') == -1]
            if DVBS2_Modulation_Type.RESERVED not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('RESERVED') == -1]

        if dvbs2_code_rates is not None:
            if DVBS2_Code_Rates.RATE_1_4 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/4') == -1]
            if DVBS2_Code_Rates.RATE_1_3 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/3') == -1]
            if DVBS2_Code_Rates.RATE_2_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('2/5') == -1]
            if DVBS2_Code_Rates.RATE_1_2 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/2') == -1]
            if DVBS2_Code_Rates.RATE_3_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('3/5') == -1]
            if DVBS2_Code_Rates.RATE_2_3 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('2/3') == -1]
            if DVBS2_Code_Rates.RATE_3_4 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('3/4') == -1]
            if DVBS2_Code_Rates.RATE_4_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('4/5') == -1]
            if DVBS2_Code_Rates.RATE_5_6 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('5/6') == -1]
            if DVBS2_Code_Rates.RATE_8_9 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('8/9') == -1]
            if DVBS2_Code_Rates.RATE_9_10 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('9/10') == -1]
            if DVBS2_Code_Rates.RATE_DUMMY not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('DUMMY') == -1]
            if DVBS2_Code_Rates.RATE_RESERVED not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('RESERVED') == -1]

        self.descrambling_sequence = DVBS2_ModCod_Configuration.generate_pl_descrambling_sequence()

        if (dvbs2_frame_length is None) or ((DVBS2_Frame_Length.Short_Frames in dvbs2_frame_length)
                                            and (DVBS2_Frame_Length.Normal_Frames in dvbs2_frame_length)):
            frame_length = 'Both'
        elif (DVBS2_Frame_Length.Normal_Frames in dvbs2_frame_length) and \
                (DVBS2_Frame_Length.Short_Frames not in dvbs2_frame_length):
            frame_length = 'Normal'
        elif (DVBS2_Frame_Length.Short_Frames in dvbs2_frame_length) and \
                (DVBS2_Frame_Length.Normal_Frames not in dvbs2_frame_length):
            frame_length = 'Short'
        else:
            raise Exception('Unhandled frame length configuration')

        self.num_fec_banks = 1

        def instantiate_fec_processor_bank(processor_bank_index):
            if DVBS2_FEC_Processor_Bank is None:
                raise Exception('DVBS2_FEC_Processor_Bank is None.  ' +
                                'from py_dvbs2_fec import DVBS2_FEC_Processor_Bank probably failed')
            else:
                code_rates = list(set([data['dvbs2_modcod'].get_code_rate() for data in self.dvbs2_pl_header_data]))
                self.dvbs2_fec_processor_banks[processor_bank_index] = \
                    DVBS2_FEC_Processor_Bank(frame_length=frame_length, code_rates=code_rates,
                                             num_ldpc_iterations=self.num_ldpc_iterations)
            return None

        self.dvbs2_fec_processor_banks = [None]*self.num_fec_banks
        instantiate_fec_processor_bank(0)



    def run_server(self):
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind("tcp://*:5555")
        while True:
            fec_request = socket.recv_pyobj()
            command = fec_request['command']
            index = fec_request['index']
            uuid = fec_request['uuid']
            if command == 'decode_plframe':
                dvbs2_modcod = fec_request['dvbs2_modcod']
                llr = fec_request['llr']
                esn0_db = fec_request['esn0_db']
                bits_ldpc, passed_fec = self.decode_plframe(llr=llr, dvbs2_modcod=dvbs2_modcod, esn0_db=esn0_db)
                fec_response = {'id': index, 'uuid': uuid, 'bits_ldpc': bits_ldpc, 'passed_fec': passed_fec}
            elif command == 'encode_plframe':
                dvbs2_modcod = fec_request['dvbs2_modcod']
                bits = fec_request['bits']
                bits_bch_ldpc = self.encode_plframe(bits=bits, dvbs2_modcod=dvbs2_modcod)
                fec_response = {'id': index, 'uuid': uuid, 'bits_bch_ldpc': bits_bch_ldpc}
            elif command == 'get_config':
                fec_response = {'id': index, 'uuid': uuid,
                                'dvbs2_frame_length': self.dvbs2_frame_length,
                                'dvbs2_code_rates': self.dvbs2_code_rates,
                                'dvbs2_modulation_types': self.dvbs2_modulation_types}
            else:
                raise Exception('Unhandled command: {}'.format(command))
            socket.send_pyobj(fec_response)

        socket.close()
        context.term()


    def decode_plframe(self, llr: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration, esn0_db: float = None):
        """
        Performs LDPC and BCH FEC decoding of llr values from a PL frame demodulation
        :param llr:
        :param dvbs2_modcod:
        :return:
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        fec_processor_index = self.get_ldpc_processor_bank()
        passed_ldpc, bits_ldpc = self.decode_ldpc(llr=llr, dvbs2_modcod=dvbs2_modcod,
                                                  fec_processor_index=fec_processor_index,
                                                  esn0_db=esn0_db)
        self.dvbs2_fec_processor_banks[fec_processor_index].mutex.release()
        bits_bch, bitflips_bch = self.decode_bch(bits=bits_ldpc, dvbs2_modcod=dvbs2_modcod)
        if bitflips_bch != 0:
            bits_ldpc[0:k_ldpc] = bits_bch
        passed_fec = passed_ldpc and (bitflips_bch <= t_bch)
        return bits_ldpc, passed_fec


    def encode_plframe(self, bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Performs LDPC and BCH FEC decoding of llr values from a PL frame demodulation
        :param llr: Array of LLR values
        :param dvbs2_modcod: ModCod to operate on
        :param esn0_db: Es/N0 of received PLFRAME
        :return: (passed_ldpc, bits)
        """
        bits_bch = self.encode_bch(bits=bits, dvbs2_modcod=dvbs2_modcod)
        fec_processor_index = self.get_ldpc_processor_bank()
        bits_bch_ldpc = self.encode_ldpc(bits=bits_bch, dvbs2_modcod=dvbs2_modcod)
        self.dvbs2_fec_processor_banks[fec_processor_index].mutex.release()
        return bits_bch_ldpc


    def get_ldpc_processor_bank(self):
        fec_processor_index = -1
        acquire_result = False
        while not acquire_result:
            for fec_processor_index, in_use in enumerate(
                    [dvbs2_fec_processor_bank.in_use for dvbs2_fec_processor_bank in self.dvbs2_fec_processor_banks]):
                if not in_use:
                    acquire_result = self.dvbs2_fec_processor_banks[fec_processor_index].mutex.acquire(timeout=0)
                    if acquire_result is True:
                        break
        return fec_processor_index


    def decode_ldpc(self, llr: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration, fec_processor_index: int = 0,
                    esn0_db: float = None):
        """
        Performs soft symbol LDPC FEC decoding of llr values from a PL frame demodulation
        :param llr: Array of LLR values
        :param dvbs2_modcod: ModCod to operate on
        :param fec_processor_index: Index into processor bank
        :return: (passed_ldpc, bits)
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        [passed_ldpc, bits] = \
            self.dvbs2_fec_processor_banks[fec_processor_index].decode_ldpc(llr=llr, include_cw=True, n_ldpc=n_ldpc,
                                                                            k_ldpc=k_ldpc, k_bch=k_bch, t_bch=t_bch,
                                                                            esn0_db=esn0_db)
        return passed_ldpc, bits


    def encode_ldpc(self, bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Performs soft symbol LDPC FEC decoding of llr values from a PL frame demodulation
        :param llr: Array of LLR values
        :param dvbs2_modcod: ModCod to operate on
        :param esn0_db: Es/N0 of received PLFRAME
        :return: (passed_ldpc, bits)
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        bits_ldpc = \
            self.dvbs2_fec_processor.encode_ldpc(bits=bits, n_ldpc=n_ldpc, k_ldpc=k_ldpc, k_bch=k_bch, t_bch=t_bch)
        return bits_ldpc


    @staticmethod
    def decode_bch(bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Performs hard bit BCH DEC decoding of a PL frame
        :param bits:
        :param dvbs2_modcod:
        :return:
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        bch_primitive_polynomial = dvbs2_modcod.get_bch_primitive_polynomial()
        bch_primitive_polynomial_int = int("".join(str(x) for x in bch_primitive_polynomial), 2)
        bch = bchlib.BCH(bch_primitive_polynomial_int, t_bch, False)
        data_bytes = bytearray(np.packbits(bits[0:k_ldpc]))
        bch_bytes, ecc_bytes = data_bytes[:-bch.ecc_bytes], data_bytes[-bch.ecc_bytes:]
        bitflips = bch.decode_inplace(bch_bytes, ecc_bytes)
        bch_bytes.extend(ecc_bytes)

        bits = []
        for num in bch_bytes:
            for bit in [int(b) for b in '{0:08b}'.format(num)]:
                bits.append(bit)
        bits = np.asarray(bits)

        return bits, bitflips


    @staticmethod
    def encode_bch(bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Performs BCH encoding of a PL frame
        :param bits: Array of bits to operate on
        :param dvbs2_modcod: ModCod to operate on
        :return: bits
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        bch_primitive_polynomial = dvbs2_modcod.get_bch_primitive_polynomial()
        bch_primitive_polynomial_int = int("".join(str(x) for x in bch_primitive_polynomial), 2)
        bch = bchlib.BCH(bch_primitive_polynomial_int, t_bch, False)
        bch_bytes = bytearray(np.packbits(bits[0:k_bch]))
        ecc_bytes = bch.encode(bch_bytes)
        bch_bytes.extend(ecc_bytes)

        bits = []
        for num in bch_bytes:
            for bit in [int(b) for b in '{0:08b}'.format(num)]:
                bits.append(bit)
        bits = np.asarray(bits)

        return bits


def main():
    filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths, mod_types, dvbs2_code_rates, fec_processing_mode, \
    zmq_fec_server, pilots, num_fec_workers = \
        process_command_line_args(input_file_required=False)

    fec_server = FECServer(dvbs2_frame_length=frame_lengths, dvbs2_pilots=pilots, dvbs2_modulation_types=mod_types,
                           dvbs2_code_rates=dvbs2_code_rates, no_fec_processor=False, threading_config=num_fec_workers)
    fec_server.run_server()



if __name__ == '__main__':
    main()