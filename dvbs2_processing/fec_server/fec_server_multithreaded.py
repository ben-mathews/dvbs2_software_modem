#!/usr/bin/env python3

import pickle
import threading
from typing import List

import numpy as np
import zmq

from dvbs2_processing.constant_data import DVBS2_ModCod_Configuration, DVBS2_Frame_Length, DVBS2_Pilots, \
    DVBS2_Modulation_Type, DVBS2_Code_Rates

from dvbs2_processing.utilities import process_command_line_args

import bchlib
from py_dvbs2_fec import DVBS2_FEC_Processor_Bank

import matplotlib.pyplot as plt
# matplotlib.pyplot.switch_backend('Qt5Agg')
plt.style.use('dark_background')

LRU_READY = "\x01"


# noinspection PyUnreachableCode
class FECServer:
    """
    Class to hold FEC Server functionality
    """
    def __init__(self,
                 dvbs2_frame_length: List[DVBS2_Frame_Length] = None,
                 dvbs2_pilots: List[DVBS2_Pilots] = None,
                 dvbs2_modulation_types: List[DVBS2_Modulation_Type] = None,
                 dvbs2_code_rates: List[DVBS2_Code_Rates] = None,
                 num_ldpc_iterations: int = 50,
                 num_fec_worker_threads: int = 8,
                 zmq_addr_frontend: str = 'tcp://*:5555',
                 zmq_addr_backend: str = 'tcp://*:6556',
                 ):
        """
        Constructor for DVBS2StreamProcessor object
        :param dvbs2_frame_length: list of DVBS2_Frame_Length values that the processor can support
        :type dvbs2_frame_length: list[DVBS2_Frame_Length]
        :param dvbs2_pilots: list of DVBS2_Pilots values that the processor can support
        :type dvbs2_pilots: list[DVBS2_Pilots]
        :param dvbs2_modulation_types: list of DVBS2_Modulation_Type values that the processor can support
        :type dvbs2_modulation_types: list[DVBS2_Modulation_Type]
        :param num_ldpc_iterations: Number of LDPC iterations
        :type num_ldpc_iterations: int
        :param num_fec_worker_threads: Number of FEC worker threads
        :type num_fec_worker_threads: int
        :param zmq_addr_frontend: Address for frontend client sockets
        :type zmq_addr_frontend: str
        :param zmq_addr_backend: Address for backend worker sockets
        :type zmq_addr_backend: str
        """
        self.num_ldpc_iterations = num_ldpc_iterations

        self.zmq_addr_frontend = zmq_addr_frontend
        self.zmq_addr_backend = zmq_addr_backend

        self.mod_type = None
        self.fec_rate = None
        self.frame_size = None
        self.pilots = None

        self.fs_hz = None
        self.iq = None
        self.t_iq_sec = None

        self.fsym_hz = None
        self.symbols = None
        self.t_symbols_sec = None

        self.rrc_beta = 0.2
        self.fs_resampled_hz = None
        self.iq_resampled = None
        self.t_iq_resampled_sec = None

        self.fsym_hz = None
        self.clock_phase_offset_rad = None
        self.fractional_sample_shift = None

        self.frames = []

        self.debug_plots = False
        self.dvbs2_frame_length = dvbs2_frame_length
        self.dvbs2_code_rates = dvbs2_code_rates
        self.dvbs2_modulation_types = dvbs2_modulation_types
        self.dvbs2_pl_header_data = DVBS2_ModCod_Configuration.generate_dvb_s2_pls_data()
        if dvbs2_frame_length is not None:
            if DVBS2_Frame_Length.Short_Frames not in dvbs2_frame_length:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].frame_length is not DVBS2_Frame_Length.Short_Frames]
            if DVBS2_Frame_Length.Normal_Frames not in dvbs2_frame_length:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].frame_length is not DVBS2_Frame_Length.Normal_Frames]
        if dvbs2_pilots is not None:
            if DVBS2_Pilots.Pilots_On not in dvbs2_pilots:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].pilots is not DVBS2_Pilots.Pilots_On]
            if DVBS2_Pilots.Pilots_Off not in dvbs2_pilots:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].pilots is not DVBS2_Pilots.Pilots_Off]
        if dvbs2_modulation_types is not None:
            if DVBS2_Modulation_Type.QPSK not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('QPSK') == -1]
            if DVBS2_Modulation_Type.PSK8 not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('8PSK') == -1]
            if DVBS2_Modulation_Type.APSK16 not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('16APSK') == -1]
            if DVBS2_Modulation_Type.APSK32 not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('32APSK') == -1]
            if DVBS2_Modulation_Type.DUMMY not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('DUMMY') == -1]
            if DVBS2_Modulation_Type.RESERVED not in dvbs2_modulation_types:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].modcod_pretty().find('RESERVED') == -1]

        if dvbs2_code_rates is not None:
            if DVBS2_Code_Rates.RATE_1_4 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/4') == -1]
            if DVBS2_Code_Rates.RATE_1_3 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/3') == -1]
            if DVBS2_Code_Rates.RATE_2_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('2/5') == -1]
            if DVBS2_Code_Rates.RATE_1_2 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('1/2') == -1]
            if DVBS2_Code_Rates.RATE_3_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('3/5') == -1]
            if DVBS2_Code_Rates.RATE_2_3 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('2/3') == -1]
            if DVBS2_Code_Rates.RATE_3_4 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('3/4') == -1]
            if DVBS2_Code_Rates.RATE_4_5 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('4/5') == -1]
            if DVBS2_Code_Rates.RATE_5_6 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('5/6') == -1]
            if DVBS2_Code_Rates.RATE_8_9 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('8/9') == -1]
            if DVBS2_Code_Rates.RATE_9_10 not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('9/10') == -1]
            if DVBS2_Code_Rates.RATE_DUMMY not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('DUMMY') == -1]
            if DVBS2_Code_Rates.RATE_RESERVED not in dvbs2_code_rates:
                self.dvbs2_pl_header_data = [data for data in self.dvbs2_pl_header_data if
                                             data['dvbs2_modcod'].get_code_rate().find('RESERVED') == -1]

        self.descrambling_sequence = DVBS2_ModCod_Configuration.generate_pl_descrambling_sequence()

        if (dvbs2_frame_length is None) or ((DVBS2_Frame_Length.Short_Frames in dvbs2_frame_length)
                                            and (DVBS2_Frame_Length.Normal_Frames in dvbs2_frame_length)):
            frame_length = 'Both'
        elif (DVBS2_Frame_Length.Normal_Frames in dvbs2_frame_length) and \
                (DVBS2_Frame_Length.Short_Frames not in dvbs2_frame_length):
            frame_length = 'Normal'
        elif (DVBS2_Frame_Length.Short_Frames in dvbs2_frame_length) and \
                (DVBS2_Frame_Length.Normal_Frames not in dvbs2_frame_length):
            frame_length = 'Short'
        else:
            raise Exception('Unhandled frame length configuration')

        self.num_fec_worker_threads = num_fec_worker_threads

        def instantiate_fec_processor_bank(processor_bank_index):
            if DVBS2_FEC_Processor_Bank is None:
                raise Exception('DVBS2_FEC_Processor_Bank is None.  ' +
                                'from py_dvbs2_fec import DVBS2_FEC_Processor_Bank probably failed')
            else:
                code_rates = list(set([data['dvbs2_modcod'].get_code_rate() for data in self.dvbs2_pl_header_data]))
                this_processor = DVBS2_FEC_Processor_Bank(frame_length=frame_length, code_rates=code_rates,
                                             num_ldpc_iterations=self.num_ldpc_iterations)
                self.dvbs2_fec_processor_banks.append(this_processor)
                this_thread = threading.Thread(target=self.run_worker, args=(processor_bank_index,))
                self.dvbs2_fec_processor_threads.append(this_thread)
                this_thread.start()
            return None
        self.dvbs2_fec_processor_banks = []
        self.dvbs2_fec_processor_threads = []
        do_parallel = True
        if do_parallel:
            from joblib import Parallel, delayed
            Parallel(n_jobs=8, require='sharedmem')(delayed(instantiate_fec_processor_bank)(i)
                                                    for i in range(self.num_fec_worker_threads))
        else:
            for bank_index in range(0, self.num_fec_worker_threads):
                instantiate_fec_processor_bank(bank_index)


    def run_server(self):
        """
        Main run method for server
        """
        context = zmq.Context(1)

        frontend = context.socket(zmq.ROUTER)  # ROUTER
        backend = context.socket(zmq.ROUTER)  # ROUTER
        frontend.bind(self.zmq_addr_frontend)  # For clients
        backend.bind(self.zmq_addr_backend)  # For workers

        poll_workers = zmq.Poller()
        poll_workers.register(backend, zmq.POLLIN)

        poll_both = zmq.Poller()
        poll_both.register(frontend, zmq.POLLIN)
        poll_both.register(backend, zmq.POLLIN)

        workers = []

        # Main queue working loop
        while True:
            print('run_server: Polling -----------------------------------')
            if workers:  # If we have workers available, see if there are any requests from client
                socks = dict(poll_both.poll())
            else:  # If we don't have works available (meaning they're all working), then check and see if any are done
                socks = dict(poll_workers.poll())
            print('run_server: Message received (1) -----------------------------------')

            # Handle worker activity on backend - This can either be a new worker registering its presence with the
            # queue, or it can be a worker responding with results from a client request
            if socks.get(backend) == zmq.POLLIN:
                # Use worker address for LRU routing
                msg = backend.recv_multipart()
                print('run_server: Backend message received (2) -----------------------------------')
                if not msg:
                    break
                address = msg[0]
                workers.append(address)

                # Everything after the second (delimiter) frame is reply
                reply = msg[2:]

                # Forward message to client if it's not a READY
                if 'LRU_READY' not in reply:
                    frontend.send_multipart(reply)

            if socks.get(frontend) == zmq.POLLIN:
                #  Get client request, route to first available worker
                msg = frontend.recv_multipart()
                print('run_server: Client message received (2) -----------------------------------')
                request = [workers.pop(0), ''.encode()] + msg
                backend.send_multipart(request)


    def run_worker(self, processor_bank_index):
        """
        Run function for worker
        """
        print('run_worker ({}): Launching Worker ---------------------------------'.format(processor_bank_index))
        context = zmq.Context(1)
        worker = context.socket(zmq.REQ)

        identity = '{:04X}'.format(processor_bank_index)
        worker.setsockopt_string(zmq.IDENTITY, identity)
        zmq_addr_backend_worker = self.zmq_addr_backend.replace('*', 'localhost')
        worker.connect(zmq_addr_backend_worker)
        worker.send_pyobj({'LRU_READY': LRU_READY})
        print('run_worker ({}): LRU_READY Sent ---------------------------------'.format(processor_bank_index))

        while True:
            print('run_worker ({}): Waiting for Message ---------------------------------'.format(processor_bank_index))
            msg = worker.recv_multipart()
            print('run_worker ({}): Message Received ---------------------------------'.format(processor_bank_index))

            if not msg:
                break
            fec_request = pickle.loads(msg[2])
            command = fec_request['command']
            index = fec_request['index']
            uuid = fec_request['uuid']
            if command == 'decode_plframe':
                dvbs2_modcod = fec_request['dvbs2_modcod']
                llr = fec_request['llr']
                esn0_db = fec_request['esn0_db']
                bits_ldpc, passed_fec = self.decode_plframe(llr=llr, dvbs2_modcod=dvbs2_modcod, esn0_db=esn0_db)
                fec_response = {'id': index, 'uuid': uuid, 'bits_ldpc': bits_ldpc, 'passed_fec': passed_fec}
            elif command == 'encode_plframe':
                dvbs2_modcod = fec_request['dvbs2_modcod']
                bits = fec_request['bits']
                bits_bch_ldpc = self.encode_plframe(bits=bits, dvbs2_modcod=dvbs2_modcod)
                fec_response = {'id': index, 'uuid': uuid, 'bits_bch_ldpc': bits_bch_ldpc}
            elif command == 'get_config':
                fec_response = {'id': index, 'uuid': uuid,
                                'dvbs2_frame_length': self.dvbs2_frame_length,
                                'dvbs2_code_rates': self.dvbs2_code_rates,
                                'dvbs2_modulation_types': self.dvbs2_modulation_types}
            else:
                raise Exception('Unhandled command: {}'.format(command))
            msg[2] = pickle.dumps(fec_response)
            worker.send_multipart(msg)


    def decode_plframe(self, llr: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration, esn0_db: float = None):
        """
        Performs LDPC and BCH FEC decoding of llr values from a PL frame demodulation
        :param llr: Array of LLR values
        :param dvbs2_modcod: ModCod to operate on
        :param esn0_db: Es/N0 of received PLFRAME
        :return: (passed_ldpc, bits)
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        fec_processor_index = self.get_ldpc_processor_bank()
        passed_ldpc, bits_ldpc = self.decode_ldpc(llr=llr, dvbs2_modcod=dvbs2_modcod,
                                                  fec_processor_index=fec_processor_index,
                                                  esn0_db=esn0_db)
        self.dvbs2_fec_processor_banks[fec_processor_index].mutex.release()
        bits_bch, bitflips_bch = self.decode_bch(bits=bits_ldpc, dvbs2_modcod=dvbs2_modcod)
        if bitflips_bch != 0:
            bits_ldpc[0:k_ldpc] = bits_bch
        passed_fec = passed_ldpc and (bitflips_bch <= t_bch)
        return bits_ldpc, passed_fec


    def encode_plframe(self, bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Performs LDPC and BCH FEC decoding of llr values from a PL frame demodulation
        :param llr: Array of LLR values
        :param dvbs2_modcod: ModCod to operate on
        :param esn0_db: Es/N0 of received PLFRAME
        :return: (passed_ldpc, bits)
        """
        bits_bch = self.encode_bch(bits=bits, dvbs2_modcod=dvbs2_modcod)
        fec_processor_index = self.get_ldpc_processor_bank()
        bits_bch_ldpc = self.encode_ldpc(bits=bits_bch, dvbs2_modcod=dvbs2_modcod)
        self.dvbs2_fec_processor_banks[fec_processor_index].mutex.release()
        return bits_bch_ldpc


    def get_ldpc_processor_bank(self):
        """
        Get an available FEC processor
        @return: fec_processor_index
        """
        fec_processor_index = -1
        acquire_result = False
        while not acquire_result:
            for fec_processor_index, in_use in enumerate(
                    [dvbs2_fec_processor_bank.in_use for dvbs2_fec_processor_bank in self.dvbs2_fec_processor_banks]):
                if not in_use:
                    acquire_result = self.dvbs2_fec_processor_banks[fec_processor_index].mutex.acquire(timeout=0)
                    if acquire_result is True:
                        break
        return fec_processor_index


    def decode_ldpc(self, llr: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration, fec_processor_index: int = 0,
                    esn0_db: float = None):
        """
        Performs soft symbol LDPC FEC decoding of llr values from a PL frame demodulation
        :param llr: Array of LLR values
        :param dvbs2_modcod: ModCod to operate on
        :param esn0_db: Es/N0 of received PLFRAME
        :param fec_processor_index: FEC processor to use
        :return: (passed_ldpc, bits)
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        [passed_ldpc, bits] = \
            self.dvbs2_fec_processor_banks[fec_processor_index].decode_ldpc(llr=llr, include_cw=True, n_ldpc=n_ldpc,
                                                                            k_ldpc=k_ldpc, k_bch=k_bch, t_bch=t_bch,
                                                                            esn0_db=esn0_db)
        return passed_ldpc, bits


    def encode_ldpc(self, bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration, fec_processor_index: int = 0):
        """
        Performs soft symbol LDPC FEC decoding of llr values from a PL frame demodulation
        :param llr: Array of LLR values
        :param dvbs2_modcod: ModCod to operate on
        :param esn0_db: Es/N0 of received PLFRAME
        :return: (passed_ldpc, bits)
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        bits_ldpc = \
            self.dvbs2_fec_processor_banks[fec_processor_index].encode_ldpc(bits=bits, n_ldpc=n_ldpc, k_ldpc=k_ldpc,
                                                                            k_bch=k_bch, t_bch=t_bch)
        return bits_ldpc


    @staticmethod
    def decode_bch(bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Performs hard bit BCH DEC decoding of a PL frame
        :param bits: Array of bits to operate on
        :param dvbs2_modcod: ModCod to operate on
        :return: bits
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        bch_primitive_polynomial = dvbs2_modcod.get_bch_primitive_polynomial()
        bch_primitive_polynomial_int = int("".join(str(x) for x in bch_primitive_polynomial), 2)
        bch = bchlib.BCH(bch_primitive_polynomial_int, t_bch, False)
        data_bytes = bytearray(np.packbits(bits[0:k_ldpc]))
        bch_bytes, ecc_bytes = data_bytes[:-bch.ecc_bytes], data_bytes[-bch.ecc_bytes:]
        bitflips = bch.decode_inplace(bch_bytes, ecc_bytes)
        bch_bytes.extend(ecc_bytes)

        bits = []
        for num in bch_bytes:
            for bit in [int(b) for b in '{0:08b}'.format(num)]:
                bits.append(bit)
        bits = np.asarray(bits)

        return bits, bitflips


    @staticmethod
    def encode_bch(bits: np.array, dvbs2_modcod: DVBS2_ModCod_Configuration):
        """
        Performs BCH encoding of a PL frame
        :param bits: Array of bits to operate on
        :param dvbs2_modcod: ModCod to operate on
        :return: bits
        """
        (k_bch, k_ldpc, t_bch, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()
        bch_primitive_polynomial = dvbs2_modcod.get_bch_primitive_polynomial()
        bch_primitive_polynomial_int = int("".join(str(x) for x in bch_primitive_polynomial), 2)
        bch = bchlib.BCH(bch_primitive_polynomial_int, t_bch, False)
        bch_bytes = bytearray(np.packbits(bits[0:k_bch]))
        ecc_bytes = bch.encode(bch_bytes)
        bch_bytes.extend(ecc_bytes)

        bits = []
        for num in bch_bytes:
            for bit in [int(b) for b in '{0:08b}'.format(num)]:
                bits.append(bit)
        bits = np.asarray(bits)

        return bits


def main():
    args_override = None
    # args_override.append('--frame_lengths')
    # args_override.append('Short_Frames')

    filename, fc_estimate_hz, fsym_estimate_hz, frame_lengths, mod_types, dvbs2_code_rates, fec_processing_mode, \
    zmq_fec_server, pilots, num_fec_workers = \
        process_command_line_args(args_override=args_override, input_file_required=False)

    fec_server = FECServer(dvbs2_frame_length=frame_lengths, dvbs2_pilots=pilots, dvbs2_modulation_types=mod_types,
                           dvbs2_code_rates=dvbs2_code_rates, num_fec_worker_threads=num_fec_workers)
    fec_server.run_server()


if __name__ == '__main__':
    main()
