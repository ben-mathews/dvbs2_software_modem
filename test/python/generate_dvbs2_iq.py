#!/usr/bin/env python3
import sys
import os
from argparse import ArgumentParser
from copy import deepcopy
import fractions

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import resample_poly

from dvbs2_processing.constant_data import DVBS2_ModCods, DVBS2_Frame_Length, DVBS2_Pilots, \
                DVBS2_Modulation_Type, DVBS2_Code_Rates, DVBS2_MATYPE1_TS_GS, DVBS2_MATYPE1_SIS_MIS, \
                DVBS2_MATYPE1_CCM_ACM, DVBS2_MATYPE1_ISSYI, DVBS2_MATYPE1_Rolloff, DVBS2_MATYPE1_NPD, \
                DVBS2_ModCod_Configuration

from dvbs2_processing.stream_processor import DVBS2StreamProcessor
from dvbs2_processing.signal_utilities.modulation import modulate_plframe
from dvbs2_processing.signal_utilities.interleaving import interleave_plframe

from dsp_utilities.file_handlers.iq_file import IQFile, IQFileType, IQDataFormat, IQDataOrder
from dsp_utilities.plotting.iq_plot import iq_plot


def argument_parser():
    parser = ArgumentParser()
    #parser.add_argument(
    #    "--source-filename", dest="source_filename", type=str, default='adv16apsk910.ts',
    #    help="Set Source Filename (from http://www.w6rz.net/adv16apsk910.ts) [default=%(default)r]")
    parser.add_argument(
        "--dest-filename", dest="dest_filename", type=str, default='../iq_data/dvbs2_iq.cfile',
        help="Set Dest Filename [default=%(default)r] (Set extension to .cfile for raw binary IQ double pairs, and .tmp for bluefile)")
    parser.add_argument(
        "--constellation", dest="constellation", type=str, default="QPSK",
        help="Set Constellation [default=%(default)r]")
    parser.add_argument(
        "--code-rate", dest="code_rate", type=str, default="3/4",
        help="Set Code Rate [default=%(default)r]")
    parser.add_argument(
        "--fecframe-size", dest="fecframe_size", type=str, default='normal',
        help="Set FECFRAME size ('Normal' or 'Short') [default=%(default)r]")
    parser.add_argument(
        "--pilots", dest="pilots", type=str, default="on",
        help="Set Pilots [default=%(default)r]")
    parser.add_argument(
        "--rolloff", dest="rolloff", type=float, default="350.0e-3",
        help="Set RRC Roll-Off Percentage (0.20, 0.25, 0.35) [default=%(default)r]")
    parser.add_argument(
        "--num-frames", dest="num_frames", type=int, default=100,
        help="Set Number of IQ samples to generate [default=%(default)r]")
    parser.add_argument(
        "--esn0_db-db", dest="esn0_db", type=float, default="20.0",
        help="Set Es/N0 (Symbol to Noise Ratio) in dB [default=%(default)r]")
    parser.add_argument(
        "--symbol-rate-hz", dest="symbol_rate_hz", type=float, default="0.4e6",
        help="Set Symbol Rate in Symbols/Second [default=%(default)r]")
    parser.add_argument(
        "--sample-rate-hz", dest="sample_rate_hz", type=float, default="1.0e6",
        help="Set Symbol Rate in Symbols/Second [default=%(default)r]")
    parser.add_argument(
        '--fec_processing_mode', dest='fec_processing_mode', type=str, default='none',
        help='FEC processing mode (\'local_processor\', \'zmq_processor\', or \'none\')')
    parser.add_argument(
        '--zmq_fec_server', dest='zmq_fec_server', type=str, default='',
        help='ZMQ server URL if FEC processing mode is zmq_processor')

    return parser


def main():
    """
    Main function implementation called when Python file is called as a script.
    See documentation in 'if __name__ == "__main__"' for more details.
    """

    options = argument_parser().parse_args()
    generate_iq(options=options)


def generate_iq(options):

    if options.fecframe_size.lower() == 'normal':
        options.fecframe_size = DVBS2_Frame_Length.Normal_Frames
    elif options.fecframe_size.lower() == 'short':
        options.fecframe_size = DVBS2_Frame_Length.Short_Frames
    else:
        raise Exception('Unhandled --fecframe_size argument: {} (expecting normal or short'.format(options.fecframe_size))

    if options.pilots.lower() == 'on':
        options.pilots = DVBS2_Pilots.Pilots_On
    elif options.pilots.lower() == 'off':
        options.pilots = DVBS2_Pilots.Pilots_Off
    else:
        raise Exception('Unhandled --pilots argument: {} (expecting on or off'.format(options.pilots))

    if options.constellation.upper() == 'QPSK':
        options.constellation = DVBS2_Modulation_Type.QPSK
    elif options.constellation.upper() == '8PSK':
        options.constellation = DVBS2_Modulation_Type.PSK8
    elif options.constellation.upper() == '16APSK':
        options.constellation = DVBS2_Modulation_Type.APSK16
    elif options.constellation.upper() == '32APSK':
        options.constellation = DVBS2_Modulation_Type.APSK32
    else:
        raise Exception('Unhandled --constellation argument: {} (expecting QPSK, 8PSK, 16APSK, or 32APSK'.format(options.constellation))

    if options.code_rate == '1/4':
        options.code_rate = DVBS2_Code_Rates.RATE_1_4
    elif options.code_rate == '1/3':
        options.code_rate = DVBS2_Code_Rates.RATE_1_3
    elif options.code_rate == '2/5':
        options.code_rate = DVBS2_Code_Rates.RATE_2_5
    elif options.code_rate == '1/2':
        options.code_rate = DVBS2_Code_Rates.RATE_1_2
    elif options.code_rate == '3/5':
        options.code_rate = DVBS2_Code_Rates.RATE_3_5
    elif options.code_rate == '2/3':
        options.code_rate = DVBS2_Code_Rates.RATE_2_3
    elif options.code_rate == '3/4':
        options.code_rate = DVBS2_Code_Rates.RATE_3_4
    elif options.code_rate == '4/5':
        options.code_rate = DVBS2_Code_Rates.RATE_4_5
    elif options.code_rate == '5/6':
        options.code_rate = DVBS2_Code_Rates.RATE_5_6
    elif options.code_rate == '8/9':
        options.code_rate = DVBS2_Code_Rates.RATE_8_9
    elif options.code_rate == '9/10':
        options.code_rate = DVBS2_Code_Rates.RATE_9_10
    elif options.code_rate.upper() == 'DUMMY':
        options.code_rate = DVBS2_Code_Rates.RATE_DUMMY
    else:
        raise Exception('Unhandled --code_rate argument: {} (expecting 1/4, 1/3, 2/5, 1/2, 3/5, 2/3, 3/4, 4/5, 5/6, 8/9, 9/10, or DUMMY)'.format(options.constellation))


    dvbs2_stream_processor = DVBS2StreamProcessor(
        dvbs2_frame_length=[options.fecframe_size],
        dvbs2_pilots=[options.pilots],
        dvbs2_modulation_type=[options.constellation],
        dvbs2_code_rates=[options.code_rate],
        fec_processing_mode=options.fec_processing_mode,
        zmq_fec_server=options.zmq_fec_server,
        threading_config=0
    )

    # This only works because there is a single entry in dvbs2_pl_header_data, because we passed in modcod, frame size, and pilots
    dvbs2_modcod = dvbs2_stream_processor.dvbs2_bbframe_processor.dvbs2_pl_header_data[0]['dvbs2_modcod']
    (k_bch, k_ldpc, t_error, n_ldpc, q) = dvbs2_modcod.get_coding_parameters()

    source_file_name = '/home/ben/dev/gitlab/dvbs2_software_modem/dvbs2_processing/test/int_ramp.bin'
    file = open(source_file_name, "rb")
    bytes_ts_packet = file.read()
    file.close()
    bits = np.asarray([int(byte) for byte in bytes_ts_packet], dtype=np.uint8)
    bits = np.unpackbits(bits)

    modulated_symbols_list = []
    bit_index = 0
    for frame_number in range(0, options.num_frames):
        print('Generating frame {} of {}  ({}%% complete'.format(frame_number, options.num_frames, 100*frame_number/options.num_frames))
        if dvbs2_modcod.modcod is not DVBS2_ModCods.DVBS2_DUMMY_PLFRAME:
            if False:
                bits_tx = np.random.randint(0, 2, k_bch)
            else:
                if len(bits) < bit_index+k_bch:
                    raise Exception('Not enough bits in file')
                bits_tx = bits[bit_index:bit_index+k_bch]
                bit_index = bit_index + k_bch
            bits_tx = \
                dvbs2_stream_processor.dvbs2_bbframe_processor.scramble_bbframe(dvbs2_modcod, bits_tx)
            bits_tx_bch_ldpc = \
                dvbs2_stream_processor.dvbs2_bbframe_processor.encode_plframe(bits_tx, dvbs2_modcod)
            bits_tx_bch_ldpc = interleave_plframe(dvbs2_modcod, bits_tx_bch_ldpc)
        else:
            bits_tx_bch_ldpc = k_bch*[0]

        modulated_symbols = modulate_plframe(dvbs2_modcod, bits_tx_bch_ldpc)
        dvbs2_stream_processor.dvbs2_bbframe_processor.scramble_plframe(
            dvbs2_modcod=dvbs2_modcod, symbols=modulated_symbols)
        modulated_symbols_list.append(modulated_symbols)

    oversample_factor = np.ceil(options.sample_rate_hz / options.symbol_rate_hz)
    modulated_symbols = np.concatenate(modulated_symbols_list)
    iq = dvbs2_stream_processor.dvbs2_symbol_processor.generate_samples(
        modulated_symbols, options.symbol_rate_hz, oversample_factor * options.symbol_rate_hz, options.rolloff)

    resample_frequency_frac = fractions.Fraction(options.sample_rate_hz / (oversample_factor * options.symbol_rate_hz)).limit_denominator(10000)
    iq = resample_poly(iq, resample_frequency_frac.numerator, resample_frequency_frac.denominator)

    if False:
        from digital_demod.utilities.rrc_filter import rrc_filter
        h_rrc, time_idx = rrc_filter(128, options.rolloff, options.symbol_rate_hz,
                                     oversample_factor * options.symbol_rate_hz)
        h_rrc = h_rrc / np.sum(h_rrc)
        from scipy import signal
        w, h = signal.freqz(h_rrc, 1, fs=oversample_factor * options.symbol_rate_hz)
        h_dB = 20 * np.log10(np.abs(h))
        cn_b_db = 10 * np.log10(10 ** (options.esn0_db / 10) + 1)
        B = 2 * w[np.min(np.where(h_dB < np.max(h_dB) - cn_b_db))]
        c_n_db = options.esn0_db + 10 * np.log10(options.symbol_rate_hz / B)
        noise_var = np.mean(np.abs(iq)) / (10 ** (c_n_db / 10.0))
    else:
        noise_var = (options.sample_rate_hz / options.symbol_rate_hz / 2) / (10 ** (options.esn0_db / 10.0))
    n = np.random.normal(0, np.sqrt(noise_var), len(iq)) + 1j*np.random.normal(0, np.sqrt(noise_var), len(iq))
    iq = iq + n
    if False:
        iq_plot(n+iq)
        10*np.log10(np.mean(np.abs(iq))**2 / np.var(n))
        #c_plus_n_over_n_to_c_over_n(6.2)
        #c_plus_n_over_n_to_c_over_n(5)


    iq_file = IQFile(filename=options.dest_filename, file_type=IQFileType.BLUEFILE,
                     iq_data_format=IQDataFormat.DOUBLE64,
                     iq=iq, fs_hz=options.sample_rate_hz, t_start_sec=0.0,
                     center_frequency_hz=0.0,
                     num_samples=len(iq), header_ignore_bytes=0)
    iq_file.write_file(file_type=IQFileType.BLUEFILE, filename=options.dest_filename)


if __name__ == "__main__":
    """
    Simple script to generate DVB-S2 IQ data

    Mimics    

    Arguments
    ----------
    See argument_parser() for description of arguments

    Example Usage:
      generate_dvbs2_iq --dest-filename test.tmp --fecframe-size short --num-frames 200 --fec_processing_mode zmq_processor --zmq_fec_server tcp://localhost:5555

    """
    main()