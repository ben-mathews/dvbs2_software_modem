# Testing DVB-S2 Software-Based Demodulation

## Test data source
Download MPEG stream from [http://www.w6rz.net/adv16apsk910.ts](http://www.w6rz.net/adv16apsk910.ts), or do:
```shell script
wget http://www.w6rz.net/adv16apsk910.ts
```

## Generating Data in GNURadio

### Gnuradio Test Data Generation Implementation
Two GNURadio flowgraphs are provided to generate test data.  
1) The generate_dvbs2_iq.grc flowgraph allows users to manipulate the parameters of the IQ data generation in the flowgraph.  This is useful for inspecting the model for how data is generated or quickly modifying parameters to generate a single set of data.
2) The generate_dvbs2_iq_headless.grc flowgraph generates a python file (generate_dvbs2_iq_headless.py) that we can make simple modifications to that enable us to call this programmatically for easily generating multiple datasets or for use in regression testing.  This file is modified with the update_headless_py_file.sh script to replace GNURadio/Python enumerations that can't be parameterized and put into the `__init__()` call.  This is not a desirable approach, but I can find no other way of parameterizing the FECFRAME size, constellation, code rate, and roll-off in such a way that GNURadio Parameter or Variable object can be created and passed to the config settings of the various GNURadio DVB-S2 objects.

![Stream Processing Architecture](doc/images/gnuradio_generate_dvbs2_iq.png)

These two flowgraph are meant to be identical except:
* Options block:
    1) The id is different: generate_dvbs2_iq vs generate_dvbs2_iq_headless
    2) The title is different: "Generate DVB-S2 IQ" vs "Generate DVB-S2 IQ Headless"
    3) Generate Options is different: "QT GUI" vs "No GUI"
* In the `generate_dvbs2_iq.grc` the QT GUI Frequency Sink is disabled, and it is enabled in the `generate_dvbs2_iq.grc` flowgraph.

These two flowgraphs are otherwise meant to be identical.  When making modifications to them it is usually easier to just modify one of them, and them make the changes described above and save to the appropriate other filename.

To use the headless version, the Python files must be generated.  Open the `generate_dvbs2_iq_headless.grc` file in gnuradio-companion, select Run -> Generate.  This will generate the `generate_dvbs2_iq_headless.py` file and a few other Python files.  Using this file to generate DVB-S2 IQ data is described in the section below.  I take this approach because I want to preserve the ability to operate on the flowgraph to modify the resulting Python script. 

Other notes:
* The rolloff value is represented in two different places:
    1) As an enumerated value in the BBHeader block
    2) In the `rolloff` Parameter which is used as part of the RRC filter
    
    I know of know way to link these in the context of GNURadio.  If one oc these is changed the other must also be changed.
    
### Gnuradio Test Data Generation Usage from Command Line
Example usage:
```shell script
python3 generate_dvbs2_iq_headless.py --source-filename adv16apsk910.ts \
            --dest-filename ../iq_data/dvbs2_iq.cfile --num-samples 10000000 --symbol-rate 5000000 \ 
            --esn0-db 10.0 --constellation QPSK --code-rate 3/4 --fecframe-size Normal --pilots On \
            --rolloff 0.35 --samples-per-symbol 2 --resample-decimation 10  --resample-interpolation 9 \
            --rrc-filter-numtaps 100 --samples-offset-beginning 123 --frequency-offset-hz 0.0
```

## Installing and Running GNURadio Into a VirtualEnv
It is frequently desirable to operate in a Python virtualenv environment.  So far I've been unable to figure out a way to install to a virtualenv via PyBOMBS.  The following instructions describe how to do so from source.  These borrow from [https://wiki.gnuradio.org/index.php/InstallingGR](https://wiki.gnuradio.org/index.php/InstallingGR) and [https://kb.ettus.com/index.php?title=Building_and_Installing_UHD_and_GNU_Radio_to_a_Custom_Prefix&action=pdfbook&format=single](https://kb.ettus.com/index.php?title=Building_and_Installing_UHD_and_GNU_Radio_to_a_Custom_Prefix&action=pdfbook&format=single).

If not already in a virtual environment, activate one.  For example:
```shell script
source /home/ben/dev/venvs/venv-dsp-python3.8-20200512/bin/activate
``` 
Then run the following:
```shell script
sudo apt-get -y install swig libboost-all-dev
sudo apt install git cmake g++ libboost-all-dev libgmp-dev swig python3-numpy python3-mako python3-sphinx python3-lxml doxygen libfftw3-dev libsdl1.2-dev libgsl-dev libqwt-qt5-dev libqt5opengl5-dev python3-pyqt5 liblog4cpp5-dev libzmq3-dev python3-yaml python3-click python3-click-plugins python3-zmq python3-scipy python3-gi python3-gi-cairo gobject-introspection gir1.2-gtk-3.0 python3-gi python3-gi-cairo libgirepository1.0-dev
pip3 install numpy mako pyyaml pygi pgi pycairo PyGObject click click-plugins
export PYTHONPATH=$VIRTUAL_ENV/lib/python3.8/site-packages # WARNING: This is specific to your environment and may be slightly different.
export LD_LIBRARY_PATH=$VIRTUAL_ENV/lib
cd $VIRTUAL_ENV
mkdir gnuradio
cd gnuradio
git clone --recursive https://github.com/gnuradio/gnuradio
cd gnuradio
git checkout maint-3.8
mkdir build
cd build
git pull --recurse-submodules=on
git submodule update --init
cmake -DENABLE_GR_UHD=OFF -DCMAKE_INSTALL_PREFIX=$VIRTUAL_ENV -DPYTHON_EXECUTABLE=$VIRTUAL_ENV/bin/python -DENABLE_PYTHON=ON ..
make -j $(nproc --all)
make install
```

### Handling LD_LIBRARY_PATH Environment Variable for Running GNURadio in a VENV

The LD_LIBRARY_PATH environment variable will have to be set every time you run in the future.  Not setting this will result in a `No module named '_analog_swig'` error.  One simple way of handing this is to update the venv `activate` script.  At the end of the `activate` script, add:
```shell script
export OLD_LD_LIBRARY_PATH=${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=$VIRTUAL_ENV/lib:${LD_LIBRARY_PATH}
```  

Then at the end of the deactivate() function, add:
```shell script
export LD_LIBRARY_PATH=${OLD_LD_LIBRARY_PATH}
unset OLD_LD_LIBRARY_PATH
```

If you're using PyCharm, then a different work-around is required.  Having venvs set environment variables seems to be a long-standing issue with PyCharm.  The simplest work-around seems to be to simply set the LD_LIBRARY_PATH environment variable on a per-script basis for scripts that need it (so far, only stream_processor_test.py does).  To do this, go to Run -> Edit Configurations, select the appropriate configuration, and add it. 

### Other Notes
1) On Ubuntu 18.04 systems I have had issues getting the Swig bindings to work with gr-trellis, which results in compile errors.  This can be worked around by adding the `-DENABLE_GR_TRELLIS=OFF` flag to the cmake command. 

## Limitations

* Does not support ACM mode functions

## Usage examples

### Generate 10 seconds of IQ with a 10 Hz/second drift
This generates IQ that has carrier frequency drift out to 100 Hz after 10 seconds.  Note that to achieve a ramp effect we use the triangle model and generate 1/4 of a period.
python3 generate_dvbs2_iq_headless.py --source-filename adv16apsk910.ts \
            --dest-filename dvbs2_iq_short_frames_pilots_on_10_hz_sec_drift.tmp --num-samples 9010000 \ 
            --symbol-rate 500000 --esn0-db 10.0 --constellation QPSK --code-rate 3/4 --fecframe-size Short \ 
            --pilots On --rolloff 0.2 --samples-per-symbol 2 --resample-decimation 10  --resample-interpolation 9 \
            --rrc-filter-numtaps 100 --samples-offset-beginning 123 \
            --frequency-offset-hz 0.0 --frequency-drift-rate-hz 10 --frequency-drift-max-hz 100 \
            --frequency-drift-model triangle 

python3 generate_dvbs2_iq_headless.py --source-filename adv16apsk910.ts --dest-filename dvbs2_iq_short_frames_pilots_on_10_hz_sec_drift.tmp --num-samples 9010000 --symbol-rate 500000 --esn0-db 10.0 --constellation QPSK --code-rate 3/4 --fecframe-size Short --pilots On --rolloff 0.2 --samples-per-symbol 2 --resample-decimation 10  --resample-interpolation 9 --rrc-filter-numtaps 100 --samples-offset-beginning 123 --frequency-offset-hz 0.0 --frequency-drift-rate-hz 0.025 --frequency-drift-max-hz 100 --frequency-drift-model triangle 

