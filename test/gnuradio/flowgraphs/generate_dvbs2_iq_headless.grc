options:
  parameters:
    author: ''
    category: Custom
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: generate_dvbs2_iq_headless
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: Generate DVB-S2 IQ Headless
    window_size: 1280, 1024
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [14, 11]
    rotation: 0
    state: enabled

blocks:
- name: noise_var
  id: variable
  parameters:
    comment: ''
    value: 1.0 / (10**(esn0_db/10.0))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [372, 107]
    rotation: 0
    state: true
- name: output_sample_rate_hz
  id: variable
  parameters:
    comment: ''
    value: sample_rate_hz * resample_interpolation / resample_decimation
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1557, 11]
    rotation: 0
    state: true
- name: sample_rate_hz
  id: variable
  parameters:
    comment: ''
    value: samples_per_symbol * symbol_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1101, 11]
    rotation: 0
    state: true
- name: analog_const_source_x_0_1
  id: analog_const_source_x
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: frequency_offset_hz * 2 * np.pi
    maxoutbuf: '0'
    minoutbuf: '0'
    type: float
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [295, 1031]
    rotation: 180
    state: enabled
- name: analog_noise_source_x_0
  id: analog_noise_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: np.sqrt(noise_var)
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    noise_type: analog.GR_GAUSSIAN
    seed: '0'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [296, 933]
    rotation: 0
    state: enabled
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: frequency_drift_max_hz * 4 * np.pi
    comment: ''
    freq: frequency_drift_rate_hz
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: np.pi/2
    samp_rate: output_sample_rate_hz
    type: float
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [313, 1087]
    rotation: 180
    state: true
- name: blocks_add_xx_0
  id: blocks_add_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [533, 848]
    rotation: 0
    state: true
- name: blocks_add_xx_1
  id: blocks_add_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [136, 1035]
    rotation: 180
    state: true
- name: blocks_file_sink_0
  id: blocks_file_sink
  parameters:
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    file: dest_filename
    type: complex
    unbuffered: 'False'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1048, 844]
    rotation: 0
    state: enabled
- name: blocks_file_sink_0_0
  id: blocks_file_sink
  parameters:
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    file: bbframes.dat
    type: byte
    unbuffered: 'False'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [790, 351]
    rotation: 0
    state: enabled
- name: blocks_file_sink_1
  id: blocks_file_sink
  parameters:
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    file: ../iq_data/dvbs2_iq_freq_offset.float32
    type: float
    unbuffered: 'False'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [121, 1166]
    rotation: 0
    state: disabled
- name: blocks_file_source_0
  id: blocks_file_source
  parameters:
    affinity: ''
    alias: ''
    begin_tag: pmt.PMT_NIL
    comment: ''
    file: source_filename
    length: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    repeat: 'True'
    type: byte
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [81, 242]
    rotation: 0
    state: enabled
- name: blocks_head_0
  id: blocks_head
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_items: num_samples
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [848, 860]
    rotation: 0
    state: true
- name: blocks_multiply_const_vxx_0
  id: blocks_multiply_const_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: np.exp(1j*phase_offset_rad)
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [787, 643]
    rotation: 0
    state: true
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [339, 832]
    rotation: 0
    state: true
- name: blocks_pack_k_bits_bb_0
  id: blocks_pack_k_bits_bb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    k: '8'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [558, 367]
    rotation: 0
    state: true
- name: blocks_skiphead_0
  id: blocks_skiphead
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_items: samples_offset_beginning
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [661, 860]
    rotation: 0
    state: true
- name: blocks_vco_c_0
  id: blocks_vco_c
  parameters:
    affinity: ''
    alias: ''
    amplitude: '1'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: output_sample_rate_hz
    sensitivity: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [128, 844]
    rotation: 0
    state: enabled
- name: code_rate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Code Rate
    short_id: ''
    type: str
    value: '"3/4"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1839, 107]
    rotation: 0
    state: enabled
- name: constellation
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Constellation
    short_id: ''
    type: str
    value: '"QPSK"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1670, 204]
    rotation: 0
    state: enabled
- name: dest_filename
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Dest Filename
    short_id: ''
    type: str
    value: '"../iq_data/dvbs2_iq.cfile"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [386, 11]
    rotation: 0
    state: true
- name: dtv_dvb_bbheader_bb_0
  id: dtv_dvb_bbheader_bb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    fecblocks: '168'
    framesize1: FECFRAME_NORMAL
    framesize2: FECFRAME_NORMAL
    inband: INBAND_OFF
    maxoutbuf: '0'
    minoutbuf: '0'
    mode: INPUTMODE_NORMAL
    rate1: C1_2
    rate2: C1_3
    rate3: C3_4
    rate4: C1_5_MEDIUM
    rate5: C3_5
    rolloff: RO_0_35
    standard: STANDARD_DVBS2
    tsrate: '4000000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [314, 250]
    rotation: 0
    state: enabled
- name: dtv_dvb_bbscrambler_bb_0
  id: dtv_dvb_bbscrambler_bb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    framesize1: FECFRAME_NORMAL
    framesize2: FECFRAME_NORMAL
    maxoutbuf: '0'
    minoutbuf: '0'
    rate1: C1_2
    rate2: C1_3
    rate3: C3_4
    rate4: C1_5_MEDIUM
    rate5: C3_5
    standard: STANDARD_DVBS2
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [558, 258]
    rotation: 0
    state: enabled
- name: dtv_dvb_bch_bb_0
  id: dtv_dvb_bch_bb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    framesize1: FECFRAME_NORMAL
    framesize2: FECFRAME_NORMAL
    maxoutbuf: '0'
    minoutbuf: '0'
    rate1: C1_2
    rate2: C1_3
    rate3: C3_4
    rate4: C1_5_MEDIUM
    rate5: C3_5
    standard: STANDARD_DVBS2
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [790, 258]
    rotation: 0
    state: enabled
- name: dtv_dvb_ldpc_bb_0
  id: dtv_dvb_ldpc_bb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: MOD_OTHER
    framesize1: FECFRAME_NORMAL
    framesize2: FECFRAME_NORMAL
    maxoutbuf: '0'
    minoutbuf: '0'
    rate1: C1_2
    rate2: C1_3
    rate3: C3_4
    rate4: C1_5_MEDIUM
    rate5: C3_5
    standard: STANDARD_DVBS2
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1063, 250]
    rotation: 0
    state: enabled
- name: dtv_dvbs2_interleaver_bb_0
  id: dtv_dvbs2_interleaver_bb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: MOD_QPSK
    framesize: FECFRAME_NORMAL
    maxoutbuf: '0'
    minoutbuf: '0'
    rate1: C3_5
    rate2: C1_5_MEDIUM
    rate3: C3_5
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [81, 452]
    rotation: 0
    state: enabled
- name: dtv_dvbs2_modulator_bc_0
  id: dtv_dvbs2_modulator_bc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: MOD_QPSK
    framesize: FECFRAME_NORMAL
    interpolation: INTERPOLATION_OFF
    maxoutbuf: '0'
    minoutbuf: '0'
    rate1: C3_4
    rate2: C1_5_MEDIUM
    rate3: C3_5
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [314, 444]
    rotation: 0
    state: enabled
- name: dtv_dvbs2_physical_cc_0
  id: dtv_dvbs2_physical_cc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: MOD_QPSK
    framesize: FECFRAME_NORMAL
    goldcode: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    pilots: PILOTS_ON
    rate1: C3_4
    rate2: C1_5_MEDIUM
    rate3: C3_5
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [81, 611]
    rotation: 0
    state: enabled
- name: epy_module_0
  id: epy_module
  parameters:
    alias: ''
    comment: ''
    source_code: '# this module will be imported in the into your flowgraph

      import numpy


      import os

      script_path = os.path.dirname(os.path.realpath(__file__))

      os.chdir(script_path)

      '
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [14, 157]
    rotation: 0
    state: true
- name: epy_module_1
  id: epy_module
  parameters:
    alias: ''
    comment: ''
    source_code: "import sys\nimport json\nimport math\nimport numpy as np\nfrom gnuradio\
      \ import dtv\nfrom gnuradio import analog\nfrom dsp_utilities.file_handlers.convert_rawfile_to_bluefile\
      \ import convert_rawfile_to_bluefile\nfrom dsp_utilities.file_handlers import\
      \ bluefile\n\n\ndef update_gr_top_block(gr_obj):\n    ##################################################\n\
      \    # Ben Custom Code\n    ##################################################\n\
      \    if gr_obj.fecframe_size.lower() == 'normal':\n        gr_obj.fecframe_size\
      \ = dtv.FECFRAME_NORMAL\n    elif gr_obj.fecframe_size.lower() == 'short':\n\
      \        gr_obj.fecframe_size = dtv.FECFRAME_SHORT\n    else:\n        sys.exit('Unrecogized\
      \ fecframe_size: {}  -  Should be normal or short'.format(gr_obj.fecframe_size))\n\
      \    \n    if gr_obj.constellation.lower() == 'qpsk':\n        gr_obj.constellation\
      \ = dtv.MOD_QPSK\n    elif gr_obj.constellation.lower() == '8psk':\n       \
      \ gr_obj.constellation = dtv.MOD_8PSK\n    elif gr_obj.constellation.lower()\
      \ == '16apsk':\n        gr_obj.constellation = dtv.MOD_16APSK\n    elif gr_obj.constellation.lower()\
      \ == '32apsk':\n        gr_obj.constellation = dtv.MOD_32APSK\n    else:\n \
      \       sys.exit(\n            'Unrecogized constellation: {}  -  Should be\
      \ QPSK, 8PSK, 16APSK, or 32APSK'.format(gr_obj.constellation))\n    \n    if\
      \ gr_obj.code_rate == '1/4':\n        gr_obj.code_rate = dtv.C1_4\n    elif\
      \ gr_obj.code_rate == '1/3':\n        gr_obj.code_rate = dtv.C1_3\n    elif\
      \ gr_obj.code_rate == '2/5':\n        gr_obj.code_rate = dtv.C2_5\n    elif\
      \ gr_obj.code_rate == '1/2':\n        gr_obj.code_rate = dtv.C1_2\n    elif\
      \ gr_obj.code_rate == '3/5':\n        gr_obj.code_rate = dtv.C3_5\n    elif\
      \ gr_obj.code_rate == '2/3':\n        gr_obj.code_rate = dtv.C2_3\n    elif\
      \ gr_obj.code_rate == '3/4':\n        gr_obj.code_rate = dtv.C3_4\n    elif\
      \ gr_obj.code_rate == '4/5':\n        gr_obj.code_rate = dtv.C4_5\n    elif\
      \ gr_obj.code_rate == '5/6':\n        gr_obj.code_rate = dtv.C5_6\n    elif\
      \ gr_obj.code_rate == '8/9':\n        gr_obj.code_rate = dtv.C8_9\n    elif\
      \ gr_obj.code_rate == '9/10':\n        gr_obj.code_rate = dtv.C9_10\n    else:\n\
      \        sys.exit(\n            'Unrecogized code rate: {}  -  Should be 1/4,\
      \ 1/3, 2/5, 1/2, 3/5, 2/3, 3/4, 4/5, 5/6, 8/9, or 9/10'.format(\n          \
      \      gr_obj.code_rate))\n    \n    if math.isclose(gr_obj.rolloff, 0.2, rel_tol=0.0001):\n\
      \        gr_obj.rolloff = dtv.RO_0_20\n    elif math.isclose(gr_obj.rolloff,\
      \ 0.25, rel_tol=0.0001):\n        gr_obj.rolloff = dtv.RO_0_25\n    elif math.isclose(gr_obj.rolloff,\
      \ 0.35, rel_tol=0.0001):\n        gr_obj.rolloff = dtv.RO_0_35\n    else:\n\
      \        sys.exit('Unrecogized rolloff: {}  -  Should be 0.20, 0.25, or 0.35'.format(gr_obj.rolloff))\n\
      \n    if gr_obj.pilots.lower() == 'on':\n        gr_obj.pilots = dtv.PILOTS_ON\n\
      \    elif gr_obj.pilots.lower() == 'off':\n        gr_obj.pilots = dtv.PILOTS_OFF\n\
      \    else:\n        sys.exit('Unrecogized pilots: {}  -  Should be On or Off'.format(gr_obj.pilots))\n\
      \n    if gr_obj.frequency_drift_model.lower() == 'cosine':\n        gr_obj.frequency_drift_model\
      \ = analog.GR_COS_WAVE\n        gr_obj.frequency_drift_model_offset = 0\n  \
      \  elif gr_obj.frequency_drift_model.lower() == 'triangle':\n        gr_obj.frequency_drift_model\
      \ = analog.GR_TRI_WAVE\n        gr_obj.frequency_drift_model_offset = gr_obj.frequency_drift_max_hz\
      \ * -2 * np.pi\n    elif gr_obj.frequency_drift_model.lower() == 'none':\n \
      \       gr_obj.frequency_drift_model = analog.GR_CONST_WAVE\n        gr_obj.frequency_drift_model_offset\
      \ = 0.0\n        gr_obj.frequency_drift_max_hz = 0.0\n        gr_obj.frequency_drift_rate_hz\
      \ = 0.0\n    else:\n        sys.exit('Unrecogized frequency_drift_model: {}\
      \  -  \\'cosine\\' and \\'triangle\\; '\n                 'are the only formats\
      \ currently supported'.format(gr_obj.frequency_drift_model))\n\n    return gr_obj\n\
      \n\ndef convert_to_bluefile(filename, fs_hz, t_start_sec):\n    new_filename\
      \ = filename.replace('.cfile', '.tmp')\n    convert_rawfile_to_bluefile(filename=filename,\
      \ new_filename=new_filename, \n                                fs_hz=fs_hz,\
      \ t_start_sec=t_start_sec, center_frequency_hz=0.0)\n    hdr, iq_array = bluefile.read(new_filename,\
      \ start=None, end=None, ext_header_type=str)\n    ext_header = json.loads(hdr['ext_header'])\n\
      \    ext_header.update({'argv': ' '.join(sys.argv)})\n    hdr['ext_header']\
      \ = json.dumps(ext_header)\n    bluefile.write(filename=new_filename, hdr=hdr,\
      \ ext_header_type=type(hdr['ext_header']), data=iq_array)\n"
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [14, 189]
    rotation: 0
    state: true
- name: esn0_db
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Es/N0 (Symbol to Noise Ratio) in dB
    short_id: ''
    type: eng_float
    value: '20.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [209, 107]
    rotation: 0
    state: true
- name: fecframe_size
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: FECFRAME size ('Normal' or 'Short')
    short_id: ''
    type: str
    value: '"normal"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1670, 107]
    rotation: 0
    state: enabled
- name: fft_filter_xxx_0
  id: fft_filter_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decim: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    nthreads: '1'
    samp_delay: '0'
    taps: firdes.root_raised_cosine(1.41, sample_rate_hz, symbol_rate, rolloff, rrc_filter_numtaps)
    type: ccc
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [314, 627]
    rotation: 0
    state: enabled
- name: frequency_drift_max_hz
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Frequency Deviation (Hz)
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [765, 107]
    rotation: 0
    state: true
- name: frequency_drift_model
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Frequency Drift Model
    short_id: ''
    type: str
    value: '"none"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1104, 107]
    rotation: 0
    state: true
- name: frequency_drift_rate_hz
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Frequency Deviation Rate (Hz)
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [935, 107]
    rotation: 0
    state: true
- name: frequency_offset_hz
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Frequency Offset (Hz)
    short_id: ''
    type: eng_float
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [590, 107]
    rotation: 0
    state: true
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import numpy as np
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [14, 107]
    rotation: 0
    state: true
- name: import_0_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import os
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [95, 107]
    rotation: 0
    state: true
- name: num_samples
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Number of IQ samples to generate
    short_id: ''
    type: intx
    value: '10000000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [587, 11]
    rotation: 0
    state: true
- name: phase_offset_rad
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Phase Offset (rad)
    short_id: ''
    type: eng_float
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1487, 205]
    rotation: 0
    state: true
- name: pilots
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Pilots
    short_id: ''
    type: str
    value: '"on"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1791, 204]
    rotation: 0
    state: true
- name: qtgui_freq_sink_x_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'True'
    average: '0.2'
    axislabels: 'True'
    bw: output_sample_rate_hz
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'False'
    fc: '0'
    fftsize: '1024'
    freqhalf: 'True'
    grid: 'True'
    gui_hint: ''
    label: Amplitude
    label1: ''
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: firdes.WIN_BLACKMAN_hARRIS
    ymax: '10'
    ymin: '-140'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1092, 720]
    rotation: 0
    state: disabled
- name: rational_resampler_xxx_0
  id: rational_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decim: resample_decimation
    fbw: '0'
    interp: resample_interpolation
    maxoutbuf: '0'
    minoutbuf: '0'
    taps: ''
    type: ccc
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [558, 619]
    rotation: 0
    state: enabled
- name: resample_decimation
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Resampler Decimation
    short_id: ''
    type: intx
    value: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1388, 11]
    rotation: 0
    state: true
- name: resample_interpolation
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Resampler Interpolation
    short_id: ''
    type: intx
    value: '9'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1219, 11]
    rotation: 0
    state: true
- name: rolloff
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: RRC Roll-Off Percentage (0.20, 0.25, 0.35)
    short_id: ''
    type: eng_float
    value: '0.35'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1486, 107]
    rotation: 0
    state: enabled
- name: rrc_filter_numtaps
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: RRC Roll-Off Percentage (0.20, 0.30)
    short_id: ''
    type: intx
    value: '100'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1320, 107]
    rotation: 0
    state: true
- name: samples_offset_beginning
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Number of samples to offset from start (helps ensure first symbol has non-zero
      symbol phase)
    short_id: ''
    type: intx
    value: '123'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1711, 11]
    rotation: 0
    state: true
- name: samples_per_symbol
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Samples per Symbol
    short_id: ''
    type: intx
    value: '2'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [941, 11]
    rotation: 0
    state: true
- name: source_filename
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Source Filename (from http://www.w6rz.net/adv16apsk910.ts)
    short_id: ''
    type: str
    value: '"adv16apsk910.ts"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [211, 11]
    rotation: 0
    state: true
- name: symbol_rate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Symbol Rate in Symbols/Second
    short_id: ''
    type: eng_float
    value: 5e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [765, 11]
    rotation: 0
    state: true
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: ldpc-int
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1311, 274]
    rotation: 0
    state: true
- name: virtual_sink_1
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: mod-phy
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [558, 468]
    rotation: 0
    state: true
- name: virtual_sink_1_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: resampled_iq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1051, 643]
    rotation: 0
    state: true
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: ldpc-int
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [81, 405]
    rotation: 180
    state: true
- name: virtual_source_1
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: mod-phy
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [81, 549]
    rotation: 180
    state: true
- name: virtual_source_1_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: resampled_iq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [81, 781]
    rotation: 0
    state: true

connections:
- [analog_const_source_x_0_1, '0', blocks_add_xx_1, '0']
- [analog_noise_source_x_0, '0', blocks_add_xx_0, '1']
- [analog_sig_source_x_0, '0', blocks_add_xx_1, '1']
- [blocks_add_xx_0, '0', blocks_skiphead_0, '0']
- [blocks_add_xx_1, '0', blocks_file_sink_1, '0']
- [blocks_add_xx_1, '0', blocks_vco_c_0, '0']
- [blocks_file_source_0, '0', dtv_dvb_bbheader_bb_0, '0']
- [blocks_head_0, '0', blocks_file_sink_0, '0']
- [blocks_head_0, '0', qtgui_freq_sink_x_0, '0']
- [blocks_multiply_const_vxx_0, '0', virtual_sink_1_0, '0']
- [blocks_multiply_xx_0, '0', blocks_add_xx_0, '0']
- [blocks_pack_k_bits_bb_0, '0', blocks_file_sink_0_0, '0']
- [blocks_skiphead_0, '0', blocks_head_0, '0']
- [blocks_vco_c_0, '0', blocks_multiply_xx_0, '1']
- [dtv_dvb_bbheader_bb_0, '0', blocks_pack_k_bits_bb_0, '0']
- [dtv_dvb_bbheader_bb_0, '0', dtv_dvb_bbscrambler_bb_0, '0']
- [dtv_dvb_bbscrambler_bb_0, '0', dtv_dvb_bch_bb_0, '0']
- [dtv_dvb_bch_bb_0, '0', dtv_dvb_ldpc_bb_0, '0']
- [dtv_dvb_ldpc_bb_0, '0', virtual_sink_0, '0']
- [dtv_dvbs2_interleaver_bb_0, '0', dtv_dvbs2_modulator_bc_0, '0']
- [dtv_dvbs2_modulator_bc_0, '0', virtual_sink_1, '0']
- [dtv_dvbs2_physical_cc_0, '0', fft_filter_xxx_0, '0']
- [fft_filter_xxx_0, '0', rational_resampler_xxx_0, '0']
- [rational_resampler_xxx_0, '0', blocks_multiply_const_vxx_0, '0']
- [virtual_source_0, '0', dtv_dvbs2_interleaver_bb_0, '0']
- [virtual_source_1, '0', dtv_dvbs2_physical_cc_0, '0']
- [virtual_source_1_0, '0', blocks_multiply_xx_0, '0']

metadata:
  file_format: 1
