#!/usr/bin/env bash

# Playing games with cat, tr, and sed because sed doesn't work easily over multiple lines
rm -f generate_dvbs2_iq_headless2.py
cat generate_dvbs2_iq_headless.py | tr '\n' '\r' | sed -e 's/##################################################\r        # Variables\r        ##################################################/self = epy_module_1\.update_gr_top_block\(self\)\r\r        ##################################################\r        # Variables\r        ##################################################/g;' | tr '\r' '\n' > generate_dvbs2_iq_headless2.py
mv generate_dvbs2_iq_headless2.py generate_dvbs2_iq_headless.py

rm -f generate_dvbs2_iq_headless2.py
cat generate_dvbs2_iq_headless.py | tr '\n' '\r' | sed -e 's/tb\.start[(][)]\r    tb.wait[(][)]\r\r/tb\.start\(\)\r    tb\.wait\(\)\r\r    fs_hz = options\.symbol_rate * options\.samples_per_symbol * options\.resample_interpolation\/options\.resample_decimation\r    dest_filename, dest_filename_extension = os\.path\.splitext(options\.dest_filename)\r    if dest_filename_extension == '\''\.tmp'\'':\r        epy_module_1\.convert_to_bluefile\(filename=options\.dest_filename, fs_hz=fs_hz, t_start_sec=0\.0\)\r\r/g;' | tr '\r' '\n' > generate_dvbs2_iq_headless2.py
mv generate_dvbs2_iq_headless2.py generate_dvbs2_iq_headless.py

sed -i 's/dtv\.FECFRAME_[A-Z]*/self\.fecframe_size/g; s/dtv\.C.*[0-9]*_[0-9]*/self\.code_rate/g; s/dtv\.MOD_[A-Z]*/self\.constellation/g; s/dtv\.RO_[0-9]*_[0-9]*/self\.rolloff/g; s/dtv\.PILOTS_[A-Z]*/self\.pilots/g;' generate_dvbs2_iq_headless.py

sed -i 's/analog\.sig_source_f(output_sample_rate_hz.*$/analog\.sig_source_f(output_sample_rate_hz, self\.frequency_drift_model, self\.frequency_drift_rate_hz, self\.frequency_drift_max_hz * 4 * np\.pi, self\.frequency_drift_model_offset , np\.pi\/2)/g' generate_dvbs2_iq_headless.py

sed -i 's/Set Dest Filename \[default=%(default)r\]/Set Dest Filename \[default=%(default)r\] (Set extension to \.cfile for raw binary IQ double pairs, and \.tmp for bluefile)/'  generate_dvbs2_iq_headless.py

sed -i 's/Set Frequency Drift Model \[default=%(default)r\]/Set Frequency Drift Model \[default=%(default)r\] \(Valid options are '\''cosine'\'', '\''triangle'\'', and '\''none'\''\)/'  generate_dvbs2_iq_headless.py

sed -i '0,/^$/ s/^$/\r#\r# This script is programmatically modified version of generate_dvbs2_iq_headless\.py,\r# which is generated from generate_dvbs2_iq_headless\.grc in GNURadio Companion\.\r# It is modified by using the updated_headles_py_file\.py file\.  The purpose of\r# this script is to provide a programmatic way of generating simulated DVB-S2 IQ\r# data outside of GNURadio companion\.\r#\r# This script was last updated with a flowgraph generated from GNURadio 3\.8\.1\.0\r# using Python 3\.8\.2\r#\r\r/' generate_dvbs2_iq_headless.py
